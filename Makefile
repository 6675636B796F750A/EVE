#
#AUTOMATIZE THE COMPILATION
#OF THE CODE
#

FC=f95
FFLAGS=-c -Wall

LDFLAGS=
files=src/*.f
objs=$(src:.f=.o)

#CREATION OF THE EXECUTABLE FILE EVE
eve: obj_files
	$(FC) obj/*.o -o eve 2>> log/log
obj_files:
	$(FC) $(FFLAGS) $(files) 2> log/log
	mv *.o obj
clean:
	rm -f eve obj/*.o log/log 
