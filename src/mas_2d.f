COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %  //////  MODULE REGROUPANT LES SUBROUTINES DE CALCULS  //////   %
COMM %  //////  DE LA MATRICE DE MASSE COHERENTE  -- 2D  --   //////   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE MCOE2D(RMA,IDIA,COOR,MAIL,EMAI,EPAI,
     *                  NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   CALCUL DE LA MATRICE DE MASSE COHERENTE GLOBALE               %
COMM %         DE LA STRUCTURE BIDIMENSIONNELLE                        %
COMM %                                                                 %
COMM %   RMA   MATRICE DE MASSE    GLOBALE                             %
COMM %   IDIA  TABLEAU POINTEUR DE LA DIAGONALE                        %
COMM %   MAIL  TABLEAU DECRIVANT LA NUMEROTATION DES ELEMENTS          %
COMM %   COOR  COORDONNEES DES NOEUDS                                  %
COMM %   EMAI  CARACTERISTIQUES MECANIQUES DES ELEMENTS                %
COMM %   NMAI  NOMBRE D ELEMENTS                                       %
COMM %   NDL   NOMBRE DE DDL TOTAL                                     %
COMM %   NCPN  NOMBRE DE COORDONNEES PAR NOEUD                         %
COMM %   NDPN  NOMBRE DE DDL PAR NOEUD                                 %
COMM %   NNPE  NOMBRE DE NOEUDS MAXIMUN PAR ELEMENT                    %
COMM %   KTYP = 1 --> DEFORMATION PLANE                                %
COMM %   KTYP = 2 --> CONTRAINTE PLANE                                 %
COMM %   KTYP = 3 --> AXISYMETRIQUE                                    %
COMM %                                                                 %
COMM %   NBN   NOMBRE DE NOEUD DE L'ELEMENT                            %
COMM %   NDDLE NOMBRE DE DDL DE L'ELEMENT                              %
COMM %   NBG   NOMBRE DE POINT DE GAUSS DE L'ELEMENT                   %
COMM %   GAUSS COORDONNEES POINT DE GAUSS DE L'ELEMENT                 %
COMM %   POIDS POID AU POINT DE GAUSS DE L'ELEMENT                     %
COMM %   RGL   MATRICE DE RIGIDITE ELEMENTAIRE                         %
COMM %   NO    NUMEROS GLOBAUX  DES NOEUDS                             %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RMA(1),IDIA(1),COOR(NCPN,1),MAIL(NNPE+2,1),
     *          EMAI(NVMA,1),EPAI(1)      
      DIMENSION RML(136),NO(8),IGLO(16),X(8),Y(8)
COMM ...........................////////////...........................
      CALL DZERO(RMA,IDIA(NDL))
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////.............
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)     
      CALL CAEL2D(ITYP,NBN,NDDLE,NBG) 
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))      
 3    CONTINUE        
COMM  ..........//////// MASSE VOLUMIQUE ET EPAISSEUR//////............
      RO=EMAI(3,IMAT)
      H =EPAI(IM)
COMM......///// CALCUL DE LA MATRICE DE MASSE ELEMENTAIRE /////.......
      IF(ITYP.EQ.30.OR.ITYP.EQ.40.OR.ITYP.EQ.41.OR.
     *   ITYP.EQ.60.OR.ITYP.EQ.80.OR.ITYP.EQ.81) THEN
        CALL RMAELE_ISOPARA2D(X,Y,RO,H,RML,KTYP,ITYP,NBN,NDDLE,NBG)
      ELSE      
        STOP ' MCOE2D : pb sur ITYP '  
      ENDIF
COMM .........////// ASSEMBLAGE DE LA MAT. MASSE ELEM. ////...........       
      CALL ASSMAT(RMA,IDIA,RML,NO,IGLO,NBN,NDPN)
 20   CONTINUE
      END


      SUBROUTINE RMAELE_ISOPARA2D(X,Y,RO,H,RML,
     *                            KTYP,ITYP,NBN,NDDLE,NBG)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE MASSE COERENTE ELEMENTAIRE : ELEMENTS ISOPARAM.  2D %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION X(8),Y(8),RML(136),ARML(136),
     *          GAUSS(2,9),POIDS(9)          
      DOUBLE PRECISION KSI,ETA,NN(32)
COMM
      ITS=(NDDLE*NDDLE+NDDLE)/2
      CALL DZERO(RML,ITS)        
      CALL GMAS2D(ITYP,NBGM,GAUSS,POIDS)       
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////.............
      DO 7 NG=1,NBGM     
        KSI=GAUSS(1,NG)
        ETA=GAUSS(2,NG)
COMM ......//// CALCUL DE LA MATRICE N ET DE DETJ ( et RAY)////........
        CALL NMATISO2D(X,Y,KSI,ETA,NN,DETJ,RAY,NDDLE,NBN,ITYP,KTYP)
COMM..............////// CALCUL DE Nt.N  //////////..................
        CALL TNN(NN,ARML,2,NDDLE,ITS)
COMM...........////// CALCUL DU COEFFICIENT  ////////////............   
        IF(KTYP.EQ.1.OR.KTYP.EQ.2) THEN        
          COEF=DETJ*H*RO*POIDS(NG)
        ELSE IF (KTYP.EQ.3) THEN
          COEF=DETJ*6.2831853*RAY*RO*POIDS(NG)
        ENDIF
        CALL DACTUA(RML,ARML,ITS,COEF)
 7    CONTINUE
      END
            
      SUBROUTINE GMAS2D(ITYP,NBGM,GAUSS,POIDS)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  EN FONCTION DU TYPE DE L'ELEMENT, ON RENVOIE LES COORDONNEES   %
COMM %  DES POINTS DE GAUSS, ET LES POIDS DE GAUSS                     %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION GAUSS(2,9),POIDS(9),RQ8(3),WQ8(3)
COMM      
      DATA RQ8/-0.774596669241483,0.000000000000000,0.774596669241483/
      DATA WQ8/ 0.555555555555556,0.888888888888889,0.555555555555556/      
COMM            
      IF      (ITYP.EQ.30) THEN
        NBGM=3
        GAUSS(1,1)=1.D0/6.D0
        GAUSS(1,2)=2.D0/3.D0
        GAUSS(1,3)=1.D0/6.D0
        GAUSS(2,1)=1.D0/6.D0
        GAUSS(2,2)=1.D0/6.D0
        GAUSS(2,3)=2.D0/3.D0
        DO 2 I=1,3
 2      POIDS(I)=1.D0/6.D0      
      ELSE IF (ITYP.EQ.40) THEN
        NBGM=9
        DO 4 I=1,3
        DO 4 J=1,3
        K=I+(J-1)*3
        GAUSS(1,K)=RQ8(I)
        GAUSS(2,K)=RQ8(J)
        POIDS(K)=WQ8(I)*WQ8(J)
 4      CONTINUE 
      ELSE IF (ITYP.EQ.60) THEN
        NBGM=6
        A=0.445948490915965
        B=0.091576213509771
        GAUSS(1,1)=A
        GAUSS(1,2)=1.D0-2.D0*A
        GAUSS(1,3)=A
        GAUSS(1,4)=B
        GAUSS(1,5)=1.D0-2.D0*B
        GAUSS(1,6)=B                
        GAUSS(2,1)=A
        GAUSS(2,2)=A
        GAUSS(2,3)=1.D0-2.D0*A
        GAUSS(2,4)=B
        GAUSS(2,5)=B
        GAUSS(2,6)=1.D0-2.D0*B        
        POIDS(1)=0.111690794839005
        POIDS(2)=0.111690794839005        
        POIDS(3)=0.111690794839005
        POIDS(4)=0.054975871827661        
        POIDS(5)=0.054975871827661
        POIDS(6)=0.054975871827661                                    
      ELSE IF (ITYP.EQ.80.OR.ITYP.EQ.81) THEN
        NBGM=9
        DO 5 I=1,3
        DO 5 J=1,3
        K=I+(J-1)*3
        GAUSS(1,K)=RQ8(I)
        GAUSS(2,K)=RQ8(J)
        POIDS(K)=WQ8(I)*WQ8(J)
 5      CONTINUE 
      ELSE
       STOP ' PB : GMAS2D '
      ENDIF
      END
      
      SUBROUTINE NMATISO2D(X,Y,KSI,ETA,NN,DETJ,R,NDDLE,NBN,ITYP,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM % CALCUL DE LA MATRICE N AU POINT DE COORDONNEES LOCALES KSI,ETA  % 
COMM %                                                                 % 
COMM %   --  ELEMENTS ISOPARAMETRIQUES  2D :  T3  Q4  T6  Q8  Q8R --   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION  X(NBN),Y(NBN)
      DOUBLE PRECISION KSI,ETA,NN(2,NDDLE),J(4),
     *N(NBN),NKSI(NBN),NETA(NBN),NX(NBN),NY(NBN)     
COMM
      IF(ITYP.EQ.30) THEN
        CALL FORMT3(KSI,ETA,N,NKSI,NETA)
      ELSE IF(ITYP.EQ.40) THEN
        CALL FORMQ4(KSI,ETA,N,NKSI,NETA)
      ELSE IF(ITYP.EQ.60) THEN
        CALL FORMT6(KSI,ETA,N,NKSI,NETA)        
      ELSE IF(ITYP.EQ.80) THEN
        CALL FORMQ8(KSI,ETA,N,NKSI,NETA)
      ELSE IF(ITYP.EQ.81) THEN
        CALL FORMQ8(KSI,ETA,N,NKSI,NETA)        
      ELSE
        STOP ' NMATISO2D : pb sur ITYP '
      ENDIF         
COMM .......///// Matrice jacobienne et son determinant///////.......... 
      DO 5 I=1,4
 5    J(I)=0.D0     
      DO 1 I=1,NBN
      J(1)=J(1)+NKSI(I)*X(I)
      J(2)=J(2)+NKSI(I)*Y(I)
      J(3)=J(3)+NETA(I)*X(I)
      J(4)=J(4)+NETA(I)*Y(I)
  1   CONTINUE 
      DETJ=J(1)*J(4)-J(2)*J(3)
      IF(DABS(DETJ).LT.1.D-10) STOP ' Mat. Jacob. non inversible '
COMM ..................///// Matrice N  ///////.........................
      DO 3 I=1,NBN
      NN(1,1+2*(I-1))=N(I)
      NN(2,1+2*(I-1))=0.D0
      NN(1,2+2*(I-1))=0.D0
      NN(2,2+2*(I-1))=N(I)
 3    CONTINUE
COMM          
COMM ......./////  Complement pour le cas axisymetrique ///////........
COMM
      IF(KTYP.EQ.3) THEN   
        R=0.D0
        DO 6 I=1,NBN
 6      R=R+N(I)*X(I)     
      ENDIF             
      END

COMM ...........................////////////...........................
COMM ...//////  FIN DU MODULE REGROUPANT LES SUBROUTINES DE  //////....
COMM ...//////  CALCULS  DE LA MATRICE DE MASSE COHERENTE    //////....
COMM ...........................////////////...........................

