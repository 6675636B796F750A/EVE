COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %        //////   MODULE DE CALCULS DES FORCES   //////           %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE CAFORC(DFOR,F)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM % CALCUL DU VECTEUR FORCES F POUR LES DONNEES DE CHARGEMENT DFOR  %
COMM %                                                                 %
COMM %  FORCES NODALES, PRESSION, POIDS PROPRE,CHAMP DE TEMPERATURE    %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DIMENSION DFOR(1)
      DOUBLE PRECISION F(1)
      COMMON T(1)
      COMMON/INI/ICO(200),IT(200)
COMM ..........................////////////............................
      NDPN=ICO(3)
      NDL =ICO(7)
      NNOE=ICO(6)
      CALL DZERO(F,NDL)
COMM
COMM ..........................////////////............................
COMM /// POUR CUMULER LES DIFFERENTS CHARGEMENTS T(IAF) N'EST PAS ///
COMM /// INITIALISE A ZERO DANS FVOLRO , FVOLTH , FSURFA......... ///
COMM ..........................////////////............................
COMM
COMM ......//////  FORCES DE VOLUME DUES AU POIDS PROPRE  //////.......
COMM
      IF(INT(DFOR(2)).EQ.1) THEN
        GX=DFOR(3)
        GY=DFOR(4)
        CALL FVOLRO(GX,GY,T(IAF),T(IT(2)),T(IT(3)),T(IT(1)))
      ENDIF
COMM
COMM  /// FORCES DE VOLUME DUES A UN CHAMP DE TEMPERATURE AUX NOEUDS ///
COMM
      IF(INT(DFOR(6)).EQ.1) THEN
C      CALL LEFICH(T(IT( )),NNOE,73)
C      CALL FVOLTH(T(IT( )),T(IAF),T(IT(2)),T(IT(3)),T(IT(1)),T(IT(11)))
      ENDIF 
COMM
COMM  ...............//////  FORCES DE SURFACE  //////.................
COMM
      IF(INT(DFOR(8)).GT.0) THEN
       CALL FSURFA(DFOR,F,NDPN)
      ENDIF
      END


      SUBROUTINE FVOLRO(GX,GY,F,MAIL,EMAI,COOR)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %           CALCUL DES FORCES DUES AU POIDS VOLUMIQUE            %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      END
      

       SUBROUTINE FVOLTH(T,F,MAIL,EMAI,COOR,SGO)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %    CALCUL DES FORCES NODALES EQUIVALENTES  A UN CHAMP DE       %
COMM %    TEMPERATURE T DEFINI AUX NOEUDS                             %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      END

      SUBROUTINE FSURFA(DFOR,F,NDPN)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   CALCUL DES FORCES CONCENTREES ET DES PRESSIONS               %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION F(1)
      REAL DFOR(1)
COMM ..........................////////////............................
      NKF=INT(DFOR(8))
      INIT=8
      IAD=INIT+1
      DO 1 IKF=1,NKF
       ITYPF=INT(DFOR(IAD))
       NLEC =INT(DFOR(IAD+1))
       IAD=IAD+2 
COMM ..............//////  CAS DES FORCES IMPOSEES  //////.............
       IF(ITYPF.EQ.1) THEN
        DO 56 I=1,NLEC
        J =INT(DFOR(IAD  ))
        FX=    DFOR(IAD+1)
        ND=INT(DFOR(IAD+2))
        IAD=IAD+3
 56     F(NDPN*(J-1)+ND)=F(NDPN*(J-1)+ND)+FX
       ENDIF
COMM ............//////  CAS DES PRESSIONS REPARTIES  //////...........
       IF(ITYPF.EQ.2) THEN
         WRITE(6,*) ' LE CHARGEMENT DE TYPE PRESSION N''EST PAS ', 
     *              'OPERATIONEL  !!!! '
         IAD=IAD+3*NLEC
       ENDIF
1     CONTINUE
      END
      
COMM ..........................////////////............................
COMM .......//////  FIN DU MODULE DE CALCULS DES FORCES  //////........
COMM ..........................////////////............................
