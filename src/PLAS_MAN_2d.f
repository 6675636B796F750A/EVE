          SUBROUTINE MAN_PLASTIC_CHARGE_2D(IDIMT)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%             %
COMM %         %      METHODE ASYMPTOTIQUE_NUMERIQUE     %             %
COMM %         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%             %
COMM %                                                                 %
COMM %                 version de  MARS 2003                           %
COMM %                                                                 %
COMM %                                                                 %
COMM %          Methode asymptotique_numerique Plasticite 2D           %
COMM %          --------------------------------------                 %
COMM %                                                                 %
COMM %                                                                 %
COMM %                                                                 %
COMM %   Organisation de la memoire  (Super-Tableau T + pointeurs IT)  %
COMM %   --------------------------                                    %
COMM %                                                                 %
COMM %      adresse              contenu de T                 type     %
COMM %                                                                 %
COMM %   IT(11)---IDIA       Pointeurs de la diagonale       (ENTIER)  %
COMM %   IT(12)---F          Le vecteur de charge appliquee  (DP)      %
COMM %   IT(13)---RIG        Matrice de rigidite             (DP)      %
COMM %   IT(14)---C0         Parametre de charge initial     (DP)      %
COMM %   IT(15)---V0         Vecteur deplacement initial     (DP)      %
COMM %   IT(16)---SIG0       Vecteur contrainte initial      (DP)      %
COMM %   IT(17)---Ci         Les coefficients aux ordres i   (DP)      %
COMM %   IT(18)---Vi         Les deplacements aux ordres i   (DP)      %
COMM %   IT(19)---SIGi       Les contraintes aux ordres i    (DP)      %
COMM %   IT(20)---Ti         Les gradients de Vi aux ordres i(DP)      %
COMM %   IT(21)---FQi        Les vecteurs FQ aux ordres i    (DP)      %
COMM %   IT(22)---Ftra       vecteur de travail (NDL)        (DP)      %
COMM %   IT(23)---Vtra       Vecteur de travail (NDL)        (DP)      %
COMM %   IT(24)---Stra       Vecteur de travail (NSIG)       (DP)      %
COMM %   IT(25)---RES        Vecteur Residu     (NDL)        (DP)      %
COMM %   IT(26)---DCi        Les coeff. de l'indicateur      (DP)      %
COMM %   IT(27)---DVi        Les deplac. perturbes ordres i  (DP)      %
COMM %   IT(28)---DSIGi      Les contr.  perturbes ordres i  (DP)      %
COMM %   IT(29)---DTi        Les gradie. perturbes ordres i  (DP)      %
COMM %   IT(30)---DF         Le vecteur de forces perturb.   (DP)      %
COMM %   IT(31)---ALPHA      Coefficients d'orthogonalisation(DP)      %
COMM %   IT(32)---DEN        Coefficients du denominateur Pade(DP)     %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION XL1,XL2,CRIT,RAYON,RAYON_SIG,RAYON_GP,RAYON_H,
     &      RAYON_OLD,tab_rayon(17),TAB_RAYONP(17),CHARGE,DA,A,XNORM1R,
     &       FNLNORM(100),RAPP,COMPX,COMPY,COMPS,COMP_FC,COMP_GP,
     &       COMPEPS,Cm,Tm,ETA4,CRIT2,CRIT1,COMPEPSP,
     &       COMP_x,COMP_y,COMP_SIGQ,COMP_SIGe,RAYONS,
     &       Temps,RAYONP,D(100),RAYONP_F_SIGN,DAP,AP,POLE,POLE_C,
     &       COMP_XP,COMP_YP      
     &       BETA      

      CHARACTER*20 NOMFICH
      CHARACTER*4  RMOT,RMOT2
      LOGICAL APOSITIF,DIVERGE
      COMMON T(1)
      COMMON/INI/ICO(200),IT(200)
COMM ....................../////////////////............................
      KTYP =ICO(1)
      NCPN =ICO(2)
      NDPN =ICO(3)
      NNPE =ICO(4)
      NSPG =ICO(5)
      NNOE =ICO(6)
      NDL  =ICO(7)
      NMAI =ICO(8)
      NVMA =ICO(10)
      NLIAI=ICO(11)
      NVF1 =ICO(13)
      IF(KTYP.EQ.1.OR.KTYP.EQ.2.OR.KTYP.EQ.3) THEN ! Elasticite et Plasticite 2D
        NTPG=4
      ELSE IF(KTYP.EQ.4) THEN                      ! Elasticite 3D
        NTPG=9 
      ELSE IF(KTYP.EQ.5) THEN                      ! Coques
        NTPG=2       
      ELSE
       STOP ' Pb KTYP ANMCA2 '
      ENDIF                    
COMM ....................../////////////////............................
COMM
      READ(5,*) NORDRE,NORDRECO,NITERMAX,NOEUD,ETA4,Cm,Tm
      READ(5,*) INMAI,IG       !NUM MAILLE ,NUM PT GAUUSS POUR LA SORTIE
      ICO(30)=NORDRE 
      ICO(31)=NORDRECO   
COMM
COMM .....//// RESERVATION DES PLACES DANS LE SUPER-TABLEAU ////......
COMM
      IT(12)=IT(11)+NDL     
      IT(13)=IT(12)+2*NDL                ! Chargement exterieur
      
      CALL PROFBI(T(IT(11)),T(IT(2)),NDL,NMAI,NRIG,NNPE,NDPN,KTYP,NBG)
      ICO(26)=NRIG
      IT(14)=IT(13)+2*NRIG	          ! Matrice de rigidite
COMM
COMM %%%%%%%%%%% VECTEURS DE SOLUTIONS INITIALES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM
      CALL LONSIG(T(IT(2)),NMAI,NNPE,NSPG,NSIG,NEPS,KTYP)
      ICO(27)=NSIG
      IT(15)=IT(14)+2                     ! Parametre de charge mu0
      IT(16)=IT(15)+2*NDL                 ! Deplacement U0
      IT(17)=IT(16)+2*NSIG 		  ! Contrainte SIG0
      IT(18)=IT(17)+2*NSIG 		  ! Deformation plastique EPSP0
      IT(19)=IT(18)+2*NBG*NMAI		  ! Multiplicateur plastique lambda0
      IT(20)=IT(19)+2*NSIG 		  ! normal PN0=SIGD/q
      IT(21)=IT(20)+2*NBG*NMAI		  ! Contraintes equivalente q0
      IT(22)=IT(21)+2*NBG*NMAI		  ! Fonction de charge FC0
      IT(23)=IT(22)+2*NBG*NMAI		  ! variables SIGe0
      IT(24)=IT(23)+2*NBG*NMAI		  ! variables GP0
      IT(25)=IT(24)+2*NBG*NMAI		  ! variables EPSPe0
      IT(26)=IT(25)+2*NBG*NMAI		  ! variables ZP0
      IT(27)=IT(26)+2*NBG*NMAI	          ! DEN0
      IT(28)=IT(27)+2*NBG*NMAI	          ! XP0
      IT(29)=IT(28)+2*NBG*NMAI	          ! YP0
COMM
COMM %%%%%%%%%%% VECTEURS DE SOLUTIONS ORDRE i %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM
      IT(30)=IT(29)+2*(NORDRE+2)          ! Parametre de charge mui
      IT(31)=IT(30)+2*NDL*(NORDRE+2)	  ! Deplacement Ui
      IT(32)=IT(31)+2*NSIG*(NORDRE+2)	  ! Contrainte SIGi
      IT(33)=IT(32)+2*NSIG*(NORDRE+2)	  ! Deformation plastique EPSPi
      IT(34)=IT(33)+2*NBG*NMAI*(NORDRE+2) ! Multiplicateur plastique lambdai
      IT(35)=IT(34)+2*NSIG*(NORDRE+2)	  ! normale ni
      IT(36)=IT(35)+2*NBG*NMAI*(NORDRE+2) ! qi
      IT(37)=IT(36)+2*NBG*NMAI*(NORDRE+2) ! Fonction de charge FCi
      IT(38)=IT(37)+2*NBG*NMAI*(NORDRE+2) ! SIGei
      IT(39)=IT(38)+2*NBG*NMAI*(NORDRE+2) ! GPi
      IT(40)=IT(39)+2*NBG*NMAI*(NORDRE+2) ! EPSPei
      IT(41)=IT(40)+2*NBG*NMAI*(NORDRE+2) ! ZPi
      IT(42)=IT(41)+2*NBG*NMAI*(NORDRE+2) ! DENi
      IT(43)=IT(42)+2*NBG*NMAI*(NORDRE-1+2) ! XPi
      IT(44)=IT(43)+2*NBG*NMAI*(NORDRE-1+2) ! YPi

      IT(45)=IT(44)+2*NDL*(NORDRE+2) ! Second membre Fnl
COMM
COMM %%%%%%%%%%% VECTEURS SOLUTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM0000
      IT(46)=IT(45)+2                     ! Parametre de charge mu
      IT(47)=IT(46)+2*NDL                 ! Deplacement U
      IT(48)=IT(47)+2*NSIG 		  ! Contrainte SIG
      IT(49)=IT(48)+2*NSIG 		  ! Deformation EPS
      IT(50)=IT(49)+2*NSIG 		  ! Deformation plastique EPSP
      IT(51)=IT(50)+2*NBG*NMAI		  ! Multiplicateur plastique lambda
      IT(52)=IT(51)+2*NSIG 		  ! normal PN = SIGD/q
      IT(53)=IT(52)+2*NBG*NMAI		  ! Contrainte equivalente q
      IT(54)=IT(53)+2*NBG*NMAI		  ! Fonction de charge FC
      IT(55)=IT(54)+2*NBG*NMAI		  ! variables SIGe_sol
      IT(56)=IT(55)+2*NBG*NMAI		  ! variables GP
      IT(57)=IT(56)+2*NBG*NMAI		  ! variables EPSPe_sol
      IT(58)=IT(57)+2*NBG*NMAI		  ! variables ZP
      IT(59)=IT(58)+2*NBG*NMAI	          ! DEN
      IT(60)=IT(59)+2*NBG*NMAI	          ! XP
      IT(61)=IT(60)+2*NBG*NMAI	          ! YP
C-------------- parametres pour le chargement  en /\  -------------------------
      IT(62)=IT(61)+2                     ! XC0
      IT(63)=IT(62)+2*(NORDRE+2)          ! XCi
      IT(64)=IT(63)+2                     ! XC
      IT(65)=IT(64)+2                     ! YC0
      IT(66)=IT(65)+2*(NORDRE+2)          ! YCi
      IT(67)=IT(66)+2                     ! YC
C------------- RESIDU ---------------------------------------------------------
      IT(68)=IT(67)+2*NDL		  ! Vecteur residu
      IT(69)=IT(68)+2*NSIG		  ! RES_COMP
      IT(70)=IT(69)+2*NDL		  ! FTRA
      IT(71)=IT(70)+2*NSIG*(NORDRE+2)	  ! Deformation EPSi
      IT(72)=IT(71)+2*NSIG       	  ! Deformation EPSP1 tangente
      IT(73)=IT(72)+2*NORDRE*NORDRE       ! ALPHA coef orthogonalisation
COMM  %%%%%%%%%%% VECTEURS CORRECTION  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IT(74)=IT(73)+2*(NORDRE+2)          ! Parametre de charge mui
      IT(75)=IT(74)+2*NDL*(NORDRE+2)	  ! Deplacement Ui
      IT(76)=IT(75)+2*NSIG*(NORDRE+2)	  ! Contrainte SIGi
      IT(77)=IT(76)+2*NSIG*(NORDRE+2)	  ! Deformation plastique EPSP
      IT(78)=IT(77)+2*NBG*NMAI*(NORDRE+2) ! Multiplicateur plastique lambdai
      IT(79)=IT(78)+2*NSIG*(NORDRE+2)	  ! normale ni
      IT(80)=IT(79)+2*NBG*NMAI*(NORDRE+2) ! qi
      IT(81)=IT(80)+2*NBG*NMAI*(NORDRE+2) ! Fonction de charge FCi
      IT(82)=IT(81)+2*NBG*NMAI*(NORDRE+2) ! SIGei
      IT(83)=IT(82)+2*NBG*NMAI*(NORDRE+2) ! GPi
      IT(84)=IT(83)+2*NBG*NMAI*(NORDRE+2) ! EPSPei
      IT(85)=IT(84)+2*NBG*NMAI*(NORDRE+2) ! ZPi
      IT(86)=IT(85)+2*NBG*NMAI*(NORDRE+2) ! DENi
      IT(87)=IT(86)+2*NBG*NMAI*(NORDRE+2) ! XPi
      IT(88)=IT(87)+2*NBG*NMAI*(NORDRE+2) ! YPi
      IT(89)=IT(88)+2*NDL*(NORDRE+2)      ! Second membre Fnl
      IT(90)=IT(89)+2*NSIG*(NORDRE+2)     ! EPSi
      IT(91)=IT(90)+2*NSIG                ! Deformation plastique EPSP0.95amax
COMM  %%%%%%%%%%% LES VECTEUR TANGENTS RESERVATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IT(92)=IT(91)+2*NDL                   !U1
      IT(93)=IT(92)+2*NSIG                  !SIG1
      IT(94)=IT(93)+2*NSIG                  !EPSP1
      IT(95)=IT(94)+2*NBG*NMAI              !LAMBDA1
      IT(96)=IT(95)+2*NSIG                  !PN1
      IT(97)=IT(96)+2*NBG*NMAI              !FC1
      IT(98)=IT(97)+2*NBG*NMAI              !SIGQ1
      IT(99)=IT(98)+2*NBG*NMAI              !SIGe1
      IT(100)=IT(99)+2*NBG*NMAI             !GP1
      IT(101)=IT(100)+2*NBG*NMAI            !ZP1
      IT(102)=IT(101)+2*NBG*NMAI            !EPSPe1
      IT(103)=IT(102)+2*NBG*NMAI            !DEN1
      IT(104)=IT(103)+2                     !XC1
      IT(105)=IT(104)+2                     !YC1
      IT(106)=IT(105)+2                     !CHARGE1
      IT(107)=IT(106)+2*NBG*NMAI            !XP1
      IT(108)=IT(107)+2*NBG*NMAI            !YP1
COMM %%%%%%%%%%% AFFICHAGE DES LONGUEURS IT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM
      WRITE(6,150)
      WRITE(6,100)
      DO 1 I=1,107
  1   WRITE(6,120) I,IT(I),IT(I+1)-IT(I)
      WRITE(6,130) IT(108),IDIMT
      WRITE(6,110)
      IF(IT(108).GT.IDIMT) STOP 'Taille du Super-Tableau'
COMM
COMM  ...//// on lit le point de demarrage qui est nul///..........
COMM
      CALL DZERO(T(IT(14)),1)      !charge0
      CALL DZERO(T(IT(15)),NDL)    !U0
      CALL DZERO(T(IT(16)),NSIG)   !SIG0
      CALL DZERO(T(IT(17)),NSIG)   !EPSP0
      CALL DZERO(T(IT(19)),NSIG)   !PN0
      CALL DZERO(T(IT(71)),NSIG)   !EPSP1 TANGENTE 
      CALL DZERO(T(IT(90)),NSIG)   !EPSP0.95amax TANGENTE 
      
      CALL VINIT_VARIABLES_PLAS_CHARGE(T(IT(20)),T(IT(22)),
     &     T(IT(21)),T(IT(23)),T(IT(25)),T(IT(27)),T(IT(28)),
     &  T(IT(24)),T(IT(2)),T(IT(3)),T(IT(26)),NDL,NSIG,NBG,NMAI,NVMA)
      CALL CHARGEV_INIT(T(IT(14)),T(IT(61)),T(IT(64)),Cm,Tm,ETA4)
      
            
      CALL CAFORC(T(IT(6)),T(IT(12)))
      
      CALL CALCUL_U1_DEPART(T(IT(1)),T(IT(2)),T(IT(3)),T(IT(4)),
     &          T(IT(5)),T(IT(11)),T(IT(6)),T(IT(12)),T(IT(13)),
     &          T(IT(29)),T(IT(30)),T(IT(62)),T(IT(65)),T(IT(61)),
     &          T(IT(64)),Cm,Tm,ETA4,NNOE,NMAI,NDL,NCPN,NDPN,NNPE,
     &          NLIAI,NVMA,KTYP)

COMM  ........//// on forme le vecteur de chargement ///...............
      CALL CALCUL1_PLAS(T(IT(30)),T(IT(1)),T(IT(2)),T(IT(3)),T(IT(4)),
     &    T(IT(31)),T(IT(16)),T(IT(35)),T(IT(20)),T(IT(37)),T(IT(22)),
     &    T(IT(48)),T(IT(32)),T(IT(17)),T(IT(39)),T(IT(24)),T(IT(71)),
     &    T(IT(34)),T(IT(19)),T(IT(36)),T(IT(21)),T(IT(33)),T(IT(38)),
     &    T(IT(23)),T(IT(40)),T(IT(25)),T(IT(42)),T(IT(27)),T(IT(43)),
     &    T(IT(28)),T(IT(26)),T(IT(41)),KTYP,NBG,NMAI,NDL,NSIG,NNPE,
     &    NCPN,NDPN,NSPG,NVMA)


COMM
COMM  ...//// Lecture du fichier de sortie et du MOT CLE  ////...................
COMM          NP : nombre de pas   XL: Longueur du pas tangent      
      READ(5,'(A20)') NOMFICH
      READ(5,'(A4)') RMOT
COMM
COMM  ...//// ecriture de l'entete dans le fichier de sortie ////..........
COMM                        
      OPEN(29,FILE='courbe',STATUS='UNKNOWN',ACCESS='SEQUENTIAL')
      OPEN(30,FILE=NOMFICH,STATUS='UNKNOWN',ACCESS='SEQUENTIAL')
      WRITE(30,*) ' Nombre de Noeuds et de d.d.l par noeuds '

      Temps=Tm*(1.D0-DSQRT(1.D0-ETA4))
COMM
COMM  ...////  analyse d'un seul pas de calcul (NPas=0) ///......................
COMM
      IF (RMOT.EQ.'1PAS') STOP '1PAS NON INSTALE'

      IF(RMOT.EQ.'CONT') THEN
      READ(5,*) XL1,XL2,CRIT,NPAS,NPOI,CRIT1,CRIT2
      READ(5,'(A4)') RMOT2

      DO 15 IPAS=1,NPAS

      write(9,*)'------------- on est au pas',IPAS,'DE NPAS =',NPAS
      CALL SERIES_PLAS_DECHARGE(T(IT(1)),T(IT(2)),T(IT(3)),T(IT(4)),
     &           T(IT(5)),T(IT(11)),T(IT(6)),T(IT(12)),T(IT(13)),
     &           T(IT(31)),T(IT(16)),T(IT(35)),T(IT(20)),
     &           T(IT(37)),T(IT(22)),T(IT(70)),T(IT(32)),T(IT(17)),
     &           T(IT(39)),T(IT(24)),
     &           T(IT(34)),T(IT(19)),T(IT(36)),T(IT(21)),
     &           T(IT(33)),T(IT(18)),T(IT(38)),T(IT(23)),
     &           T(IT(40)),T(IT(25)),
     &           T(IT(42)),T(IT(27)),T(IT(43)),T(IT(28)),
     &           T(IT(62)),T(IT(61)),T(IT(65)),T(IT(64)),
     &           T(IT(29)),T(IT(30)),T(IT(44)),
     &           XL1,XL2,Cm,Tm,T(IT(71)),T(IT(26)),T(IT(41)),
     &           NBG,NNOE,NMAI,NDL,NCPN,NDPN,NNPE,NLIAI,NVMA,
     &           KTYP,NSPG,NSIG,NORDRE,NRIG,IPAS)
      
C-------- calcul des differents rayons : pour chaque equation qui intervient ds le pb
      WRITE(39,*)'RAYON_U '
      CALL DETRAY_PLAS(CRIT,T(IT(30)),tab_rayon(1),NDL,NORDRE)        !RAYON_U     
      WRITE(39,*)'RAYON_SIG '
      CALL DETRAY_PLAS(CRIT,T(IT(31)),tab_rayon(2),NSIG,NORDRE)       !RAYON_SIG
      WRITE(39,*)'RAYON_EPSP '
      CALL DETRAY_PLAS(CRIT,T(IT(32)),tab_rayon(3),NSIG,NORDRE)       !RAYON_EPSP
            WRITE(39,*)'RAYON_N '
      CALL DETRAY_PLAS(CRIT,T(IT(34)),tab_rayon(4),NSIG,NORDRE)       !RAYON_N
            WRITE(39,*)'RAYON_q'
      CALL DETRAY_PLAS(CRIT,T(IT(35)),tab_rayon(5),NBG*NMAI,NORDRE)   !RAYON_q
            WRITE(39,*)'RAYON_SIGe '
      CALL DETRAY_PLAS(CRIT,T(IT(37)),tab_rayon(6),NBG*NMAI,NORDRE)   !RAYON_SIGe
            WRITE(39,*)'RAYON_FC '
      CALL DETRAY_PLAS(CRIT,T(IT(36)),tab_rayon(7),NBG*NMAI,NORDRE)   !RAYON_FC
            WRITE(39,*)'RAYON_lambda '
      CALL DETRAY_PLAS(CRIT,T(IT(33)),tab_rayon(8),NBG*NMAI,NORDRE)   !RAYON_LAMBDA
            WRITE(39,*)'RAYON_GP '
      CALL DETRAY_PLAS(CRIT,T(IT(38)),tab_rayon(9),NBG*NMAI,NORDRE)   !RAYON_GP
            WRITE(39,*)'RAYON_ZP '
      CALL DETRAY_PLAS(CRIT,T(IT(40)),tab_rayon(10),NBG*NMAI,NORDRE)  !RAYON_ZP
            WRITE(39,*)'RAYON_EPSPe '
      CALL DETRAY_PLAS(CRIT,T(IT(39)),tab_rayon(11),NBG*NMAI,NORDRE)  !RAYON_EPSPe
            WRITE(39,*)'RAYON_DEN '
      CALL DETRAY_PLAS(CRIT,T(IT(41)),tab_rayon(12),NBG*NMAI,NORDRE)  !RAYON_Den
            WRITE(39,*)'RAYON_xp '
      CALL DETRAY_PLAS(CRIT,T(IT(42)),tab_rayon(13),NBG*NMAI,NORDRE-1)  !RAYON_xp
            WRITE(39,*)'RAYON_yp '
      CALL DETRAY_PLAS(CRIT,T(IT(43)),tab_rayon(14),NBG*NMAI,NORDRE-1)  !RAYON_yp 
             WRITE(39,*)'RAYON_ci '
      CALL DETRAY_PLAS(CRIT,T(IT(29)),tab_rayon(15),NBG*NMAI,NORDRE)  !RAYON_ci
             WRITE(39,*)'RAYON_xci '
      CALL DETRAY_PLAS(CRIT,T(IT(62)),tab_rayon(16),NBG*NMAI,NORDRE)  !RAYON_xci
             WRITE(39,*)'RAYON_yci '
      CALL DETRAY_PLAS(CRIT,T(IT(65)),tab_rayon(17),NBG*NMAI,NORDRE)  !RAYON_yci
      
                      
      RAYONS=tab_rayon(1)
      RAYON_OLD=RAYONS

      DO I=1,14
       IF (RAYONS.GT.tab_rayon(I).AND.tab_rayon(I).GT.0.D0) THEN
       RAYONS=tab_rayon(I)
       ENDIF
      ENDDO  
      DA=RAYONS/DBLE(NPOI)
      WRITE(39,*)'IPAS,tab_rayon',IPAS,tab_rayon
      WRITE(39,*)'IPAS,RAYONS',IPAS,RAYONS
C---------------------------------------------------------------
      IF(NORDRE.EQ.1) RAYONS=0.01D0
C    RAYON=tab_rayon(1)
      DA=RAYONS/DBLE(NPOI)

C------------------ CALCUL DU RAYON PADE -----------------------       
      IF (RMOT2.EQ.'PADE') THEN   

      CALL RAYON_PADE(T(IT(15)),T(IT(30)),T(IT(46)),T(IT(72)),
     &     CRIT,NDL,NORDRE,RAYONS,TAB_RAYONP(1),DA,DIVERGE,
     &     POLE,POLE_C,
     &     NB_RAC,NB_RAC_POS,D,NB_RAC_C)                         !RAYON U 
  
      CALL RAYON_PADE(T(IT(16)),T(IT(31)),T(IT(47)),T(IT(72)),
     &     CRIT,NSIG,NORDRE,RAYONS,TAB_RAYONP(2),DA,DIVERGE,
     &     POLE,POLE_C,
     &     NB_RAC,NB_RAC_POS,D,NB_RAC_C)                         !RAYON SIG
      CALL RAYON_PADE(T(IT(17)),T(IT(32)),T(IT(49)),T(IT(72)),
     &     CRIT,NSIG,NORDRE,RAYONS,TAB_RAYONP(3),DA,DIVERGE,
     &     POLE,POLE_C,
     &     NB_RAC,NB_RAC_POS,D,NB_RAC_C)                         !RAYON EPSP
      CALL RAYON_PADE(T(IT(19)),T(IT(34)),T(IT(51)),T(IT(72)),
     &     CRIT,NSIG,NORDRE,RAYONS,TAB_RAYONP(4),DA,DIVERGE,
     &     POLE,POLE_C,
     &     NB_RAC,NB_RAC_POS,D,NB_RAC_C)                         !RAYON PN
      CALL RAYON_PADE(T(IT(20)),T(IT(35)),T(IT(52)),T(IT(72)),
     &     CRIT,NSIG,NORDRE,RAYONS,TAB_RAYONP(5),DA,DIVERGE,
     &     POLE,POLE_C,
     &     NB_RAC,NB_RAC_POS,D,NB_RAC_C)                         !RAYON SIGQ
      CALL RAYON_PADE(T(IT(22)),T(IT(37)),T(IT(54)),T(IT(72)),
     &     CRIT,NBG*NMAI,NORDRE,RAYONS,TAB_RAYONP(6),DA,DIVERGE,
     &     POLE,POLE_C,
     &     NB_RAC,NB_RAC_POS,D,NB_RAC_C)                         !RAYON SIGe
      CALL RAYON_PADE(T(IT(21)),T(IT(36)),T(IT(53)),T(IT(72)),
     &     CRIT,NBG*NMAI,NORDRE,RAYONS,TAB_RAYONP(7),DA,DIVERGE,
     &     POLE,POLE_C,
     &     NB_RAC,NB_RAC_POS,D,NB_RAC_C)                         !RAYON FC
      CALL RAYON_PADE(T(IT(23)),T(IT(38)),T(IT(55)),T(IT(72)),
     &     CRIT,NBG*NMAI,NORDRE,RAYONS,TAB_RAYONP(8),DA,DIVERGE,
     &     POLE,POLE_C,
     &     NB_RAC,NB_RAC_POS,D,NB_RAC_C)                         !RAYON GP
      CALL RAYON_PADE(T(IT(25)),T(IT(40)),T(IT(57)),T(IT(72)),
     &     CRIT,NBG*NMAI,NORDRE,RAYONS,TAB_RAYONP(9),DA,DIVERGE,
     &     POLE,POLE_C,
     &     NB_RAC,NB_RAC_POS,D,NB_RAC_C)                         !RAYON ZP
      CALL RAYON_PADE(T(IT(24)),T(IT(39)),T(IT(56)),T(IT(72)),
     &     CRIT,NBG*NMAI,NORDRE,RAYONS,TAB_RAYONP(10),DA,DIVERGE,
     &     POLE,POLE_C,
     &     NB_RAC,NB_RAC_POS,D,NB_RAC_C)                         !RAYON EPSPe
      CALL RAYON_PADE(T(IT(26)),T(IT(41)),T(IT(58)),T(IT(72)),
     &     CRIT,NBG*NMAI,NORDRE,RAYONS,TAB_RAYONP(11),DA,DIVERGE,
     &     POLE,POLE_C,
     &     NB_RAC,NB_RAC_POS,D,NB_RAC_C)                         !RAYON DEN
      CALL RAYON_PADE(T(IT(27)),T(IT(42)),T(IT(59)),T(IT(72)),
     &     CRIT,NBG*NMAI,NORDRE,RAYONS,TAB_RAYONP(12),DA,DIVERGE,
     &     POLE,POLE_C,
     &     NB_RAC,NB_RAC_POS,D,NB_RAC_C)                         !RAYON XP
      CALL RAYON_PADE(T(IT(28)),T(IT(43)),T(IT(60)),T(IT(72)),
     &     CRIT,NBG*NMAI,NORDRE,RAYONS,TAB_RAYONP(13),DA,DIVERGE,
     &     POLE,POLE_C,
     &     NB_RAC,NB_RAC_POS,D,NB_RAC_C)                         !RAYON YP
      
      CALL RAYON_PADE(T(IT(18)),T(IT(33)),T(IT(50)),T(IT(72)),
     &     CRIT,NBG*NMAI,NORDRE,RAYONS,TAB_RAYONP(14),DA,DIVERGE,
     &     POLE,POLE_C,
     &     NB_RAC,NB_RAC_POS,D,NB_RAC_C)                         !RAYON YP
      NB_RAC_R=NB_RAC-NB_RAC_C                                                 
      RAYONP=TAB_RAYONP(1)
      DO I=1,14
       IF (RAYONP.GT.TAB_RAYONP(I).AND.TAB_RAYONP(I).GT.0.D0) THEN
       RAYONP=TAB_RAYONP(I)
       ENDIF
      ENDDO
      ENDIF    !fin pade

      WRITE(19,*) RAYONS,RAYONP
      do i=1,10
      WRITE(19,*) TAB_RAYONP(i)
      enddo 
       IF(NORDRE.EQ.1) RAYONP=0.05D0
      DAP=RAYONP/DBLE(NPOI)
      WRITE(16,*)'RAYONS,RAYONP=',RAYONS,RAYONP,DAP 
      ITER=0
      ITERMAX=50
250   CONTINUE   
      WRITE(16,*)'RAYONS,RAYONP=',RAYONS,RAYONP,DAP 

      DO 6 IPOI=1,NPOI

      IF (RMOT2.EQ.'POLY') THEN
      A=DA*IPOI
      CALL SOLPOL_PLAS(T(IT(14)),T(IT(29)),CHARGE,1,NORDRE,A)       ! charge
      CALL SOLPOL_PLAS(T(IT(15)),T(IT(30)),T(IT(46)),NDL,NORDRE,A)  ! deplacement
      CALL SOLPOL_PLAS(T(IT(16)),T(IT(31)),T(IT(47)),NSIG,NORDRE,A) ! contraintes
      CALL SOLPOL_PLAS(T(IT(17)),T(IT(32)),T(IT(49)),NSIG,NORDRE,A) ! deformations EPSP
      CALL SOLPOL_PLAS(T(IT(19)),T(IT(34)),T(IT(51)),NSIG,NORDRE,A) ! PN
      CALL SOLPOL_PLAS(T(IT(20)),T(IT(35)),T(IT(52)),NBG*NMAI,NORDRE,A)  ! SIGQ
      CALL SOLPOL_PLAS(T(IT(22)),T(IT(37)),T(IT(54)),NBG*NMAI,NORDRE,A)  ! SIGe
      CALL SOLPOL_PLAS(T(IT(18)),T(IT(33)),T(IT(50)),NBG*NMAI,NORDRE,A)  ! lambDa
      CALL SOLPOL_PLAS(T(IT(21)),T(IT(36)),T(IT(53)),NBG*NMAI,NORDRE,A)  ! FC
      CALL SOLPOL_PLAS(T(IT(23)),T(IT(38)),T(IT(55)),NBG*NMAI,NORDRE,A)  ! GP
      CALL SOLPOL_PLAS(T(IT(25)),T(IT(40)),T(IT(57)),NBG*NMAI,NORDRE,A)  ! ZP
      CALL SOLPOL_PLAS(T(IT(24)),T(IT(39)),T(IT(56)),NBG*NMAI,NORDRE,A)  ! EPSPe
      CALL SOLPOL_PLAS(T(IT(27)),T(IT(42)),T(IT(59)),NBG*NMAI,
     &                                                      NORDRE-1,A)  ! XP
      CALL SOLPOL_PLAS(T(IT(28)),T(IT(43)),T(IT(60)),NBG*NMAI,
     &                                                      NORDRE-1,A)  ! YP
      CALL SOLPOL_PLAS(T(IT(26)),T(IT(41)),T(IT(58)),NBG*NMAI,NORDRE,A)  ! DEN
      CALL SOLPOL_PLAS(T(IT(61)),T(IT(62)),T(IT(63)),1,NORDRE,A) !X_chargeV
      CALL SOLPOL_PLAS(T(IT(64)),T(IT(65)),T(IT(66)),1,NORDRE,A) !Y_chargeV
           
COMM  /////////////////////  CALCUL ES VECTEUR TABGENTS  ////////////////
      CALL DERIVE_VECT(T(IT(29)),T(IT(105)),NORDRE,1,A)            !CHARGE1
      CALL DERIVE_VECT(T(IT(30)),T(IT(91)),NORDRE,NDL,A)           !U1
      CALL DERIVE_VECT(T(IT(31)),T(IT(92)),NORDRE,NSIG,A)          !SIG1
      CALL DERIVE_VECT(T(IT(32)),T(IT(93)),NORDRE,NSIG,A)          !EPSP1
      CALL DERIVE_VECT(T(IT(34)),T(IT(95)),NORDRE,NSIG,A)          !PN1
      CALL DERIVE_VECT(T(IT(33)),T(IT(94)),NORDRE,NBG*NMAI,A)      !LAMBDA1 
      CALL DERIVE_VECT(T(IT(35)),T(IT(97)),NORDRE,NBG*NMAI,A)      !SIGQ1 
      CALL DERIVE_VECT(T(IT(36)),T(IT(96)),NORDRE,NBG*NMAI,A)      !FC1   
      CALL DERIVE_VECT(T(IT(37)),T(IT(98)),NORDRE,NBG*NMAI,A)      !SIGe1 
      CALL DERIVE_VECT(T(IT(38)),T(IT(99)),NORDRE,NBG*NMAI,A)      !GP1 
      CALL DERIVE_VECT(T(IT(40)),T(IT(100)),NORDRE,NBG*NMAI,A)     !ZP1
      CALL DERIVE_VECT(T(IT(39)),T(IT(101)),NORDRE,NBG*NMAI,A)     !EPSPe1
      CALL DERIVE_VECT(T(IT(41)),T(IT(102)),NORDRE,NBG*NMAI,A)     !DEN1
      CALL DERIVE_VECT(T(IT(42)),T(IT(106)),NORDRE-1,NBG*NMAI,A)   !XP1
      CALL DERIVE_VECT(T(IT(43)),T(IT(107)),NORDRE-1,NBG*NMAI,A)   !YP1
      CALL DERIVE_VECT(T(IT(62)),T(IT(103)),NORDRE,1,A)            !XC1
      CALL DERIVE_VECT(T(IT(65)),T(IT(104)),NORDRE,1,A)            !YC1
      
      
      
      ELSEIF (RMOT2.EQ.'PADE') THEN      ! fin de if poly
      if (IPAS.EQ.1) then
      A=DA*IPOI
      CALL SOLPOL_PLAS(T(IT(14)),T(IT(29)),CHARGE,1,NORDRE,A)       ! charge
      CALL SOLPOL_PLAS(T(IT(15)),T(IT(30)),T(IT(46)),NDL,NORDRE,A)  ! deplacement
      CALL SOLPOL_PLAS(T(IT(16)),T(IT(31)),T(IT(47)),NSIG,NORDRE,A) ! contraintes
      CALL SOLPOL_PLAS(T(IT(17)),T(IT(32)),T(IT(49)),NSIG,NORDRE,A) ! deformations EPSP
      CALL SOLPOL_PLAS(T(IT(19)),T(IT(34)),T(IT(51)),NSIG,NORDRE,A) ! PN
      CALL SOLPOL_PLAS(T(IT(20)),T(IT(35)),T(IT(52)),NBG*NMAI,NORDRE,A)  ! SIGQ
      CALL SOLPOL_PLAS(T(IT(22)),T(IT(37)),T(IT(54)),NBG*NMAI,NORDRE,A)  ! SIGe
      CALL SOLPOL_PLAS(T(IT(18)),T(IT(33)),T(IT(50)),NBG*NMAI,NORDRE,A)  ! lambDa
      CALL SOLPOL_PLAS(T(IT(21)),T(IT(36)),T(IT(53)),NBG*NMAI,NORDRE,A)  ! FC
      CALL SOLPOL_PLAS(T(IT(23)),T(IT(38)),T(IT(55)),NBG*NMAI,NORDRE,A)  ! GP
      CALL SOLPOL_PLAS(T(IT(25)),T(IT(40)),T(IT(57)),NBG*NMAI,NORDRE,A)  ! ZP
      CALL SOLPOL_PLAS(T(IT(24)),T(IT(39)),T(IT(56)),NBG*NMAI,NORDRE,A)  ! EPSPe
      CALL SOLPOL_PLAS(T(IT(27)),T(IT(42)),T(IT(59)),NBG*NMAI,
     &                                                      NORDRE-1,A)  ! XP
      CALL SOLPOL_PLAS(T(IT(28)),T(IT(43)),T(IT(60)),NBG*NMAI,
     &                                                      NORDRE-1,A)  ! YP
      CALL SOLPOL_PLAS(T(IT(26)),T(IT(41)),T(IT(58)),NBG*NMAI,NORDRE,A)  ! DEN
      CALL SOLPOL_PLAS(T(IT(61)),T(IT(62)),T(IT(63)),1,NORDRE,A) !X_chargeV
      CALL SOLPOL_PLAS(T(IT(64)),T(IT(65)),T(IT(66)),1,NORDRE,A) !Y_chargeV
           
COMM  /////////////////////  CALCUL ES VECTEUR TABGENTS  ////////////////
      CALL DERIVE_VECT(T(IT(29)),T(IT(105)),NORDRE,1,A)            !CHARGE1
      CALL DERIVE_VECT(T(IT(30)),T(IT(91)),NORDRE,NDL,A)           !U1
      CALL DERIVE_VECT(T(IT(31)),T(IT(92)),NORDRE,NSIG,A)          !SIG1
      CALL DERIVE_VECT(T(IT(32)),T(IT(93)),NORDRE,NSIG,A)          !EPSP1
      CALL DERIVE_VECT(T(IT(34)),T(IT(95)),NORDRE,NSIG,A)          !PN1
      CALL DERIVE_VECT(T(IT(33)),T(IT(94)),NORDRE,NBG*NMAI,A)      !LAMBDA1 
      CALL DERIVE_VECT(T(IT(35)),T(IT(97)),NORDRE,NBG*NMAI,A)      !SIGQ1 
      CALL DERIVE_VECT(T(IT(36)),T(IT(96)),NORDRE,NBG*NMAI,A)      !FC1   
      CALL DERIVE_VECT(T(IT(37)),T(IT(98)),NORDRE,NBG*NMAI,A)      !SIGe1 
      CALL DERIVE_VECT(T(IT(38)),T(IT(99)),NORDRE,NBG*NMAI,A)      !GP1 
      CALL DERIVE_VECT(T(IT(40)),T(IT(100)),NORDRE,NBG*NMAI,A)     !ZP1
      CALL DERIVE_VECT(T(IT(39)),T(IT(101)),NORDRE,NBG*NMAI,A)     !EPSPe1
      CALL DERIVE_VECT(T(IT(41)),T(IT(102)),NORDRE,NBG*NMAI,A)     !DEN1
      CALL DERIVE_VECT(T(IT(42)),T(IT(106)),NORDRE-1,NBG*NMAI,A)   !XP1
      CALL DERIVE_VECT(T(IT(43)),T(IT(107)),NORDRE-1,NBG*NMAI,A)   !YP1
      CALL DERIVE_VECT(T(IT(62)),T(IT(103)),NORDRE,1,A)            !XC1
      CALL DERIVE_VECT(T(IT(65)),T(IT(104)),NORDRE,1,A)            !YC1
      
      else
      
      AP=DAP*IPOI   
c      CALL ORTHOGONALISER(T(IT(72)),T(IT(30)),NDL,NORDRE,D)        ! base: Ui
      CALL ORTHOGONALISER(T(IT(72)),T(IT(31)),NSIG,NORDRE,D)        ! base: SIGi
      CALL SOL_PAD(D,T(IT(14)),T(IT(29)),CHARGE,1,NORDRE,AP)       ! charge
      CALL SOL_PAD(D,T(IT(15)),T(IT(30)),T(IT(46)),NDL,NORDRE,AP)  ! deplacement
      CALL SOL_PAD(D,T(IT(16)),T(IT(31)),T(IT(47)),NSIG,NORDRE,AP) ! contraintes
      CALL SOL_PAD(D,T(IT(17)),T(IT(32)),T(IT(49)),NSIG,NORDRE,AP) ! deformations EPSP
      CALL SOL_PAD(D,T(IT(19)),T(IT(34)),T(IT(51)),NSIG,NORDRE,AP) ! PN
      CALL SOL_PAD(D,T(IT(20)),T(IT(35)),T(IT(52)),NBG*NMAI,NORDRE,AP)  ! SIGQ
      CALL SOL_PAD(D,T(IT(22)),T(IT(37)),T(IT(54)),NBG*NMAI,NORDRE,AP)  ! SIGe
      CALL SOL_PAD(D,T(IT(21)),T(IT(36)),T(IT(53)),NBG*NMAI,NORDRE,AP)  ! FC
      CALL SOL_PAD(D,T(IT(23)),T(IT(38)),T(IT(55)),NBG*NMAI,NORDRE,AP)  ! GP
      CALL SOL_PAD(D,T(IT(25)),T(IT(40)),T(IT(57)),NBG*NMAI,NORDRE,AP)  ! ZP
      CALL SOL_PAD(D,T(IT(24)),T(IT(39)),T(IT(56)),NBG*NMAI,NORDRE,AP)  ! EPSPe
      CALL SOL_PAD(D,T(IT(27)),T(IT(42)),T(IT(59)),NBG*NMAI,NORDRE-1
     &                                                            ,AP)  ! XP
      CALL SOL_PAD(D,T(IT(28)),T(IT(43)),T(IT(60)),NBG*NMAI,NORDRE-1
     &                                                            ,AP)  ! YP  
      CALL SOL_PAD(D,T(IT(26)),T(IT(41)),T(IT(58)),NBG*NMAI,NORDRE,AP)  ! den    
      CALL SOL_PAD(D,T(IT(61)),T(IT(62)),T(IT(63)),1,NORDRE,AP) !X_chargeV
      CALL SOL_PAD(D,T(IT(64)),T(IT(65)),T(IT(66)),1,NORDRE,AP) !Y_chargeV
      CALL SOL_PAD(D,T(IT(18)),T(IT(33)),T(IT(50)),NBG*NMAI,NORDRE,AP)  ! lambDa
      
COMM  /////////////////////  CALCUL ES VECTEUR TABGENTS  ////////////////
      CALL VECT_TANGENT_PADE(D,T(IT(29)),T(IT(105)),1,NORDRE,AP)            !CHARGE1
      CALL VECT_TANGENT_PADE(D,T(IT(30)),T(IT(91)),NDL,NORDRE,AP)           !U1
      CALL VECT_TANGENT_PADE(D,T(IT(31)),T(IT(92)),NSIG,NORDRE,AP)          !SIG1
      CALL VECT_TANGENT_PADE(D,T(IT(32)),T(IT(93)),NSIG,NORDRE,AP)          !EPSP1
      CALL VECT_TANGENT_PADE(D,T(IT(34)),T(IT(95)),NSIG,NORDRE,AP)          !PN1
      CALL VECT_TANGENT_PADE(D,T(IT(33)),T(IT(94)),NBG*NMAI,NORDRE,AP)      !LAMBDA1 
      CALL VECT_TANGENT_PADE(D,T(IT(35)),T(IT(97)),NBG*NMAI,NORDRE,AP)      !SIGQ1 
      CALL VECT_TANGENT_PADE(D,T(IT(36)),T(IT(96)),NBG*NMAI,NORDRE,AP)      !FC1   
      CALL VECT_TANGENT_PADE(D,T(IT(37)),T(IT(98)),NBG*NMAI,NORDRE,AP)      !SIGe1 
      CALL VECT_TANGENT_PADE(D,T(IT(38)),T(IT(99)),NBG*NMAI,NORDRE,AP)      !GP1 
      CALL VECT_TANGENT_PADE(D,T(IT(40)),T(IT(100)),NBG*NMAI,NORDRE,AP)     !ZP1
      CALL VECT_TANGENT_PADE(D,T(IT(39)),T(IT(101)),NBG*NMAI,NORDRE,AP)     !EPSPe1
      CALL VECT_TANGENT_PADE(D,T(IT(41)),T(IT(102)),NBG*NMAI,NORDRE,AP)     !DEN1
      CALL VECT_TANGENT_PADE(D,T(IT(42)),T(IT(106)),NBG*NMAI,NORDRE-1
     &                                                             ,AP)   !XP1
      CALL VECT_TANGENT_PADE(D,T(IT(43)),T(IT(107)),NBG*NMAI,NORDRE-1
     &                                                             ,AP)   !YP1
      CALL VECT_TANGENT_PADE(D,T(IT(62)),T(IT(103)),1,NORDRE,AP)            !XC1
      CALL VECT_TANGENT_PADE(D,T(IT(65)),T(IT(104)),1,NORDRE,AP)            !YC1
      ENDIF
      ENDIF
      CALL DINCRE(T(IT(12)),T(IT(69)),NDL,CHARGE)
      CALL ECDEPL(T(IT(69)),19)
      CALL RESI_PLAS_2D(T(IT(46)),T(IT(1)),T(IT(2)),T(IT(3)),
     &                T(IT(4)),T(IT(47)),T(IT(48)),T(IT(67)),
     &                T(IT(69)),T(IT(52)),T(IT(54)),T(IT(53)),
     &                T(IT(57)),T(IT(55)),T(IT(58)),T(IT(49)),
     &                T(IT(51)),NBG,NMAI,NDL,NCPN,NDPN,NSPG,
     &                NNPE,NVMA,KTYP,NSIG,IPAS,T(IT(92)),
     &                T(IT(93)),T(IT(59)),T(IT(60)))
      CALL CALIMP(T(IT(5)),T(IT(67)),NLIAI,NDPN)
      WRITE(11,*) 'RESIDU AU PAS : ',IPAS
      CALL ECDEPL(T(IT(67)),11)
      CALL NORMER(T(IT(67)),T(IT(69)),IPOI,NDL,XNORM1R)

      CALL COMPOS(T(IT(46)),NOEUD,1,NDPN,COMPX)
      CALL COMPOS(T(IT(46)),NOEUD,2,NDPN,COMPY)
      CALL COMPOS_SIG(T(IT(47)),INMAI,IG,1,NSPG,NBG,COMPS)
      CALL COMPOS_SIG(T(IT(48)),INMAI,IG,1,NSPG,NBG,COMPEPS)
      CALL COMPOS_SIG(T(IT(52)),INMAI,IG,1,1,NBG,COMP_SIGQ)
      CALL COMPOS_SIG(T(IT(54)),INMAI,IG,1,1,NBG,COMP_SIGe)
      CALL COMPOS_SIG(T(IT(55)),INMAI,IG,1,1,NBG,COMP_GP)
      CALL COMPOS_SIG(T(IT(59)),INMAI,IG,1,1,NBG,COMP_XP)  !XP
      CALL COMPOS_SIG(T(IT(60)),INMAI,IG,1,1,NBG,COMP_YP)  !YP
      CALL COMPOS_SIG(T(IT(53)),INMAI,IG,1,1,NBG,COMP_FC)
      CALL COMPOS_SIG(T(IT(49)),INMAI,IG,1,NSPG,NBG,COMPEPSP)

      CALL ECCONT(T(IT(47)),T(IT(2)),16)
      call ECDEPL(T(IT(46)),16)
            BETA=10.d0      
      Temps=Temps+A/BETA

      WRITE(29,51)IPAS,COMPX,COMPY,COMPEPS,COMPS,COMP_SIGQ,
     &          COMP_SIGe,COMP_GP,COMP_FC,COMPEPSP
      WRITE(12,*) IPAS,COMPX,XNORM1R,CHARGE,Temps,COMP_FC
      WRITE(38,*) COMP_XP,COMP_YP,(1.D0+COMP_FC)*COMP_XP,
     &            COMP_FC

 6    CONTINUE



      CALL SHIFTD(CHARGE,T(IT(14)),1)          !charge
      CALL SHIFTD(T(IT(46)),T(IT(15)),NDL)     !U
      CALL SHIFTD(T(IT(47)),T(IT(16)),NSIG)    !SIG
      CALL SHIFTD(T(IT(49)),T(IT(17)),NSIG)    !EPSP
      CALL SHIFTD(T(IT(51)),T(IT(19)),NSIG)    !PN
      CALL SHIFTD(T(IT(52)),T(IT(20)),NBG*NMAI)     ! q
      CALL SHIFTD(T(IT(54)),T(IT(22)),NBG*NMAI)     ! SIGe
      CALL SHIFTD(T(IT(53)),T(IT(21)),NBG*NMAI)     ! FC0
      CALL SHIFTD(T(IT(55)),T(IT(23)),NBG*NMAI)     ! GP
      CALL SHIFTD(T(IT(57)),T(IT(25)),NBG*NMAI)     ! ZP
      CALL SHIFTD(T(IT(56)),T(IT(24)),NBG*NMAI)     ! EPSPe
      CALL SHIFTD(T(IT(59)),T(IT(27)),NBG*NMAI)     ! XP
      CALL SHIFTD(T(IT(60)),T(IT(28)),NBG*NMAI)     ! YP
      CALL SHIFTD(T(IT(58)),T(IT(26)),NBG*NMAI)     ! DEN      
      CALL SHIFTD(T(IT(63)),T(IT(61)),1)   ! X_chargeV
      CALL SHIFTD(T(IT(66)),T(IT(64)),1)   ! Y_chargeV
      CALL SHIFTD(T(IT(50)),T(IT(18)),NBG*NMAI)   ! LAMBDA 
      
comm   /////////////////MISE AJOUR E L'ORRE 1/////////////   
      CALL SHIFTD(T(IT(105)),T(IT(29)),1)          !charge
      CALL SHIFTD(T(IT(91)),T(IT(30)),NDL)         !U
      CALL SHIFTD(T(IT(92)),T(IT(31)),NSIG)        !SIG
      CALL SHIFTD(T(IT(93)),T(IT(32)),NSIG)        !EPSP 
      CALL SHIFTD(T(IT(95)),T(IT(34)),NSIG)        !PN
      CALL SHIFTD(T(IT(97)),T(IT(35)),NBG*NMAI)      ! q
      CALL SHIFTD(T(IT(98)),T(IT(37)),NBG*NMAI)      ! SIGe
      CALL SHIFTD(T(IT(96)),T(IT(36)),NBG*NMAI)      ! FC
      CALL SHIFTD(T(IT(99)),T(IT(38)),NBG*NMAI)      ! GP
      CALL SHIFTD(T(IT(100)),T(IT(40)),NBG*NMAI)     ! ZP
      CALL SHIFTD(T(IT(101)),T(IT(39)),NBG*NMAI)     ! EPSPe
      CALL SHIFTD(T(IT(106)),T(IT(42)),NBG*NMAI)     ! XP
      CALL SHIFTD(T(IT(107)),T(IT(43)),NBG*NMAI)     ! YP
      CALL SHIFTD(T(IT(102)),T(IT(41)),NBG*NMAI)     ! DEN   
      CALL SHIFTD(T(IT(94)),T(IT(33)),NBG*NMAI)   ! LAMBDA   
      CALL SHIFTD(T(IT(103)),T(IT(62)),1)   ! X_chargeV
      CALL SHIFTD(T(IT(104)),T(IT(65)),1)   ! Y_chargeV

         
      APOSITIF=.TRUE.
C      CALL VORIEN(T(IT(27)),T(IT(44)),APOSITIF,NORDRE,NDL,RAYON)  !EPSP1=dEPSP/da(rayon)

      CALL ECCONT(T(IT(16)),T(IT(2)),6)
      CALL ECCONT(T(IT(16)),T(IT(2)),10)
      write(10,*)'DEFORMATION PLASTIQUE'
      CALL ECCONT(T(IT(17)),T(IT(2)),10)
      write(10,*)'LA DIRECTION PN'
      CALL ECCONT(T(IT(19)),T(IT(2)),10)

 15   CONTINUE
111   CONTINUE
      ELSE                   !de  CONT
      STOP 'ERREUR DE MOT CLE'
      ENDIF
COMM ..................../////////////................................
 150  FORMAT(////////,
     ;'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%',/,
     ;'  %                                                        %',/, 
     ;'  %         CALCUL   ASYMPTOTIQUE  NUMERIQUE               %',/,      
     ;'  %                                                        %',/,
     ;'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')           
 100  FORMAT('1',//,' ',60('%'),///,
     ;'  ORGANISATION DU SUPER-TABLEAU  T() ',//)
 110  FORMAT(' ',60('%'),/////)
 120  FORMAT('   IT(',I3,')=',I8,'     Longueur=',I8)
 130  FORMAT(///,'   Taille memoire necessaire   : ',I13,/,
     ;           '   Dimension du Super-Tableau  : ',I13,///)
 50   FORMAT(1X,G15.8,1X,G15.8,1X,G15.8,1X,G15.8,1X,G15.8,1X,G15.8,1X,
     &       G15.8)
 51   FORMAT(I4,10(1X,G15.8))
 52   FORMAT('#IPAS,COMPX,COMPS,COMPEPS')
 53   FORMAT('#IPAS,EPS11,S11,SIGQ,SIGe,GP,FC,EPSP11')
      END
      

 
