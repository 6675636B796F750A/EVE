      SUBROUTINE SERIES_PLAS_DECHARGE(COOR,MAIL,EMAI,EPAI,
     &	         LIAI,IDIA,DFOR,F,RIG,SIG,SIG0,SIGQ,SIGQ0,
     &           SIGe,SIGe0,EPS,EPSP,EPSP0,EPSPe,EPSPe0,
     &           PN,PN0,FC,FC0,
     &           LAMBDA,LAMBDA0,
     &           GP,GP0,ZP,ZP0,
     &           XP,XP0,YP,YP0,
     &           XCi,XC0,YCi,YC0,C,V,FQ,XL1,XL2,Cm,Tm,EPSP1,
     &           DEN0,DEN,
     &           NBG,NNOE,NMAI,NDL,NCPN,NDPN,NNPE,NLIAI,NVMA,
     &           KTYP,NSPG,NSIG,NORDRE,NRIG,IP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   ON RESOUT LES PROBLEMES A CHAQUE ORDRE POUR DETERMINER       %
COMM %   LES TERMES   C(p)  V(p)   SIG(p)   T(p)                      %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION COOR(NCPN,1),MAIL(NNPE+2,1),EMAI(NVMA,1),EPAI(1),
     &          LIAI(2,1),IDIA(1),DFOR(1),F(1),RIG(1),SIG(NSIG,1:*),
     &          SIG0(1),SIGQ(NBG*NMAI,1:*),SIGQ0(NBG*NMAI),
     &          SIGe(NBG*NMAI,1:*),SIGe0(NBG*NMAI),
     &          EPS(NSIG,1:*),EPSP(NSIG,1:*),EPSP0(NSIG),
     &          PN(NSIG,1:*),PN0(NSIG),FC(NMAI*NBG,1:*),
     &          FC0(NBG*NMAI),F1(NDL),
     &          GP(NMAI*NBG,1:*),GP0(NBG*NMAI),
     &          ZP(NMAI*NBG,1:*),ZP0(NBG*NMAI),
     &          XP(NMAI*NBG,1:*),XP0(NBG*NMAI),
     &          YP(NMAI*NBG,1:*),YP0(NBG*NMAI),
     &          EPSPe(NBG*NMAI,1:*),EPSPe0(NBG*NMAI),
     &          XC0(1),YC0(1),XCi(1),YCi(1),DEN0(NBG*NMAI),
     &          DEN(NBG*NMAI,1:*),
     &          C(1),V(NDL,1:*),FQ(NDL,1:*),EPSP1(NSIG),VRES(NDL)
      DOUBLE PRECISION LAMBDA(NMAI*NBG,1:*),LAMBDA0(NMAI*NBG)
              
      
c      CALL DZERO(C,(NORDRE+1))
c      CALL DZERO(V,(NORDRE+1)*NDL)
c      CALL DZERO(FQ,(NORDRE+1)*NDL)
c      CALL DZERO(SIG,(NORDRE+1)*NSIG)
c      CALL DZERO(SIGQ,(NORDRE+1)*NMAI*NBG)
c      CALL DZERO(SIGe,(NORDRE+1)*NMAI*NBG)
c      CALL DZERO(EPSP,(NORDRE+1)*NSIG)
c      CALL DZERO(PN,(NORDRE+1)*NSIG)
c      CALL DZERO(FC,(NORDRE+1)*NMAI*NBG)
c      CALL DZERO(LAMBDA,(NORDRE+1)*NMAI*NBG)
c      CALL DZERO(EPSPe,(NORDRE+1)*NMAI*NBG)
c      CALL DZERO(GP,(NORDRE+1)*NMAI*NBG)
c      CALL DZERO(DEN,(NORDRE+1)*NMAI*NBG)
c      CALL DZERO(ZP,(NORDRE+1)*NMAI*NBG)
c      CALL DZERO(XP,(NORDRE+1)*NMAI*NBG)
c      CALL DZERO(YP,(NORDRE+1)*NMAI*NBG)
c      CALL DZERO(XCi,(NORDRE+1))
c      CALL DZERO(YCi,(NORDRE+1))
       

COMM  ....//// Cn Vn SIGn et Tn A CHAQUE ORDRE , ON LES IMPRIME  ////....
COMM     - on forme le second membre FQ  relatif a l'operateur Q
COMM     - on resout Kt U = FQ
COMM     - on calcule le coefficient C(p) (depend du choix de "a")
COMM       et on termine de former le vecteur Vn
COMM  ..................///////////////////............................
      
      CALL RIGTAN_PLAS_DECHARGE_2D(RIG,IDIA,COOR,MAIL,EMAI,EPAI,
     &      PN0,XP0,YP0,GP0,NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP,
     &      NSIG,NBG)
        
      CALL CALIRI(RIG,IDIA,LIAI,NLIAI,NDPN,NDL)
      CALL DECCHO(RIG,IDIA,NDL,0)

      DO 2 IORDRE=2,NORDRE  
      CALL DZERO(VRES,NDL)
      CALL SMFQP_PLAS_DECHARGE_2D(FQ(1,IORDRE),COOR,MAIL,EMAI,
     &        EPAI,SIG,SIG0,SIGQ,SIGQ0,EPS,EPSP,PN,PN0,FC,FC0,
     &        LAMBDA,GP,GP0,ZP,ZP0,XP,XP0,YP,YP0,SIGe,SIGe0,
     &        EPSPe,EPSPe0,DEN0,DEN,
     &  KTYP,NBG,NMAI,NDL,NSIG,NNPE,NCPN,NDPN,NSPG,NVMA,IORDRE)
       
      CALL CVORD_CHARGEV(C,XCi,YCi,XC0,YC0,Cm,Tm,IORDRE)
c      C(IORDRE)=0.D0
      CALL SHIFTD(F,VRES,NDL)
      CALL DINCRE(VRES,VRES,NDL,C(IORDRE))
      CALL DSOMME(VRES,FQ(1,IORDRE),NDL)
      
      CALL SHIFTD(VRES,V(1,IORDRE),NDL)
      CALL CALIMP(LIAI,V(1,IORDRE),NLIAI,NDPN)
      CALL RESCHO(RIG,V(1,IORDRE),IDIA,NDL)
      
      CALL IMPCVP(C,V,NDL,IORDRE)

      CALL CALCULP_LOI3_2D(V,COOR,MAIL,EMAI,EPAI,
     &           SIG,SIG0,SIGQ,SIGQ0,EPS,EPSP,EPSPe,EPSPe0,PN,PN0,
     &           FC,FC0,LAMBDA,GP,GP0,ZP,ZP0,XP,XP0,YP,YP0,SIGe,
     &           SIGe0,DEN0,DEN,KTYP,NBG,NMAI,NDL,NSIG,NNPE,NCPN,
     &           NDPN,NSPG,NVMA,IORDRE)
      
 2    CONTINUE

      END
      

