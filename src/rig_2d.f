
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %  //////  MODULE REGROUPANT LES SUBROUTINES DE CALCULS  //////   %
COMM %  //////   DE RIGIDITE ELASTIQUE   -- LINEAIRE 2D --    //////   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE RELA2D(RIG,IDIA,COOR,MAIL,EMAI,EPAI,
     *                  NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   CALCUL DE LA MATRICE DE RIGIDITE ELASTIQUE GLOBALE            %
COMM %         DE LA STRUCTURE BIDIMENSIONNELLE                        %
COMM %                                                                 %
COMM %   RIG   MATRICE DE RIGIDITE GLOBALE                             %
COMM %   IDIA  TABLEAU POINTEUR DE LA DIAGONALE                        %
COMM %   MAIL  TABLEAU DECRIVANT LA NUMEROTATION DES ELEMENTS          %
COMM %   COOR  COORDONNEES DES NOEUDS                                  %
COMM %   EMAI  CARACTERISTIQUES MECANIQUES DES ELEMENTS                %
COMM %   NMAI  NOMBRE D ELEMENTS                                       %
COMM %   NDL   NOMBRE DE DDL TOTAL                                     %
COMM %   NCPN  NOMBRE DE COORDONNEES PAR NOEUD                         %
COMM %   NDPN  NOMBRE DE DDL PAR NOEUD                                 %
COMM %   NNPE  NOMBRE DE NOEUDS MAXIMUN PAR ELEMENT                    %
COMM %   KTYP = 1 --> DEFORMATION PLANE                                %
COMM %   KTYP = 2 --> CONTRAINTE PLANE                                 %
COMM %   KTYP = 3 --> AXISYMMETRIQUE                                   %
COMM %                                                                 %
COMM %   NBN   NOMBRE DE NOEUD DE L'ELEMENT                            %
COMM %   NDDLE NOMBRE DE DDL DE L'ELEMENT                              %
COMM %   NBG   NOMBRE DE POINT DE GAUSS DE L'ELEMENT                   %
COMM %   GAUSS COORDONNEES POINT DE GAUSS DE L'ELEMENT                 %
COMM %   POIDS POID AU POINT DE GAUSS DE L'ELEMENT                     %
COMM %   RGL   MATRICE DE RIGIDITE ELEMENTAIRE                         %
COMM %   NO    NUMEROS GLOBAUX DES NOEUDS                              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RIG(1),IDIA(1),COOR(NCPN,1),MAIL(NNPE+2,1),
     *          EMAI(NVMA,1),EPAI(1)      
      DIMENSION RGL(136),NO(8),IGLO(16),X(8),Y(8),D(16) 
COMM ...........................////////////...........................
      CALL DZERO(RIG,IDIA(NDL))
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////.............
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)       
      CALL CAEL2D(ITYP,NBN,NDDLE,NBG)             
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))      
 3    CONTINUE
      H=EPAI(IM)
COMM  ......//////// ON FORME LA MATRICE DECOMPORTEMENT D //////........
      IF      (KTYP.EQ.1) THEN
         ND=3
         CALL DISODP(EMAI(1,IMAT),EMAI(2,IMAT),D)
      ELSE IF (KTYP.EQ.2) THEN
         ND=3
         CALL DISOCP(EMAI(1,IMAT),EMAI(2,IMAT),D)
      ELSE IF (KTYP.EQ.3) THEN
         ND=4
         CALL DISOAX(EMAI(1,IMAT),EMAI(2,IMAT),D)
      ELSE 
         STOP ' RELA2D : pb sur KTYP '
      ENDIF      
COMM..........///// CALCUL DE LA RIGIDITE ELEMENTAIRE /////.............
      IF(ITYP.EQ.30.OR.ITYP.EQ.40.OR.
     *   ITYP.EQ.60.OR.ITYP.EQ.80.OR.ITYP.EQ.81) THEN
        CALL RIGELE_ISOPARA2D(X,Y,D,H,RGL,KTYP,ITYP,NBN,NDDLE,NBG,ND)
      ELSE IF(ITYP.EQ.41) THEN
C        CALL RIGELE_Q4WT(     )      
      ELSE      
        STOP ' RELA2D : pb sur ITYP '  
      ENDIF
COMM .........////// ASSEMBLAGE DE LA MAT. RIGID. ELEM. ////...........       
      CALL ASSMAT(RIG,IDIA,RGL,NO,IGLO,NBN,NDPN)   
 20   CONTINUE
      END      
      
      SUBROUTINE RIGELE_ISOPARA2D(X,Y,D,H,RGL,
     *                            KTYP,ITYP,NBN,NDDLE,NBG,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE RIGIDITE ELEMENTAIRE : ELEMENTS ISOPARAMETRIQUES 2D %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION X(8),Y(8),D(16),RGL(136),ARGL(136),B(64),DB(64),
     *          GAUSS(2,9),POIDS(9)      
      DOUBLE PRECISION KSI,ETA         
COMM 
      ITS=(NDDLE*NDDLE+NDDLE)/2
      CALL DZERO(RGL,ITS)     
      CALL GRIG2D(ITYP,GAUSS,POIDS)       
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////.............
      DO 7 NG=1,NBG     
        KSI=GAUSS(1,NG)
        ETA=GAUSS(2,NG)
COMM ......//// CALCUL DE LA MATRICE B ET DE DETJ ( et RAY)////........
        CALL BLINISO2D(X,Y,KSI,ETA,B,DETJ,RAY,NDDLE,NBN,ND,ITYP,KTYP)
COMM...........////// CALCUL DE Bt.D.B  ////////////..................       
        CALL TBDB(B,D,DB,ARGL,ND,NDDLE,ITS)
COMM...........////// CALCUL DU COEFFICIENT  ////////////............           
        IF (KTYP.EQ.1.OR.KTYP.EQ.2) THEN
          COEF=DETJ*H*POIDS(NG)
        ELSE IF (KTYP.EQ.3) THEN
          COEF=DETJ*6.2831853*RAY*POIDS(NG)
        ENDIF
        CALL DACTUA(RGL,ARGL,ITS,COEF)
 7    CONTINUE 
      END
     
      SUBROUTINE CAEL2D(ITYP,NBN,NDDLE,NBG)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  EN FONCTION DU TYPE DE L'ELEMENT ON RENVOI LE NOMBRE DE NOEUDS %
COMM %  LE NOMBRE DE DDL, LE NOMBRE DE POINTS DE GAUSS                 %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IF      (ITYP.EQ.30) THEN
        NBN=3
        NDDLE=6
        NBG=1
      ELSE IF (ITYP.EQ.40) THEN
        NBN=4
        NDDLE=8
        NBG=4
      ELSE IF (ITYP.EQ.60) THEN
        NBN=6
        NDDLE=12
        NBG=3      
      ELSE IF (ITYP.EQ.80) THEN
        NBN=8
        NDDLE=16
        NBG=9
      ELSE IF (ITYP.EQ.81) THEN
        NBN=8
        NDDLE=16
        NBG=4
      ELSE
       STOP ' PB : CAEL2D '
      ENDIF
      END
      
      SUBROUTINE GRIG2D(ITYP,GAUSS,POIDS)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  EN FONCTION DU TYPE DE L'ELEMENT, ON RENVOIE LES COORDONNEES   %
COMM %  DES POINTS DE GAUSS, ET LES POIDS DE GAUSS                     %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION GAUSS(2,9),POIDS(9),RQ4(2),RQ8(3),WQ8(3)
COMM    
      DATA RQ4/-0.577350269189626,0.577350269189626/  
      DATA RQ8/-0.774596669241483,0.000000000000000,0.774596669241483/
      DATA WQ8/ 0.555555555555556,0.888888888888889,0.555555555555556/      
COMM            
      IF      (ITYP.EQ.30) THEN
        GAUSS(1,1)=1.D0/3.D0
        GAUSS(2,1)=1.D0/3.D0
        POIDS(1)=0.5D0
      ELSE IF (ITYP.EQ.40) THEN
        DO 1 I=1,2
        DO 1 J=1,2
        K=I+(J-1)*2
        GAUSS(1,K)=RQ4(I)
        GAUSS(2,K)=RQ4(J)
        POIDS(K)=1.D0
 1      CONTINUE       
      ELSE IF (ITYP.EQ.60) THEN
        GAUSS(1,1)=1.D0/6.D0
        GAUSS(1,2)=2.D0/3.D0
        GAUSS(1,3)=1.D0/6.D0
        GAUSS(2,1)=1.D0/6.D0
        GAUSS(2,2)=1.D0/6.D0
        GAUSS(2,3)=2.D0/3.D0
        DO 2 I=1,3
 2      POIDS(I)=1.D0/6.D0                     
      ELSE IF (ITYP.EQ.80) THEN
        DO 4 I=1,3
        DO 4 J=1,3
        K=I+(J-1)*3
        GAUSS(1,K)=RQ8(I)
        GAUSS(2,K)=RQ8(J)
        POIDS(K)=WQ8(I)*WQ8(J)
 4      CONTINUE 
      ELSE IF (ITYP.EQ.81) THEN
        DO 11 I=1,2
        DO 11 J=1,2
        K=I+(J-1)*2
        GAUSS(1,K)=RQ4(I)
        GAUSS(2,K)=RQ4(J)
        POIDS(K)=1.D0
 11     CONTINUE        
      ELSE
       STOP ' PB : GRIG2D '
      ENDIF
      END 
                  
      SUBROUTINE DISOCP(YG,PS,D)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE COMPORTEMENT ISOTROPE D   -- CONTRAINTE PLANE --    %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION D(3,3)
      COEF=YG/(1.D0-PS*PS)
      D(1,1)=COEF
      D(1,2)=COEF*PS
      D(1,3)=0.D0
      D(2,1)=D(1,2)
      D(2,2)=COEF
      D(2,3)=0.D0
      D(3,1)=0.D0
      D(3,2)=0.D0
      D(3,3)=0.5*COEF*(1.D0-PS)
      END
      
      SUBROUTINE DISODP(YG,PS,D)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE COMPORTEMENT ISOTROPE D   -- DEFORMATION PLANE --   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION D(3,3)
      COEF= YG / ( (1.D0-2.D0*PS)*(1.D0+PS) )
      D(1,1)=COEF*(1.D0-PS)
      D(1,2)=COEF*PS
      D(1,3)=0.D0
      D(2,1)=D(1,2)
      D(2,2)=D(1,1)
      D(2,3)=0.D0
      D(3,1)=0.D0
      D(3,2)=0.D0
      D(3,3)=0.5*COEF*(1.D0-2.D0*PS)
      END
       
      SUBROUTINE DISOAX(YG,PS,D)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE COMPORTEMENT ISOTROPE  D -- AXISYMETRIQUE     --    %         %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION D(4,4)
      COEF= YG / ( (1.D0-2.D0*PS)*(1.D0+PS) )
      D(1,1)=COEF*(1.D0-PS)
      D(1,2)=COEF*PS
      D(1,3)=0.D0
      D(1,4)=D(1,2)
      D(2,1)=D(1,2)
      D(2,2)=D(1,1)
      D(2,3)=0.D0
      D(2,4)=D(1,2)
      D(3,1)=0.D0
      D(3,2)=0.D0
      D(3,3)=0.5*COEF*(1.D0-2.D0*PS)
      D(3,4)=0.D0
      D(4,1)=D(1,2)
      D(4,2)=D(1,2)
      D(4,3)=0.D0
      D(4,4)=D(1,1)
      END
      
             
      SUBROUTINE BLINISO2D(X,Y,KSI,ETA,B,DETJ,R,NDDLE,NBN,ND,ITYP,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM % CALCUL DE LA MATRICE B AU POINT DE COORDONNEES LOCALES KSI,ETA  % 
COMM %                                                                 % 
COMM %   --  ELEMENTS ISOPARAMETRIQUES  2D :  T3  Q4  T6  Q8 --        %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION  X(NBN),Y(NBN),B(ND,NDDLE)
      DOUBLE PRECISION KSI,ETA,J(4),INVJ(4),
     *N(NBN),NKSI(NBN),NETA(NBN),NX(NBN),NY(NBN)     
COMM
      IF(ITYP.EQ.30) THEN
        CALL FORMT3(KSI,ETA,N,NKSI,NETA)
      ELSE IF(ITYP.EQ.40) THEN
        CALL FORMQ4(KSI,ETA,N,NKSI,NETA)
      ELSE IF(ITYP.EQ.60) THEN
        CALL FORMT6(KSI,ETA,N,NKSI,NETA)        
      ELSE IF(ITYP.EQ.80) THEN
        CALL FORMQ8(KSI,ETA,N,NKSI,NETA)
      ELSE IF(ITYP.EQ.81) THEN
        CALL FORMQ8(KSI,ETA,N,NKSI,NETA)        
      ELSE
        STOP ' BLINISO2D : pb sur ITYP '
      ENDIF         
COMM .......///// Matrice jacobienne et son determinant///////.......... 
      DO 5 I=1,4
 5    J(I)=0.D0     
      DO 1 I=1,NBN
      J(1)=J(1)+NKSI(I)*X(I)
      J(2)=J(2)+NKSI(I)*Y(I)
      J(3)=J(3)+NETA(I)*X(I)
      J(4)=J(4)+NETA(I)*Y(I)
  1   CONTINUE 
      DETJ=J(1)*J(4)-J(2)*J(3)
      IF(DABS(DETJ).LT.1.D-10) STOP ' Mat. Jacob. non inversible '   
COMM ............///// Matrice jacobienne inverse///////................
      INVJ(1)= J(4)/DETJ
      INVJ(2)=-J(2)/DETJ
      INVJ(3)=-J(3)/DETJ
      INVJ(4)= J(1)/DETJ       
COMM .....//// Derivee des fonctions d' interpolation (X,Y) //////......
      DO 2 I=1,NBN     
      NX(I) =INVJ(1)*NKSI(I) + INVJ(2)*NETA(I)
      NY(I) =INVJ(3)*NKSI(I) + INVJ(4)*NETA(I)
 2    CONTINUE
COMM ..................///// Matrice B  ///////.........................
      DO 3 I=1,NBN
      B(1,1+2*(I-1))=NX(I)
      B(2,1+2*(I-1))=0.D0
      B(3,1+2*(I-1))=NY(I) 
      B(1,2+2*(I-1))=0.D0
      B(2,2+2*(I-1))=NY(I)
      B(3,2+2*(I-1))=NX(I)
 3    CONTINUE
COMM          
COMM ......./////  Complement pour le cas axisymetrique ///////........
COMM
      IF(KTYP.EQ.3) THEN  
COMM ..................//// Rayon ////////.............................
        R=0.D0
        DO 6 I=1,NBN
 6      R=R+N(I)*X(I)     
COMM ..................///// Matrice B  ///////........................      
        DO 7 I=1,NBN
        B(4,1+2*(I-1))=N(I)/R
        B(4,2+2*(I-1))=0.D0
 7      CONTINUE
COMM ..................////////////////////////........................
      ENDIF
      END
      

      SUBROUTINE FORMT3(KSI,ETA,N,NKSI,NETA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  FONCTIONS DE FORME ET LEURS DERIVEES  AU POINT  KSI,ETA        % 
COMM %           -- TRIANGLE A TROIS NOEUDS : T3 --                    %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION KSI,ETA,N(3),NKSI(3),NETA(3)
COMM ...........//// Fonctions d' interpolation ////////...............
      N(1)= 1.D0-KSI-ETA
      N(2)= KSI      
      N(3)= ETA     
COMM ....//// Derivee des fonctions d' interpolation (KSI,ETA) //////...
      NKSI(1)=-1.D0
      NKSI(2)= 1.D0
      NKSI(3)= 0.D0
      NETA(1)=-1.D0
      NETA(2)= 0.D0
      NETA(3)= 1.D0
      END

      SUBROUTINE FORMQ4(KSI,ETA,N,NKSI,NETA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  FONCTIONS DE FORME ET LEURS DERIVEES  AU POINT  KSI,ETA        % 
COMM %           -- QUADRANGLE A QUATRE NOEUDS : Q4 --                 %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION KSI,ETA,N(4),NKSI(4),NETA(4)
COMM....................../////////////////////////....................
      X1MK=1.D0-KSI
      X1PK=1.D0+KSI
      X1ME=1.D0-ETA
      X1PE=1.D0+ETA
COMM ...........//// Fonctions d' interpolation ////////...............
      N(1)= 0.25D0 * X1MK * X1ME
      N(2)= 0.25D0 * X1PK * X1ME      
      N(3)= 0.25D0 * X1PK * X1PE      
      N(4)= 0.25D0 * X1MK * X1PE    
COMM ....//// Derivee des fonctions d' interpolation (KSI,ETA) //////...
      NKSI(1)=-0.25D0 * X1ME
      NKSI(2)= 0.25D0 * X1ME
      NKSI(3)= 0.25D0 * X1PE
      NKSI(4)=-0.25D0 * X1PE
      NETA(1)=-0.25D0 * X1MK
      NETA(2)=-0.25D0 * X1PK
      NETA(3)= 0.25D0 * X1PK
      NETA(4)= 0.25D0 * X1MK           
      END      
      
      SUBROUTINE FORMT6(KSI,ETA,N,NKSI,NETA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  FONCTIONS DE FORME ET LEURS DERIVEES  AU POINT  KSI,ETA        % 
COMM %           -- TRIANGLE A SIX NOEUDS : T6 --                      %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION KSI,ETA,N(6),NKSI(6),NETA(6)
COMM....................../////////////////////////....................      
      X1MKME=1.D0-KSI-ETA
COMM .....//// Derivee des fonctions d' interpolation (X,Y) //////......             
      N(1)= X1MKME*(2.D0*X1MKME-1.D0)
      N(2)= KSI   *(2.D0*KSI   -1.D0)  	
      N(3)= ETA   *(2.D0*ETA   -1.D0)
      N(4)= 4.D0 *X1MKME *KSI        
      N(5)= 4.D0 *KSI    *ETA
      N(6)= 4.D0 *ETA    *X1MKME
COMM .....//// Derivee des fonctions d' interpolation (KSI,ETA) //////..                        
      NKSI(1)= 1.D0-4.D0*X1MKME
      NKSI(2)= 4.D0*KSI-1.D0
      NKSI(3)= 0.D0
      NKSI(4)= 4.D0*(X1MKME-KSI)
      NKSI(5)= 4.D0*ETA
      NKSI(6)=-4.D0*ETA
      NETA(1)= NKSI(1)
      NETA(2)= 0.D0
      NETA(3)= 4.D0*ETA-1.D0
      NETA(4)=-4.D0*KSI
      NETA(5)= 4.D0*KSI
      NETA(6)= 4.D0*(X1MKME-ETA)         
      END
      
      SUBROUTINE FORMQ8(KSI,ETA,N,NKSI,NETA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  FONCTIONS DE FORME ET LEURS DERIVEES  AU POINT  KSI,ETA        % 
COMM %           -- QUADRANGLE A HUIT NOEUDS : Q8 --                   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION KSI,ETA,N(8),NKSI(8),NETA(8)
COMM....................../////////////////////////....................      
      X1PK=1.D0+KSI
      X1PE=1.D0+ETA
      X1MK=1.D0-KSI
      X1ME=1.D0-ETA
      X1MKC=1.D0-KSI*KSI
      X1MEC=1.D0-ETA*ETA
      X2KPE=2.D0*KSI+ETA
      X2KME=2.D0*KSI-ETA
      XKP2E=KSI+2.D0*ETA
      XKM2E=KSI-2.D0*ETA
COMM ...........//// Fonctions d' interpolation ////////................
      N(1)=-0.25D0*X1MK*X1ME*(1.D0+KSI+ETA)
      N(2)=-0.25D0*X1PK*X1ME*(1.D0-KSI+ETA)
      N(3)=-0.25D0*X1PK*X1PE*(1.D0-KSI-ETA)
      N(4)=-0.25D0*X1MK*X1PE*(1.D0+KSI-ETA)
      N(5)= 0.5D0*X1MKC*X1ME
      N(6)= 0.5D0*X1PK*X1MEC
      N(7)= 0.5D0*X1MKC*X1PE
      N(8)= 0.5D0*X1MK*X1MEC      
COMM .....//// Derivee des fonctions d' interpolation (KSI,ETA) //////....                        
      NKSI(1)=0.25D0 *X1ME *X2KPE
      NKSI(2)=0.25D0 *X1ME *X2KME
      NKSI(3)=0.25D0 *X1PE *X2KPE
      NKSI(4)=0.25D0 *X1PE *X2KME      
      NKSI(5)=-X1ME*KSI
      NKSI(6)= 0.5D0*X1MEC      
      NKSI(7)=-X1PE*KSI
      NKSI(8)=-0.5D0*X1MEC                                     
      NETA(1)= 0.25D0 *X1MK *XKP2E
      NETA(2)=-0.25D0 *X1PK *XKM2E
      NETA(3)= 0.25D0 *X1PK *XKP2E
      NETA(4)=-0.25D0 *X1MK *XKM2E
      NETA(5)=-0.5D0*X1MKC
      NETA(6)=-X1PK*ETA      
      NETA(7)= 0.5D0*X1MKC 
      NETA(8)=-X1MK*ETA       
      END
           
COMM ...........................////////////...........................
COMM ...//////  FIN DU MODULE REGROUPANT LES SUBROUTINES DE  //////....
COMM ...//////  CALCULS  DE RIGIDITE ELASTIQUE EN 2D         //////....
COMM ...........................////////////...........................

