      SUBROUTINE RAYON_PADE(V0,V,U,ALPHA,CRIT,NDL,NORDRE,RAYONS,RAYONP,
     &           DA,DIVERGE,POLE,POLE_C,NB_RAC,NB_RAC_POS,D,NB_RAC_C)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ON DETERMINE LE RAYON DE CONVERGENCE DE LA SERIE              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION V0(1),V(NDL,*),U(1),ALPHA(NORDRE,1),D(NORDRE-1)
      INTEGER DIVERGE
     
      IF (DA.LT.0.D0) THEN
      CALL DETRAY_PADE_POLE_G(V0,V,U,ALPHA,CRIT,RAYONS,RAYONP,
     &     NDL,NORDRE,DIVERGE,POLE,POLE_C,NB_RAC,NB_RAC_NEG,
     &     D,NB_RAC_C)
      ELSE

      CALL DETRAY_PADE_POLE(V0,V,U,ALPHA,CRIT,RAYONS,RAYONP,
     &     NDL,NORDRE,DIVERGE,POLE,POLE_C,NB_RAC,NB_RAC_NEG,
     &     D,NB_RAC_C)
      ENDIF
      
      END

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	SUBROUTINE ORTHOGONALISER(ALPHA,V,NDL,NORDRE,D)
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
	DIMENSION D(NORDRE-1),V(NDL,1),ALPHA(NORDRE,NORDRE),
     * V1(NDL,NORDRE)
        DO I=1,NORDRE
	DO J=1,NDL
        WRITE(16,*) I,J,V(J,I)
	ENDDO
	ENDDO
	
	DO I=1,NDL
     	DO J=1,NORDRE
     	V1(I,J)=V(I,J)
     	ENDDO
     	ENDDO
    	CALL KORTHO_EUCL(V1,ALPHA,NDL,NORDRE)
	CALL CALCUL_D(ALPHA,D,NORDRE)


	END

COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ???????????????????????              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       SUBROUTINE NUMPAD_MODIF(D,NDEG,A,SERNUM)
       IMPLICIT DOUBLE PRECISION(A-H,O-Z)
       DIMENSION D(NDEG)
       SERNUM=1.D0
       IF (NDEG.NE.0) THEN
       DO 1 I=1,NDEG
       SERNUM=SERNUM+A**I*D(I)
 1     CONTINUE
       ENDIF 
       END
      
      SUBROUTINE SOL_PAD_MODIF(D,V0,V,U,ALPHA,NDL,NORDRE,A)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION D(NORDRE-1),V(NDL,1),V0(NDL),U(NDL),
     &  PADE(NORDRE-1),ALPHA(NORDRE,NORDRE) 
      CALL DENPADE(ALPHA,D,NORDRE,NORDRE-1)
      CALL NUMPAD_MODIF(D,NORDRE-1,A,SERDEN)
      DO 2 K=1,NORDRE-1
      CALL NUMPAD_MODIF(D,NORDRE-1-K,A,SERNUM)
      WRITE(13,*)'SERNUM,SERDEN=',SERNUM,SERDEN
 2    PADE(K)=SERNUM/SERDEN
 
      DO 3 I=1,NDL
 3    U(I)=V0(I)
      DO 4 I=1,NDL
      DO 4 J=1,NORDRE-1
      PA=1.D0
      DO 5 JJ=1,J
5     PA=PA*A
4     U(I)=U(I)+PA*V(I,J)*PADE(J)
      END
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %              VERSION MARS 2003                                  %
COMM %              SOLUTION PADE A DENOMINATEUR COMMUN                %
COMM %                                                                 % 
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM
      SUBROUTINE SOL_PAD(D,V0,V,U,NDL,NORDRE,A)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DOUBLE PRECISION RLAMBDA
      DIMENSION D(NORDRE-1),V(NDL,1),V0(NDL),U(NDL),PADE(NORDRE-1)

      CALL DEN_PAD_CONT(D,NORDRE-1,A,SERDEN)
C      CALL DENPADE(ALPHA,D,NORDRE,NPAD)
      DO 2 K=1,NORDRE-1
      CALL NUM_PAD_CONT(D,NORDRE-1-K,A,SERNUM)

      WRITE(13,*)'SERNUM,SERDEN=',SERNUM,SERDEN

 2    PADE(K)=SERNUM/SERDEN
      DO 3 I=1,NDL
 3    U(I)=V0(I)
      DO 4 I=1,NDL
      DO 4 J=1,NORDRE-1
      PA=1.D0
      DO 5 JJ=1,J
5     PA=PA*A
4     U(I)=U(I)+PA*V(I,J)*PADE(J)
      END

        SUBROUTINE VECT_TANGENT_PADE(D,V,U,NDIM,NORDRE,A)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   calcul d'un vecteur tangent                                   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
	DIMENSION D(NORDRE-1),V(NDIM,1),U(NDIM),
     &            PADE1(NORDRE-1),PADE2(NORDRE-1)
     
        A2=0.95*A
	CALL DEN_PAD_CONT(D,NORDRE-1,A,SERDEN1)
	CALL DEN_PAD_CONT(D,NORDRE-1,A2,SERDEN2)
	DO K=1,NORDRE-1
	CALL NUM_PAD_CONT(D,NORDRE-1-K,A,SERNUM1)
	CALL NUM_PAD_CONT(D,NORDRE-1-K,A2,SERNUM2)
	PADE1(K)=SERNUM1/SERDEN1
	PADE2(K)=SERNUM2/SERDEN2
	ENDDO
	
	DO I=1,NDIM
	U(I)=0.0
	ENDDO

	DO I=1,NDIM
	DO J=1,NORDRE-1
	PA1=1.D0
	PA2=1.D0
	DO JJ=1,J
	PA1=PA1*A
	PA2=PA2*A2
	ENDDO
	U(I)=U(I)+(PA1*PADE1(J)-PA2*PADE2(J))*V(I,J)
	ENDDO
	ENDDO
	END
C--------------------------------------------------------------



	SUBROUTINE SOL_PAD_CONT(D,C0,C,V0,V,U,NORDRE,
     *  NDL,A,RLAMBDA)
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
	DIMENSION D(NORDRE-1),V(NDL,1),V0(NDL),U(NDL),C(NORDRE),
     *   PADE(NORDRE-1)
     
cc     	CALL CALCUL_D(ALPHA,D,NORDRE)
	CALL DEN_PAD_CONT(D,NORDRE-1,A,SERDEN)
	DO K=1,NORDRE-1
	CALL NUM_PAD_CONT(D,NORDRE-1-K,A,SERNUM)
	PADE(K)=SERNUM/SERDEN
	ENDDO
	
	DO I=1,NDL
	U(I)=V0(I)
	ENDDO
	
	RLAMBDA=C0
	
	DO I=1,NDL
	DO J=1,NORDRE-1
	PA=1.D0
	DO JJ=1,J
	PA=PA*A
	ENDDO
	U(I)=U(I)+PA*V(I,J)*PADE(J)
	
	ENDDO
	ENDDO
		
	DO J=1,NORDRE-1
	PA=1.D0
	DO JJ=1,J
	PA=PA*A
	ENDDO
	RLAMBDA=RLAMBDA+PA*C(J)*PADE(J)
	ENDDO
	END


COMM
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM
      SUBROUTINE NUM_PAD_CONT(D,NDEG,A,SERNUM)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION D(NDEG)
      SERNUM=1.D0
      IF (NDEG.NE.0) THEN
      DO 1 I=1,NDEG
      PA=1.D0
      DO 2 J=1,I
      PA=PA*A
 2    CONTINUE
      SERNUM=SERNUM+PA*D(I)
 1    CONTINUE
      ENDIF 
      END
COMM
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM
      SUBROUTINE DEN_PAD_CONT(D,NDEG,A,SERDEN)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION D(NDEG)
      SERDEN=1.D0
      DO 1 I=1,NDEG
      PA=1.D0
      DO 2 J=1,I
      PA=PA*A
 2    CONTINUE
      SERDEN=SERDEN+PA*D(I)
 1    CONTINUE
      END
COMM

COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM
      SUBROUTINE CALCUL_D(ALPHA,D,NORDRE)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION D(NORDRE-1),ALPHA(NORDRE,NORDRE)
      D(1)=-ALPHA(NORDRE,NORDRE-1)/ALPHA(NORDRE-1,NORDRE-1)
      DO 1 K=2,NORDRE-1
      SOMME=ALPHA(NORDRE,NORDRE-K)
      DO 2 KK=1,K-1
      SOMME=SOMME+ALPHA(NORDRE-KK,NORDRE-K)*D(KK)
 2    CONTINUE
      D(K)=-SOMME/ALPHA(NORDRE-K,NORDRE-K)
 1    CONTINUE
      END

      SUBROUTINE KORTHO_EUCL(V,ALPHA,NDL,NORDRE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %            ORTHOGONALISATION DE GRAM-SCHMIDT                    %
COMM %                                                                 %
COMM %         V      :  NORDRE vecteurs de dimension NDL              %
COMM %         VTRA   :  Vecteur de travail                            %
COMM %         ALPHA  :  Coefficients d orthogonalisation              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)   
      DIMENSION V(NDL,1),ALPHA(NORDRE,NORDRE)
COMM
COMM %%%%%%%%%%%%%%%%//////INITIALISATION////%%%%%%%%%%%%%%%%%%%%%%%%%%%   
COMM
      DO 4 I=1,NORDRE
      DO 4 J=1,NORDRE
 4    ALPHA(I,J)=0.D0
COMM
      ALPHA(1,1)=DSQRT(PROSCA(V(1,1),V(1,1),NDL))
      DO 6 K=1,NDL
 6    V(K,1)=V(K,1)/ALPHA(1,1)
COMM
COMM %%%%%//////pour chacun des vecteurs a partir du deuxieme////%%%%%%%
COMM
      DO 1 I=2,NORDRE 
      DO 2 J=1,I-1   
      ALPHA(I,J)=PROSCA(V(1,I),V(1,J),NDL)  
      DO 3 K=1,NDL
 3    V(K,I)=V(K,I)-ALPHA(I,J)*V(K,J)
 2    CONTINUE
      ALPHA(I,I)=DSQRT(PROSCA(V(1,I),V(1,I),NDL))
      DO 7 K=1,NDL
 7    V(K,I)=V(K,I)/ALPHA(I,I)      
 1    CONTINUE
COMM
COMM %%%%%%%%%%%%%%%%%//////impression des ALPHA ////%%%%%%%%%%%%%%%%%%%
COMM
      WRITE(6,50)
      DO 15 I=1,NORDRE
      DO 15 J=1,I
      WRITE(6,60) I,J,ALPHA(I,J)
 15   CONTINUE
 50   FORMAT( ///,' ORTHOGONALISATION DE GRAM-SCHMIT ',/,
     &            ' -------------------------------- ',//)
 60   FORMAT( ' Alpha(',I2,',',I2,')=',G15.8)     
      END

COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE DETRAY_PADE_POLE(V0,V,U,ALPHA,CRIT,RAYONS,RAYONP,
     &     NDL,NORDRE,diverge,pole,POLE_C,NB_RAC,NB_RAC_NEG,
     &     D,NB_RAC_C)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ON DETERMINE LE RAYON DE CONVERGENCE DE LA SERIE              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION V(NDL,*),V0(1),UBID(NDL)
      DIMENSION U(1),ALPHA(nordre,1),
     & ALPHA1(NORDRE-1,NORDRE-1),D(NORDRE-1),D1(NORDRE-1),
     & DD(0:nordre-1)
      INTEGER DIVERGE,fin_dicho

      write(11,*)'entree dans DETRAY_PADE_POLE'

      AM=RAYONS
C-----calcul de am sur la base du   (Un - Un-1)/U1 < eps
      CALL ORTHOGONALISER(ALPHA1,V,NDL,NORDRE-1,D1)
      CALL ORTHOGONALISER(ALPHA,V,NDL,NORDRE,D)

c-------calcul des racines de D
	DD(0)=1.D0
	DO JJ=1,NORDRE-1
	DD(JJ)=D(JJ)
	ENDDO
	EPSILON2=1.D-15

      rac_min=am+am    !a resoudre
      if (rac_min.le.am) then
      a=am
      else
      fin_dicho=0
      borninf=am
      bornsup=3.D0*am

      do while(fin_dicho.eq.0)
      a=(borninf+bornsup)/2.d0

      CALL SOL_PAD(D,V0,V,U,NDL,NORDRE,A)
      CALL SOL_PAD(D1,V0,V,UBID,NDL,NORDRE-1,A)

c------critere mpf

       CALL DDIFFE(UBID,U,NDL)
       CALL DINCRE(UBID,UBID,NDL,-1.D0)

       XNOR1=DSQRT(PROSCA(UBID,UBID,NDL))

       XNORP=DSQRT(PROSCA(U,U,NDL))

c------fin critere mpf
	RAP=XNOR1/XNORP
      if (RAP.lt.CRIT) then
      borninf=a
      endif
      if (rap.gt.crit) then
      bornsup=a
      endif
      diff1=dabs(a-am)
      diff2=dabs(a-rac_min)
      diff3=dabs(bornsup-borninf)
    
c      if ((rap.le.crit.and.rap.ge.(crit/10.d0)).or.
c     *    (diff1.lt.10.d-6).or.(diff2.lt.10.d-6) ) then
      
      
      if (((rap.le.crit).and.(diff3.le.(10.d-6))).or.
     *(diff1.lt.10.d-6).or.(diff2.lt.10.d-6)) then  
        
      fin_dicho=1
      endif
      enddo
      endif
      rayonP=a
      END
      
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SUBROUTINE DETRAY_PADE_POLE_G(V0,V,U,ALPHA,CRIT,RAYONS,RAYONP,
     &     NDL,NORDRE,DIVERGE,pole,POLE_C,NB_RAC,NB_RAC_NEG,
     &     D,NB_RAC_C)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ON DETERMINE LE RAYON DE CONVERGENCE DE LA SERIE              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION V0(1),V(NDL,*),U(1),ALPHA(NORDRE,1),UBID(NDL),
     &          ALPHA1(NORDRE-1,NORDRE-1),D(NORDRE-1),D1(NORDRE-1),
     &          DD(0:nordre-1)
      INTEGER DIVERGE,fin_dicho
C
C-----calcul de am sur la base du   (Un - Un-1)/U1 < eps
C
      AM=-RAYONS
      CALL ORTHOGONALISER(ALPHA1,V,NDL,NORDRE-1,D1)
      CALL ORTHOGONALISER(ALPHA,V,NDL,NORDRE,D)
C-------calcul des racines de D     
	DD(0)=1.D0
	DO JJ=1,NORDRE-1
	DD(JJ)=D(JJ)
	ENDDO
	EPSILON2=1.D-17
	CALL RACINES_G(DD,NORDRE-1,EPSILON2,RAC_MAX,RAC_MIN_C,
     *  DIVERGE,AM,NB_RAC,NB_RAC_NEG,NB_RAC_C)
c	WRITE (135,*) 'RAC_MIN',RAC_MAX,NB_RAC
c-------fin calcul racines D
      pole=rac_max
      pole_c=RAC_MIN_C
      IF (rac_max.GE.am) THEN
      a=am
      ELSE
      fin_dicho=0
      bornsup=am
      borninf=rac_max
      do while(fin_dicho.eq.0)
      a=(borninf+bornsup)/2.d0
      CALL SOL_PAD(D,V0,V,U,NDL,NORDRE,A)
      CALL SOL_PAD(D1,V0,V,UBID,NDL,NORDRE-1,A)
c------critere mpf
       CALL DDIFFE(UBID,U,NDL)
       CALL DINCRE(UBID,UBID,NDL,-1.D0)
       XNOR1=DSQRT(PROSCA(UBID,UBID,NDL))
       XNORP=DSQRT(PROSCA(U,U,NDL))
c------fin critere mpf
	RAP=XNOR1/XNORP
c------critere aeh
c
c------fin critere aeh
c      write(*,*) 'am,a,pole,rap',am,a,rac_max,rap
      IF (RAP.LT.CRIT) THEN
      bornsup=a
      ENDIF
      IF (RAP.GT.CRIT) THEN
      borninf=a
      ENDIF
      diff1=dabs(a-am)
      diff2=dabs(a-rac_mAX)
      diff3=dabs(bornsup-borninf)

      IF (((RAP.LE.CRIT).AND.(diff3.LE.(10.d-6))).OR.
     *(diff1.LT.10.d-6).OR.(diff2.LT.10.d-6)) THEN
      fin_dicho=1
      endif
      enddo
      endif
c----- a est negatif ici --> on change le signe pour as2.f
      rayonP=-a
c------
      END



