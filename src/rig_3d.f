COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %  //////  MODULE REGROUPANT LES SUBROUTINES DE CALCULS  //////   %
COMM %  //////   DE RIGIDITE ELASTIQUE   -- LINEAIRE 3D --    //////   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE RELA3D(RIG,IDIA,COOR,MAIL,EMAI,
     *                  NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   CALCUL DE LA MATRICE DE RIGIDITE ELASTIQUE GLOBALE            %
COMM %         DE LA STRUCTURE TRIDMENSIONNELLE                        %
COMM %                                                                 %
COMM %   RIG   MATRICE DE RIGIDITE GLOBALE                             %
COMM %   IDIA  TABLEAU POINTEUR DE LA DIAGONALE                        %
COMM %   MAIL  TABLEAU DECRIVANT LA NUMEROTATION DES ELEMENTS          %
COMM %   COOR  COORDONNEES DES NOEUDS                                  %
COMM %   EMAI  CARACTERISTIQUES MECANIQUES DES ELEMENTS                %
COMM %   NMAI  NOMBRE D ELEMENTS                                       %
COMM %   NDL   NOMBRE DE DDL TOTAL                                     %
COMM %   NCPN  NOMBRE DE COORDONNEES PAR NOEUD                         %
COMM %   NDPN  NOMBRE DE DDL PAR NOEUD                                 %
COMM %   NNPE  NOMBRE DE NOEUDS MARIMUN PAR ELEMENT                    %
COMM %                                                                 %
COMM %   NBN   NOMBRE DE NOEUD DE L'ELEMENT                            %
COMM %   NDDLE NOMBRE DE DDL DE L'ELEMENT                              %
COMM %   NBG   NOMBRE DE POINT DE GAUSS DE L'ELEMENT                   %
COMM %   GAUSS COORDONNEES POINT DE GAUSS DE L'ELEMENT                 %
COMM %   POIDS POID AU POINT DE GAUSS DE L'ELEMENT                     %
COMM %   RGL   MATRICE DE RIGIDITE ELEMENTAIRE                         %
COMM %   NO    NUMEROS GLOBAUX DES NOEUDS                              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RIG(1),IDIA(1),COOR(NCPN,1),MAIL(NNPE+2,1),EMAI(NVMA,1)      
      DIMENSION RGL(1830),NO(20),IGLO(60),X(20),Y(20),Z(20),D(6,6)  
COMM ...........................////////////...........................
      CALL DZERO(RIG,IDIA(NDL)) 
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////.............        
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)     
      CALL CAEL3D(ITYP,NBN,NDDLE,NBG)              
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))
      Z(I)=COOR(3,NO(I))            
 3    CONTINUE
COMM  ......//////// ON FORME LA MATRICE DE COMPORTEMENT D //////........
      ND=6
      CALL DISO3D(EMAI(1,IMAT),EMAI(2,IMAT),D)
COMM.....................///// CALCUL DE RGL /////.....................
      IF(ITYP.EQ.81.OR.ITYP.EQ.200.OR.ITYP.EQ.201) THEN
         CALL RIGELE_ISOPARA3D(X,Y,Z,D,RGL,ITYP,NBN,NDDLE,NBG,ND)
      ELSE
         STOP ' RELA3D : pb sur ITYP  '
      ENDIF
COMM .........////// ASSEMBLAGE DE LA MAT. RIGID. ELEM. ////...........       
      CALL ASSMAT(RIG,IDIA,RGL,NO,IGLO,NBN,NDPN)
 20   CONTINUE
      END
      
      SUBROUTINE RIGELE_ISOPARA3D(X,Y,Z,D,RGL,
     *                            ITYP,NBN,NDDLE,NBG,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE RIGIDITE ELEMENTAIRE : ELEMENTS ISOPARAMETRIQUES 3D %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION X(20),Y(20),Z(20),D(6,6),RGL(1830),ARGL(1830),
     *          B(6,60),DB(6,60),
     *          GAUSS(3,27),POIDS(27)      

      ITS=(NDDLE*NDDLE+NDDLE)/2
      CALL DZERO(RGL,ITS) 
      CALL GRIG3D(ITYP,GAUSS,POIDS)      
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////.............
      DO 7 NG=1,NBG     
        R=GAUSS(1,NG)
        S=GAUSS(2,NG)
        T=GAUSS(3,NG)
COMM ......//// CALCUL DE LA MATRICE B ET DE DETJ ////..................
        CALL BLINISO3D(X,Y,Z,R,S,T,B,DETJ,NDDLE,NBN,ITYP)
COMM...............////// CALCUL DE Bt.D.B  ////////////................. 
        CALL TBDB(B,D,DB,ARGL,ND,NDDLE,ITS)
        COEF=DETJ*POIDS(NG)
        CALL DACTUA(RGL,ARGL,ITS,COEF)
 7    CONTINUE
      END
     
      SUBROUTINE CAEL3D(ITYP,NBN,NDDLE,NBG)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  EN FONCTION DU TYPE DE L'ELEMENT ON RENVOI LE NOMBRE DE NOEUDS %
COMM %  LE NOMBRE DE DDL, LE NOMBRE DE POINTS DE GAUSS                 %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IF      (ITYP.EQ.81) THEN
        NBN=8
        NDDLE=24
        NBG=8
      ELSE IF (ITYP.EQ.200) THEN
        NBN=20
        NDDLE=60
        NBG=27
      ELSE IF (ITYP.EQ.201) THEN
        NBN=20
        NDDLE=60
        NBG=8         
      ELSE
       STOP ' PB : CHAREL_3D '
      ENDIF
      END
      
      SUBROUTINE GRIG3D(ITYP,GAUSS,POIDS)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  EN FONCTION DU TYPE DE L'ELEMENT, ON RENVOIE LES COORDONNEES   %
COMM %  DES POINTS DE GAUSS, ET LES POIDS DE GAUSS                     %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION GAUSS(3,27),POIDS(27),RH8(2),RH20(3),WH20(3)
COMM    
      DATA RH8/-0.577350269189626,0.577350269189626/  
      DATA RH20/-0.774596669241483,0.000000000000000,0.774596669241483/
      DATA WH20/ 0.555555555555556,0.888888888888889,0.555555555555556/      
COMM            
      IF (ITYP.EQ.81.OR.ITYP.EQ.201) THEN
        DO 1 I=1,2
        DO 1 J=1,2
        DO 1 K=1,2
        N=I+(J-1)*2+(K-1)*4
        GAUSS(1,N)=RH8(I)
        GAUSS(2,N)=RH8(J)
        GAUSS(3,N)=RH8(K)
        POIDS(N)=1.D0
 1      CONTINUE                      
      ELSE IF (ITYP.EQ.200) THEN
        DO 4 I=1,3
        DO 4 J=1,3
        DO 4 K=1,3
        N=I+(J-1)*3+(K-1)*9
        GAUSS(1,N)=RH20(I)
        GAUSS(2,N)=RH20(J)
        GAUSS(3,N)=RH20(K)
        POIDS(N)=WH20(I)*WH20(J)*WH20(K)
 4      CONTINUE 
      ELSE
       STOP ' PB : GAURIG_3D '
      ENDIF
      END 
                  
      SUBROUTINE DISO3D(YG,PS,D)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE COMPORTEMENT ISOTROPE D   -- 3D --                  %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION D(6,6)
      COEF= YG / ( (1.D0-2.D0*PS)*(1.D0+PS) )
      DO 1 I=1,6
      DO 1 J=1,6
 1    D(I,J)=0.D0
      COEF1=COEF*(1.D0-PS)       
      D(1,1)=COEF1
      D(2,2)=COEF1
      D(3,3)=COEF1
      COEF2=COEF*PS  
      D(1,2)=COEF2
      D(1,3)=COEF2
      D(2,1)=COEF2
      D(2,3)=COEF2
      D(3,1)=COEF2
      D(3,2)=COEF2
      COEF3=COEF*0.5*(1.D0-2.D0*PS)      
      D(4,4)=COEF3
      D(5,5)=COEF3
      D(6,6)=COEF3
      END


      SUBROUTINE BLINISO3D(X,Y,Z,R,S,T,B,DETJ,NDDLE,NBN,ITYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM % CALCUL DE LA MATRICE B AU POINT DE COORDONNEES LOCALES KSI,ETA  % 
COMM %                                                                 % 
COMM %   --  ELEMENTS ISOPARAMETRIQUES  3D :  H8  H20 H20R   --        %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION  X(NBN),Y(NBN),Z(NBN),B(6,NDDLE)
      DOUBLE PRECISION J(9),INVJ(9),N(NBN),NR(NBN),NS(NBN),NT(NBN),
     *NX(NBN),NY(NBN),NZ(NBN)     
COMM
      IF(ITYP.EQ.80) THEN
        CALL FORMH8(R,S,T,N,NR,NS,NT)
      ELSE IF(ITYP.EQ.200) THEN
        CALL FORMH20(R,S,T,N,NR,NS,NT)
      ELSE IF(ITYP.EQ.201) THEN
        CALL FORMH20(R,S,T,N,NR,NS,NT)         
      ELSE
        STOP ' NMATISO3D : pb sur ITYP '
      ENDIF         
COMM .......///// Matrice jacobienne et son determinant///////.......... 
      DO 5 I=1,9
 5    J(I)=0.D0     
      DO 1 I=1,NBN
      J(1)=J(1)+NR(I)*X(I)
      J(2)=J(2)+NR(I)*Y(I)
      J(3)=J(3)+NR(I)*Z(I)
      J(4)=J(4)+NS(I)*X(I)
      J(5)=J(5)+NS(I)*Y(I)
      J(6)=J(6)+NS(I)*Z(I)
      J(7)=J(7)+NT(I)*X(I)
      J(8)=J(8)+NT(I)*Y(I)
      J(9)=J(9)+NT(I)*Z(I)
  1   CONTINUE 
      DETJ=  J(1)*(J(5)*J(9)-J(6)*J(8))
     *      -J(2)*(J(4)*J(9)-J(6)*J(7))
     *      +J(3)*(J(4)*J(8)-J(5)*J(7))
      IF(DABS(DETJ).LT.1.D-10) STOP ' Mat. Jacob. non inversible '
COMM ............///// Matrice jacobienne inverse///////................
      INVJ(1)= (J(5)*J(9)-J(6)*J(8))/DETJ
      INVJ(2)=-(J(2)*J(9)-J(3)*J(8))/DETJ
      INVJ(3)= (J(2)*J(6)-J(3)*J(5))/DETJ
      INVJ(4)=-(J(4)*J(9)-J(6)*J(7))/DETJ
      INVJ(5)= (J(1)*J(9)-J(3)*J(7))/DETJ
      INVJ(6)=-(J(1)*J(6)-J(3)*J(4))/DETJ
      INVJ(7)= (J(4)*J(8)-J(5)*J(7))/DETJ
      INVJ(8)=-(J(1)*J(8)-J(2)*J(7))/DETJ
      INVJ(9)= (J(1)*J(5)-J(2)*J(4))/DETJ
COMM .....//// Derivee des fonctions d' interpolation (X,Y,Z) //////......
      DO 2 I=1,NBN     
      NX(I) =INVJ(1)*NR(I) + INVJ(2)*NS(I) + INVJ(3)*NT(I)
      NY(I) =INVJ(4)*NR(I) + INVJ(5)*NS(I) + INVJ(6)*NT(I)
      NZ(I) =INVJ(7)*NR(I) + INVJ(8)*NS(I) + INVJ(9)*NT(I)
 2    CONTINUE
COMM ..................///// Matrice B  ///////.........................
      DO 3 I=1,NBN
      B(1,1+3*(I-1))=NX(I)
      B(2,1+3*(I-1))=0.D0
      B(3,1+3*(I-1))=0.D0
      B(4,1+3*(I-1))=NY(I)
      B(5,1+3*(I-1))=0.D0
      B(6,1+3*(I-1))=NZ(I)
C      
      B(1,2+3*(I-1))=0.D0
      B(2,2+3*(I-1))=NY(I)
      B(3,2+3*(I-1))=0.D0
      B(4,2+3*(I-1))=NX(I)
      B(5,2+3*(I-1))=NZ(I)
      B(6,2+3*(I-1))=0.D0
C        
      B(1,3+3*(I-1))=0.D0
      B(2,3+3*(I-1))=0.D0
      B(3,3+3*(I-1))=NZ(I)
      B(4,3+3*(I-1))=0.D0
      B(5,3+3*(I-1))=NY(I)
      B(6,3+3*(I-1))=NX(I)         
 3    CONTINUE  
      END


      SUBROUTINE FORMH8(R,S,T,N,NR,NS,NT)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  FONCTIONS DE FORME ET LEURS DERIVEES  AU POINT  R,S,T          % 
COMM %           -- HEXAEDRE A HUIT NOEUDS : H8 --                     %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION N(8),NR(8),NS(8),NT(8)
COMM
      CO=0.125
      A1=1.0+R
      B1=1.0+S
      C1=1.0+T
      A2=1.0-R
      B2=1.0-S
      C2=1.0-T
COMM ...........//// Fonctions d' interpolation ////////...............
            
COMM ....//// Derivee des fonctions d' interpolation (R,S,T) //////...
      NR(1)=-CO*B2*C2
      NR(2)= CO*B2*C2
      NR(3)= CO*B1*C2
      NR(4)=-CO*B1*C2
      NR(5)=-CO*B2*C1
      NR(6)= CO*B2*C1
      NR(7)= CO*B1*C1
      NR(8)=-CO*B1*C1
C
      NS(1)=-CO*A2*C2
      NS(2)=-CO*A1*C2
      NS(3)= CO*A1*C2
      NS(4)= CO*A2*C2
      NS(5)=-CO*A2*C1
      NS(6)=-CO*A1*C1
      NS(7)= CO*A1*C1
      NS(8)= CO*A2*C1
C
      NT(1)=-CO*A2*B2
      NT(2)=-CO*A1*B2
      NT(3)=-CO*A1*B1
      NT(4)=-CO*A2*B1
      NT(5)= CO*A2*B2
      NT(6)= CO*A1*B2
      NT(7)= CO*A1*B1
      NT(8)= CO*A2*B1      
      END

      SUBROUTINE FORMH20(R,S,T,N,NR,NS,NT)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  FONCTIONS DE FORME ET LEURS DERIVEES  AU POINT  R,S,T          % 
COMM %           -- HEXAEDRE A VINGT NOEUDS : H20 --                   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION N(20),NR(20),NS(20),NT(20)
COMM
      C1=0.125
      C2=0.5
      VR=1.0-R
      VS=1.0-S
      VT=1.0-T
      WR=1.0+R
      WS=1.0+S
      WT=1.0+T
      VR2=1.0-(R*R)
      VS2=1.0-(S*S)
      VT2=1.0-(T*T)
COMM ...........//// Fonctions d' interpolation ////////...............
      
COMM ....//// Derivee des fonctions d' interpolation (R,S,T) //////...
      NR( 1)=-C1*VS*VT*(-1.0-2.0*R-S-T)
      NR( 2)= C1*VS*VT*(-1.0+2.0*R-S-T)      
      NR( 3)= C1*WS*VT*(-1.0+2.0*R+S-T)      
      NR( 4)=-C1*WS*VT*(-1.0-2.0*R+S-T)
      NR( 5)=-C1*VS*WT*(-1.0-2.0*R-S+T)      
      NR( 6)= C1*VS*WT*(-1.0+2.0*R-S+T)      
      NR( 7)= C1*WS*WT*(-1.0+2.0*R+S+T)
      NR( 8)=-C1*WS*WT*(-1.0-2.0*R+S+T)                              
      NR( 9)=-C2*R *VS *VT
      NR(10)= C2*C2*VS2*VT
      NR(11)=-C2*R *WS *VT
      NR(12)=-C2*C2*VS2*VT
      NR(13)=-C2* R*VS *WT
      NR(14)= C2*C2*VS2*WT
      NR(15)=-C2*R *WS *WT
      NR(16)=-C2*C2*VS2*WT
      NR(17)=-C2*C2*VS*VT2
      NR(18)= C2*C2*VS*VT2
      NR(19)= C2*C2*WS*VT2
      NR(20)=-C2*C2*WS*VT2
C
      NS( 1)=-C1*VR*VT*(-1.0-R-2.0*S-T)
      NS( 2)=-C1*WR*VT*(-1.0+R-2.0*S-T)      
      NS( 3)= C1*WR*VT*(-1.0+R+2.0*S-T)
      NS( 4)= C1*VR*VT*(-1.0-R+2.0*S-T)
      NS( 5)=-C1*VR*WT*(-1.0-R-2.0*S+T)
      NS( 6)=-C1*WR*WT*(-1.0+R-2.0*S+T)
      NS( 7)= C1*WR*WT*(-1.0+R+2.0*S+T)
      NS( 8)= C1*VR*WT*(-1.0-R+2.0*S+T)                  
      NS( 9)=-C2*C2*VR2*VT      
      NS(10)=-C2*S *WR *VT
      NS(11)= C2*C2*VR2*VT
      NS(12)=-C2*S *VR *VT
      NS(13)=-C2*C2*VR2*WT
      NS(14)=-C2*S *WR *WT
      NS(15)= C2*C2*VR2*WT
      NS(16)=-C2*S *VR *WT      
      NS(17)=-C2*C2*VR*VT2
      NS(18)=-C2*C2*WR*VT2
      NS(19)= C2*C2*WR*VT2
      NS(20)= C2*C2*VR*VT2
C
      NT( 1)=-C1*VR*VS*(-1.0-R-S-2.0*T)
      NT( 2)=-C1*WR*VS*(-1.0+R-S-2.0*T)      
      NT( 3)=-C1*WR*WS*(-1.0+R+S-2.0*T)     
      NT( 4)=-C1*VR*WS*(-1.0-R+S-2.0*T)            
      NT( 5)= C1*VR*VS*(-1.0-R-S+2.0*T)   
      NT( 6)= C1*WR*VS*(-1.0+R-S+2.0*T)   
      NT( 7)= C1*WR*WS*(-1.0+R+S+2.0*T)
      NT( 8)= C1*VR*WS*(-1.0-R+S+2.0*T)                  
      NT( 9)=-C2*C2*VR2*VS
      NT(10)=-C2*C2*WR*VS2
      NT(11)=-C2*C2*VR2*WS
      NT(12)=-C2*C2*VR*VS2
      NT(13)= C2*C2*VR2*VS
      NT(14)= C2*C2*WR*VS2
      NT(15)= C2*C2*VR2*WS
      NT(16)= C2*C2*VR*VS2      
      NT(17)=-C2*T *VR *VS
      NT(18)=-C2*T *WR *VS
      NT(19)=-C2*T *WR *WS
      NT(20)=-C2*T *VR *WS
      END

COMM ...........................////////////...........................
COMM ...//////  FIN DU MODULE REGROUPANT LES SUBROUTINES DE  //////....
COMM ...//////  CALCULS  DE RIGIDITE EN 3D                   //////....
COMM ...........................////////////...........................

