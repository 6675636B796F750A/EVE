COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %  //////  MODULE REGROUPANT LES SUBROUTINES DE CALCULS  //////   %
COMM %  //////   DE MASSE COHERENTE  -- COQ --                //////   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

       SUBROUTINE MCOECO(RMA,IDIA,COOR,MAIL,EMAI,EPAI,
     *                   NMAI,NDL,NCPN,NDPN,NNPE,NSPG,NVMA,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   CALCUL DE LA MATRICE DE MASSE COHERENTE GLOBALE               %
COMM %   POUR UNE STRUCTURE FORMEE DE PLAQUES ET DE COQUES             %
COMM %                                                                 %
COMM %   RIG   MATRICE DE RIGIDITE GLOBALE                             %
COMM %   IDIA  TABLEAU POINTEUR DE LA DIAGONALE                        %
COMM %   MAIL  TABLEAU DECRIVANT LA NUMEROTATION DES ELEMENTS          %
COMM %   COOR  COORDONNEES DES NOEUDS                                  %
COMM %   EMAI  CARACTERISTIQUES MECANIQUES DES ELEMENTS                %
COMM %   EPAI  EPAISSEUR DES ELEMENTS                                  %
COMM %   NMAI  NOMBRE D ELEMENTS                                       %
COMM %   NDL   NOMBRE DE DDL TOTAL                                     %
COMM %   NCPN  NOMBRE DE COORDONNEES PAR NOEUD                         %
COMM %   NDPN  NOMBRE DE DDL PAR NOEUD                                 %
COMM %   NNPE  NOMBRE DE NOEUDS MARIMUN PAR ELEMENT                    %
COMM %                                                                 %
COMM %   NBN   NOMBRE DE NOEUD DE L'ELEMENT                            %
COMM %   NDDLE NOMBRE DE DDL DE L'ELEMENT                              %
COMM %   NBG   NOMBRE DE POINT DE GAUSS DE L'ELEMENT                   %
COMM %   GAUSS COORDONNEES POINT DE GAUSS DE L'ELEMENT                 %
COMM %   POIDS POID AU POINT DE GAUSS DE L'ELEMENT                     %
COMM %   RML   MATRICE DE MASSE ELEMENTAIRE                            %
COMM %   NO    NUMEROS GLOBAUX DES NOEUDS                              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RMA(1),IDIA(1),COOR(NCPN,1),MAIL(NNPE+2,1),
     *          EMAI(NVMA,1),EPAI(1)      
      DIMENSION RML(171),IGLO(18),NO(3),X(3),Y(3),Z(3)
COMM ...........................////////////...........................
      CALL DZERO(RMA,IDIA(NDL))
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////.............       
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)           
      CALL CAELCO(ITYP,NBN,NDDLE,NBG)          
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))
      Z(I)=COOR(3,NO(I))              
 3    CONTINUE
COMM  ..........//////// MASSE VOLUMIQUE ET EPAISSEUR//////............
      RO=EMAI(3,IMAT)
      H =EPAI(IM)
COMM......///// CALCUL DE LA MATRICE DE MASSE ELEMENTAIRE /////.......
      IF(ITYP.EQ.31) THEN      
       CALL RMAELE_DKT(X,Y,Z,RO,H,RML,NBN,NDDLE,NBG)
      ELSE
       STOP ' Pb ITYP : MCOECO '
      ENDIF      
COMM .........////// ASSEMBLAGE DE LA MAT. RIGID. ELEM. ////...........       
      CALL ASSMAT(RMA,IDIA,RML,NO,IGLO,NBN,NDPN)
 20   CONTINUE
      END

      SUBROUTINE RMAELE_DKT(X,Y,Z,RO,H,RML,NBN,NDDLE,NBG)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE MASSE COHERENTE ELEMENTAIRE      ' DKT+CST '        %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION X(3),Y(3),Z(3),TRA(3,3)
      DIMENSION RML(171),RMEL(18,18),RME(18,18),R(18,18),RINT(18,18)
COMM  ........////  Calcul des coordonnees locales  ////................
      CALL COOLOC(X,Y,Z,X2,X3,Y3,TRA,R)
COMM  .. //// Matrice de rigidite elementaire dans le repere local ////..
      CALL RMELLO_DKT(X2,X3,Y3,RO,H,RME) 
COMM  .....///// PASSAGE AU REPERE GLOBAL /////.........................
      CALL PASGLO(RME,R,RINT,RMEL)
COMM  ...//// passage du stockage en matrice au stockage en ligne  /////......
      CALL MATLIG(RMEL,RML,NDDLE)
      END

      SUBROUTINE RMELLO_DKT(X2,X3,Y3,RO,H,RME)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE MASSE COHERENTE ELEMENTAIRE REPERE LOCAL 'DKT+CST'  %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION RME(18,18)
COMM
      CALL DZERO(RME,18*18)
      S=X2*Y3/2.D0
      C1=S*H*RO/6.D0
      C2=S*H*RO/12.D0
      DO 3 I=1,3
      RME(I   ,I   )=C1
      RME(I+ 6,I+ 6)=C1
      RME(I+12,I+12)=C1
      RME(I+ 6,I   )=C2
      RME(I+12,I   )=C2
      RME(I+12,I+ 6)=C2 
      RME(I   ,I+ 6)=C2
      RME(I   ,I+12)=C2
      RME(I+ 6,I+12)=C2                        
  3   CONTINUE
      END


COMM ...........................////////////...........................
COMM ...//////  FIN DU MODULE REGROUPANT LES SUBROUTINES DE  //////....
COMM ...//////  CALCULS  DE  MASSE COHERENTE -- COQUES --    //////....
COMM ...........................////////////...........................

