COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE      plasticité 2003                         %
COMM %                                                                 %
COMM %  //////  MODULE REGROUPANT LES SUBROUTINES DE CALCULS  //////   %
COMM %  ////// DE RIGIDITE TANGENTE  PLASTIQUE --  2D --      //////   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SUBROUTINE RIGTAN_PLAS_DECHARGE_2D(RIG,IDIA,COOR,MAIL,EMAI,EPAI,
     &                    PN0,XP0,YP0,GP0,
     &                    NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP,NSIG,NBG)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   CALCUL DE LA MATRICE DE RIGIDITE PLASTIQUE LOI3               %
COMM %         GLOBALE  DE LA STRUCTURE BIDIMENSIONNELLE               %
COMM %                                                                 %
COMM %   RIG   MATRICE DE RIGIDITE GLOBALE                             %
COMM %   IDIA  TABLEAU POINTEUR DE LA DIAGONALE                        %
COMM %   MAIL  TABLEAU DECRIVANT LA NUMEROTATION DES ELEMENTS          %
COMM %   COOR  COORDONNEES DES NOEUDS                                  %
COMM %   EMAI  CARACTERISTIQUES MECANIQUES DES ELEMENTS                %
COMM %   NMAI  NOMBRE D ELEMENTS                                       %
COMM %   NDL   NOMBRE DE DDL TOTAL                                     %
COMM %   NCPN  NOMBRE DE COORDONNEES PAR NOEUD                         %
COMM %   NDPN  NOMBRE DE DDL PAR NOEUD                                 %
COMM %   NNPE  NOMBRE DE NOEUDS MAXIMUN PAR ELEMENT                    %
COMM %   KTYP = 1 --> DEFORMATION PLANE                                %
COMM %   KTYP = 2 --> CONTRAINTE PLANE                                 %
COMM %   KTYP = 3 --> AXISYMMETRIQUE                                   %
COMM %                                                                 %
COMM %   NBN   NOMBRE DE NOEUD DE L'ELEMENT                            %
COMM %   NDDLE NOMBRE DE DDL DE L'ELEMENT                              %
COMM %   NBG   NOMBRE DE POINT DE GAUSS DE L'ELEMENT                   %
COMM %   GAUSS COORDONNEES POINT DE GAUSS DE L'ELEMENT                 %
COMM %   POIDS POID AU POINT DE GAUSS DE L'ELEMENT                     %
COMM %   RGL   MATRICE DE RIGIDITE ELEMENTAIRE                         %
COMM %   NO    NUMEROS GLOBAUX DES NOEUDS                              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RIG(1),IDIA(1),COOR(NCPN,1),MAIL(NNPE+2,1),
     &          EMAI(NVMA,1),EPAI(1),
     &          PN0(NSIG),XP0(NBG*NMAI),YP0(NBG*NMAI),GP0(NBG*NMAI),
     &          RGL(136),NO(8),IGLO(16),X(8),Y(8),D(16)
COMM ...........................////////////...........................
      CALL DZERO(RIG,IDIA(NDL))
      IADS=1     ! compteur pour SIG0
      IAD0=1     ! compteur pour SIGQ0
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////.............
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)
      YG=EMAI(1,IMAT)
      PS=EMAI(2,IMAT)
      SIGY=EMAI(3,IMAT)
      hp=EMAI(4,IMAT)
      DEPSc=EMAI(5,IMAT)
      ETA1=EMAI(6,IMAT)
      ETA2=EMAI(7,IMAT)
      ETA3=EMAI(8,IMAT)
      ETA4=EMAI(9,IMAT)
      CALL CAEL2D(ITYP,NBN,NDDLE,NBG)             
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))      
 3    CONTINUE
      H=EPAI(IM)
COMM  ......//////// ON FORME LA MATRICE DECOMPORTEMENT D //////........
      IF      (KTYP.EQ.1) THEN
         ND=3
         CALL DISODP(YG,PS,D)
      ELSE IF (KTYP.EQ.2) THEN
         ND=3
         CALL DISOCP(YG,PS,D)
      ELSE IF (KTYP.EQ.3) THEN
         ND=4
         CALL DISOAX(YG,PS,D)
      ELSE 
         STOP ' RELA2D : pb sur KTYP '
      ENDIF      
COMM..........///// CALCUL DE LA RIGIDITE ELEMENTAIRE /////.............
      IF(ITYP.EQ.30.OR.ITYP.EQ.40.OR.
     &   ITYP.EQ.60.OR.ITYP.EQ.80.OR.ITYP.EQ.81) THEN 
      CALL RIG_ISO_2D(X,Y,D,H,RGL,PN0,XP0,YP0,GP0,YG,PS,
     &     KTYP,ITYP,NBN,NDDLE,NBG,ND,IADS,IAD0,NMAI,NSIG)
      ELSE IF(ITYP.EQ.41) THEN
C        CALL RIGELE_Q4WT(     )      
      ELSE      
        STOP ' RELA2D : pb sur ITYP '  
      ENDIF
COMM .........////// ASSEMBLAGE DE LA MAT. RIGID. ELEM. ////...........       
      CALL ASSMAT(RIG,IDIA,RGL,NO,IGLO,NBN,NDPN)   
 20   CONTINUE

      END      

      SUBROUTINE RIG_ISO_2D(X,Y,D,H,RGL,PN0,XP0,YP0,GP0,YG,PS,KTYP,
     &                   ITYP,NBN,NDDLE,NBG,ND,IADS,IAD0,NMAI,NSIG)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE RIGIDITE ELEMENTAIRE : ELEMENTS ISOPARAMETRIQUES 2D %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION X(8),Y(8),D(16),RGL(136),ARGL(136),B(64),DB(64),
     &          SIG0(NSIG),GAUSS(2,9),POIDS(9),
     &          PN0(NSIG),DT(ND,ND),XP0(NBG*NMAI),YP0(NBG*NMAI),
     &          GP0(NBG*NMAI)

      DOUBLE PRECISION KSI,ETA
COMM
      ITS=(NDDLE*NDDLE+NDDLE)/2
      CALL DZERO(RGL,ITS)     
      CALL GRIG2D(ITYP,GAUSS,POIDS)
COMM   
            
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////.............
      DO 7 NG=1,NBG     
      KSI=GAUSS(1,NG)
      ETA=GAUSS(2,NG)
COMM ......//// CALCUL DE LA MATRICE B ET DE DETJ ( et RAY)////........
      CALL BLINISO2D(X,Y,KSI,ETA,B,DETJ,RAY,NDDLE,NBN,ND,ITYP,KTYP)
        
COMM...........////// CALCUL DE Bt.D.B  ////////////.................. 
COMM........... CALCUL DU MODIULE TANGENT ............................
      CALL MAT_DT_PLAS_DECHARGE(DT,XP0(IAD0),YP0(IAD0),GP0(IAD0),
     &                         PN0(IADS),YG,PS,ND)
 
      CALL TBDB(B,DT,DB,ARGL,ND,NDDLE,ITS)
COMM...........////// CALCUL DU COEFFICIENT  ////////////............           
      IF (KTYP.EQ.1.OR.KTYP.EQ.2) THEN
      COEF=DETJ*H*POIDS(NG)
      ELSE IF (KTYP.EQ.3) THEN
      COEF=DETJ*6.2831853*RAY*POIDS(NG)
      ENDIF
      CALL DACTUA(RGL,ARGL,ITS,COEF)

      IADS=IADS+4  
      IAD0=IAD0+1

 7    CONTINUE 
      END
     

      SUBROUTINE MAT_DT_PLAS_DECHARGE(DT,XP0,YP0,GP0,PN0,YG,PS,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  Module tangent pour la loi3 en 2D                              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION DT(ND,ND),PN0(ND)
      
      
      WRITE(9,*)'PN0, dans DT ',PN0(1),PN0(2),PN0(3)
      RMU2=YG/(1.D0+PS)
      XI=GP0*YP0/(2.D0*YP0-XP0)
      XI1=XI/(1.D0-1.5D0*XI)
      

      a11=1.D0/YG+XI1*PN0(1)*PN0(1)/RMU2
      a12=-PS/YG +XI1*PN0(2)*PN0(1)/RMU2
      a13=       +2.D0*XI1*PN0(3)*PN0(1)/RMU2
      a22=1.D0/YG+XI1*PN0(2)*PN0(2)/RMU2
      a23=       +2.D0*XI1*PN0(3)*PN0(2)/RMU2
      a33=2.D0/RMU2+4.D0*XI1*PN0(3)*PN0(3)/RMU2

      delta=a11*a22*a33-a11*a23**2-a22*a13**2-a33*a12**2+
     &      2.D0*a12*a13*a23

      DT(1,1)=(a22*a33-a23**2)/delta
      DT(1,2)=(-a12*a33+a13*a23)/delta
      DT(1,3)=(a12*a23-a13*a22)/delta
      DT(2,1)=DT(1,2)
      DT(2,2)=(a11*a33-a13**2)/delta
      DT(2,3)=(-a11*a23+a12*a13)/delta
      DT(3,1)=DT(1,3)
      DT(3,2)=DT(2,3)
      DT(3,3)=(a11*a22-a12**2)/delta
      END
COMM ...........................////////////...........................
COMM ...//////  FIN DU MODULE REGROUPANT LES SUBROUTINES DE  //////....
COMM ...//////  CALCULS  DE RIGIDITE ELASTIQUE EN 2D         //////....
COMM ...........................////////////...........................

