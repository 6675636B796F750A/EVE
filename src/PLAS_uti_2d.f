      SUBROUTINE CALCUL_U1_DEPART(COOR,MAIL,EMAI,EPAI,LIAI,IDIA,DFOR,
     &               F,RIG,C,V,XCi,YCi,XC0,YC0,Cm,Tm,ETA4,NNOE,NMAI,
     &               NDL,NCPN,NDPN,NNPE,NLIAI,NVMA,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %         ON RESOUT LE PROBLEME DE DEPART [k]{U1}=[F1]           %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION COOR(NCPN,1),MAIL(NNPE+2,1),EMAI(NVMA,1),EPAI(1),
     &          LIAI(2,1),IDIA(1),DFOR(1),F(1),RIG(1),C(1),XCi(1),
     &          YCi(1),V(NDL,1:*),F1(NDL),XC0(1),YC0(1)
COMM  ..................///////////////////............................
COMM    - on initialise C(p)  V(p)  SIG(p)   T(p)   a zero
COMM    - on calcule la matrice de rigidite tangente a partir de V(0)
COMM    - on tient compte des liaisons
COMM    - on decompose la matrice une fois pour toute
COMM       XL    = norme du pas tangent
COMM  ..................///////////////////............................
COMM
      CALL DZERO(F1,NDL)
      CALL RELA2D(RIG,IDIA,COOR,MAIL,EMAI,EPAI,NMAI,NDL,NCPN,NDPN,
     &                                             NNPE,NVMA,KTYP)
      CALL CALIRI(RIG,IDIA,LIAI,NLIAI,NDPN,NDL)
      CALL DECCHO(RIG,IDIA,NDL,0)
      IORDRE=1
      CALL CVORD_CHARGEV(C,XCi,YCi,XC0,YC0,Cm,Tm,IORDRE)
c      C(IORDRE)=1.D0
      CALL SHIFTD(F,F1,NDL)
      CALL DINCRE(F1,F1,NDL,C(IORDRE))
      CALL SHIFTD(F1,V(1,IORDRE),NDL)
      CALL CALIMP(LIAI,V(1,IORDRE),NLIAI,NDPN)
      CALL RESCHO(RIG,V(1,IORDRE),IDIA,NDL)
      END

      SUBROUTINE MAT_SIGMA_I(SIG_I,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  SIG_D=SIG - 1/3(SIG:I)I = SIG_I x  SIG                         %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION SIG_I(ND,ND)
      DO I=1,ND
      DO J=1,ND
      SIG_I(I,J)=0.D0
      ENDDO
      ENDDO
      SIG_I(1,1)=2.D0/3.D0
      SIG_I(1,2)=-1.D0/3.D0
      SIG_I(2,1)=-1.D0/3.D0
      SIG_I(2,2)=2.D0/3.D0
      SIG_I(3,3)=1.D0
      END

      SUBROUTINE DETRAY_PLAS(CRIT,V,RAYON,NDL,NORDRE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ON DETERMINE LE RAYON DE CONVERGENCE DE LA SERIE              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION V(NDL,*)

      IORD=1
      XNOR1=DSQRT(PROSCA(V(1,IORD),V(1,IORD),NDL))


      DO WHILE(XNOR1.EQ.0.AND.IORD.LT.NORDRE)
      IORD=IORD+1
      XNOR1=DSQRT(PROSCA(V(1,IORD),V(1,IORD),NDL))
      ENDDO

      IORDN=NORDRE
      XNORP=DSQRT(PROSCA(V(1,NORDRE),V(1,NORDRE),NDL))
      DO WHILE(XNORP.EQ.0.AND.IORDN.GT.1)
      IORDN=IORDN-1
      XNORP=DSQRT(PROSCA(V(1,IORDN),V(1,IORDN),NDL))
      ENDDO



      RAYON=(CRIT*(XNOR1/XNORP))**(1.D0/DBLE(IORDN-IORD))

      WRITE(39,*)'IORD,IORDN,XNOR1,XNORP,RAYON=' ,
     &            IORD,IORDN,XNOR1,XNORP,RAYON



      END

      SUBROUTINE DETRAY_LOI3_H(CRIT,V0,V,RAYON,NDL,NORDRE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ON DETERMINE LE RAYON DE CONVERGENCE DE LA SERIE              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION V0(NDL),V(NDL,*)
      XNOR1=DSQRT(PROSCA(V0,V0,NDL))
      XNORP=DSQRT(PROSCA(V(1,NORDRE-1),V(1,NORDRE-1),NDL))
      RAYON=(CRIT*(XNOR1/XNORP))**(1.D0/DBLE(NORDRE-1))
      END


      SUBROUTINE VINIT_VARIABLES_PLAS_CHARGE(SIGQ0,SIGe0,FC0,
     &           GP0,ZP0,XP0,YP0,EPSPe0,MAIL,EMAI,DEN0,
     &           NDL,NSIG,NBG,NMAI,NVMA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  ON INITIALISE LES VARIABLES POUR DEMMARER                      %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION MAIL(1),EMAI(NVMA,1),SIGQ0(1),SIGe0(1),FC0(1),
     &          GP0(1),ZP0(1),XP0(1),YP0(1),EPSPe0(1),DEN0(1)
COMM
COMM %%%%%%%  DEMARRAGE A PARTIR D'UN ETAT OU DEPLACEMENT EST NUL %%%%%%
COMM
      IMAT=1
      YG=EMAI(1,IMAT)
      PS=EMAI(2,IMAT)
      SIGY=EMAI(3,IMAT)
      hp=EMAI(4,IMAT)
      DEPSc=EMAI(5,IMAT)
      ETA1=EMAI(6,IMAT)
      ETA2=EMAI(7,IMAT)
      ETA3=EMAI(8,IMAT)
      ETA4=EMAI(9,IMAT)
      RMU2=YG/(1.D0+PS)
      
      SIGQ0sol=ETA1*SIGY
      CALL VECT_INIT(SIGQ0,NBG*NMAI,SIGQ0sol)
      EPSPe0sol=0.D0    
      CALL VECT_INIT(EPSPe0,NBG*NMAI,EPSPe0sol)
      SIGe0sol=SIGY    
      CALL VECT_INIT(SIGe0,NBG*NMAI,SIGe0sol)
      FC0sol=(SIGQ0sol-SIGe0sol)/SIGe0sol
      CALL VECT_INIT(FC0,NBG*NMAI,FC0sol)
      FFC0sol=FC0sol**2 
      CALL VECT_INIT(ZP0,NBG*NMAI,FFC0sol)
      DEN0sol=(SIGe0sol*FFC0sol/RMU2)+ETA3*
     &         (3.D0/2.D0+hp*(1.D0+FC0sol)/RMU2)
      CALL VECT_INIT(DEN0,NBG*NMAI,DEN0sol)
      GP0sol=ETA3/DEN0sol
      CALL VECT_INIT(GP0,NBG*NMAI,GP0sol)
      XP0sol=0.D0
      CALL VECT_INIT(XP0,NBG*NMAI,XP0sol)
      YP0sol=ETA2
      CALL VECT_INIT(YP0,NBG*NMAI,YP0sol)
      END

      SUBROUTINE VECT_INIT(A,N,VAL)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %                MISE A VAL DE A(I),I=1,N                        %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION A(1),VAL
      DO 1 I=1,N
 1    A(I)=VAL
      END

 
      SUBROUTINE EC_XPYP(XP,MAIL,NIO)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %  IMPRESSION DU VECTEUR CONTRAINTE S(I) DANS LE FICHIER NIO     %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION XP(1),SGM(6)
      DIMENSION MAIL(1)
      COMMON/INI/ICO(100),IT(100)
COMM      
      KTYP=ICO(1)
      NNPE=ICO(4)
      NSPG=1              
      NMAI=ICO(8) 
COMM 
      ITYP=40
      CALL CAEL2D(ITYP,NBN,NDDLE,NBG)
      WRITE(NIO,110)
      DO I=1,NMAI*NBG
      WRITE(NIO,120) I,XP(I)
      ENDDO

COMM .........................////////////............................. 
 110  FORMAT(' MAILLE  PG     VALEUR          ')
 120  FORMAT(I5,4X,6(2X,G15.8))  
      END

      SUBROUTINE DERIVE_VECTPADE(D,V,DV,NDIM,NOR,A)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ON FORME UN VECTEUR DERIVE DV DE V EN 'A'                     %
COMM %                                                                 %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION V(NDIM,1),DV(NDIM),D(NOR-1)

      DO 5 K=1,NDIM
 5     DV(K)=V(K,1)
       DO 3  J=2,NOR
        PA=1.D0
        DO 32 JJ=1,J-1
 32     PA=PA*A
        PA=PA*J
        DO 31 K=1,NDIM
 31     DV(K)=DV(K)+V(K,J)*PA
 3     CONTINUE
      END
      
      DOUBLE PRECISION FUNCTION  DERIVE_D(D,NORDRE,IORDRE,A)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %    CALCUL DE SIGD1:D:SIGD2 DANS LE CAS 2D,                      %
COMM %    ENTREE: SIG1,SIG2 DE DIMENSION ND= 3                         %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION D(NORDRE-1)

      DD=0.D0
      DO I=1,IORDRE
      DD=DD+DBLE(I)*A**(I-1)*D(I)
      ENDDO
      DERIVE_D=DD
      END

      SUBROUTINE TANG_VECT_PADE(D,V,DV,NOR,NSIG,A)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION V(NSIG,1),D(NOR-1),DV(NSIG)
      DV(J)=0.D0
      
      DO  I=1,NOR-1
      DO  J=1,NSIG
      DV(J)=DV(J)+V(J,I)*(DBLE(I)*A**(I-1)*D(NOR-I-1)/D(NOR-1)+A**I*
     & (DERIVE_D(D,NOR,NOR-I-1,A)*D(NOR-1)-DERIVE_D(D,NOR,NOR-1,A)*
     &  D(NOR-I-1))/D(NOR-1)**2)
      ENDDO
      ENDDO     
      
      
      END
          
      SUBROUTINE DERIVE_VECT(V,DV,NOR,NDIM,A)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ON FORME UN VECTEUR DERIVE DV DE V EN 'A'                     %
COMM %                                                                 %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION V(NDIM,1),DV(NDIM)

       DO 1 K=1,NDIM
 1     DV(K)=V(K,1)
       DO 2 I=2,NOR
        DO 3 K=1,NDIM
 3     DV(K)=DV(K)+V(K,I)*DBLE(I)*A**(I-1)
 2     CONTINUE 
      END 

      SUBROUTINE tangente(V,NORDRE,NDIM,A,U)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   calcul d'un vecteur tangent                                   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION D(NORDRE-1),V(NDIM,1),V0(NDIM),U(NDIM),
     *   PADE1(NORDRE-1),PADE2(NORDRE-1),ALPHA(NORDRE,NORDRE)
     

      CALL ORTHOGONALISER(ALPHA,V,NDIM,NORDRE,D)
      A2=0.95*A
      CALL NUMPAD_MODIF(D,NORDRE-1,A,SERDEN1)
      CALL NUMPAD_MODIF(D,NORDRE-1,A2,SERDEN2)

      DO K=1,NORDRE-1
      CALL NUMPAD_MODIF(D,NORDRE-1-K,A,SERNUM1)
      CALL NUMPAD_MODIF(D,NORDRE-1-K,A2,SERNUM2)
      PADE1(K)=SERNUM1/SERDEN1
      PADE2(K)=SERNUM2/SERDEN2
      ENDDO

      CALL DZERO(U,NDIM)
      DO I=1,NDIM
      DO J=1,NORDRE-1
      PA1=1.D0
      PA2=1.D0
      DO JJ=1,J
      PA1=PA1*A
      PA2=PA2*A2
      ENDDO
      U(I)=U(I)+(PA1*PADE1(J)-PA2*PADE2(J))*V(I,J)
      ENDDO
      ENDDO

      END


      SUBROUTINE PADE_DERIVE(V,V95,DV,NDIM)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ON FORME UN VECTEUR DERIVE DV DE V EN 'A'                     %
COMM %                                                                 %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION V(NDIM),DV(NDIM),V95(NDIM)

       DO  K=1,NDIM
       DV(K)=V(K)-V95(K)
       DV(K)=DV(K)/0.05D0
       write(19,*) 'dv(',K,')=',DV(K)
       ENDDO
      END 
      
      SUBROUTINE CVORD1(C,V,VRES,XL1,XL2,NDL,IP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 % 
COMM %  ON CALCULE LE COEFFICIENT  C(1) ET LE VECTEUR V(1)             %
COMM %                                                                 %    
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION C(1),V(NDL,1),VRES(NDL)
COMM    
      A=XL2*PROSCA(V(1,1),V(1,1),NDL)+XL1
      B=XL2*PROSCA(V(1,1),VRES,NDL)
      CC=XL2*PROSCA(VRES,VRES,NDL)-1.D0

      C(1)=(-B+DSQRT(B*B-A*CC))/A
      DO 1 I=1,NDL
 1    V(I,1)=V(I,1)*C(1)+VRES(I)

      END
       
      SUBROUTINE CVORDP(C,V,VRES,XL1,XL2,NDL,IORDRE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 % 
COMM %  ON CALCULE LE COEFFICIENT  C(p) ET ON TERMINE LE VECTEUR V(p)  %
COMM %                                                                 %    
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION C(1),V(NDL,1),VRES(NDL)
COMM
COMM ....//// a={u,v1} +  l*C(1) /{v1,v1} + C(1)*C(1) /////..............
COMM
COMM  
COMM ..../////   calcul de V(p) //////...................................
COMM	
       d=PROSCA(V(1,IORDRE),V(1,1),NDL)
       e=PROSCA(VRES,V(1,1),NDL)
       C(IORDRE)=-XL2*d/(XL2*e+XL1*C(1))
      DO 20 I=1,NDL
 20   V(I,IORDRE)=V(I,IORDRE)+VRES(I)*C(IORDRE)               
      END 

      SUBROUTINE CALIRI_DEPL(RIG,IDIA,LIAI,NLIAI,NDPN,NDL,ALPHA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %        PRISE EN COMPTE DES CONDITIONS AUX LIMITES              %
COMM %          deplacement impose                                    %
COMM %   RIG  MATRICE DE RIGIDITE GLOBALE DE LA STRUCTURE             %
COMM %   IDIA TABLEAU POINTEUR DE LA DIAGONALE                        %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION IDIA(1),RIG(1),LIAI(2,1)
COMM ...........................////////////...........................
      DO 1 I=1,NLIAI
      N =LIAI(1,I)
      ND=LIAI(2,I)
      IF(ND.LE.NDPN) THEN
        NN=NDPN*(N-1)+ND
      ELSE
        STOP ' Pb BLOCAGE : CALIMP  - Verifier les blocages - '
      ENDIF      
      RIG(IDIA(NN))=RIG(IDIA(NN))+ALPHA
 1    CONTINUE
      END

      SUBROUTINE CALIMP_DEPL(LIAI,F,NLIAI,NDPN,ALPHA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %        PRISE EN COMPTE DES DEPLACEMENTS IMPOSES NULS           %
COMM %        MODIFICATION DU SECOND MEMBRE F                         %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION F(1),ALPHA
      DIMENSION LIAI(2,1)

       write(9,*) 'ALPHA= ',ALPHA

      DO 1 I=1,NLIAI
      N =LIAI(1,I)
      ND=LIAI(2,I)
      IF(ND.LE.NDPN) THEN
        NN=NDPN*(N-1)+ND
        F(NN)=F(NN)*ALPHA
        write(9,*) 'Noeud,ND,NDPN,NN =',N,ND,NDPN,NN
        write(9,*) 'NN,F(NN)= ',NN,F(NN)
      ELSE
        STOP ' Pb BLOCAGE : CALIMP  - Verifier les blocages - '
      ENDIF
 1    CONTINUE
      END           



      SUBROUTINE EPSk_PN0(SIG,EPSPnl,PN0,xi,RMU2,PROD)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %     CALCUL DE        EPSk : PN0                                %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION SIG(3),EPSPnl(3),PN0(3)

      beta=1.D0-1.5D0*xi
      SIG_N0=SIG(1)*PN0(1)+SIG(2)*PN0(2)+2.D0*SIG(3)*PN0(3)
      EPSPnl_N0=EPSPnl(1)*PN0(1)+EPSPnl(2)*PN0(2)+EPSPnl(3)*PN0(3)+
     &          (EPSPnl(1)+EPSPnl(2))*(PN0(1)+PN0(2))
      PROD=(SIG_N0/RMU2 + EPSPnl_N0)/beta
      END

      SUBROUTINE EPSi_PNj(SIG,EPSP,PN,RMU2,PROD)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %     CALCUL DE        EPSi : PNj                                %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION SIG(3),EPSP(3),PN(3)

      SIG_N=SIG(1)*PN(1)+SIG(2)*PN(2)+2.D0*SIG(3)*PN(3)
      EPSP_N=EPSP(1)*PN(1)+EPSP(2)*PN(2)+EPSP(3)*PN(3)+
     &          (EPSP(1)+EPSP(2))*(PN(1)+PN(2))
      PROD=SIG_N/RMU2 + EPSP_N
      END

      SUBROUTINE SIG_PN(SIG,PN,PROD)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %     CALCUL DE        SIG : PN                                  %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION SIG(3),PN(3)
      PROD=SIG(1)*PN(1)+SIG(2)*PN(2)+2.D0*SIG(3)*PN(3)
      END

      SUBROUTINE EPSP_PN(EPSP,PN,PROD)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %     CALCUL DE        EPSP : PN                                 %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION EPSP(3),PN(3)

      PN33=-(PN(1)+PN(2))
      EPSp33=-(EPSP(1)+EPSP(2))
      PROD=EPSP(1)*PN(1)+EPSP(2)*PN(2)+EPSP(3)*PN(3)+EPSp33*PN33
      END


      SUBROUTINE PN_PN(PN1,PN2,PROD)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %     CALCUL DE        PN : PN                                   %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION PN1(3),PN2(3)
      PROD= PN1(1)*PN2(1)+PN1(2)*PN2(2)+2.D0*PN1(3)*PN2(3)+
     &      (PN1(1)+PN1(2))*(PN2(1)+PN2(2))
      END
      

      SUBROUTINE SIGDEPS(SIGD,EPS,EPSP,YG,PS,RES)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %     CALCUL DE        SIGD:EPS                                  %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION SIGD(3),EPS(3),EPSP(3)
      V1=-PS/(1.D0-PS)
      V2=(2.D0*PS-1)/(1.D0-PS)
      EPS33=V1*(EPS(1)+EPS(2))-(EPSP(1)+EPSP(2))
      SIGD33=-(SIGD(1)+SIGD(2))
      RES=SIGD(1)*EPS(1)+SIGD(2)*EPS(2)+SIGD(3)*EPS(3)+SIGD33*EPS33
      END

      SUBROUTINE PROD_TENSOR_SIG33(SIG1,SIG2,RES)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %    CALCUL DE SIG1:SIG2 DANS LE CAS 2D,                          %
COMM %    ENTREE: SIG1(3,3),SIG2(3,3)                                  %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION SIG1(3,3),SIG2(3,3)
      RES=0.D0
	DO I=1,3
	DO J=1,3
	RES=RES+SIG1(I,J)*SIG2(I,J)
	ENDDO
	ENDDO
      END


      SUBROUTINE DEVIATEUR_2D(SIG,SIGD,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %    CALCUL DU DEVIATEUR DANS LE CAS 2D,                          %
COMM %    ENTREE: SIG DE DIMENSION ND= 3                               %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION SIG(ND),SIGD(3,3)

      TRACE=(SIG(1)+SIG(2))
      CALL DZERO(SIGD,9)
      SIGD(1,1)=SIG(1)-TRACE/3.D0
   	 SIGD(2,2)=SIG(2)-TRACE/3.D0
      SIGD(3,3)=-TRACE/3.D0
	 SIGD(1,2)=SIG(3)
	 SIGD(2,1)=SIG(3)
      END

      DOUBLE PRECISION FUNCTION  SIGD_SIGD_2D(SIG1,SIG2,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %    CALCUL DE SIGD1:D:SIGD2 DANS LE CAS 2D,                      %
COMM %    ENTREE: SIG1,SIG2 DE DIMENSION ND= 3                         %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION SIG1(ND),SIG2(ND),SIGD1(3,3),SIGD2(3,3)

	CALL DEVIATEUR_2D(SIG1,SIGD1,ND)
	CALL DEVIATEUR_2D(SIG2,SIGD2,ND)
	SIGD_SIGD_2D=SIGD1(1,1)*SIGD2(1,1)+SIGD1(2,2)*SIGD2(2,2)+
     & SIGD1(3,3)*SIGD2(3,3)+2.D0*SIGD1(1,2)*SIGD2(1,2)
      END

       SUBROUTINE PROD_TENSOR_SIG(SIG1,SIG2,PROD,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  PROD = SIG1 : SIG2                                             %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION SIG1(ND),SIG2(ND)
      PROD=SIG1(1)*SIG2(1)+SIG1(2)*SIG2(2)+2.D0*SIG1(3)*SIG2(3)
      END

      SUBROUTINE MAT_SIGMA_D(SIGD0,SIG_D,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  (SIGD0:SIGD1)SIGD0 = SIG_D x  SIG1                             %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION SIG_D(ND,ND),SIGD0(ND)


      SIG_D(1,1)=SIGD0(1)*SIGD0(1)
      SIG_D(1,2)=SIGD0(1)*SIGD0(2)
      SIG_D(1,3)=2.D0*SIGD0(1)*SIGD0(3)

      SIG_D(2,1)=SIGD0(2)*SIGD0(1)
      SIG_D(2,2)=SIGD0(2)*SIGD0(2)
      SIG_D(2,3)=2.D0*SIGD0(2)*SIGD0(3)

      SIG_D(3,1)=2.D0*SIGD0(3)*SIGD0(1)
      SIG_D(3,2)=2.D0*SIGD0(3)*SIGD0(2)
      SIG_D(3,3)=4.D0*SIGD0(3)*SIGD0(3)
      END



