COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %          //////   MODULE PILOTE DES CALCULS    //////           %
COMM %          //////  EN NON-LINEAIRE GEOMETRIQUE   //////           %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE NLGEOM(IDIMT)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %              CALCUL NON-LINEAIRE GEOMETRIQUE                    %
COMM %                                                                 %
COMM %   Organisation de la memoire  (Super-Tableau T + pointeurs IT)  %
COMM %   --------------------------                                    %
COMM %                                                                 %
COMM %      adresse              contenu de T                 type     %
COMM %                                                                 %
COMM %   IT(11)---IDIA       Pointeurs de la diagonale       (ENTIER)  %
COMM %   IT(12)---F1         Le vecteur de charge (Lam=1)    (DP)      %
COMM %   IT(13)---F          Le vecteur de charge appliquee  (DP)      %
COMM %   IT(14)---RIG        Matrice de rigidite             (DP)      %
COMM %   IT(15)---V          Vecteur deplacement             (DP)      %
COMM %   IT(16)---SIG        Contraintes aux points de Gauss (DP)      % 
COMM %   IT(17)---RES        Residu  (ou deplacement Vr)     (DP)      %
COMM %   IT(18)---DF         Increment de charge (ou Vf)     (DP)      % 
COMM %   IT(19)---DV0        Increment du pas tangent        (DP)      %
COMM %   IT(20)---Vtra       Vecteur de travail              (DP)      %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION LAMBDA,INCRE,CRITER,ValMax,Depl
      CHARACTER*3 TYPPAS       
      COMMON T(1)
      COMMON/INI/ICO(200),IT(200)
COMM ...........................////////////...........................
      KTYP =ICO(1)
      NCPN =ICO(2)
      NDPN =ICO(3)
      NNPE =ICO(4)
      NSPG =ICO(5)            
      NNOE =ICO(6)
      NDL  =ICO(7)
      NMAI =ICO(8)
      NVMA =ICO(10)      
      NLIAI=ICO(11)
COMM
COMM .....//// RESERVATION DES PLACES DANS LE SUPER-TABLEAU ////......
COMM
      IT(12)=IT(11)+NDL     
      IT(13)=IT(12)+2*NDL
      IT(14)=IT(13)+2*NDL      
      CALL PROFBI(T(IT(11)),T(IT(2)),NDL,NMAI,NRIG,NNPE,NDPN,KTYP)
      ICO(26)=NRIG
      IT(15)=IT(14)+2*NRIG
      IT(16)=IT(15)+2*NDL
      CALL LONSIG(T(IT(2)),NMAI,NNPE,NSPG,NSIG,NEPS,KTYP)
      ICO(27)=NSIG
      ICO(28)=NEPS
      IT(17)=IT(16)+2*NSIG
      IT(18)=IT(17)+2*NDL
      IT(19)=IT(18)+2*NDL
      IT(20)=IT(19)+2*NDL
      IT(21)=IT(20)+2*NDL
COMM
      WRITE(6,150)        
      WRITE(6,100)       
      DO 1 I=1,20
  1   WRITE(6,120) I,IT(I),IT(I+1)-IT(I)
      WRITE(6,130) IT(21),IDIMT
      IF(IT(21).GT.IDIMT) STOP 'Taille du Super-Tableau'     
COMM            
COMM ........../// ASSEMBLAGE DU VECTEUR DE CHARGE //.............
COMM    
      CALL CAFORC(T(IT(6)),T(IT(12)))
COMM
COMM ..........///// INITIALISATION /////........................
COMM 
      CALL DZERO(T(IT(13)),NDL)
      CALL DZERO(T(IT(15)),NDL)
      CALL DZERO(T(IT(16)),NSIG)
      LAMBDA=0.D0
C-------- fichier des resultats      
      OPEN(UNIT=31,FILE='courbeNR',ACCESS='SEQUENTIAL')       
COMM
COMM .../// LECTURE DU PILOTAGE : --> NOMBRE DE SEQUENCE (20 MAXI)
COMM
      READ(5,*) CRITER,NITER     
      READ(5,*) NSEQ 
      DO 2 ISEQ=1,NSEQ
      READ(5,'(A3)')  TYPPAS
      IF     (TYPPAS.EQ.'FOR') THEN
COMM .....//////  Force imposee 
C--------------------------------
          READ(5,*) NPAS,INCRE,Nod,ValMax,IDOF
          DO 4 IP=1,NPAS
          CALL FORIMP(T(IT(1)),T(IT(2)),T(IT(3)),T(IT(4)),T(IT(5)),
     *                T(IT(11)),T(IT(12)),T(IT(13)),T(IT(14)),
     *                T(IT(15)),T(IT(16)),T(IT(17)),T(IT(18)),
     *           NNOE,NMAI,NDL,NCPN,NDPN,NNPE,NLIAI,NSPG,NVMA,
     *           KTYP,IP,LAMBDA,INCRE,CRITER,NITER) 
       CALL VALEUR_ATTEINTE(T(IT(15)),NDL,NDPN,Nod,ValMax,IDOF,Num,Depl) 
       IF(Num.EQ.1) THEN
       GO TO 111
       ENDIF                    
 4        CONTINUE
111     CONTINUE 
      ELSE IF(TYPPAS.EQ.'DEP') THEN
COMM .....//////  Deplacement impose (on lit le noeud et le d.d.l.)
          READ(5,*) NPAS,INCRE,NOEINC,NDLINC             
          DO 5 IP=1,NPAS
          CALL DEPIMP(T(IT(1)),T(IT(2)),T(IT(3)),T(IT(4)),T(IT(5)),
     *                T(IT(11)),T(IT(12)),T(IT(13)),T(IT(14)),
     *                T(IT(15)),T(IT(16)),T(IT(17)),T(IT(18)),
     *           NNOE,NMAI,NDL,NCPN,NDPN,NNPE,NLIAI,NSPG,NVMA,          
     *           KTYP,IP,LAMBDA,INCRE,NOEINC,NDLINC,CRITER,NITER) 
 5        CONTINUE    
      ELSE IF(TYPPAS.EQ.'LON') THEN
COMM .....//////  Longueur d'arc imposee (s*s= tV.V + Lambda*Lambda)
          READ(5,*) NPAS,INCRE,Nod,ValMax,IDOF             
          DO 6 IP=1,NPAS
          CALL LONIMP(T(IT(1)),T(IT(2)),T(IT(3)),T(IT(4)),T(IT(5)),
     *                T(IT(11)),T(IT(12)),T(IT(13)),T(IT(14)),
     *                T(IT(15)),T(IT(16)),T(IT(17)),T(IT(18)),
     *                T(IT(19)),T(IT(20)),
     *                NNOE,NMAI,NDL,NCPN,NDPN,NNPE,NLIAI,NSPG,NVMA,          
     *                KTYP,IP,LAMBDA,INCRE,NOEINC,NDLINC,CRITER,NITER) 
       CALL VALEUR_ATTEINTE(T(IT(15)),NDL,NDPN,Nod,ValMax,IDOF,Num,Depl) 
c----------------- analyse des resultats ---------------------------
c--------- j'ecris les resultats en fin de pas pour tracer les courbes
      WRITE(31,'(2(G15.8,2X))') Depl,LAMBDA
c-------------------------------------------------------------------         
       
       IF(Num.EQ.1) THEN
       GO TO 112
       ENDIF     
 6        CONTINUE 
112     CONTINUE     
      ELSE        
       CALL ERROR1
      ENDIF 
 2    CONTINUE
COMM ..................../////////////................................      
 150  FORMAT(////////,
     ;'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%',/,
     ;'  %                                                        %',/, 
     ;'  %         CALCUL   NON-LINEAIRE   GEOMETRIQUE            %',/,      
     ;'  %                                                        %',/,      
     ;'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')           
 100  FORMAT('1',//,' ',60('%'),///,
     ;'  ORGANISATION DU SUPER-TABLEAU  T() ',//)
 110  FORMAT(' ',60('%'),/////)
 120  FORMAT('   IT(',I3,')=',I8,'     Longueur=',I8)
 130  FORMAT(///,'   Taille memoire necessaire   : ',I8,/,
     ;           '   Dimension du Super-Tableau  : ',I8,///)       
      END
      
      SUBROUTINE FORIMP(COOR,MAIL,EMAI,EPAI,LIAI,
     *                  IDIA,F1,F,RIG,V,SIG,RES,DF,
     *                  NNOE,NMAI,NDL,NCPN,NDPN,NNPE,NLIAI,NSPG,NVMA,
     *                  KTYP,IP,LAMBDA,INCRE,CRITER,NITER,
     *                  Nod,ValMax,IDOF)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %     PAS DE NEWTON-RAPHSON : PILOTAGE A FORCE IMPOSEE            %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)      
      DOUBLE PRECISION LAMBDA,INCRE,CRITER
      DIMENSION COOR(1),MAIL(1),EMAI(1),EPAI(1),LIAI(1),
     *          IDIA(1),F1(1),F(1),RIG(1),V(1),SIG(1),RES(1),DF(1)
      LOGICAL TOURNE  
COMM      
      WRITE(6,100) IP 
COMM  
COMM ............../////   CALCUL TANGEANT  /////......................
COMM 
      ITER=0
      TOURNE=.TRUE.
      CALL RIGTAN(RIG,IDIA,COOR,MAIL,EMAI,EPAI,V,SIG,
     *            NMAI,NDL,NCPN,NDPN,NNPE,NSPG,NVMA,KTYP)
      CALL CALIRI(RIG,IDIA,LIAI,NLIAI,NDPN,NDL)
      CALL DECCHO(RIG,IDIA,NDL,0) 
C
      LAMBDA=LAMBDA+INCRE
      CALL DINCRE(F1,DF,NDL,INCRE)
      CALL DSOMME(F,DF,NDL)
C
      CALL CALIMP(LIAI,DF,NLIAI,NDPN)
      CALL RESCHO(RIG,DF,IDIA,NDL)     
      CALL DSOMME(V,DF,NDL)
C 
      CALL RESIDU(V,COOR,MAIL,EMAI,EPAI,SIG,RES,F,
     *            NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
      CALL CALIMP(LIAI,RES,NLIAI,NDPN) 
C------ ecriture des numeros d' iteration.
c       CALL ECPAS(V,RES,ITER)
c        WRITE(6,*) '  ITERATION NUMERO ',ITER
c--------------------------------------------        
      CALL CRITCO(RES,CRITER,NNOE,NDL,ITER,TOURNE,6)
COMM  
COMM ........../////   ITERATION DE NEWTON-RAPHSON  /////........
COMM 
      DO WHILE(TOURNE.AND.ITER.LT.NITER)
      ITER=ITER+1
C   
      CALL RIGTAN(RIG,IDIA,COOR,MAIL,EMAI,EPAI,V,SIG,
     *            NMAI,NDL,NCPN,NDPN,NNPE,NSPG,NVMA,KTYP)
      CALL CALIRI(RIG,IDIA,LIAI,NLIAI,NDPN,NDL)
      CALL DECCHO(RIG,IDIA,NDL,0)
C
      CALL CALIMP(LIAI,RES,NLIAI,NDPN)
      CALL RESCHO(RIG,RES,IDIA,NDL)
      CALL DSOMME(V,RES,NDL)
C
      CALL RESIDU(V,COOR,MAIL,EMAI,EPAI,SIG,RES,F,
     *            NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
      CALL CALIMP(LIAI,RES,NLIAI,NDPN)
C------ ecriture des numeros d' iteration.      
c      CALL ECPAS(V,RES,ITER) 
       WRITE(6,*) '  ITERATION NUMERO ',ITER
c--------------------------------------------               
      CALL CRITCO(RES,CRITER,NNOE,NDL,ITER,TOURNE,6)
C 
      ENDDO
C---- ecriture des contraintes ----------
c      CALL IMPNLG(LAMBDA,V,SIG,MAIL,IP)
C-----------------------------------------           
 100  FORMAT(//,80(1H-),//,' ON FAIT LE PAS ',I3,//)
      END 
      
      SUBROUTINE DEPIMP(COOR,MAIL,EMAI,EPAI,LIAI,
     *                  IDIA,F1,F,RIG,V,SIG,RES,DF,
     *                  NNOE,NMAI,NDL,NCPN,NDPN,NNPE,NLIAI,NSPG,NVMA,
     *                  KTYP,IP,LAMBDA,INCRE,NOEINC,NDLINC,CRITER,NITER)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %     PAS DE NEWTON-RAPHSON : PILOTAGE A DEPLACEMENT IMPOSE       %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)      
      DOUBLE PRECISION LAMBDA,INCRE,CRITER,DLAMBDA,DUFI,DURI
      DIMENSION COOR(1),MAIL(1),EMAI(1),EPAI(1),LIAI(1),
     *          IDIA(1),F1(1),F(1),RIG(1),V(1),SIG(1),RES(1),DF(1)
      LOGICAL TOURNE
COMM
      WRITE(6,100) IP 
COMM  
COMM ............../////   CALCUL TANGEANT  /////......................
COMM 
      ITER=0
      TOURNE=.TRUE.
      CALL RIGTAN(RIG,IDIA,COOR,MAIL,EMAI,EPAI,V,SIG,
     *            NMAI,NDL,NCPN,NDPN,NNPE,NSPG,NVMA,KTYP)
      CALL CALIRI(RIG,IDIA,LIAI,NLIAI,NDPN,NDL)
      CALL DECCHO(RIG,IDIA,NDL,0) 
C
      CALL DINCRE(F1,DF,NDL,1.D0)
      CALL CALIMP(LIAI,DF,NLIAI,NDPN)
      CALL RESCHO(RIG,DF,IDIA,NDL)      
      CALL COMPOS(DF,NOEINC,NDLINC,NDPN,DUFI)
      DLAMBDA=INCRE/DUFI
C
      LAMBDA=LAMBDA+DLAMBDA
      CALL DACTUA(F,F1,NDL,DLAMBDA) 
      CALL DACTUA(V,DF,NDL,DLAMBDA)
C
      CALL RESIDU(V,COOR,MAIL,EMAI,EPAI,SIG,RES,F,
     *            NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
      CALL CALIMP(LIAI,RES,NLIAI,NDPN)
C
C      CALL ECPAS(V,RES,ITER)
      CALL CRITCO(RES,CRITER,NNOE,NDL,ITER,TOURNE,6)
COMM  
COMM ........../////   ITERATION DE NEWTON-RAPHSON  /////........
COMM 
      DO WHILE(TOURNE.AND.ITER.LT.NITER)
      ITER=ITER+1
C   
      CALL RIGTAN(RIG,IDIA,COOR,MAIL,EMAI,EPAI,V,SIG,
     *            NMAI,NDL,NCPN,NDPN,NNPE,NSPG,NVMA,KTYP)
      CALL CALIRI(RIG,IDIA,LIAI,NLIAI,NDPN,NDL)
      CALL DECCHO(RIG,IDIA,NDL,0)
C
      CALL CALIMP(LIAI,RES,NLIAI,NDPN)
      CALL RESCHO(RIG,RES,IDIA,NDL)
      CALL COMPOS(RES,NOEINC,NDLINC,NDPN,DURI)
C
      CALL DINCRE(F1,DF,NDL,1.D0)
      CALL CALIMP(LIAI,DF,NLIAI,NDPN)
      CALL RESCHO(RIG,DF,IDIA,NDL)      
      CALL COMPOS(DF,NOEINC,NDLINC,NDPN,DUFI)
      DLAMBDA=-(DURI/DUFI)
C
      LAMBDA=LAMBDA+DLAMBDA
      CALL DACTUA(F,F1,NDL,DLAMBDA) 
      CALL DSOMME(V,RES,NDL)
      CALL DACTUA(V,DF,NDL,DLAMBDA)
C 
      CALL RESIDU(V,COOR,MAIL,EMAI,EPAI,SIG,RES,F,
     *            NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
      CALL CALIMP(LIAI,RES,NLIAI,NDPN)
C      
C      CALL ECPAS(V,RES,ITER)
      CALL CRITCO(RES,CRITER,NNOE,NDL,ITER,TOURNE,6)
C
      ENDDO
C
c affichage      CALL IMPNLG(LAMBDA,V,SIG,MAIL,IP)
C      
 100  FORMAT(//,80(1H-),//,' ON FAIT LE PAS ',I3,//)
      END 
      
      SUBROUTINE LONIMP(COOR,MAIL,EMAI,EPAI,LIAI,
     *                IDIA,F1,F,RIG,V,SIG,RES,DF,DV0,VAUX,
     *                NNOE,NMAI,NDL,NCPN,NDPN,NNPE,NLIAI,NSPG,NVMA,
     *                KTYP,IP,LAMBDA,INCRE,NOEINC,NDLINC,CRITER,NITER,
     *                Nod,ValMax,IDOF)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %     PAS DE NEWTON-RAPHSON : PILOTAGE A LONGUEUR D'ARC IMPOSEE   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)      
      DOUBLE PRECISION LAMBDA,INCRE,CRITER,DLAMBDA,DUFI,DURI
      DIMENSION COOR(1),MAIL(1),EMAI(1),EPAI(1),LIAI(1),
     *          IDIA(1),F1(1),F(1),RIG(1),V(1),SIG(1),RES(1),DF(1),
     *          DV0(1),VAUX(1)
      LOGICAL TOURNE
COMM
      WRITE(6,100) IP 
COMM  
COMM ............../////   CALCUL TANGEANT  /////......................
COMM 
      ITER=0
      TOURNE=.TRUE.
      CALL RIGTAN(RIG,IDIA,COOR,MAIL,EMAI,EPAI,V,SIG,
     *            NMAI,NDL,NCPN,NDPN,NNPE,NSPG,NVMA,KTYP)
      CALL CALIRI(RIG,IDIA,LIAI,NLIAI,NDPN,NDL)
      CALL DECCHO(RIG,IDIA,NDL,0) 
C
      CALL DINCRE(F1,DF,NDL,1.D0)
      CALL CALIMP(LIAI,DF,NLIAI,NDPN)
      CALL RESCHO(RIG,DF,IDIA,NDL) 
COMM  ...//// CHOIX DU SENS DE PARCOURT  ////...............................
      IF(IP.EQ.1) THEN
        DLAMBDA=INCRE/DSQRT(1.D0+PROSCA(DF,DF,NDL))
      ELSE      
        SIGNE=PROSCA(DF,VAUX,NDL)
        IF(SIGNE.GT.0) THEN     
          DLAMBDA=INCRE/DSQRT(1.D0+PROSCA(DF,DF,NDL))      
        ELSE
          DLAMBDA=-INCRE/DSQRT(1.D0+PROSCA(DF,DF,NDL))
        ENDIF
      ENDIF 
COMM  ....//// STOCKAGE DU PAS TANGENT  ////...........................
      DL0=DLAMBDA     
      IF(SIGNE.GT.0) THEN     
        DO 30 I=1,NDL
 30     DV0(I)=DF(I)      
      ELSE
        DO 31 I=1,NDL
 31     DV0(I)=-DF(I)             
      ENDIF      
           
COMM ....//// STOCKAGE DU POINT DE DEPART ////.........................
      CALL SHIFTD(V,VAUX,NDL)
COMM  ...//// ACTUALISATION APRES LE PAS TANGENT ////..................
      LAMBDA=LAMBDA+DLAMBDA
      CALL DACTUA(F,F1,NDL,DLAMBDA) 
      CALL DACTUA(V,DF,NDL,DLAMBDA)
C
      CALL RESIDU(V,COOR,MAIL,EMAI,EPAI,SIG,RES,F,
     *            NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
      CALL CALIMP(LIAI,RES,NLIAI,NDPN)
C
C      CALL ECPAS(V,RES,ITER)
      CALL CRITCO2(RES,F,CRITER,NNOE,NDL,ITER,TOURNE,6)
COMM  
COMM ........../////   ITERATION DE NEWTON-RAPHSON  /////........
COMM 
      DO WHILE(TOURNE.AND.ITER.LT.NITER)
      ITER=ITER+1
C   
      CALL RIGTAN(RIG,IDIA,COOR,MAIL,EMAI,EPAI,V,SIG,
     *            NMAI,NDL,NCPN,NDPN,NNPE,NSPG,NVMA,KTYP)
      CALL CALIRI(RIG,IDIA,LIAI,NLIAI,NDPN,NDL)
      CALL DECCHO(RIG,IDIA,NDL,0)
C
      CALL CALIMP(LIAI,RES,NLIAI,NDPN)
      CALL RESCHO(RIG,RES,IDIA,NDL)
      DURI=PROSCA(DV0,RES,NDL)
C
      CALL DINCRE(F1,DF,NDL,1.D0)
      CALL CALIMP(LIAI,DF,NLIAI,NDPN)
      CALL RESCHO(RIG,DF,IDIA,NDL)      
      DUFI=PROSCA(DV0,DF,NDL)
      DLAMBDA=-(DURI/(DL0+DUFI))
C
      LAMBDA=LAMBDA+DLAMBDA
      CALL DACTUA(F,F1,NDL,DLAMBDA) 
      CALL DSOMME(V,RES,NDL)
      CALL DACTUA(V,DF,NDL,DLAMBDA)
C 
      CALL RESIDU(V,COOR,MAIL,EMAI,EPAI,SIG,RES,F,
     *            NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
      CALL CALIMP(LIAI,RES,NLIAI,NDPN)
C      
C      CALL ECPAS(V,RES,ITER)
      CALL CRITCO2(RES,F,CRITER,NNOE,NDL,ITER,TOURNE,6)
C
      ENDDO
C      
c affichage     CALL IMPNLG(LAMBDA,V,SIG,MAIL,IP)
      DO 20 I=1,NDL
 20   VAUX(I)=V(I)-VAUX(I)
C      
 100  FORMAT(//,80(1H-),//,' ON FAIT LE PAS ',I3,//)
      END 
            
      SUBROUTINE CRITCO(RES,CRITER,NNOE,NDL,ITER,TOURNE,Nfich)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  TEST DE CONVERGENCE                                            %
COMM %  SI CHAQUE COMPOSANTE DE RU .LT. CRIT   ON A CONVERGE           %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION RES(1),CRITER 
      INTEGER Nfich
      LOGICAL TOURNE
      TOURNE=.FALSE. 
      DO 1 I=1,NDL
 1    IF(DABS(RES(I)).GT.CRITER) TOURNE=.TRUE. 
      IF(.NOT.TOURNE)  WRITE(Nfich,100) ITER
 100  FORMAT( //' CONVERGENCE  EN ',I3,' ITERATIONS'//)
      IF(ITER.EQ.15) WRITE(Nfich,101)
 101  FORMAT( //' PAS DE CONVERGENCE  EN 15 ITERATIONS'//)      
      END
      SUBROUTINE CRITCO2(RES,F,CRITER,NNOE,NDL,ITER,TOURNE,Nfich)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  TEST DE CONVERGENCE                                            %
COMM %  SI DABS(RESmax)/NORME(F)  .LT. CRIT    ON A CONVERGE           %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      DOUBLE PRECISION RES(NDL),F(NDL),CRITER 
      INTEGER Nfich
      LOGICAL TOURNE
      TOURNE=.FALSE.
      XNORM_R=0.D0
      DO 1 I=1,NDL
 1    IF(DABS(RES(I)).GT.XNORM_R) XNORM_R=DABS(RES(I))
      XNORM_F=DSQRT(PROSCA(F,F,NDL))
      XNORM_R=XNORM_R/XNORM_F
      IF(XNORM_R.GT.CRITER) TOURNE=.TRUE. 
      IF(.NOT.TOURNE)  WRITE(Nfich,100) ITER
 100  FORMAT( //' CONVERGENCE  EN ',I3,' ITERATIONS'//)
      IF(ITER.EQ.15) WRITE(Nfich,101)
 101  FORMAT( //' PAS DE CONVERGENCE  EN 15 ITERATIONS'//)      
      END         
COMM ...........................////////////...........................
COMM ...........////// FIN DU MODULE PILOTE DE CALCUL //////...........
COMM ...........//////   NON-LINEAIRE GEOMETRIQUE     //////...........
COMM ...........................////////////...........................
