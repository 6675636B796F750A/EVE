      SUBROUTINE CVORD1_PLAS2(C,V,VRES,XL,NDL,IP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 % 
COMM %  ON CALCULE LE COEFFICIENT  C(1) ET LE VECTEUR V(1)             %
COMM %                                                                 %    
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION C(1),V(NDL,1),VRES(NDL)
COMM    
C      C(1)=DSQRT((XL*XL)/(1.D0+PROSCA(V(1,1),V(1,1),NDL))) 
C       C(1)=DSQRT((XL*XL)/(PROSCA(V(1,1),V(1,1),NDL))) 
C     C(1)=1.D0
      A=PROSCA(V(1,1),V(1,1),NDL)
      B=PROSCA(V(1,1),VRES,NDL)
      CC=PROSCA(VRES,VRES,NDL)

c      IF(IP.GE.12.AND.IP.LE.30)THEN
c      C(1)=(-B-DSQRT(B*B-A*(CC-XL**2)))/A
c      ELSE
c      C(1)=(-B+DSQRT(B*B-A*(CC-XL**2)))/A
c      ENDIF

      C(1)=(-B+DSQRT(B*B-A*(CC-XL**2)))/A
      DO 1 I=1,NDL
 1    V(I,1)=V(I,1)*C(1)+VRES(I)
      END
       
      SUBROUTINE CVORDP_PLAS2(C,V,VRES,XL,NDL,IORDRE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 % 
COMM %  ON CALCULE LE COEFFICIENT  C(p) ET ON TERMINE LE VECTEUR V(p)  %
COMM %                                                                 %    
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION C(1),V(NDL,1),VRES(NDL)
COMM
COMM ....//// a={u,v1} +  l*C(1) /{v1,v1} + C(1)*C(1) /////..............
COMM
C     C(IORDRE)=-PROSCA(V(1,IORDRE),V(1,1),NDL)*C(1)/(XL*XL)
      C(IORDRE)=-PROSCA(V(1,IORDRE),V(1,1),NDL)*C(1)/
     &           (PROSCA(V(1,1),V(1,1),NDL)-PROSCA(VRES,V(1,1),NDL))
C     C(IORDRE)=0.D0
COMM  
COMM ..../////   calcul de V(p) //////...................................
COMM	
      COEF=C(IORDRE)/C(1)
      DO 20 I=1,NDL
 20   V(I,IORDRE)=V(I,IORDRE)+(V(I,1)-VRES(I))*COEF
      END 



      SUBROUTINE CVORD_CHARGEV(C,X,Y,X0,Y0,Cm,Tm,IORDRE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 % 
COMM %  ON CALCULE LES COEFFICIENTS C(I) POUR LE CAS DU CHARGEMENT     %
COMM %   EN V                                                          %    
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION C(1),X(1),Y(1)
COMM    
      ALPHA=Cm*(Y0-X0)/(Tm*(X0+Y0))  
      BETA=10.D0
      IF(IORDRE.EQ.1)THEN
              T1=1.D0/BETA
      C(1)=ALPHA*T1  
      X(1)=C(1)-(Cm/Tm)*T1
      Y(1)=C(1)+(Cm/Tm)*T1
      ELSE   !cas IORDRE > 1
      SOMk=0.D0
      DO 1 IR=1,IORDRE-1
      SOMk=SOMk+X(IR)*Y(IORDRE-IR)
 1    CONTINUE
              T1=1.D0/BETA
      C(1)=ALPHA*T1  

      C(IORDRE)=-SOMk/(X0+Y0)
      X(IORDRE)=C(IORDRE)
      Y(IORDRE)=C(IORDRE)
      ENDIF
      END
      
      SUBROUTINE CHARGEV_INIT(C0,X0,Y0,Cm,Tm,ETA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 % 
COMM %  ON CALCULE LES COEFFICIENTS C(I) POUR LE CAS DU CHARGEMENT     %
COMM %   EN V   ETA: REGULARISATION DE LA CHARGE = 1.E-2               %    
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

       t0=Tm*(1.D0-DSQRT(1.D0-ETA))
       C0=0.D0
       X0=C0-Cm*t0/Tm
       Y0=C0-Cm*(2.D0-t0/Tm)
      END



      SUBROUTINE VALEUR_ATTEINTE(V,NDL,NDPN,Nod,ValMax,IDOF,Num,Depl) 
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 % 
COMM % ON VERIFIE SI LA ValMax DU NOEUD Nod DS LA DIR DOF EST ATTEINTE %
COMM %                                                                 %    
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION V(NDL),Depl
      Num=0
      Depl=V(NDPN*(Nod-1)+IDOF)      
      IF (DABS(Depl).GT.ValMax) THEN
      Num=1
      ENDIF
      WRITE(6,*)'les val  Nod,ValMax,IDOF,Num =',Nod,ValMax,IDOF,Num
      WRITE(6,*)' Le deplacement atteint est = ',Depl
      END  

