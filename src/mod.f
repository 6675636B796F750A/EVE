COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                 EVE  Version  2.1  (Novembre 1994)              %
COMM %                                                                 %
COMM %          //////    MODULE PILOTE DE CALCULS    //////           %
COMM %          //////  DE DYNAMIQUE LINEAIRE PAR LA  //////           %
COMM %          //////  TECHNIQUE DE SYNTHESE MODALE  //////           %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE MODALE(IDIMT)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %          CALCUL DE DYNAMIQUE LINEAIRE : ANALYSE MODALE          %
COMM %                                                                 %
COMM %   Organisation de la memoire  (Super-Tableau T + pointeurs IT)  %
COMM %   --------------------------                                    %
COMM %                                                                 %
COMM %      adresse              contenu de T                 type     %
COMM %                                                                 %
COMM %   IT(11)---IDIA       Pointeurs de la diagonale       (ENTIER)  %
COMM %   IT(12)---F          Vecteur de charge donne         (DP)      %
COMM %   IT(13)---F1         Vecteur de charge a T+DT        (DP)      % 
COMM %   IT(14)---RIG        Matrice de rigidite             (DP)      %
COMM %   IT(15)---RMA        Matrice de masse                (DP)      %
COMM %   IT(16)---AMO        Matrice d'amortissement         (DP)      %
COMM %   IT(17)---VEP        Vecteurs propres                (DP)      %
COMM %   IT(18)---VAP        Valeurs propres                 (DP)      % 
COMM %   IT(19)---Q          Inconnues modales a T           (DP)      %
COMM %   IT(20)---QP         Inconnues modales a T           (DP)      % 
COMM %   IT(21)---QPP        Inconnues modales a T           (DP)      %
COMM %   IT(22)---Q1         Inconnues modales a T+DT        (DP)      %
COMM %   IT(23)---QP1        Inconnues modales a T+DT        (DP)      % 
COMM %   IT(24)---QPP1       Inconnues modales a T+DT        (DP)      %
COMM %   IT(25)---W1         Vecteur ou matrice de travail   (DP)      %
COMM %   IT(26)---W2         Vecteur ou matrice de travail   (DP)      %
COMM %   IT(27)---W3         Vecteur ou matrice de travail   (DP)      % 
COMM %   IT(28)---W4         Vecteur ou matrice de travail   (DP)      %
COMM %   IT(29)---W5         Vecteur ou matrice de travail   (DP)      %
COMM %   IT(30)---W6         Vecteur ou matrice de travail   (DP)      %
COMM %   IT(31)---W7         Vecteur ou matrice de travail   (DP)      %
COMM %   IT(32)---W8         Vecteur ou matrice de travail   (DP)      %
COMM %   IT(33)---W9         Vecteur ou matrice de travail   (DP)      %
COMM %   IT(34)---W10        Vecteur ou matrice de travail   (DP)      %
COMM %   IT(35)---W11        Vecteur ou matrice de travail   (DP)      %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM
      DOUBLE PRECISION ALPHA,BETA,ABSI(100),ORDO(100),RTOL
      CHARACTER*4 RMOT
      COMMON T(1)
      COMMON/INI/ICO(50),IT(50)
COMM
COMM .../// RECUPERATIONS DE DONNEES DIVERS ///.......................
COMM    
      KTYP =ICO(1)
      NCPN =ICO(2)
      NDPN =ICO(3)
      NNPE =ICO(4)
      NDL  =ICO(7)
      NMAI =ICO(8)
      NVMA =ICO(10)
      NLIAI=ICO(11)
COMM
COMM .../// LECTURE DU NOMBRE DE MODES A CALCULER ///...............
COMM
      READ(5,*) NMODE
      READ(5,*) NITEM,IFSS
      NC =MIN(2*NMODE,NMODE+8)
      NNC=NC*(NC+1)/2
COMM
COMM .../// RESERVATION DES PLACES DANS LE SUPER-TABLEAU ///..........
COMM
      IT(12)=IT(11)+NDL
      IT(13)=IT(12)+2*NDL
      IT(14)=IT(13)+2*NDL      
      CALL PROFBI(T(IT(11)),T(IT(2)),NDL,NMAI,NRIG,NNPE,NDPN,KTYP)
      ICO(26)=NRIG
      IT(15)=IT(14)+2*NRIG
      IT(16)=IT(15)+2*NRIG
      IT(17)=IT(16)+2*NRIG
      IT(18)=IT(17)+2*NDL*NC
      IT(19)=IT(18)+2*NC  
      IT(20)=IT(19)+2*NDL
      IT(21)=IT(20)+2*NDL
      IT(22)=IT(21)+2*NDL
      IT(23)=IT(22)+2*NDL
      IT(24)=IT(23)+2*NDL
      IT(25)=IT(24)+2*NDL
      IT(26)=IT(25)+2*NDL
      IT(27)=IT(26)+2*NDL
      IT(28)=IT(27)+2*NNC
      IT(29)=IT(28)+2*NNC
      IT(30)=IT(29)+2*NC*NC
      IT(31)=IT(30)+2*NC
      IT(32)=IT(31)+2*NC
      IT(33)=IT(32)+2*NDL*NC
      IT(34)=IT(33)+2*NC      
      IT(35)=IT(34)+2*NC
      IT(36)=IT(35)+2*NC
COMM
COMM .../// IMPRESSION DE L'ORGANISATION DU SUPER-TABLEAU ///.........
COMM
      WRITE(6,150)       
      WRITE(6,100)       
      DO 1 I=1,35
  1    WRITE(6,120) I,IT(I),IT(I+1)-IT(I)
      WRITE(6,130) IT(36),IDIMT
      WRITE(6,110) 
      IF(IT(36).GT.IDIMT) STOP 'TAILLE DU SUPER-TABLEAU'
COMM
COMM .../// LECTURE DU MOT CLE AMORTISSEMENT ///......................    
COMM
      READ(5,'(A4)')  RMOT
      IF(RMOT.EQ.'AMOR') THEN
       READ(5,*) ALPHA,BETA
      ELSE
       CALL ERROR1
      ENDIF
COMM
COMM .../// LECTURE DU MOT CLE VARIATION DE CHARGE ///................
COMM
      READ(5,'(A4)')  RMOT
      IF(RMOT.EQ.'VARI') THEN
       READ(5,*) NPOINT
       DO 2 I=1,NPOINT
 2      READ(5,*) ABSI(I),ORDO(I)
      ELSE
       CALL ERROR1
      ENDIF       
COMM
COMM .../// ASSEMBLAGE DU VECTEUR DE CHARGE ///.......................
COMM    
      CALL CAFORC(T(IT(6)),T(IT(12)))
COMM            
COMM .../// ASSEMBLAGE DE LA MATRICE DE RIGIDITE ///..................
COMM
      CALL RIGELA(T(IT(14)),T(IT(11)),T(IT(1)),T(IT(2)),T(IT(3)),
     *            T(IT(4)),NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP) 
COMM            
COMM .../// ASSEMBLAGE DE LA MATRICE DE MASSE COHERENTE ///...........
COMM
      CALL MASCOE(T(IT(15)),T(IT(11)),T(IT(1)),T(IT(2)),T(IT(3)),
     *            T(IT(4)),NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
COMM
COMM .../// CALCUL DES VALEURS ET DES MODES PROPRES ///...............
COMM
      RTOL=1.0D-6
      CALL SSPACE(T(IT(11)),T(IT(14)),T(IT(15)),T(IT(17)),T(IT(18)),
     *            T(IT(25)),T(IT(26)),T(IT(27)),T(IT(28)),T(IT(29)),
     *            T(IT(30)),T(IT(31)),T(IT(32)),T(IT(33)),T(IT(34)),
     *            T(IT(35)),T(IT(5)),NLIAI,NDPN,NDL,NRIG,NRIG,
     *            NMODE,NC,NNC,NITEM,IFSS,RTOL)
COMM
COMM .../// IMPRESSION DES VALEURS ET DES MODES PROPRES ///...........
COMM
      CALL IMPVIB(T(IT(17)),T(IT(18)),NMODE,NDL)
COMM
COMM .../// CALCUL DU VECTEUR DE CHARGE REDUIT.ON UTLISE COMME  ///...
COMM .../// VECTEUR INTERMEDIAIRE LES VECTEURS Q ET QP CAR ILS  ///...
COMM .../// ONT UNE TAILLE COMPATIBLE AVEC CELLE DES VECTEURS   ///...
COMM .../// INTERMEDIAIRES QU'UTILISE LA SUBROUTINE VECT-REDUIT ///...
COMM
      CALL VECT_REDUIT(T(IT(17)),NMODE,T(IT(12)),T(IT(19)),
     *                 T(IT(20)),NDL)
COMM
COMM .../// CALCUL DE LA MATRICE DE RIGIDITE REDUITE.ON UTILISE  ///..
COMM .../// COMME VECTEUR INTERMEDIAIRE LES VECTEURS Q,QP ET QPP ///..
COMM .../// CAR ILS ONT UNE TAILLE COMPATIBLE AVEC CELLE DES     ///..
COMM .../// VECTEURS INTERMEDIAIRES QU'UTILISE LA SUBROUTINE     ///..
COMM .../// MAT-REDUIT                                           ///..
COMM
      CALL MAT_REDUIT(T(IT(11)),T(IT(17)),NMODE,T(IT(14)),NRIG,
     *                T(IT(19)),T(IT(20)),T(IT(21)),NDL)
COMM
COMM .../// CALCUL DE LA MATRICE DE MASSE REDUITE.ON UTILISE     ///.. 
COMM .../// COMME VECTEUR INTERMEDIAIRE LES VECTEURS Q,QP ET QPP ///..
COMM .../// CAR ILS ONT UNE TAILLE COMPATIBLE AVEC CELLE DES     ///..
COMM .../// VECTEURS INTERMEDIAIRES QU'UTILISE LA SUBROUTINE     ///..
COMM .../// MAT-REDUIT                                           ///..
COMM
      CALL MAT_REDUIT(T(IT(11)),T(IT(17)),NMODE,T(IT(15)),NRIG,
     *                T(IT(19)),T(IT(20)),T(IT(21)),NDL)
COMM
COMM .../// AMORTISSEMENT DE RAYLEIGH : C=ALPHA*K+BETA*M ///..........
COMM
      CALL  DZERO (T(IT(16)),NRIG) 
      CALL  DACTUA(T(IT(16)),T(IT(14)),NRIG,ALPHA)  
      CALL  DACTUA(T(IT(16)),T(IT(15)),NRIG,BETA)
COMM
COMM .../// INITIALISATION DE Q,QP,QPP A 0.CONDITIONS ///.............
COMM .../// INITIALES NULLES                          ///.............
COMM
      CALL DZERO(T(IT(19)),NDL)
      CALL DZERO(T(IT(20)),NDL)
      CALL DZERO(T(IT(21)),NDL)
COMM
COMM .../// LECTURE DU MOT CLE NEWMARK ///............................
COMM
      READ(5,'(A4)')  RMOT
      IF(RMOT.EQ.'NEWM') THEN
       CALL INTEG_NEWM(T(IT(12)),T(IT(14)),T(IT(15)),T(IT(16)),
     *                 T(IT(19)),T(IT(20)),T(IT(21)),NDL,NMODE,
     *                 T(IT(17)),NPOINT,ABSI,ORDO,T(IT(13)),
     *                 T(IT(22)),T(IT(23)),T(IT(24)))
      ELSE
       CALL ERROR1
      ENDIF
COMM
COMM .../// DIFFERENTS FORMATS POUR LES IMPRESSIONS ///...............
COMM
 150  FORMAT(//////,
     * '  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%',/,
     * '  %                                                       %',/,
     * '  %     CALCUL DE DYNAMIQUE LINEAIRE : ANALYSE MODALE     %',/,
     * '  %                                                       %',/,
     * '  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
 100  FORMAT(//,' ',59('%'),///,
     * '  ORGANISATION DU SUPER-TABLEAU T() ',//)
 110  FORMAT(' ',59('%'),/////)
 120  FORMAT('   IT(',I3,')=',I8,'     TAILLE=',I8)
 130  FORMAT(///,'   TAILLE MEMOIRE NECESSAIRE  : ',I8,/,
     * '   DIMENSION DU SUPER-TABLEAU : ',I8,//)  
      END



      SUBROUTINE VECT_REDUIT(RMODE,NMODE,FORC,VINT1,VINT2,NDL)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %     DETERMINE LE VECTEUR DE CHARGE REDUIT PAR L'OPERATION :     %
COMM %                 FORC=TRMODE*FORC                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RMODE(1),FORC(1),VINT1(1),VINT2(1)
COMM .../// VINT1=RMODE(I) ET VINT2(I)=VINT1.FORC AVEC I=1,NMODE ///..
      CALL DZERO(VINT2,NDL)
      IAD=0
      DO 1 IMODE=1,NMODE
       CALL DZERO(VINT1,NDL)
       DO 2 IDL=1,NDL
        VINT1(IDL)=RMODE(IAD+IDL)
 2     CONTINUE
       VINT2(IMODE)=PROSCA(VINT1,FORC,NDL)
       IAD=IAD+NDL
 1    CONTINUE
COMM .../// REMET LE RESULTAT DANS LE VECTEUR FORC ///................
      CALL DZERO(FORC,NDL)
      CALL SHIFTD(VINT2,FORC,NMODE)
      END

      SUBROUTINE MAT_REDUIT(IDIA,RMODE,NMODE,RMAT,NRIG,VINT1,VINT2,
     *                      VINT3,NDL)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %      DETERMINE LA MATRICE DE MASSE OU DE RIGIDITE REDUITE       %
COMM %      PAR L'OPERATION :                                          %
COMM %                  RMAT=TRMODE*RMAT*RMODE                         %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION IDIA(1),RMODE(1),RMAT(1),VINT1(1),VINT2(1),VINT3(1)
COMM .../// VINT2=RMODE(I) ET VINT3(I)=VINT2.(RMAT*VINT2) ///.........
COMM .../// AVEC I=1,NMODE                                ///.........
      CALL DZERO(VINT3,NDL)
      IAD=0
      DO 1 IMODE=1,NMODE
       CALL DZERO(VINT1,NDL)
       CALL DZERO(VINT2,NDL)
       DO 2 IDL=1,NDL
        VINT2(IDL)=RMODE(IAD+IDL)
 2     CONTINUE
       CALL MULTMATVEC(RMAT,VINT2,VINT1,IDIA,NDL)
       VINT3(IMODE)=PROSCA(VINT1,VINT2,NDL)
       IAD=IAD+NDL
 1    CONTINUE
COMM .../// REMET LE RESULTAT DANS LA MATRICE ///.....................
      CALL DZERO(RMAT,NRIG)
      CALL SHIFTD(VINT3,RMAT,NMODE)
      END



      SUBROUTINE INTEG_NEWM(F,RIG,RMA,AMO,Q,QP,QPP,NDL,NMODE,RMODE,
     *                      NPOINT,ABSI,ORDO,F1,Q1,QP1,QPP1)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %          RESOLUTION DES N (=NMODE) EQUATIONS MODALES :          %
COMM %                      M*QPPI+C*QPI+K*QI=FI                       %
COMM %       A L'AIDE D'UN SCHEMA D'INTEGRATION DE TYPE NEWMARK        %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION LAMBDA,ABSI(100),ORDO(100)
      DIMENSION F(1),RIG(1),RMA(1),AMO(1),Q(1),QP(1),QPP(1),RMODE(1),
     *          F1(1),Q1(1),QP1(1),QPP1(1)
COMM
COMM .../// LECTURE DES PARAMETRES DU SCHEMA D'INTEGRATION ///........
COMM
      READ(5,*) A,B
COMM
COMM .../// NOMBRES DE SEQUENCES A PAS CONSTANT (20 MAXI) ///.........
COMM
      READ(5,*) NSEQ
COMM
COMM .../// BOUCLE SUR LES SEQUENCES ///..............................
COMM
      TEMPS=0.D0
      DO 1 ISEQ=1,NSEQ
       READ(5,*) NPAS,DT
COMM
COMM .../// DEFINITION DE QUELQUES CONSTANTES ///.....................
COMM
       A0=1.D0/(A*DT*DT)
       A1=1.D0/(A*DT)
       A2=1.D0/(2.D0*A)-1.D0
       A3=B/(A*DT)
       A4=B/A-1.D0
       A5=DT*(B/(2.D0*A)-1.D0)
COMM
COMM .../// BOUCLE SUR LES PAS DE TEMPS ///...........................
COMM
       DO 2 IPAS=1,NPAS
        TEMPS=TEMPS+DT
COMM
COMM .../// CHARGE A T+DT ///.........................................
COMM
        CALL DZERO(F1,NDL)
        CALL INTLAM(NPOINT,ABSI,ORDO,TEMPS,LAMBDA)
        CALL DACTUA(F1,F,NMODE,LAMBDA)
COMM
COMM .../// BOUCLE SUR LES MODES ///..................................
COMM
        CALL DZERO(Q1,NDL)
        CALL DZERO(QP1,NDL)
        CALL DZERO(QPP1,NDL)
        DO 3 IMODE=1,NMODE
COMM
COMM .../// INCONNUE MODALE QI A T+DT ///.............................
COMM
         Q1(IMODE)=(F1(IMODE)+(A0*RMA(IMODE)+A3*AMO(IMODE))*Q(IMODE)+
     *              (A1*RMA(IMODE)+A4*AMO(IMODE))*QP(IMODE)+
     *              (A2*RMA(IMODE)+A5*AMO(IMODE))*QPP(IMODE))/
     *             (A0*RMA(IMODE)+A3*AMO(IMODE)+RIG(IMODE))
COMM
COMM .../// INCONNUE MODALE QPI A T+DT ///............................
COMM
         QP1(IMODE)=A3*(Q1(IMODE)-Q(IMODE))-A4*QP(IMODE)-A5*QPP(IMODE)
COMM
COMM .../// INCONNUE MODALE QPPI A T+DT ///...........................
COMM
         QPP1(IMODE)=A0*(Q1(IMODE)-Q(IMODE))-A1*QP(IMODE)-A2*QPP(IMODE)
 3      CONTINUE
COMM
COMM .../// DETERMINATION DES DEPLACEMENTS A T+DT.ON UTLISE   ///.....
COMM .../// LES VECTEURS Q,QP ET QPP POUR DETERMINER LES      ///..... COMM .../// DEPLACEMENTS CAR ILS ONT LA MEME TAILLE QUE CELLE ///.....
COMM .../// DES VECTEURS DEPLACEMENTS                         ///.....
COMM
        CALL DZERO(Q,NDL)
        CALL DZERO(QP,NDL)
        CALL DZERO(QPP,NDL)
        DO 4 IDL=1,NDL
         IAD=IDL
         DO 5 IMODE=1,NMODE
          Q(IDL)=Q(IDL)+RMODE(IAD)*Q1(IMODE)
          QP(IDL)=QP(IDL)+RMODE(IAD)*QP1(IMODE)
          QPP(IDL)=QPP(IDL)+RMODE(IAD)*QPP1(IMODE)
          IAD=IAD+NDL
 5       CONTINUE
 4      CONTINUE
COMM
COMM .../// IMPRESSION DES RESULTATS POUR CHAQUE PAS DE TEMPS ///.....
COMM
        CALL IMPDYN(TEMPS,Q,QP,QPP)
COMM
COMM .../// TRANSLATION DES RESULTATS ///.............................
COMM
        CALL DZERO(Q,NDL)
        CALL DZERO(QP,NDL)
        CALL DZERO(QPP,NDL)
        CALL SHIFTD(Q1,Q,NMODE)
        CALL SHIFTD(QP1,QP,NMODE)
        CALL SHIFTD(QPP1,QPP,NMODE)
 2     CONTINUE
 1    CONTINUE
      END
      
COMM ...........................////////////...........................
COMM ...........////// FIN DU MODULE PILOTE DE CALCUL //////...........
COMM ...........//////   DYNAMIQUE EN BASE MODALE     //////...........
COMM ...........................////////////...........................

