      SUBROUTINE CALCUL1_PLAS(V,COOR,MAIL,EMAI,EPAI,
     &           SIG,SIG0,SIGQ,SIGQ0,SIGe,SIGe0,EPS,EPSP,EPSP0,
     &           EPSPe,EPSPe0,EPSP1,
     &           PN,PN0,FC,FC0,LAMBDA,GP,GP0,ZP,ZP0,XP,XP0,YP,YP0,
     &           DEN0,DEN,KTYP,NBG,NMAI,NDL,NSIG,NNPE,NCPN,NDPN,NSPG,
     &           NVMA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM                                                                   %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION KSI,ETA,LAMBDA(NMAI*NBG,1:*)
      DIMENSION V(NDL),COOR(NCPN,1),MAIL(NNPE+2,1),
     &          EMAI(NVMA,1),EPAI(1),SIG(NSIG,1:*),SIG0(NSIG),
     &          SIGQ(NBG*NMAI,1:*),SIGQ0(NBG*NMAI),
     &          SIGe(NBG*NMAI,1:*),SIGe0(NBG*NMAI),EPS(NSIG,1:*),
     &          EPSP(NSIG,1:*),EPSP0(NSIG),PN(NSIG,1:*),
     &          PN0(NSIG),FC(NMAI*NBG,1:*),FC0(NBG*NMAI),
     &          EPSPe(NBG*NMAI,1:*),EPSPe0(NBG*NMAI),
     &          GP(NMAI*NBG,1:*),GP0(NBG*NMAI),
     &          ZP(NMAI*NBG,1:*),ZP0(NBG*NMAI),
     &          XP(NMAI*NBG,1:*),XP0(NBG*NMAI),
     &          YP(NMAI*NBG,1:*),YP0(NBG*NMAI),
     &          DEN(NMAI*NBG,1:*),DEN0(NBG*NMAI),
     &          VE(16),EPSP1(NSIG),X(8),Y(8),GAUSS(2,9),
     &          POIDS(9),D(16),BL(64),IGLO(16),NO(8)
COMM

COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      IADS=1
      IAD0=1
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE LELEMENT /////.............
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)
      YG=EMAI(1,IMAT)
      PS=EMAI(2,IMAT)
      SIGY=EMAI(3,IMAT)
      hp=EMAI(4,IMAT)
      EPSc=EMAI(5,IMAT)
      BETA=10.D0
      DEPSc=EPSc/BETA
      ETA1=EMAI(6,IMAT)
      ETA2=EMAI(7,IMAT)
      ETA3=EMAI(8,IMAT)
      ETA4=EMAI(9,IMAT)

      CALL CAEL2D(ITYP,NBN,NDDLE,NBG)
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))
      DO 51 K=1,NDPN
 51   VE(NDPN*(I-1)+K)=V(NDPN*(NO(I)-1)+K)
 3    CONTINUE
COMM  ....///// ON FORME LA MATRICE DECOMPORTEMENT D (ND,ND) /////.....
      IF      (KTYP.EQ.1) THEN
      ND=3
      CALL DISODP(YG,PS,D)
      ELSE IF (KTYP.EQ.2) THEN
      ND=3
      CALL DISOCP(YG,PS,D)
      ELSE IF (KTYP.EQ.3) THEN
      ND=4
      CALL DISOAX(YG,PS,D)
      ELSE
      STOP ' SMFQ2D : pb sur KTYP '
      ENDIF
COMM

COMM
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////.............
COMM
      CALL GRIG2D(ITYP,GAUSS,POIDS)
      DO 7 IG=1,NBG
      KSI=GAUSS(1,IG)
      ETA=GAUSS(2,IG)

COMM .....//// CALCUL DE LA MATRICE BNL ET DE DETJ ( et RAY)////.......
COMM
      CALL BLINISO2D(X,Y,KSI,ETA,BL,DETJ,RAY,NDDLE,NBN,ND,ITYP,KTYP)
      
      CALL SGAU_CALCUL1(BL,D,VE,SIG(IADS,1),SIG0(IADS),
     &           EPSP(IADS,1),EPSP0(IADS),
     &           EPSPe(IAD0,1),EPSPe0(IAD0),
     &           SIGQ(IAD0,1),SIGQ0(IAD0),
     &           SIGe(IAD0,1),SIGe0(IAD0),
     &           PN(IADS,1),PN0(IADS),
     &           FC(IAD0,1),FC0(IAD0),
     &           LAMBDA(IAD0,1),EPSP1(IADS),
     &           GP(IAD0,1),GP0(IAD0),
     &           ZP(IAD0,1),ZP0(IAD0),
     &           XP(IAD0,1),XP0(IAD0),YP(IAD0,1),YP0(IAD0),
     &           DEN0(IAD0),DEN(IAD0,1),ND,NDDLE,NSIG,NMAI,
     &           NBG,YG,PS,SIGY,hp,DEPSc,ETA1,ETA2,ETA3,ETA4)
      
      IADS=IADS+NSPG
      IAD0=IAD0+1
 7    CONTINUE
 20   CONTINUE
      END
      
      SUBROUTINE SGAU_CALCUL1(BL,D,VE,SIG,SIG0,EPSP,EPSP0,
     &           EPSPe,EPSPe0,SIGQ,SIGQ0,SIGe,SIGe0,PN,PN0,
     &           FC,FC0,LAMBDA,EPSP1,GP,GP0,ZP,ZP0,XP,XP0,YP,YP0,
     &           DEN0,DEN,ND,NDDLE,NSIG,NMAI,NBG,
     &           YG,PS,SIGY,hp,DEPSc,ETA1,ETA2,ETA3,ETA4)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %    CALCUL DU SECOND MENBRE FQ EN UN POINT DE GAUSS   2D         %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION BL(64),D(ND,ND),VE(NDDLE),SIG(NSIG,1:*),
     &          SIG0(NSIG),EPSP(NSIG,1:*),EPSP0(NSIG),
     &          SIGQ(NBG*NMAI,1:*),SIGe(NBG*NMAI,1:*),
     &          EPSPe(NBG*NMAI,1:*),EPSP1(NSIG),
     &          PN(NSIG,1:*),PN0(NSIG),
     &          FC(NBG*NMAI,1:*),GP(NBG*NMAI,1:*),
     &          ZP(NBG*NMAI,1:*),
     &          XP(NBG*NMAI,1:*),YP(NBG*NMAI,1:*),DEN(NBG*NMAI,1:*)
      DOUBLE PRECISION LAMBDA(NBG*NMAI,1:*),SIG_I(3,3),SIGD(3),
     &          EPS(3),SIGD0(3)
COMM   


      IORDRE=1
      CALL MAT_SIGMA_I(SIG_I,ND)
      RMU2=YG/(1.D0+PS)
      CALL MATVEC(BL,VE,EPS,ND,NDDLE)
      CALL MATVECSI(D,EPS,SIG(1,IORDRE),ND,ND)
      CALL MATVEC(SIG_I,SIG(1,IORDRE),SIGD,ND,ND)

      DO I=1,ND
      PN(I,IORDRE)=1.5D0*SIGD(I)/SIGQ0
      EPSP(I,IORDRE)=0.D0
      ENDDO
                
      LAMBDA(1,IORDRE)=DEPSc*GP0*YP0
      EPSPe(1,IORDRE)=LAMBDA(1,IORDRE)!0.D0
      SIGe(1,IORDRE)=hp*EPSPe(1,IORDRE)
      SIGQ(1,IORDRE)=0.D0      
      FC(1,IORDRE)=(SIGQ(1,IORDRE)-SIGe(1,IORDRE)*(1.D0+FC0))/SIGe0

      ZP(1,IORDRE)=2.D0*FC0*FC(1,IORDRE)  
      
      DEN(1,IORDRE)=((SIGe(1,IORDRE)*ZP0+ZP(1,IORDRE)*
     &              SIGe0)+ETA3*hp*FC(1,IORDRE))/RMU2
      GP(1,IORDRE)=-GP0*DEN(1,IORDRE)/DEN0

      END


      SUBROUTINE CALCULP_LOI3_2D(V,COOR,MAIL,EMAI,EPAI,
     &           SIG,SIG0,SIGQ,SIGQ0,EPS,EPSP,EPSPe,EPSPe0,PN,
     &           PN0,FC,FC0,LAMBDA,GP,GP0,ZP,ZP0,XP,XP0,YP,YP0,
     &           SIGe,SIGe0,DEN0,DEN,
     &           KTYP,NBG,NMAI,NDL,NSIG,NNPE,NCPN,NDPN,NSPG,
     &           NVMA,IORDRE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %   ASSEMBLAGE DU SECOND MEMBRE ET CALCUL DES CONTRAINTES         %
COMM %                       A L'ORDRE   IORDRE                        %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION KSI,ETA,LAMBDA(NMAI*NBG,1:*)
      DIMENSION V(NDL,1:*),COOR(NCPN,1),MAIL(NNPE+2,1),
     &          EMAI(NVMA,1),EPAI(1),SIG(NSIG,1:*),SIG0(NSIG),
     &          SIGQ(NBG*NMAI,1:*),SIGQ0(NBG*NMAI),EPS(NSIG,1:*),
     &          SIGe(NBG*NMAI,1:*),SIGe0(NBG*NMAI),EPSP(NSIG,1:*),
     &          PN(NSIG,1:*),PN0(NSIG),
     &          FC(NMAI*NBG,1:*),FC0(NBG*NMAI),
     &          GP(NMAI*NBG,1:*),GP0(NBG*NMAI),
     &          ZP(NMAI*NBG,1:*),ZP0(NBG*NMAI),
     &          EPSPe(NMAI*NBG,1:*),EPSPe0(NBG*NMAI),
     &          XP(NMAI*NBG,1:*),XP0(NBG*NMAI),
     &          YP(NMAI*NBG,1:*),YP0(NBG*NMAI),
     &          DEN(NMAI*NBG,1:*),DEN0(NBG*NMAI),
     &          VE(16),IGLO(16),NO(8),X(8),Y(8),
     &          D(16),BL(64),GAUSS(2,9),POIDS(9)

COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      IADS=1
      IAD0=1
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////.............   
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)
      YG=EMAI(1,IMAT)
      PS=EMAI(2,IMAT)
      SIGY=EMAI(3,IMAT)
      HP=EMAI(4,IMAT)
c      DEPSc=EMAI(5,IMAT)
      EPSc=EMAI(5,IMAT)
      BETA=10.D0
      DEPSc=EPSc/BETA
      ETA1=EMAI(6,IMAT)
      ETA2=EMAI(7,IMAT)
      ETA3=EMAI(8,IMAT)
      ETA4=EMAI(9,IMAT)
      CALL CAEL2D(ITYP,NBN,NDDLE,NBG)          
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))
      DO 51 K=1,NDPN
 51   VE(NDPN*(I-1)+K)=V(NDPN*(NO(I)-1)+K,IORDRE)
 3    CONTINUE
COMM  ....///// ON FORME LA MATRICE DECOMPORTEMENT D (ND,ND) /////.....
      IF      (KTYP.EQ.1) THEN
      ND=3
      CALL DISODP(YG,PS,D)
      ELSE IF (KTYP.EQ.2) THEN
      ND=3
      CALL DISOCP(YG,PS,D)
      ELSE IF (KTYP.EQ.3) THEN
      ND=4
      CALL DISOAX(YG,PS,D)
      ELSE 
      STOP ' SMFQ2D : pb sur KTYP '
      ENDIF
      
COMM
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////.............
COMM
      CALL GRIG2D(ITYP,GAUSS,POIDS)  
      DO 7 IG=1,NBG
      KSI=GAUSS(1,IG)
      ETA=GAUSS(2,IG)
COMM
COMM .....//// CALCUL DE LA MATRICE BNL ET DE DETJ ( et RAY)////.......
COMM
      
      CALL BLINISO2D(X,Y,KSI,ETA,BL,DETJ,RAY,NDDLE,NBN,ND,ITYP,KTYP)
      
      CALL CALCULP_PLAS(BL,D,VE,SIG(IADS,1),SIG0(IADS),EPSP(IADS,1),
     &           SIGQ(IAD0,1),SIGQ0(IAD0),PN(IADS,1),PN0(IADS),
     &           FC(IAD0,1),FC0(IAD0),LAMBDA(IAD0,1),GP(IAD0,1),
     &           GP0(IAD0),ZP(IAD0,1),ZP0(IAD0),XP(IAD0,1),XP0(IAD0),
     &           YP(IAD0,1),YP0(IAD0),SIGe(IAD0,1),SIGe0(IAD0),
     &           EPSPe(IAD0,1),EPSPe0(IAD0),DEN0(IAD0),DEN(IAD0,1),
     &           ND,NDDLE,NSIG,NMAI,NBG,IORDRE,YG,PS,SIGY,HP,DEPSc,
     &           ETA1,ETA2,ETA3,ETA4,IADS,IAD0)

      IADS=IADS+NSPG
      IAD0=IAD0+1

 7    CONTINUE   
 20   CONTINUE
      END



      SUBROUTINE CALCULP_PLAS(BL,D,VE,SIG,SIG0,EPSP,SIGQ,SIGQ0,
     &           PN,PN0,FC,FC0,LAMBDA,GP,GP0,ZP,ZP0,XP,XP0,YP,YP0,
     &           SIGe,SIGe0,EPSPe,EPSPe0,DEN0,DEN,ND,NDDLE,NSIG,
     &           NMAI,NBG,
     &           IORDRE,YG,PS,SIGY,HP,DEPSc,ETA1,ETA2,ETA3,ETA4,
     &           IADS,IAD0)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %    CALCUL DU SECOND MENBRE FQ EN UN POINT DE GAUSS   '2D'       %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION BL(ND,NDDLE),D(ND,ND),VE(16),SIG(NSIG,1:*),
     &          SIG0(NSIG),EPSP(NSIG,1:*),
     &          SIGQ(NBG*NMAI,1:*),PN(NSIG,1:*),PN0(NSIG),
     &          SIGe(NBG*NMAI,1:*),EPSPe(NBG*NMAI,1:*),
     &          FC(NBG*NMAI,1:*),GP(NBG*NMAI,1:*),
     &          ZP(NBG*NMAI,1:*),XP(NBG*NMAI,1:*),YP(NBG*NMAI,1:*),
     &          DEN(NBG*NMAI,1:*)

      DOUBLE PRECISION LAMBDA(NBG*NMAI,1:*),SIG_I(3,3),DT(3,3),
     &                 SIGD(3),EPS(3)

COMM
COMM .../// on termine le calcul de SIGi,SIGQi,Fi,LAMBDAi,PNi,EPSPi a iordre-1  ///...
COMM
      CALL MAT_SIGMA_I(SIG_I,ND)
      CALL MATVEC(BL,VE,EPS,ND,NDDLE)
      CALL MAT_DT_PLAS_DECHARGE(DT,XP0,YP0,GP0,PN0,YG,PS,ND)
      CALL MATVECSI(DT,EPS,SIG(1,IORDRE),ND,ND)

C---------------------------------------------------------------------
      XI=GP0*YP0/(2.D0*YP0-XP0)
      RMU2=YG/(1.D0+PS)
      PROD1=SIG(1,IORDRE)*PN0(1)+SIG(2,IORDRE)*PN0(2)+
     &     SIG(3,IORDRE)*PN0(3)
      XP(1,IORDRE-1)=XP(1,IORDRE-1)+DBLE(IORDRE)*PROD1/
     &                      (DEPSc*RMU2*(1.D0-1.5D0*XI))
      YP(1,IORDRE-1)=YP(1,IORDRE-1)+
     &                 YP0*XP(1,IORDRE-1)/(2.D0*YP0-XP0)
      LAMBDA(1,IORDRE)=LAMBDA(1,IORDRE)+YP(1,IORDRE-1)*
     &                             GP0*DEPSc/DBLE(IORDRE)
      EPSPe(1,IORDRE)=LAMBDA(1,IORDRE)
C---------------------------------------------------------------------
      EPSP(1,IORDRE)=EPSP(1,IORDRE)+LAMBDA(1,IORDRE)*PN0(1)
      EPSP(2,IORDRE)=EPSP(2,IORDRE)+LAMBDA(1,IORDRE)*PN0(2)
      EPSP(3,IORDRE)=EPSP(3,IORDRE)+2.D0*LAMBDA(1,IORDRE)*PN0(3)

      SIGe(1,IORDRE)=HP*EPSPe(1,IORDRE)
      
      PROD2=SIGD_SIGD_2D(SIG(1,IORDRE),SIG0(1),ND)
      
      SIGQ(1,IORDRE)=SIGQ(1,IORDRE)+1.5D0*PROD2/SIGQ0
comm           ivi changement            
      FC(1,IORDRE)=FC(1,IORDRE)+(SIGQ(1,IORDRE)-SIGe(1,IORDRE)
     &             *(1.D0+FC0))/SIGe0
      ZP(1,IORDRE)=ZP(1,IORDRE)+2.D0*FC0*FC(1,IORDRE)
      
      DEN(1,IORDRE)=DEN(1,IORDRE)+((SIGe(1,IORDRE)*ZP0+ZP(1,IORDRE)*
     &              SIGe0)+ETA3*HP*FC(1,IORDRE))/RMU2

      GP(1,IORDRE)=GP(1,IORDRE)-(GP0*DEN(1,IORDRE))/DEN0

      CALL MATVEC(SIG_I,SIG(1,IORDRE),SIGD,ND,ND)
      DO I=1,ND
      PN(I,IORDRE)=PN(I,IORDRE)+
     &  (1.5D0*SIGD(I)-SIGQ(1,IORDRE)*PN0(I))/SIGQ0
      ENDDO

      END


