COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %       //////  MODULE REGROUPANT LES SUBROUTINES   //////        %
COMM %       //////             UTILITAIRES              //////        %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
      SUBROUTINE TBDB(B,D,DB,ARGL,ND,NDDLE,ITS)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %    CALCUL LE PRODUIT TB*D*B ---> STOCKE DANS ARGL              %
COMM %     ARGL : MATRICE TRIANGULAIRE SUPERIEURE STOCKEE PAR LIGNE   %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION ARGL(ITS),B(ND,NDDLE),D(ND,ND),DB(ND,NDDLE)
COMM
      DO 1 I=1,ND  
      DO 1 J=1,NDDLE
      DB(I,J)=0.D0
      DO 1 K=1,ND
 1    DB(I,J)=DB(I,J)+D(I,K)*B(K,J)
 
      L=1
      DO 6 I=1,NDDLE 
      DO 7 J=I,NDDLE
      ARGL(L)=0.D0
        DO 8 K=1,ND
        ARGL(L)=ARGL(L)+B(K,I)*DB(K,J)
 8      CONTINUE
        L=L+1
 7    CONTINUE
 6    CONTINUE
      END 
      
      SUBROUTINE TNN(N,ARGL,ND,NDDLE,ITS)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %    CALCUL LE PRODUIT TN*N   ---> STOCKE DANS ARGL              %
COMM %     ARGL : MATRICE TRIANGULAIRE SUPERIEURE STOCKEE PAR LIGNE   %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION ARGL(ITS),N(ND,NDDLE)
COMM
      L=1
      DO 6 I=1,NDDLE 
      DO 7 J=I,NDDLE
      ARGL(L)=0.D0
        DO 8 K=1,ND
        ARGL(L)=ARGL(L)+N(K,I)*N(K,J)
 8      CONTINUE
        L=L+1
 7    CONTINUE
 6    CONTINUE
      END 

      SUBROUTINE MATVEC(A,B,C,N1,N2)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %  EFFECTUE LE PRODUIT  C=A.B   A(N1,N2) ; B(N2) ; C(N1)         %  
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION A(N1,N2),B(N2),C(N1)
      DO 1 I=1,N1
      C(I)=0.D0
      DO 1 J=1,N2
 1    C(I)=C(I)+A(I,J)*B(J)      
      END
      
      SUBROUTINE TMATVEC(A,B,C,N1,N2)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                         T                                      %
COMM %  EFFECTUE LE PRODUIT  C= A.B   A(N1,N2) ; B(N1) ; C(N2)        %  
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION A(N1,N2),B(N1),C(N2)
      DO 1 I=1,N2
      C(I)=0.D0
      DO 1 J=1,N1
 1    C(I)=C(I)+A(J,I)*B(J)      
      END
      
      SUBROUTINE MATMAT(A,B,C,N1,N2,N3)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %  EFFECTUE LE PRODUIT  C=A.B   A(N1,N2) ; B(N2,N3) ; C(N1,N3)   %  
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION A(N1,N2),B(N2,N3),C(N1,N3)
      DO 1 I=1,N1
      DO 1 J=1,N3
      C(I,J)=0.D0
      DO 1 K=1,N2
 1    C(I,J)=C(I,J)+A(I,K)*B(K,J)      
      END
      
      SUBROUTINE MATLIG(A,B,N)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %  ON STOCKE LA MATRICE CARRE SYMETRIQUE A(N,N) SOUS FORME       % 
COMM %  DE LIGNE B( )                                                 %  
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION A(N,N),B(1)
      K=1      
      DO 1 I=1,N
      DO 1 J=I,N
      B(K)=A(I,J)
1     K=K+1
      END
      
      SUBROUTINE LIGMAT(B,A,N)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %  ON REFORME LA MATRICE CARRE SYMETRIQUE A(N,N) A PARTIR        % 
COMM %  DU VECTEUR B QUI CONTIENT SES LIGNES                          %  
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION A(N,N),B(1)
      K=1      
      DO 1 I=1,N
      DO 1 J=I,N
      A(I,J)=B(K)
      A(J,I)=B(K)
1     K=K+1
      END 
                          
      SUBROUTINE DSOMME(A,B,N)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %                 EFFECTUE LE CALCUL  A=A+B                      %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION A(1),B(1)
      DO 1 I=1,N
 1    A(I)=A(I)+B(I)
      END
      
      SUBROUTINE DDIFFE(A,B,N)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %                 EFFECTUE LE CALCUL  A=A-B                      %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION A(1),B(1)
      DO 1 I=1,N
 1    A(I)=A(I)-B(I)
      END
      
      SUBROUTINE DINCRE(A,B,N,C)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %                 EFFECTUE LE CALCUL  B(I) = A(I)*C              %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION A(1),B(1),C
      DO 1 I=1,N
 1    B(I)=A(I)*C
      END
      
      SUBROUTINE DACTUA(A,B,N,C)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %                 EFFECTUE LE CALCUL  A(I) = A(I) + B(I)*C       %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION A(1),B(1),C
      DO 1 I=1,N
 1    A(I)=A(I)+B(I)*C
      END
            
      SUBROUTINE DZERO(A,N)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %                MISE A ZERO DE A(I),I=1,N                       %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION A(1)
      DO 1 I=1,N
 1    A(I)=0.D0
      END
      SUBROUTINE AFFICHEVEC(V,NDL)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   On recupere la Ieme composante du vecteur V                  % 
COMM %   IN --> Numero du noeud   ID --> de 1 a 6 numero du d.d.l.    %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION V(1)
      DO I=1,NDL
      WRITE(*,*)I,V(I)
      ENDDO
      END 
     
      SUBROUTINE COMPOS(V,IN,ID,NDPN,COMP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   On recupere la Ieme composante du vecteur V                  % 
COMM %   IN --> Numero du noeud   ID --> de 1 a 6 numero du d.d.l.    %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION V(1),COMP
      COMP=V((IN-1)*NDPN+ID)
      END 
      SUBROUTINE COMPOS_SIG(SIG,INMAI,IG,ID,NSPG,NBG,COMP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   On recupere la Ieme composante du vecteur V                  % 
COMM %   INMAI --> Numero de l element  ID -->Numeros de la composante%
COMM %    IG--> Numeros du point de GAUSS                             %
COMM %    NSPG--> Nbre de contraintes par point de Gauss              %
COMM %    NBG--> Nbre de points de Gauss par element                  %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION SIG(1),COMP
      COMP=SIG((INMAI-1)*NBG*NSPG+(IG-1)*NSPG+ID)
      END 
                  
      FUNCTION CALMAX(X,N)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %     CALCUL DE LA COMPOSANTE MAXIMUN DE ABS(X(I)) ,I=1,N        %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION X(N),CALMAX,R
      CALMAX=0.D0
      DO 1 I=1,N
      R=DABS(X(I))
      IF(R.GT.CALMAX) CALMAX=R
 1    CONTINUE
      END

      SUBROUTINE PROFBI(IDIA,MAIL,NDL,NMAI,NRIG,NNPE,NDPN,KTYP,NBG)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %     DETERMINATION DU PROFIL DE LA MATRICE DE RIGIDITE          %
COMM %                                                                %
COMM % (ON NE TIENT PAS COMPTE DES CONDITIONS AUX LIMITES)            %
COMM %                                                                %
COMM %  IDIA  TABLEAU POINTEUR DE LA DIAGONALE                        %
COMM %  NDL   NOMBRE DE DEGRES DE LIBERTE                             %
COMM %  IDIA(NDL) DONNE LA DIMENSION DU VECTEUR STOCKANT LA MATRICE   %
COMM %  DE RIGIDITE                                                   %
COMM %                                                                %
COMM %  ALGORITHME : DAND UN PREMIER TEMPS ON CALCULE LE NOMBRE DE    %
COMM %  TERME NUL AU DESSUS DU DERNIER TERME NON NUL DE  CHAQUE       %
COMM %  COLONE DE LA MATRICE,  PUIS ON CONSTRUIT LE TABLEAU IDIA      %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DIMENSION IDIA(1),MAIL(1),K(100)
COMM
COMM .....//// AU DEBUT LA MATRICE EST VIDE  /////.....................
COMM
      DO 1 I=1,NDL
 1    IDIA(I)=I
COMM
COMM....///  POUR CHAQUE ELEMENT, ON ACTUALISE LE NOMBRE DE ZERO ///...
COMM....///  AU DESSUS DU DERNIER TERME NON NUL                  ///... 
COMM    cette procedure est a changee car une seule procedure qui
COMM    donne le cas de l'element peut etre utilisee pour tous.
      DO 2 IM=1,NMAI
C
       IA=(NNPE+2)*(IM-1)
       ITYP=MAIL(IA+1)
       IF(KTYP.EQ.1.OR.KTYP.EQ.2.OR.KTYP.EQ.3) THEN
         CALL CAEL2D(ITYP,NBN,NDDLE,NBG)        
       ELSE IF(KTYP.EQ.4) THEN
         CALL CAEL3D(ITYP,NBN,NDDLE,NBG)
       ELSE IF(KTYP.EQ.5) THEN
         CALL CAELCO(ITYP,NBN,NDDLE,NBG)                     
       ELSE 
         STOP ' Pb KTYP : PROFBI '
       ENDIF          
       DO 3 I=1,NBN
       K(I)=NDPN*( MAIL(IA+2+I) -1 )+1
 3     CONTINUE
C          
       DO 4 J=1,NBN
        MINI=IDIA(K(J))     
        DO 40 I=1,NBN     
 40     IF(K(I).LT.MINI) MINI=K(I)                      
        IDIA(K(J))=MINI
        DO 5 JJ=1,NDPN-1         
         IDIA(K(J)+JJ)=IDIA(K(J))
 5      CONTINUE     
 4     CONTINUE       
C
 2    CONTINUE
COMM
COMM .....////// A PARTIR DU NOMBRE DE ZERO DANS CHAQUE COLONNE ////.....
COMM .....////// ON CALCUL DU TABLEAU POINTEUR DE LA DIAGONALE  ////.....
COMM
      I=0
      DO 7 J=1,NDL
      I=I+J-IDIA(J)+1
 7    IDIA(J)=I
      NRIG=IDIA(NDL)
      END
    
      SUBROUTINE LONSIG(MAIL,NMAI,NNPE,NSPG,NSIG,NEPS,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %  ON DETERMINE LA LONGUEUR DES VECTEURS DE DEFORMATION          %
COMM %  ET CONTRAINTE                                                 %
COMM %  NBG  : NOMBRE DE POINTS DE GAUSS DE l' ELEMENT                %
COMM %  NSPG : NOMBRE DE CONTRAINTES PAR POINT DE GAUSS               %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DIMENSION MAIL(1)
COMM      
      NEPS=0
      DO 1 I=1,NMAI
       IA=(NNPE+2)*(I-1)
       ITYP=MAIL(IA+1)  
       IF(KTYP.EQ.1.OR.KTYP.EQ.2.OR.KTYP.EQ.3) THEN
         CALL CAEL2D(ITYP,NBN,NDDLE,NBG)        
       ELSE IF(KTYP.EQ.4) THEN
         CALL CAEL3D(ITYP,NBN,NDDLE,NBG)
       ELSE IF(KTYP.EQ.5) THEN
         CALL CAELCO(ITYP,NBN,NDDLE,NBG)                                
       ELSE
         STOP ' Pb KTYP : LONSIG --> uti.f '
       ENDIF      
       NEPS=NEPS+NBG*NSPG 
 1    CONTINUE
      NSIG=NEPS
      END
      
      SUBROUTINE ASSMAT(RIG,IDIA,RGL,NO,IGLO,NBN,NDPN)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %     ASSEMBLAGE DE LA MATRICE ELEMENTAIRE RGL DANS RIG           %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RIG(1),IDIA(1),RGL(1),NO(1),IGLO(1)  
COMM  ......//////// TABLE CORRESPONDANCE DES DDL  ////////............
      DO 5 K=1,NBN
      DO 5 KK=1,NDPN
 5    IGLO(NDPN*(K-1)+KK)=NDPN*(NO(K)-1)+KK
COMM .........////// ASSEMBLAGE DE LA MATRICE ELEMENTAIRE ////.........
      IP=0
      N=NDPN*NBN
      DO 10 I=1,N
      IP=I-1+IP
      K=(I-1)*N+I-IP
      DO 10 J=I,N
       IG=IGLO(I)
       JG=IGLO(J)
       IF(JG.LT.IG) THEN
        KG=IDIA(IG)+JG-IG
       ELSE
        KG=IDIA(JG)+IG-JG
       ENDIF
       RIG(KG)=RIG(KG)+RGL(K+J-I)
 10   CONTINUE
      END
      
      SUBROUTINE ASSVEC(F,FELE,NO,IGLO,NBN,NDPN)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %     ASSEMBLAGE DU VECTEUR ELEMENTAIRE FELE DANS F               %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION F(1),FELE(1),NO(1),IGLO(1)  
COMM  ......//////// TABLE CORRESPONDANCE DES DDL  ////////......
      DO 5 K=1,NBN
      DO 5 KK=1,NDPN
 5    IGLO(NDPN*(K-1)+KK)=NDPN*(NO(K)-1)+KK
COMM .....////// ASSEMBLAGE DU VECTEUR  ELEMENTAIRE //////.............
      N=NDPN*NBN        
      DO 15 I=1,N
15    F(IGLO(I))=F(IGLO(I))+FELE(I) 
      END
   
      SUBROUTINE CALIMP(LIAI,F,NLIAI,NDPN)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %        PRISE EN COMPTE DES DEPLACEMENTS IMPOSES NULS           %
COMM %        MODIFICATION DU SECOND MEMBRE F                         %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION F(1)
      DIMENSION LIAI(2,1)
      DO 1 I=1,NLIAI
      N =LIAI(1,I)
      ND=LIAI(2,I)
      IF(ND.LE.NDPN) THEN
        NN=NDPN*(N-1)+ND
        F(NN)=0.D0
      ELSE
        STOP ' Pb BLOCAGE : CALIMP  - Verifier les blocages - '
      ENDIF
 1    CONTINUE
      END

      SUBROUTINE CALIRI(RIG,IDIA,LIAI,NLIAI,NDPN,NDL)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %        PRISE EN COMPTE DES CONDITIONS AUX LIMITES              %
COMM %                                                                %
COMM %   RIG  MATRICE DE RIGIDITE GLOBALE DE LA STRUCTURE             %
COMM %   IDIA TABLEAU POINTEUR DE LA DIAGONALE                        %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION IDIA(1),RIG(1),LIAI(2,1)
COMM ...........................////////////...........................
      DO 1 I=1,NLIAI
      N =LIAI(1,I)
      ND=LIAI(2,I)
      IF(ND.LE.NDPN) THEN
        NN=NDPN*(N-1)+ND
      ELSE
        STOP ' Pb BLOCAGE : CALIMP  - Verifier les blocages - '
      ENDIF      
      RIG(IDIA(NN))=1.D0
      IF(NN.GT.1) THEN
       NI=IDIA(NN-1)+1
       NS=IDIA(NN)-1
       DO 2 J=NI,NS
 2     RIG(J)=0.D0
      ENDIF
      DO 3 J=NN+1,NDL
      NI=IDIA(J)+NN-J
      IF(NI.GT.IDIA(J-1)) THEN
       RIG(NI)=0.D0
      ENDIF 
 3    CONTINUE
 1    CONTINUE
      END
     
      DOUBLE PRECISION FUNCTION PROSCA(U,V,N)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %      CALCUL DU PRODUIT SCALAIRE DES VECTEURS U(N),V(N)         %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION U(N),V(N)
      PROSCA=0.D0
      DO 1 I=1,N
 1    PROSCA=PROSCA+U(I)*V(I)
      END

      SUBROUTINE SHIFTD(X,Y,LX)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %       ON MET LE TABLEAU X DANS Y SUR UNE LONGEUR LX            %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION X(1),Y(1)
      DO 1 IA=1,LX
    1 Y(IA)=X(IA)
      END
      SUBROUTINE MATVECSI(A,B,C,N1,N2)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %  EFFECTUE LE PRODUIT  C=C+A.B   A(N1,N2) ; B(N2) ; C(N1)       %  
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION A(N1,N2),B(N2),C(N1)
      DO 1 I=1,N1
      DO 1 J=1,N2
 1    C(I)=C(I)+A(I,J)*B(J)      
      END  
            SUBROUTINE TMATVECSI(A,B,C,N1,N2)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                            T                                   %
COMM %  EFFECTUE LE PRODUIT  C=C+  A.B   A(N1,N2) ; B(N1) ; C(N2)     %  
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION A(N1,N2),B(N1),C(N2)
      DO 1 I=1,N2
      DO 1 J=1,N1
 1    C(I)=C(I)+A(J,I)*B(J)      
      END      

COMM .........................////////////.............................
COMM ............////// FIN DU MODULE UTILITAIRE //////................
COMM .........................////////////.............................
