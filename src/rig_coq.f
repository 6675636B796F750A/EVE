COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %  //////  MODULE REGROUPANT LES SUBROUTINES DE CALCULS  //////   %
COMM %  //////   DE RIGIDITE ELASTIQUE   -- LINEAIRE COQ --   //////   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE RELACO(RIG,IDIA,COOR,MAIL,EMAI,EPAI,
     *                  NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   CALCUL DE LA MATRICE DE RIGIDITE ELASTIQUE GLOBALE            %
COMM %   POUR UNE STRUCTURE FORMEE DE PLAQUES ET DE COQUES             %
COMM %                                                                 %
COMM %   RIG   MATRICE DE RIGIDITE GLOBALE                             %
COMM %   IDIA  TABLEAU POINTEUR DE LA DIAGONALE                        %
COMM %   MAIL  TABLEAU DECRIVANT LA NUMEROTATION DES ELEMENTS          %
COMM %   COOR  COORDONNEES DES NOEUDS                                  %
COMM %   EMAI  CARACTERISTIQUES MECANIQUES DES ELEMENTS                %
COMM %   EPAI  EPAISSEUR DES ELEMENTS                                  %
COMM %   NMAI  NOMBRE D ELEMENTS                                       %
COMM %   NDL   NOMBRE DE DDL TOTAL                                     %
COMM %   NCPN  NOMBRE DE COORDONNEES PAR NOEUD                         %
COMM %   NDPN  NOMBRE DE DDL PAR NOEUD                                 %
COMM %   NNPE  NOMBRE DE NOEUDS MARIMUN PAR ELEMENT                    %
COMM %                                                                 %
COMM %   NBN   NOMBRE DE NOEUD DE L'ELEMENT                            %
COMM %   NDDLE NOMBRE DE DDL DE L'ELEMENT                              %
COMM %   NBG   NOMBRE DE POINT DE GAUSS DE L'ELEMENT                   %
COMM %   GAUSS COORDONNEES POINT DE GAUSS DE L'ELEMENT                 %
COMM %   POIDS POID AU POINT DE GAUSS DE L'ELEMENT                     %
COMM %   RGL   MATRICE DE RIGIDITE ELEMENTAIRE                         %
COMM %   NO    NUMEROS GLOBAUX DES NOEUDS                              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RIG(1),IDIA(1),COOR(NCPN,1),MAIL(NNPE+2,1),
     *          EMAI(NVMA,1),EPAI(1)      
      DIMENSION RGL(171),NO(3),IGLO(18),X(3),Y(3),Z(3),D(6,6)
COMM ...........................////////////...........................
      CALL DZERO(RIG,IDIA(NDL))
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////.............      
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)     
      CALL CAELCO(ITYP,NBN,NDDLE,NBG)          
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))
      Z(I)=COOR(3,NO(I))  
 3    CONTINUE
COMM  ......//////// ON FORME LA MATRICE DECOMPORTEMENT D //////........
      ND=6    
      CALL DISOCO(EMAI(1,IMAT),EMAI(2,IMAT),EPAI(IM),D)
COMM.....................///// CALCUL DE RGL /////.....................
      IF(ITYP.EQ.31) THEN      
       CALL RIGELE_DKT(X,Y,Z,D,RGL,NBN,NDDLE,NBG,ND)
      ELSE
       STOP ' Pb ITYP : RELACO '
      ENDIF      
COMM .........////// ASSEMBLAGE DE LA MAT. RIGID. ELEM. ////...........       
      CALL ASSMAT(RIG,IDIA,RGL,NO,IGLO,NBN,NDPN)
 20   CONTINUE
      END

      SUBROUTINE RIGELE_DKT(X,Y,Z,D,RGL,NBN,NDDLE,NBG,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE RIGIDITE ELEMENTAIRE    ' DKT+CST '                 %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION X(3),Y(3),Z(3),D(6,6),TRA(3,3)
      DIMENSION RGL(171),RGEL(18,18),RGE(18,18),R(18,18),RINT(18,18)
COMM  .../// Coordonnees locales et matrices de rotation ////...........
      CALL COOLOC(X,Y,Z,X2,X3,Y3,TRA,R)
COMM  .. //// Matrice de rigidite elementaire dans le repere local ////..
      CALL RIELLO_DKT(X2,X3,Y3,D,RGL,NBN,NDDLE,NBG,ND)
COMM  .. //// passage du stockage en ligne au stockage en matrice ///....
      CALL LIGMAT(RGL,RGE,NDDLE)      
COMM  .. ///// Rigidite additionnelles pour le d.d.l. 0z /////...........
      CT1=D(4,4)*0.0001
      CT2=-CT1*0.5
      RGE( 6, 6)=CT1
      RGE( 6,12)=CT2
      RGE( 6,18)=CT2
      RGE(12, 6)=CT2
      RGE(12,12)=CT1
      RGE(12,18)=CT2
      RGE(18, 6)=CT2
      RGE(18,12)=CT2
      RGE(18,18)=CT1
COMM  .....///// PASSAGE AU REPERE GLOBAL /////.........................
      CALL PASGLO(RGE,R,RINT,RGEL)
COMM  ...//// passage du stockage en matrice au stockage en ligne  /////......
      CALL MATLIG(RGEL,RGL,NDDLE)
      END

      SUBROUTINE RIELLO_DKT(X2,X3,Y3,D,RGL,NBN,NDDLE,NBG,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE RIGIDITE ELEMENTAIRE REPERE LOCAL  'DKT+CST'        %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION RGL(171),ARGL(171),D(6,6),B(6,18),DB(6,18),
     *          GAUSS(2,3),POIDS(3)
      DOUBLE PRECISION KSI,ETA
C
      ITS=(NDDLE*NDDLE+NDDLE)/2
      CALL DZERO(RGL,ITS)
      CALL GRIGCO(31,GAUSS,POIDS)      
      DO 7 IG=1,NBG 
        KSI=GAUSS(1,IG)
        ETA=GAUSS(2,IG)
        CALL BMAT_DKT(X2,X3,Y3,KSI,ETA,B,DETJ)
        CALL TBDB(B,D,DB,ARGL,ND,NDDLE,ITS)
        COEF=DETJ*POIDS(IG)
        CALL DACTUA(RGL,ARGL,ITS,COEF)        
 7    CONTINUE 
      END

     
      SUBROUTINE CAELCO(ITYP,NBN,NDDLE,NBG)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  EN FONCTION DU TYPE DE L'ELEMENT ON RENVOI LE NOMBRE DE NOEUDS %
COMM %  LE NOMBRE DE DDL, LE NOMBRE DE POINTS DE GAUSS                 %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IF      (ITYP.EQ.31) THEN
        NBN=3
        NDDLE=18
        NBG=3
      ELSE IF(ITYP.EQ.41) THEN
        NBN=4
        NDDLE=20
        NBG=8
      ELSE IF(ITYP.EQ.42) THEN
        NBN=4
        NDDLE=24
        NBG=8    
      ELSE
       STOP ' PB ITYP : CAELCO '
      ENDIF
      END
      
      SUBROUTINE GRIGCO(ITYP,GAUSS,POIDS)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  EN FONCTION DU TYPE DE L'ELEMENT, ON RENVOIE LES COORDONNEES   %
COMM %  DES POINTS DE GAUSS, ET LES POIDS DE GAUSS                     %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION GAUSS(2,3),POIDS(3)
COMM    

      IF (ITYP.EQ.31) THEN
        GAUSS(1,1)=1.D0/6.D0
        GAUSS(1,2)=2.D0/3.D0
        GAUSS(1,3)=1.D0/6.D0
        GAUSS(2,1)=1.D0/6.D0
        GAUSS(2,2)=1.D0/6.D0
        GAUSS(2,3)=2.D0/3.D0
        DO 2 I=1,3
 2      POIDS(I)=1.D0/6.D0       
      ELSE
       STOP ' PB : GRIGCO '
      ENDIF
      END 
                  
      SUBROUTINE DISOCO(YG,PS,H,D)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE COMPORTEMENT ISOTROPE D   -- COQUE  --              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION D(6,6)
      DO 1 I=1,6
      DO 1 J=1,6
 1    D(I,J)=0.
      C1=YG*H/(1.-PS*PS)
      C2=YG*H*H*H/(12.*(1.-PS*PS))
      D(1,1)=C1
      D(1,2)=PS*C1
      D(2,1)=D(1,2)
      D(2,2)=C1
      D(3,3)=C1*(1.-PS)*0.5
      D(4,4)=C2
      D(4,5)=PS*C2
      D(5,4)=D(4,5)
      D(5,5)=C2
      D(6,6)=C2*(1.-PS)*0.5
      END
      
      SUBROUTINE BMAT_DKT(A,B,C,KSI,ETA,BT,DETJ)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %  CALCUL DE LA MATRICE B POUR LA RIGIDITE EN UN POINT DE GAUSS   %
COMM %   ELEMENT DKT                                                   %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION KSI,ETA
      DIMENSION BT(6,18),BF(3,9)
      DIMENSION H_X_KSI(9),H_Y_KSI(9),H_X_ETA(9),H_Y_ETA(9)
C
      DETJ=A*C
COMM--------Initialisation
      DO 1 I=1,6
      DO 1 J=1,18
 1    BT(I,J)=0. 
COMM-------Termes de tension-------------
      BT(1,1)=-1./A
      BT(1,7)=-BT(1,1)
      BT(2,2)=(B-A)/A/C
      BT(2,8)=-B/A/C
      BT(2,14)=1./C
      BT(3,1)=BT(2,2)
      BT(3,2)=BT(1,1)
      BT(3,7)=BT(2,8)
      BT(3,8)=BT(1,7)
      BT(3,13)=BT(2,14)
COMM--------Termes de flexion------------
      X1=0.
      Y1=0.
      X2=A
      Y2=0.
      X3=B
      Y3=C
      X23=X2-X3
      Y23=Y2-Y3       
      X31=X3-X1
      Y31=Y3-Y1
      X12=X1-X2
      Y12=Y1-Y2
      XL23CARRE=( X23*X23 ) + ( Y23*Y23 )
      XL31CARRE=( X31*X31 ) + ( Y31*Y31 )  
      XL12CARRE=( X12*X12 ) + ( Y12*Y12 )
      P4=(-6.D0)*X23/XL23CARRE
      P5=(-6.D0)*X31/XL31CARRE
      P6=(-6.D0)*X12/XL12CARRE
      T4=(-6.D0)*Y23/XL23CARRE
      T5=(-6.D0)*Y31/XL31CARRE
      T6=(-6.D0)*Y12/XL12CARRE
      Q4=(3.D0)*X23*Y23/XL23CARRE
      Q5=(3.D0)*X31*Y31/XL31CARRE
      Q6=(3.D0)*X12*Y12/XL12CARRE
      R4=(3.D0)*Y23*Y23/XL23CARRE
      R5=(3.D0)*Y31*Y31/XL31CARRE
      R6=(3.D0)*Y12*Y12/XL12CARRE
      DEUX_A=(X31*Y12) - (X12*Y31)
C
      H_X_KSI(1)=P6*(1.D0-(2.D0*KSI)) + (P5-P6)*ETA 
      H_X_KSI(2)=Q6*(1.D0-(2.D0*KSI)) - (Q5+Q6)*ETA
      H_X_KSI(3)=(-4.D0) + (6.D0*(KSI+ETA)) + R6*(1.D0-(2.D0*KSI))
     *- (R5+R6)*ETA
      H_X_KSI(4)=P6*((2.D0*KSI)-1.D0) + (P4+P6)*ETA
      H_X_KSI(5)=Q6*(1.D0-(2.D0*KSI)) - (Q6-Q4)*ETA
      H_X_KSI(6)=(-2.D0)+(6.D0*KSI)+R6*(1.D0-(2.D0*KSI))+(R4-R6)*ETA
      H_X_KSI(7)=(P5+P4)*ETA*(-1.D0)
      H_X_KSI(8)=(Q4-Q5)*ETA
      H_X_KSI(9)=(R4-R5)*ETA 
C
      H_X_ETA(1)=P5*((2.D0*ETA)-1.D0) - (P6-P5)*KSI 
      H_X_ETA(2)=Q5*(1.D0-(2.D0*ETA)) - (Q5+Q6)*KSI
      H_X_ETA(3)=(-4.D0) + (6.D0*(ETA+KSI)) + R5*(1.D0-(2.D0*ETA))
     *- (R5+R6)*KSI
      H_X_ETA(4)=(P4+P6)*KSI
      H_X_ETA(5)=(Q4-Q6)*KSI
      H_X_ETA(6)=(R4-R6)*KSI 
      H_X_ETA(7)=P5*(1.D0-(2.D0*ETA)) - (P4+P5)*KSI
      H_X_ETA(8)=Q5*(1.D0-(2.D0*ETA)) + (Q4-Q5)*KSI
      H_X_ETA(9)=(-2.D0)+(6.D0*ETA)+R5*(1.D0-(2.D0*ETA))+(R4-R5)*KSI  
C
      H_Y_KSI(1)=T6*(1.D0-(2.D0*KSI)) + (T5-T6)*ETA
      H_Y_KSI(2)=(1.D0) + R6*(1.D0-(2.D0*KSI)) - (R5+R6)*ETA
      H_Y_KSI(3)=Q6*((2.D0*KSI)-1.D0) + (Q5+Q6)*ETA
      H_Y_KSI(4)=T6*((2.D0*KSI)-1.D0) + (T4+T6)*ETA
      H_Y_KSI(5)=(-1.D0) + R6*(1.D0-(2.D0*KSI)) + (R4-R6)*ETA
      H_Y_KSI(6)=Q6*((2.D0*KSI)-1.D0) - (Q4-Q6)*ETA
      H_Y_KSI(7)=(T5+T4)*ETA*(-1.D0)
      H_Y_KSI(8)=(R4-R5)*ETA
      H_Y_KSI(9)=(Q5-Q4)*ETA  
C                                                                   
      H_Y_ETA(1)=T5*((2.D0*ETA)-1.D0) + (T5-T6)*KSI
      H_Y_ETA(2)=(1.D0) + R5*(1.D0-(2.D0*ETA)) - (R5+R6)*KSI
      H_Y_ETA(3)=Q5*((2.D0*ETA)-1.D0) + (Q5+Q6)*KSI
      H_Y_ETA(4)=(T6+T4)*KSI
      H_Y_ETA(5)=(R4-R6)*KSI
      H_Y_ETA(6)=(Q6-Q4)*KSI 
      H_Y_ETA(7)=T5*(1.D0-(2.D0*ETA)) - (T4+T5)*KSI
      H_Y_ETA(8)=(-1.D0) + R5*(1.D0-(2.D0*ETA)) + (R4-R5)*KSI
      H_Y_ETA(9)=Q5*((2.D0*ETA)-1.D0) - (Q4-Q5)*KSI
C  
      DO 2 I=1,9
      BF(1,I)=( Y31*H_X_KSI(I) + Y12*H_X_ETA(I) )/DEUX_A
      BF(2,I)=( X31*H_Y_KSI(I) + X12*H_Y_ETA(I) )*(-1.D0)/DEUX_A
      BF(3,I)=( Y31*H_Y_KSI(I) + Y12*H_Y_ETA(I) 
     *- X31*H_X_KSI(I) - X12*H_X_ETA(I) )/DEUX_A
  2   CONTINUE 
COMM-------Construction de la matrice B------------
      DO 3 I=1,3
      DO 3 J=1,3
      BT(3+I, 2+J)=BF(I,  J)
      BT(3+I, 8+J)=BF(I,3+J)
  3   BT(3+I,14+J)=BF(I,6+J)
      END 

      SUBROUTINE COOLOC(X,Y,Z,X2,X3,Y3,TRA,R)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  CALCUL DES COORDONNEES LOCALES X2,X3,Y3                        %
COMM %  ET DES MATRICES DE PASSAGE REPERE LOCAL,GLOBAL                 %
COMM %                                                                 %                                               %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION X(3),Y(3),Z(3),TRA(3,3),R(18,18)
      X1=X(1)
      Y1=Y(1)
      Z1=Z(1)
      DO 5 I=1,3
      X(I)=X(I)-X1
      Y(I)=Y(I)-Y1
 5    Z(I)=Z(I)-Z1
      CALL TRANSF(X,Y,Z,TRA)
      X2=TRA(1,1)*X(2)+TRA(1,2)*Y(2)+TRA(1,3)*Z(2)
      X3=TRA(1,1)*X(3)+TRA(1,2)*Y(3)+TRA(1,3)*Z(3)
      Y3=TRA(2,1)*X(3)+TRA(2,2)*Y(3)+TRA(2,3)*Z(3)
      CALL ROTASS(TRA,R)
      END
                 
      SUBROUTINE ROTASS(TRA,R)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  CALCUL DE LA MATRICE DE PASSAGE REPERE LOCAL,GLOBAL            %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION TRA(3,3),R(18,18)
      CALL DZERO(R,18*18)
      DO 2 I=1,3
      DO 2 J=1,3
      R(I   ,J   )=TRA(I,J)
      R(I+ 3,J+ 3)=TRA(I,J)
      R(I+ 6,J+ 6)=TRA(I,J)
      R(I+ 9,J+ 9)=TRA(I,J)
      R(I+12,J+12)=TRA(I,J)
  2   R(I+15,J+15)=TRA(I,J)
      END
      
      SUBROUTINE TRANSF(X,Y,Z,TRA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  CALCUL DE LA MATRICE DES ROTATIONS REPERE LOCAL,GLOBAL         %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION X(3),Y(3),Z(3),TRA(3,3)
      RN = X(2)**2+Y(2)**2
      RN2= DSQRT(RN)
      IF(RN2.LT.1.D-5) THEN
      CT=1.
      ST=0.
      ELSE
      CT = X(2)/RN2
      ST = Y(2)/RN2
      ENDIF
      RN = RN + Z(2)**2
      RN2= DSQRT(RN)
      SF = Z(2)/RN2
      CF = DSQRT(1.-SF**2)
      A  = Y(2)*Z(3)-Z(2)*Y(3)
      B  = Z(2)*X(3)-X(2)*Z(3)
      C  = X(2)*Y(3)-Y(2)*X(3)
      RN2= DSQRT(A**2+B**2+C**2)
      SP =(Z(2)*Y(3)-Y(2)*Z(3))*ST+(Z(2)*X(3)-X(2)*Z(3))*CT
      SP =-SP/RN2
      RN =(X(2)*X(3)+Y(2)*Y(3)+Z(2)*Z(3))/(X(2)**2+Y(2)**2+Z(2)**2)
      A  = X(3)-X(2)*RN
      B  = Y(3)-Y(2)*RN
      C  = Z(3)-Z(2)*RN
      C  = DSQRT(A**2+B**2+C**2)
      CP =(-A*ST+B*CT)/C
      TRA(1,1)= CF*CT
      TRA(1,2)= CF*ST
      TRA(1,3)= SF
      TRA(2,1)=-ST*CP-SP*CT*SF
      TRA(2,2)= CT*CP-ST*SF*SP
      TRA(2,3)= SP*CF
      TRA(3,1)= ST*SP-CP*CT*SF
      TRA(3,2)=-SP*CT-ST*SF*CP
      TRA(3,3)= CP*CF
      END
      
      SUBROUTINE PASGLO(RGE,R,RINT,RGEL)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                   T                                             %
COMM %  CALCUL DE RGEL= R. RGE. R                                      %
COMM %                                                                 %                                               %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RGE(18,18),R(18,18),RINT(18,18),RGEL(18,18)      
C      
      DO 1 I=1,18
      DO 1 J=1,18
      RGEL(I,J)=0.D0
 1    RINT(I,J)=0.D0
      DO 3 I=1,18
      DO 3 J1=1,16,3
           J2=J1+2
      DO 3 J=J1,J2
      DO 3 K=J1,J2
 3    RINT(I,J)=RINT(I,J)+RGE(I,K)*R(K,J)
      DO 4 J=1,18
      DO 4 I1=1,16,3
           I2=I1+2
      DO 4 I=I1,I2
      DO 4 K=I1,I2
 4    RGEL(I,J)=RGEL(I,J)+R(K,I)*RINT(K,J)
      END


COMM ...........................////////////...........................
COMM ...//////  FIN DU MODULE REGROUPANT LES SUBROUTINES DE  //////....
COMM ...//////  CALCULS  DE RIGIDITE & DE COEF CINEMATIQUES  //////....
COMM ...........................////////////...........................

