
   
      SUBROUTINE SMFQP_PLAS_DECHARGE_2D(FQ,COOR,MAIL,EMAI,EPAI,SIG,
     &        SIG0,SIGQ,SIGQ0,EPS,EPSP,PN,PN0,FC,FC0,LAMBDA,GP,GP0,
     &        ZP,ZP0,XP,XP0,YP,YP0,SIGe,SIGe0,EPSPe,EPSPe0,DEN0,DEN,
     &        KTYP,NBG,NMAI,NDL,NSIG,NNPE,NCPN,NDPN,NSPG,NVMA,IORDRE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %    CALCUL DU SECOND MENBRE FQ EN UN POINT DE GAUSS   2D         %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION KSI,ETA,LAMBDA(NMAI*NBG,1:*)
      DIMENSION COOR(NCPN,1),MAIL(NNPE+2,1),EMAI(NVMA,1),EPAI(1),
     &         SIG(NSIG,1:*),SIG0(NSIG),
     &         SIGQ(NBG*NMAI,1:*),SIGQ0(NBG*NMAI),EPS(NSIG,1:*),
     &         EPSP(NSIG,1:*),PN(NSIG,1:*),PN0(NSIG),FC(NBG*NMAI,1:*),
     &         FC0(NBG*NMAI),GP(NBG*NMAI,1:*),GP0(NBG*NMAI),
     &         ZP(NBG*NMAI,1:*),ZP0(NBG*NMAI),XP0(NBG*NMAI),
     &         SIGe(NBG*NMAI,1:*),SIGe0(NBG*NMAI),
     &         EPSPe(NBG*NMAI,1:*),EPSPe0(NBG*NMAI),
     &         DEN(NBG*NMAI,1:*),DEN0(NBG*NMAI),
     &         YP0(NBG*NMAI),XP(NBG*NMAI,1:*),YP(NBG*NMAI,1:*),
     &         FQ(1),IGLO(16),NO(8),X(8),Y(8),D(16),FQE(16),
     &         BL(64),GAUSS(2,9),POIDS(9),VE(16),FEGAUSS(16) 
COMM
      
COMM ...///  On initialise le second membre a zero ///................
      CALL DZERO(FQ,NDL)
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      IADS=1
      IAD0=1
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE LELEMENT /////.............
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)
      YG=EMAI(1,IMAT)
      PS=EMAI(2,IMAT)
      SIGY=EMAI(3,IMAT)
      hp=EMAI(4,IMAT)
c      DEPSc=EMAI(5,IMAT)
      EPSc=EMAI(5,IMAT)
      BETA=10.D0
      DEPSc=EPSc/BETA
      ETA1=EMAI(6,IMAT)
      ETA2=EMAI(7,IMAT)
      ETA3=EMAI(8,IMAT)
      ETA4=EMAI(9,IMAT)
      
      CALL CAEL2D(ITYP,NBN,NDDLE,NBG)
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))
 3    CONTINUE
      
COMM  ....///// ON FORME LA MATRICE DECOMPORTEMENT D (ND,ND) /////.....
      IF      (KTYP.EQ.1) THEN
      ND=3
      CALL DISODP(YG,PS,D)
      ELSE IF (KTYP.EQ.2) THEN
      ND=3
      CALL DISOCP(YG,PS,D)
      ELSE IF (KTYP.EQ.3) THEN
      ND=4
      CALL DISOAX(YG,PS,D)
      ELSE 
      STOP ' SMFQ2D : pb sur KTYP '
      ENDIF
      
COMM .........////// INITIALISATION .........//////
      CALL DZERO(FQE,NDDLE)
COMM
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////.............
     
      CALL GRIG2D(ITYP,GAUSS,POIDS)
      DO 7 IG=1,NBG
      KSI=GAUSS(1,IG)
      ETA=GAUSS(2,IG)

COMM .....//// CALCUL DE LA MATRICE BNL ET DE DETJ ( et RAY)////.......
      
      CALL BLINISO2D(X,Y,KSI,ETA,BL,DETJ,RAY,NDDLE,NBN,ND,ITYP,KTYP)

      CALL SGAU_CALCULP(BL,SIG(IADS,1),SIG0(IADS),EPSP(IADS,1),
     &     SIGQ(IAD0,1),SIGQ0(IAD0),PN(IADS,1),PN0(IADS),
     &     GP(IAD0,1),GP0(IAD0),ZP(IAD0,1),ZP0(IAD0),XP(IAD0,1),
     &     XP0(IAD0),YP(IAD0,1),YP0(IAD0),FC(IAD0,1),FC0(IAD0),
     &     SIGe(IAD0,1),SIGe0(IAD0),EPSPe(IAD0,1),LAMBDA(IAD0,1),
     &    FEGAUSS,DEN0(IAD0),DEN(IAD0,1),ND,NDDLE,NSIG,NMAI,NVMA,
     &     NBG,IORDRE,YG,
     &     PS,SIGY,DEPSc,hp,ETA1,ETA2,ETA3,ETA4,IADS,IAD0)
     
      IADS=IADS+NSPG
      IAD0=IAD0+1
COMM ....    .//// CALCUL DU COEFFICIENT     ////.......      
      
      IF(KTYP.EQ.1.OR.KTYP.EQ.2) THEN
      COEF=DETJ*EPAI(IM)*POIDS(IG)
      ELSE IF (KTYP.EQ.3) THEN
      COEF=DETJ*6.2831853*RAY*POIDS(IG)
      ENDIF
      CALL DACTUA(FQE,FEGAUSS,NDDLE,COEF)
 7    CONTINUE  
      CALL ASSVEC(FQ,FQE,NO,IGLO,NBN,NDPN)
 20   CONTINUE
     
      END     
      

      SUBROUTINE SGAU_CALCULP(BL,SIG,SIG0,EPSP,SIGQ,SIGQ0,PN,PN0,
     &           GP,GP0,ZP,ZP0,XP,XP0,YP,YP0,FC,FC0,SIGe,SIGe0,
     &           EPSPe,LAMBDA,FEGAUSS,DEN0,DEN,ND,NDDLE,NSIG,NMAI,NVMA,
     &           NBG,IORDRE,YG,PS,SIGY,DEPSc,hp,ETA1,ETA2,ETA3,ETA4,
     &           IADS,IAD0)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %    CALCUL DU SECOND MENBRE FQ EN UN POINT DE GAUSS   2D         %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION BL(64),SIG(NSIG,1:*),SIG0(NSIG),
     &       SIGQ(NBG*NMAI,1:*),SIGe(NBG*NMAI,1:*),EPSPe(NBG*NMAI,1:*),
     &       PN(NSIG,1:*),PN0(NSIG),FC(NBG*NMAI,1:*),GP(NBG*NMAI,1:*),
     &       ZP(NBG*NMAI,1:*),XP(NBG*NMAI,1:*),YP(NBG*NMAI,1:*),
     &       EPSP(NSIG,1:*),FEGAUSS(NDDLE),DEN(NBG*NMAI,1:*)

      DOUBLE PRECISION LAMBDA(NBG*NMAI,1:*),DT(3,3),SIGD(3),SIGD0(3),
     &                 SIG_RES(3),EPSPnl(3),EPS_RES(3),SIG_I(3,3),
     &                 SOM4(3),SOM7(3)
     
      CALL DZERO(FEGAUSS,NDDLE)
      
      CALL MAT_SIGMA_I(SIG_I,ND)
      XI=GP0*YP0/(2.D0*YP0-XP0)
      XI1=XI/(1.D0-1.5D0*XI)
      RMU2=YG/(1.D0+PS)
COMM    CALCUL DES SOMMES
      
      SOM1=0.D0                  !SOM1=k-i*ni:EPSk-i      
      SOM2=0.D0                  !SOM2=YPi(YPk-i-XPk-i)
      SOM3=YP0*GP(1,IORDRE-1)    !SOM3=YPiGPk-i-1    
      CALL DZERO(SOM4,ND)        !SOM4=LAMBDAk-iNi   
      SOM5=0.D0                  !SOM5=SIGDiSIGDk-i  
      SOM6=0.D0                  !SOM6=SIGQiSIGQk-i    
      CALL DZERO(SOM7,ND)        !SOM7=SIGQi Nk-i
      SOM8=0.D0                  !SOM8=FCiFCk-i   
       
      SOM9=0.D0                  !SOM9=GPiDENk-i
      SOM10=0.D0                 !SOM10=FFCi*SIGek-i
      SOM11=0.D0                 !SOM11=FCi*SIGek-i
                    
      IF (IORDRE.GT.2) THEN
      DO 1 IR=1,IORDRE-2
      IOMR=IORDRE-1-IR
      SOM3=SOM3+YP(1,IR)*GP(1,IOMR)
      SOM2=SOM2+YP(1,IR)*(YP(1,IOMR)-XP(1,IOMR))
 1    CONTINUE        
      ENDIF
      
      DO 2 IR=1,IORDRE-1
      IOMR=IORDRE-IR
      SOM1=SOM1+IOMR*((SIG(1,IOMR)*PN(1,IR)+SIG(2,IOMR)*PN(2,IR)+
     &           2.D0*SIG(3,IOMR)*PN(3,IR))/RMU2+
     &           (EPSP(1,IOMR)*PN(1,IR)+EPSP(2,IOMR)*PN(2,IR)+
     &           EPSP(3,IOMR)*PN(3,IR))+
     &           (EPSP(1,IOMR)+EPSP(2,IOMR))*(PN(1,IR)+PN(2,IR)))

      CALL DACTUA(SOM4,PN(1,IR),ND,DBLE(IOMR)*LAMBDA(1,IOMR))
      SOM5=SOM5+SIGD_SIGD_2D(SIG(1,IR),SIG(1,IOMR),ND)
      SOM6=SOM6+SIGQ(1,IR)*SIGQ(1,IOMR)
      CALL DACTUA(SOM7,PN(1,IOMR),ND,SIGQ(1,IR))
      SOM8=SOM8+FC(1,IR)*FC(1,IOMR)
      SOM9=SOM9+GP(1,IR)*DEN(1,IOMR)
      SOM10=SOM10+ZP(1,IR)*SIGe(1,IOMR)
      SOM11=SOM11+FC(1,IR)*SIGe(1,IOMR)
 2    CONTINUE     
      write(25,*) SOM1,SOM2,SOM5,SOM6,SOM8,SOM9,SOM10,SOM11
COMM     CALCUL DE FNL


      RLAMBDAnl=XI*SOM1+DEPSc*(SOM3-SOM2*GP0/(2.D0*YP0-XP0))
      EPSPnl(1)=(RLAMBDAnl*PN0(1)+SOM4(1))/DBLE(IORDRE)   
      EPSPnl(2)=(RLAMBDAnl*PN0(2)+SOM4(2))/DBLE(IORDRE)   
      EPSPnl(3)=2.D0*(RLAMBDAnl*PN0(3)+SOM4(3))/DBLE(IORDRE)  
      
      PROD1=EPSPnl(1)*PN0(1)+EPSPnl(2)*PN0(2)+EPSPnl(3)*
     &   PN0(3)+(EPSPnl(1)+EPSPnl(2))*(PN0(1)+PN0(2))
      
      EPS_RES(1)= EPSPnl(1)+XI1*PROD1*PN0(1)
      EPS_RES(2)= EPSPnl(2)+XI1*PROD1*PN0(2)
      EPS_RES(3)= EPSPnl(3)+2.D0*XI1*PROD1*PN0(3)
      
      CALL MAT_DT_PLAS_DECHARGE(DT,XP0,YP0,GP0,PN0,YG,PS,ND)
      CALL MATVEC(DT,EPS_RES,SIG_RES,3,3)
      CALL TMATVEC(BL,SIG_RES,FEGAUSS,ND,NDDLE)
      
COMM    CALCUL DES VARIABLES INTERMEDIAIRES
      DO I=1,ND
      EPSP(I,IORDRE)=SOM4(I)/DBLE(IORDRE) 
      PN(I,IORDRE)=-SOM7(I)/SIGQ0
      SIG(I,IORDRE)=-SIG_RES(I)
      ENDDO      
      DEN(1,IORDRE)=SOM10/RMU2
      
      EPSP(3,IORDRE)=2.D0*EPSP(3,IORDRE)
      SIGQ(1,IORDRE)=(1.5D0*SOM5-SOM6)/(2.D0*SIGQ0)
      ZP(1,IORDRE)=SOM8
      
      FC(1,IORDRE)=-SOM11/SIGe0
      
      GP(1,IORDRE)=-SOM9/DEN0   
      LAMBDA(1,IORDRE)=DEPSc*SOM3/DBLE(IORDRE)
      XP(1,IORDRE-1)=SOM1/DEPSc+
     &       (DBLE(IORDRE)*PROD1/((1.D0-1.5D0*XI)*DEPSc))
      YP(1,IORDRE-1)=-SOM2/(2.D0*YP0-XP0)
      END
      
      
