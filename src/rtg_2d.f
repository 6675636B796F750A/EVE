COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %  ////    MODULE REGROUPANT LES SUBROUTINES DE CALCULS    ////   %
COMM %  ////   DE RIGIDITE TANGENTE -- NON LIN. GEOM. 2D --     ////   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE RTAN2D(RIG,IDIA,COOR,MAIL,EMAI,EPAI,V,SIG,
     *                  NMAI,NDL,NCPN,NDPN,NNPE,NSPG,NVMA,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   CALCUL DE LA MATRICE DE RIGIDITE TANGENTE GLOBALE             %
COMM %           DE LA STRUCTURE BIDIMENSIONNELLE                      %
COMM %                                                                 %
COMM %   RIG   MATRICE DE RIGIDITE GLOBALE                             %
COMM %   IDIA  TABLEAU POINTEUR DE LA DIAGONALE                        %
COMM %   MAIL  TABLEAU DECRIVANT LA NUMEROTATION DES ELEMENTS          %
COMM %   COOR  COORDONNEES DES NOEUDS                                  %
COMM %   EMAI  CARACTERISTIQUES MECANIQUES DES ELEMENTS                %
COMM %   V     VECTEUR DE DEPLACEMENT INITIAL                          %
COMM %   SIG   VECTEUR DE CONTRAINTE INITIAL                           %
COMM %   NMAI  NOMBRE D ELEMENTS                                       %
COMM %   NDL   NOMBRE DE DDL TOTAL                                     %
COMM %   NCPN  NOMBRE DE COORDONNEES PAR NOEUD                         %
COMM %   NDPN  NOMBRE DE DDL PAR NOEUD                                 %
COMM %   NNPE  NOMBRE DE NOEUDS MAXIMUN PAR ELEMENT                    %
COMM %   KTYP = 1 --> DEFORMATION PLANE                                %
COMM %   KTYP = 2 --> CONTRAINTE PLANE                                 %
COMM %   KTYP = 3 --> CONTRAINTE PLANE                                 %
COMM %                                                                 %
COMM %   NBN   NOMBRE DE NOEUD DE L'ELEMENT                            %
COMM %   NDDLE NOMBRE DE DDL DE L'ELEMENT                              %
COMM %   NBG   NOMBRE DE POINT DE GAUSS DE L'ELEMENT                   %
COMM %   GAUSS COORDONNEES POINT DE GAUSS DE L'ELEMENT                 %
COMM %   POIDS POID AU POINT DE GAUSS DE L'ELEMENT                     %
COMM %   RGL   MATRICE DE RIGIDITE ELEMENTAIRE                         %
COMM %   NO    NUMEROS GLOBAUX DES NOEUDS                              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RIG(1),IDIA(1),COOR(NCPN,1),MAIL(NNPE+2,1),
     *          EMAI(NVMA,1),EPAI(1),V(1),SIG(1)      
      DIMENSION RGL(136),NO(8),IGLO(16),X(8),Y(8),D(16),VE(16)
COMM ...........................////////////...........................
      CALL DZERO(RIG,IDIA(NDL))
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      IAD=1
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////.............
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)          
      CALL CAEL2D(ITYP,NBN,NDDLE,NBG)            
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))
      VE(2*I-1)=V(2*NO(I)-1)
      VE(2*I  )=V(2*NO(I)  )             
 3    CONTINUE
      H=EPAI(IM)
COMM  ....///// ON FORME LA MATRICE DECOMPORTEMENT D (ND,ND) /////.....
      IF      (KTYP.EQ.1) THEN
         ND=3
         CALL DISODP(EMAI(1,IMAT),EMAI(2,IMAT),D)
      ELSE IF (KTYP.EQ.2) THEN
         ND=3
         CALL DISOCP(EMAI(1,IMAT),EMAI(2,IMAT),D)
      ELSE IF (KTYP.EQ.3) THEN
         ND=4
         CALL DISOAX(EMAI(1,IMAT),EMAI(2,IMAT),D)
      ELSE 
         STOP ' RTAN2D : pb sur KTYP '
      ENDIF   
COMM..........///// CALCUL DE LA RIGIDITE ELEMENTAIRE /////............                            
      IF(ITYP.EQ.30.OR.ITYP.EQ.40.OR.
     *   ITYP.EQ.60.OR.ITYP.EQ.80.OR.ITYP.EQ.81) THEN
        CALL RTAELE_ISOPARA2D(X,Y,D,H,VE,SIG,IAD,RGL,
     *                        KTYP,ITYP,NBN,NDDLE,NBG,ND)
      ELSE      
        STOP ' RTAN2D : pb sur ITYP '  
      ENDIF
COMM .........////// ASSEMBLAGE DE LA MAT. RIGID. ELEM. ////...........       
      CALL ASSMAT(RIG,IDIA,RGL,NO,IGLO,NBN,NDPN)
 20   CONTINUE
      END

      SUBROUTINE RTAELE_ISOPARA2D(X,Y,D,H,VE,SIG,IAD,RGL,
     *                            KTYP,ITYP,NBN,NDDLE,NBG,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE RIGIDITE TAN. ELEM.  : ELEMENTS ISOPARAMETRIQUES 2D %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION SIG(1),X(8),Y(8),D(16),VE(16),RGL(136),ARGL(136),
     *          BNL(64),DBNL(64),G(80),GS(80),T(5),S(25),
     *          GAUSS(2,9),POIDS(9)      
      DOUBLE PRECISION KSI,ETA         
COMM 
      ITS=(NDDLE*NDDLE+NDDLE)/2
      CALL DZERO(RGL,ITS)     
      CALL GRIG2D(ITYP,GAUSS,POIDS)       
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////.............
      DO 7 NG=1,NBG     
        KSI=GAUSS(1,NG)
        ETA=GAUSS(2,NG)
COMM .....//// CALCUL DE LA MATRICE BNL ET DE DETJ ( et RAY)////.......
        CALL BNLGISO2D(X,Y,KSI,ETA,VE,BNL,G,T,DETJ,RAY,
     *                 NDDLE,NBN,ND,ITYP,KTYP)
COMM.........////// CALCUL DE BNLt.D.BNL  ////////////..........
        CALL TBDB(BNL,D,DBNL,ARGL,ND,NDDLE,ITS)
COMM...........////// CALCUL DU COEFFICIENT  ////////////............        
        IF(KTYP.EQ.1.OR.KTYP.EQ.2) THEN        
          COEF=DETJ*H*POIDS(NG)
        ELSE IF (KTYP.EQ.3) THEN
          COEF=DETJ*6.2831853*RAY*POIDS(NG)
        ENDIF
        CALL DACTUA(RGL,ARGL,ITS,COEF)        
COMM.......//// ON FORME LA MATRICE DE CONTRAINTE S (NS,NS) ////.......
        IF      (KTYP.EQ.1.OR.KTYP.EQ.2) THEN 
          CALL SMATPL(SIG(IAD),S)
          NS=4
        ELSE IF (KTYP.EQ.3) THEN
          CALL SMATAX(SIG(IAD),S)
          NS=5
        ELSE 
          STOP ' RTAELE_ISOPARA2D : pb sur KTYP '
        ENDIF
        IAD=IAD+4              
COMM...............////// CALCUL DE Gt.S.G  ////////................     
        CALL TBDB(G,S,GS,ARGL,NS,NDDLE,ITS)
        CALL DACTUA(RGL,ARGL,ITS,COEF)        
 7    CONTINUE
      END
      
      SUBROUTINE SMATPL(SGM,S)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE CONTRAINTE POUR LA RIGIDITE GEOMETRIQUE --  PLAN -- %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION SGM(1),S(4,4)
      DO 1 I=1,4
      DO 1 J=1,4
 1    S(I,J)=0.D0
      S(1,1)=SGM(1)
      S(2,2)=SGM(2)
      S(1,2)=SGM(3)
      S(2,1)=SGM(3)
      S(3,3)=SGM(1)
      S(4,4)=SGM(2)
      S(3,4)=SGM(3)
      S(4,3)=SGM(3)                 
      END
      
      SUBROUTINE SMATAX(SGM,S)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE CONTRAINTE POUR LA RIGIDITE GEOMETRIQUE --  AXIS -- %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION SGM(1),S(5,5)
      DO 1 I=1,5
      DO 1 J=1,5
 1    S(I,J)=0.D0
      S(1,1)=SGM(1)
      S(2,2)=SGM(2)
      S(1,2)=SGM(3)
      S(2,1)=SGM(3)
      S(3,3)=SGM(1)
      S(4,4)=SGM(2)
      S(3,4)=SGM(3)
      S(4,3)=SGM(3)
      S(5,5)=SGM(4)                 
      END

      SUBROUTINE BNLGISO2D(X,Y,KSI,ETA,VE,BNL,G,T,DETJ,R,
     *                     NDDLE,NBN,ND,ITYP,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM % CALCUL DE LA MATRICE BNL AU POINT DE COORD. LOCALES KSI,ETA     % 
COMM %                                                                 % 
COMM %   --  ELEMENTS ISOPARAMETRIQUES  2D :  T3  Q4  T6  Q8 --        %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION  X(NBN),Y(NBN),VE(NDDLE),BNL(ND,NDDLE),
     *           G(ND+1,NDDLE),T(ND+1)
      DOUBLE PRECISION KSI,ETA,J(4),INVJ(4),
     *N(NBN),NKSI(NBN),NETA(NBN),NX(NBN),NY(NBN)     
COMM
      IF(ITYP.EQ.30) THEN
        CALL FORMT3(KSI,ETA,N,NKSI,NETA)
      ELSE IF(ITYP.EQ.40) THEN
        CALL FORMQ4(KSI,ETA,N,NKSI,NETA)
      ELSE IF(ITYP.EQ.60) THEN
        CALL FORMT6(KSI,ETA,N,NKSI,NETA)        
      ELSE IF(ITYP.EQ.80) THEN
        CALL FORMQ8(KSI,ETA,N,NKSI,NETA)
      ELSE IF(ITYP.EQ.81) THEN
        CALL FORMQ8(KSI,ETA,N,NKSI,NETA)        
      ELSE
        STOP ' BNLGISO2D : pb sur ITYP '
      ENDIF         
COMM .......///// Matrice jacobienne et son determinant///////.......... 
      DO 5 I=1,4
 5    J(I)=0.D0     
      DO 1 I=1,NBN
      J(1)=J(1)+NKSI(I)*X(I)
      J(2)=J(2)+NKSI(I)*Y(I)
      J(3)=J(3)+NETA(I)*X(I)
      J(4)=J(4)+NETA(I)*Y(I)
  1   CONTINUE 
      DETJ=J(1)*J(4)-J(2)*J(3)
      IF(DABS(DETJ).LT.1.D-10) STOP ' Mat. Jacob. non inversible '
COMM ............///// Matrice jacobienne inverse ///////................
      INVJ(1)= J(4)/DETJ
      INVJ(2)=-J(2)/DETJ
      INVJ(3)=-J(3)/DETJ
      INVJ(4)= J(1)/DETJ
COMM .....//// Derivee des fonctions d' interpolation (X,Y) //////......
      DO 2 I=1,NBN     
      NX(I) =INVJ(1)*NKSI(I) + INVJ(2)*NETA(I)
      NY(I) =INVJ(3)*NKSI(I) + INVJ(4)*NETA(I)
 2    CONTINUE
COMM ..................///// Matrice G  ///////.........................
      DO 4 I=1,NBN
      G(1,1+2*(I-1))=NX(I)
      G(2,1+2*(I-1))=NY(I)
      G(3,1+2*(I-1))=0.D0
      G(4,1+2*(I-1))=0.D0       
      G(1,2+2*(I-1))=0.D0
      G(2,2+2*(I-1))=0.D0
      G(3,2+2*(I-1))=NX(I)
      G(4,2+2*(I-1))=NY(I)      
 4    CONTINUE 
COMM ..................///// Vecteur TETA  ///////......................
COMM  ..//On ne peut utiliser MATVEC ici car on effectue le produit //..
COMM  ..//G(i,j).VE(j) pour i=1 a 4, et 4 n'est pas toujours la     //..
COMM  ..//dimension du premier argument de G !!!                    //..
      DO 8 I=1,4
 8    T(I)=0.D0
      DO 9 I=1,4
      DO 9 K=1,NDDLE
 9    T(I)=T(I)+G(I,K)*VE(K)
COMM ............///// Matrice BNL = B + A(VE).TETA  ///////............      
      DO 3 I=1,NBN
      BNL(1,1+2*(I-1))=NX(I)  + T(1)*NX(I)
      BNL(2,1+2*(I-1))=0.D0   + T(2)*NY(I)
      BNL(3,1+2*(I-1))=NY(I)  + T(2)*NX(I) + T(1)*NY(I)
      BNL(1,2+2*(I-1))=0.D0   + T(3)*NX(I)
      BNL(2,2+2*(I-1))=NY(I)  + T(4)*NY(I)
      BNL(3,2+2*(I-1))=NX(I)  + T(4)*NX(I) + T(3)*NY(I)
 3    CONTINUE        
COMM          
COMM ......./////  Complement pour le cas axisymetrique ///////........
COMM
      IF(KTYP.EQ.3) THEN  
COMM ..................//// Rayon ////////.............................
      R=0.D0
      DO 6 I=1,NBN
 6    R=R+N(I)*X(I)     
COMM ..................///// Matrice G  ///////.........................
      DO 10 I=1,NBN
      G(5,1+2*(I-1))=N(I)/R
 10   G(5,2+2*(I-1))=0.D0
COMM ..................///// Vecteur TETA  ///////......................
      T(5)=0.D0 
      DO 11 K=1,NDDLE
 11   T(5)=T(5)+G(5,K)*VE(K)
COMM ............///// Matrice BNL = B + A(VE).TETA  ///////............       
      DO 7 I=1,NBN
      BNL(4,1+2*(I-1))=N(I)/R  +  T(5)*N(I)/R
      BNL(4,2+2*(I-1))=0.D0
 7    CONTINUE
COMM ..................////////////////////////........................
      ENDIF             
      END
          
COMM ...........................////////////...........................
COMM ...//////  FIN DU MODULE REGROUPANT LES SUBROUTINES DE  //////....
COMM ...//////  CALCULS  DE RIGIDITE TANGENTE 2D             //////....
COMM ...........................////////////...........................

