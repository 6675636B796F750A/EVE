COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.0  (Juillet 93)                  %
COMM %                                                                 %
COMM %       //////  MODULE REGROUPANT LES SUBROUTINES   //////        %
COMM %       //////   DE CALCULS NUMERIQUES STANDARDS    //////        %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
      SUBROUTINE DECCHO(A,IDIAG,NEQ,IOPT)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
COMM %                            T                                    %
COMM %   PROGRAM TO PERFORM  A = U * D * U  FACTORIZATION              %
COMM %   OF SYMMETRIC POSITIVE DEFINITE SYSTEM OF EQUATIONS            %
COMM %                                                                 %
COMM %   A(NA)  = COEFFICIENT MATRIX STORED IN COMPACTED COLUMN FORM   %
COMM %                     (AFTER FACTORIZATION CONTAINS D AND U)      %
COMM %   IDIAG(NEQ) = ADDRESSES OF DIAGONAL TERMS IN A(NA)             %
COMM %   NEQ        = NUMBER OF EQUATIONS                              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(1),IDIAG(1)
      DATA ZERO/0.0D0/
      ABS(X)=DABS(X)
C
      PREC=1.0D-16
C     SCALE=1.0D+50
      SCALE=1.0D+35
C
C.... FACTOR A
C
      JR = 0
      DO 600 J = 1,NEQ
      JD = IDIAG(J)
      JH = JD - JR
      IS = J - JH + 2
      IF(JH-2) 600,300,100
 100  IE = J - 1
      K = JR + 2
      ID = IDIAG(IS - 1)
C
C.... REDUCE ALL EQUATIONS EXCEPT DIAGONAL
C
      DO 200 I = IS,IE
      IR = ID
      ID = IDIAG(I)
      IH = MIN0(ID-IR-1,I-IS+1)
      IF(IH.GT.0) A(K) = A(K) - PROSCA(A(K-IH),A(ID-IH),IH)
  200 K = K + 1
C
C.... REDUCE DIAGONAL TERM
C
 300  IR = JR + 1
      IE = JD - 1
      K = J - JD
      DO 400 I = IR,IE
      ID = IDIAG(K+I)
      IF(A(ID).EQ.ZERO) GOTO 700
      D = A(I)
      A(I) = A(I)/A(ID)
      IF(IOPT.EQ.1.AND.ABS(A(I)).GT.SCALE) GOTO 800
      A(JD) = A(JD) - D*A(I)
      IF(A(JD)) 400,350,400
  350 IF(IOPT.EQ.0) GOTO 700
      A(JD)=-PREC
  400 CONTINUE
C
  600 JR = JD
C
      RETURN
C
  700 WRITE(6,1000)
      STOP
C
  800 WRITE(6,2000)
      IOPT=0
      RETURN
C
 1000 FORMAT(// ' *** ERROR  SOLUTION STOPS IN *DECCHO* ',/
     X          '     RIGID BODY MODE(S) FOUND ',/)
 2000 FORMAT(// ' *** ERROR  SOLUTION STOPS IN *DECCHO* ',/
     X          '     STURM-SEQUENCE ABANDONED BECAUSE OF MULTIPLIER',
     X          ' GROWTH')
      END  
          
      SUBROUTINE RESCHO(A,B,IDIAG,NEQ)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                            T                    %
COMM %  PROGRAM TO SOLVE    A v = B    with  A = U * D * U             %
COMM %                                                                 %
COMM %   BACKSUBSTITION OF SYMMETRIC POSITIVE DEFINITE SYSTEM          %
COMM %                                                                 %
COMM %   A(NA)  = COEFFICIENT MATRIX STORED IN COMPACTED COLUMN FORM   %
COMM %                      (AFTER FACTORIZATION CONTAINS D AND U)     %
COMM %   B(NEQ) = RIGHT SIDE VECTOR (AFTER BACKSUBSTITUTION CONTAINS   %
COMM %                        COLUMN SOLUTION)                         %
COMM %   IDIAG(NEQ) = ADDRESSES OF DIAGONAL TERMS IN A(NA)             %
COMM %   NEQ        = NUMBER OF EQUATIONS                              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(1),B(1),IDIAG(1)
      DATA ZERO/0.D0/
C
      JR = 0
      DO 600 J = 1,NEQ
      JD = IDIAG(J)
      JH = JD - JR
      IS = J - JH + 2
      IF(JH-2) 600,500,500
C
C.... REDUCE RHS
C
  500 B(J) = B(J) - PROSCA(A(JR+1),B(IS-1),JH-1)
  600 JR = JD
C
C.... DIVIDE BY DIAGONAL PIVOTS
C
      DO 700 I = 1,NEQ
      ID = IDIAG(I)
      IF(A(ID).NE.ZERO ) B(I) = B(I)/A(ID)
  700 CONTINUE
C
C.... BACKSUBSTITUTE
C
      J = NEQ
      JD = IDIAG(J)
  800 D = B(J)
      J = J - 1
      IF(J.LE.0) RETURN
      JR = IDIAG(J)
      IF(JD-JR.LE.1) GOTO 1000
      IS = J - JD + JR + 2
      K = JR - IS + 1
      DO 900 I = IS,J
  900 B(I) = B(I) - A(I+K)*D
 1000 JD = JR
      GOTO 800
C
      END
      
      SUBROUTINE SSPACE(IDIAG,A,B,R,EIGV,V,W,AR,BR,VEC,D,RTOLV,
     ;                  ROLD,DOLD,RAT,RATOLD,
     ;                  LIAI,NLIAI,NDPN,
     ;                  NN,NWK,NWM,NROOT,NC,NNC,NITEM,IFSS,RTOL)
C . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
C .                                                                   .
C .   P R O G R A M                                                   .
C .      TO SOLVE FOR THE SMALLEST EIGENVALUES AND CORRESPONDING      .
C .      EIGENVECTORS IN THE GENERALIZED EIGENPROBLEM USING THE       .
C .      SUBSPACE ITERATION METHOD                                    .
C .                                                                   .
C . --- INPUT VARIABLES ---                                           .
C .                                                                   .
C .      IDIAG(NN) = VECTOR CONTAINING ADDRESSES OF DIAGONAL          .
C .                  ELEMENTS IN STIFFNESS MATRIX                     .
C .      A(NWK)    = STIFFNESS MATRIX IN COMPACTED FORM - ASSUMED     .
C .                  POSITIVE DEFINITE                                .
C .      B(NWM)    = MASS MATRIX IN COMPACTED FORM                    .
C .      R(NN,NC)  = EIGENVECTORS ON SOLUTION EXIT                    .
C .      EIGV(NC)  = EIGENVALUES ON SOLUTION EXIT                     .
C .      V(NN)     = WORKING VECTOR                                   .
C .      W(NN)     = WORKING VECTOR                                   .
C .      AR(NNC)   = WORKING MATRIX STORING PROJECTION OF K           .
C .      BR(NNC)   = WORKING MATRIX STORING PROJECTION OF M           .
C .      VEC(NC,NC)= WORKING MATRIX                                   .
C .      D(NC)     = WORKING VECTOR                                   .
C .      RTOLV(NC) = WORKING VECTOR                                   .
C .      NN        = ORDER OF STIFFNESS AND MASS MATRICES             .
C .      NWK       = NUMBER OF ELEMENTS BELOW SKYLINE OF              .
C .                  STIFFNESS MATRIX                                 .
C .      NWM       = NUMBER OF ELEMENTS BELOW SKYLINE OF              .
C .                  MASS MATRIX                                      .
C .                     I.E. NWM=NWK FOR CONSISTENT MASS MATRIX       .
C .                          NWM=NN  FOR LUMPED MASS MATRIX           .
C .      NROOT     = NUMBER OF REQUIRED EIGENVALUES AND EIGENVECTORS  .
C .      NC        = NUMBER OF ITERATION VECTORS USED                 .
C .                     (USUALLY SET TO MIN(2*NROOT,NROOT+8), BUT NC  .
C .                     CANNOT BE LARGER THAN THE NUMBER OF MASS      .
C .                     DEGREES OF FREEDOM)                           .
C .      NNC       = NC*(NC+1)/2 DIMENSION OF STORAGE VECTORS AR,BR   .
C .      NITEM     = MAXIMUM NUMBER OF SUBSPACE ITERATIONS PERMITTED  .
C .                     (USUALLY SET TO 16)                           .
C .                  THE PARAMETERS NC AND/OR NITEM MUST BE INCREASED .
C .                  IF A SOLUTION HAS NOT CONVERGED                  .
C .      IFSS      = FLAG FOR STURM SEQUENCE CHECK                    .
C .                     EQ.0  NO CHECK                                .
C .                     EQ.1  CHECK                                   .
C .      RTOL      = CONVERGENCE TOLERANCE ON EIGENVALUES             .
C .                         (1.E-06 OR SMALLER)                       .
C .                                                                   .
C . --- OUTPUT ---                                                    .
C .                                                                   .
C .      EIGV(NROOT) = EIGENVALUES                                    .
C .      R(NN,NROOT) = EIGENVECTORS                                   .
C .                                                                   .
C . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(NWK),B(NWM),R(NN,1:*),V(NN),W(NN),EIGV(NC),
     1          D(NC),VEC(NC,1:*),AR(NNC),BR(NNC),RTOLV(NC)
      DIMENSION DOLD(NC),ROLD(NN,NC),RATOLD(NC),RAT(NC)
      DIMENSION LIAI(1)
      INTEGER IDIAG(NN)
C      COMMON/IO/IIN,IOUT
C      COMMON/FICH/NDGEO,NELAS,NPLAS,NTRAV,NRIGI,NRIGG
      ABS(X)=DA BS(X)
      SQRT(X)=DSQRT(X)
C
C     SET TOLERANCE FOR JACOBI ITERATION
C
      NSWEEP=5
      TOLJ=1.0D-08
C
C     INITIALIZATION
C
      ICONV=0
      NSCH=0
      N1=NC+1
      NC1=NC-1
C
      REWIND 91
      CALL ECFICH(A,NWK,91)
C
C     ON REND B DEFINI-POSITIF
C
      BMAX=CALMAX(B,NWM)
      IF(BMAX.LT.1) BMAX=1
      DO 123 I=1,NN
      II=IDIAG(I)
      IF(ABS(B(II)).LT.1.D-10) B(II)=1.D-10*BMAX
 123  CONTINUE
C
C     ON MODIFIE A POUR TENIR COMPTE DES DEPLACEMENTS IMPOSES NULS
C
      AMAX=CALMAX(A,NWK)
      DO 124 I=1,NLIAI
      NNLI=LIAI(2*I-1)
      NDLI=LIAI(2*I  )
      J=NDPN*(NNLI-1)+NDLI
      JJ=IDIAG(J)
      A(JJ)=1.D10*AMAX
 124  CONTINUE
C
C     ESTABLISH STARTING ITERATION VECTORS
C
      ND=NN/NC
      IF(NWM.GT.NN)GOTO 4
      DO 2 I=1,NN
      II=IDIAG(I)
      R(I,1)=B(I)
C      IF(B(I)*A(II).LE.0.) GOTO 800
   2  W(I)=B(I)/A(II)
      GOTO 20
   4  DO 10 I=1,NN
      II=IDIAG(I)
      R(I,1)=B(II)
C      IF(B(II)*A(II).LE.0.) GOTO 800
  10  W(I)=B(II)/A(II)
C
   20 L=NN-ND
      DO 30 J=2,NC
      RT=0.
      DO 40 I=1,L
      IF(W(I).LT.RT)GOTO 40
      RT=W(I)
      IJ=I
  40  CONTINUE
      DO 50 I=L,NN
      IF(W(I).LE.RT)GOTO 50
      RT=W(I)
      IJ=I
   50 CONTINUE
      V(J)=FLOAT(IJ)
      W(IJ)=0.
      L=L-ND
   30 R(IJ,J)=1.
C
C     FACTORIZE A MATRIX
C
      ISH=0
      CALL DECCHO(A,IDIAG,NN,ISH)
C
C - - - -  S T A R T    O F   I T E R A T I O N    L O O P
C
      TOTLQ=0.
      KOUNT=0
      NITE=0
  100 NITE=NITE+1
      WRITE(6,1010) NITE
C
C     CALCULATE THE PROJECTIONS OF A AND B
C
      IJ=0
      DO 110 J=1,NC
      CALL SHIFTD(R(1,J),V,NN)
      CALL CALIMP(LIAI,V,NLIAI,NDPN)      
      CALL RESCHO(A,V,IDIAG,NN)
      DO 130 I=J,NC
      IJ=IJ+1
  130 AR(IJ)=PROSCA(R(1,I),V,NN)
  110 CALL SHIFTD(V,R(1,J),NN)
      IJ=0
      DO 160 J=1,NC
      IF(NWM.GT.NN)
     XCALL MULT1(B,R(1,J),V,IDIAG,NN)
      IF(NWM.EQ.NN)
     XCALL MULT2(B,R(1,J),V,NN)
      DO 180 I=J,NC
      IJ=IJ+1
  180 BR(IJ)=PROSCA(R(1,I),V,NN)
      IF(ICONV.GT.0) GOTO 160
      CALL SHIFTD(V,R(1,J),NN)
  160 CONTINUE
C
C     SOLVE FOR EIGENSYSTEM OF SUBSPACE OPERATORS
C
      CALL JACOBI(AR,BR,VEC,EIGV,W,NC,NNC,TOLJ,NSWEEP)
C
C
C     CALCULATE B TIMES APPROXIMATE EIGENVECTORS (ICONV.EQ.0)
C     OR FINAL EIGENVECTOR APPROXIMATIONS(ICONV.GT.0)
C
      DO 420 I=1,NN
      DO 421 J=1,NC
  421 V(J)=R(I,J)
      DO 420 K=1,NC
  420 R(I,K)=PROSCA(V,VEC(1,K),NC)
      IF(ICONV.GT.0)GOTO 500
C
C     CHECK FOR CONVERGENCE OF EIGENVALUES
C
      DO 380 I=1,NC
      DIF=ABS(EIGV(I)-D(I))
  380 RTOLV(I)=DIF/EIGV(I)
      DO 390 I=1,NROOT
      IF(RTOLV(I).GT.RTOL)GOTO 400
  390 CONTINUE
      WRITE(6,1060) NITE,RTOL
      ICONV=1
      GOTO 100
C
  400 IF(NITE.LT.NITEM)GOTO 410
      RTOLQ=RTOL
      DO 405 I=1,NROOT
  405 IF(RTOLV(I).GT.RTOLQ) RTOLQ=RTOLV(I)
      IF(NITE.LT.NITEM) GOTO 410
      RTOL=RTOLQ
      WRITE(6,1070) NITEM
      WRITE(6,1060) NITE,RTOL
      ICONV=1
      GOTO 100
C
C     USE OVERRELAXATION TO SPEED CONVERGENCE IF APPROPRIATE
C
  410 IF(NITE.LT.3)GOTO 409
      DO 412 I=1,NC
      IF(RTOLV(I).LT.RTOL)GOTO 412
      DIF=ABS(EIGV(I)-D(I))
      DIF2=ABS(D(I)-DOLD(I))
      RAT(I)=DIF/DIF2
      IF(NITE.LT.4)GOTO 412
C
C     SEE IF RELAXATION CAN BE PERFORMED
C
      IF(RTOLV(I).GT.1.0D-3)GOTO 412
      TEST=ABS(1.0D0-RATOLD(I)/RAT(I))
      IF(TEST.GT.(2.5D-1))GOTO 412
C
C     FIND OVERRELAXATION FACTOR
C
      RAT2=SQRT(RAT(I))
      RLAMQ=EIGV(I)/RAT2
      TOTLQ=TOTLQ+RLAMQ
      KOUNT=KOUNT+1
      AVLQ=TOTLQ/KOUNT
      ALPH=1.0D0/(1.0D0-EIGV(I)/AVLQ)
C
C     PERFORM THE OVERRELAXATION
C
      DO 413 J=1,NN
  413 R(J,I)=ROLD(J,I)+ALPH*(R(J,I)-ROLD(J,I))
  412 CONTINUE
      CALL SHIFTD(RAT,RATOLD,NC)
  409 CALL SHIFTD(D,DOLD,NC)
      CALL SHIFTD(R,ROLD,NN*NC)
      CALL SHIFTD(EIGV,D,NC)
      GOTO 100
C
C - - - -  E N D   O F   I T E R A T I O N   L O O P
C
C     CALCULATE AND PRINT ERROR NORMS
C
  500 REWIND 91
      CALL LEFICH(A,NWK,91)
C
      WRITE(6,2000)
      DO 580 L=1,NROOT
      RT=EIGV(L)
      CALL MULT1(A,R(1,L),V,IDIAG,NN)
      IF(NWM.GT.NN)
     XCALL MULT1(B,R(1,L),W,IDIAG,NN)
      IF(NWM.EQ.NN)
     XCALL MULT2(B,R(1,L),W,NN)
      VNORM=0.
      WNORM=0.
      DO 600 I=1,NN
      VNORM=VNORM+V(I)*V(I)
      V(I)=V(I)-RT*W(I)
  600 WNORM=WNORM+V(I)*V(I)
      ERROR=SQRT(WNORM/VNORM)
  580 WRITE(6,2001) L,RT,ERROR
C
C     APPLY STURM SEQUENCE CHECK
C
      IF(IFSS.EQ.0)GOTO 700
C     CALL SCHECK(EIGV,RTOLV,W,D,NC,NEI,RTOL,DECAL)
      CALL SCHECK(EIGV,RTOLV,W,D,NROOT,NEI,RTOL,DECAL)
C
      WRITE(6,1120)DECAL
C
C     SHIFT MATRIX A
C
      IF(NWM.GT.NN)GOTO 645
      DO 640 I=1,NN
      II=IDIAG(I)
  640 A(II)=A(II)-B(I)*DECAL
      GOTO 660
  645 DO 650 I=1,NWK
  650 A(I)=A(I)-B(I)*DECAL
C
C     FACTORIZE SHIFTED MATRIX
C
  660 ISH=1
      CALL CALIRI(A,IDIAG,LIAI,NLIAI,NDPN,NN)
      CALL DECCHO(A,IDIAG,NN,ISH)
      IF(ISH.NE.1) GOTO 700
C
C     COUNT NUMBER OF NEGATIVE DIAGONAL ELEMENTS
C
      NSCH=0
      DO 664 I=1,NN
      II=IDIAG(I)
      IF(A(II).LT.0)NSCH=NSCH+1
  664 CONTINUE
      IF(NSCH.EQ.NEI)GOTO 670
      NMIS=NSCH-NEI
      WRITE(6,1130)NMIS
      GOTO 700
 670  WRITE(6,1140)NSCH
C
  700 RETURN
C
  800 WRITE(6,1000) I
      STOP
C
 1000 FORMAT(// ' *** ERROR   SOLUTION STOPS IN *SSPACE* ',/,
     X          '     NEGATIVE OR ZERO DIAGONAL ENCOUNTERED ',/,
     X          '     BEFORE DECOMPOSITION AT EQUATION ',I5/)
 1010 FORMAT(// ' I T E R A T I O N   N U M B E R ',I4)
 1060 FORMAT(// ' *** CONVERGENCE ACHIEVED AFTER ',I3,' ITERATIONS ',/,
     X          '     FOR CONVERGENCE TOLERANCE ',1PE12.3)
 1070 FORMAT(// ' *** NO CONVERGENCE IN MAXIMUM NUMBER OF ITERATIONS ',/
     X          '     PERMITTED ',I4,/,
     X          '     WE ACCEPT CURRENT ITERATION VALUES ')
 1120 FORMAT(// ' *** CHECK APPLIED AT SHIFT ',1PE15.6)
 1130 FORMAT(// ' *** THERE ARE ',I4,' EIGENVALUES MISSING ')
 1140 FORMAT(// ' *** WE FOUND THE LOWEST ',I4,' EIGENVALUES ')
 2000 FORMAT(// '  MODE ',/,' NUMBER   EIGENVALUE    ERROR BOUND ',/)
 2001 FORMAT(1H ,I5,2(4X,1PE10.3))
C
      END

      SUBROUTINE JACOBI(A,B,X,EIGV,D,N,NW,RTOL,NSMAX)
C-----------------------------------------------------------------------
C
C     PROGRAM TO SOLVE THE GENERALIZED EIGENPROBLEM  K:X = L.M:X
C                                                    - -     - -
C            USING THE GENERALIZED JACOBI ITERATION
C
C          REFERENCE : K. J. BATHE AND E. L. WILSON ( 1976 )
C                      NUMERICAL METHODS IN FINITE ELEMENT ANALYSIS
C                          PRENTICE-HALL,INC.,NEW JERSEY
C
C
C     ---INPUT VARIABLES---
C
C        A(N,N)    = STIFFNESS MATRIX ( SYMMETRIC POSITIVE DEFINITE )
C        B(N,N)    = MASS MATRIX      ( SYMMETRIC POSITIVE DEFINITE )
C        X(N,N)    = MATRIX STORING EIGENVECTORS ON SOLUTION EXIT
C        EIGV(N)   = MATRIX STORING EIGENVALUES ON SOLUTION EXIT
C        D(N)      = WORKING VECTOR
C        N         = ORDER OF MATRICES A AND B
C        NW        = N*(N+1)/2 DIMENSION OF STORAGE VECTORS A AND B
C        RTOL      = CONVERGENCE TOLERANCE (USUALLY SET TO 10.**-12)
C        NSMAX     = MAXIMUM NUMBER OF SWEEPS ALLOWED
C                         (USUALLY SET TO 10)
C
C     ---OUTPUT---
C
C        A(N,N)    = DIAGONALIZED STIFFNESS MATRIX
C        B(N,N)    = DIAGONALIZED MASS MATRIX
C        X(N,N)    = EIGENVECTORS STORED COLUMNWISE
C        EIGV(N)   = EIGENVALUES
C
C-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
      DIMENSION A(NW),B(NW),X(N,N),EIGV(N),D(N)
C      COMMON/IO/IIN,IOUT
C      COMMON/FICH/NDGEO,NELAS,NPLAS,NTRAV,NRIGI,NRIGG
C
      DATA ZERO,ONE/0.D0,1.D0/
C     DATA SCALE1,SCALE2/1.0D50,1.0D-50/
      DATA SCALE1,SCALE2/1.0D35,1.0D-35/
      ABS(XYZ)=DABS(XYZ)
      SQRT(XYZ)=DSQRT(XYZ)
C
C     INITIALIZE EIGENVALUE AND EIGENVECTOR MATRICES
C
      N1=N+1
      NR=N-1
C
      CALL DZERO(X,N*N)
C
      II=1
      DO 10 I=1,N
      IF(A(II).GT.ZERO .AND. B(II).GT.ZERO) GOTO 4
      WRITE (6,2020)
      STOP
    4 D(I)=A(II)/B(II)
      EIGV(I)=D(I)
      X(I,I)=ONE
   10 II=II+N1-I
      IF(N.EQ.1) RETURN
C
C     INITIALIZE SWEEP COUNTER AND BEGIN ITERATION
C
      NSWEEP=0
   40 NSWEEP=NSWEEP+1
C
C     CHECK IF PRESENT OFF-DIAGONAL ELEMENT IS LARGE ENOUGH
C              TO REQUIRE ZEROING
C
      EPS=0.01**NSWEEP
      DO 210 J=1,NR
      JP1=J+1
      JM1=J-1
      LJK=JM1*N-JM1*J/2
      JJ=LJK+J
      DO 210 K=JP1,N
      KP1=K+1
      KM1=K-1
      JK=LJK+K
      KK=KM1*N-KM1*K/2+K
      IF(ABS(A(JK)).LT.SCALE2) A(JK)=ZERO
      IF(ABS(B(JK)).LT.SCALE2) B(JK)=ZERO
      IF(ABS(A(JK)).LT.EPS*SQRT(A(JJ)*A(KK)).AND.
     X   ABS(B(JK)).LT.EPS*SQRT(B(JJ)*B(KK))) GOTO 210
C
C     IF ZEROING IS REQUIRED, CALCULATE THE ROTATION MATRIX
C        ELEMENTS CA AND CG
C
      AKK=A(KK)*B(JK)-B(KK)*A(JK)
      AJJ=A(JJ)*B(JK)-B(JJ)*A(JK)
      AB=A(JJ)*B(KK)-A(KK)*B(JJ)
      CHECK=AB*AB/4.+AKK*AJJ
      IF(CHECK)50,60,60
   50 WRITE(6,2020)
      STOP
   60 SQCH=SQRT(CHECK)
      D1=AB/2.+SQCH
      D2=AB/2.-SQCH
      DEN=D1
      IF(ABS(D2).GT.ABS(D1))DEN=D2
      IF(DEN)80,70,80
   70 CA=ZERO
      CG=-A(JK)/A(KK)
      GOTO 90
   80 CA=AKK/DEN
      CG=-AJJ/DEN
C
C     PERFORM THE GENERALIZED ROTATION TO ZERO THE PRESENT
C             OFF-DIAGONAL ELEMENT
C
   90 IF(ABS(CA).LT.SCALE2) CA=ZERO
      IF(ABS(CG).LT.SCALE2) CG=ZERO
      IF(N-2) 100,190,100
  100 IF(JM1-1)130,110,110
  110 DO 120 I=1,JM1
      IM1=I-1
      IJ=IM1*N-IM1*I/2+J
      IK=IM1*N-IM1*I/2+K
      AJ=A(IJ)
      BJ=B(IJ)
      AK=A(IK)
      BK=B(IK)
      A(IJ)=AJ+CG*AK
      B(IJ)=BJ+CG*BK
      A(IK)=AK+CA*AJ
  120 B(IK)=BK+CA*BJ
  130 IF(KP1-N)140,140,160
  140 LJI=JM1*N-JM1*J/2
      LKI=KM1*N-KM1*K/2
      DO 150 I=KP1,N
      JI=LJI+I
      KI=LKI+I
      AJ=A(JI)
      BJ=B(JI)
      AK=A(KI)
      BK=B(KI)
      A(JI)=AJ+CG*AK
      B(JI)=BJ+CG*BK
      A(KI)=AK+CA*AJ
  150 B(KI)=BK+CA*BJ
  160 IF(JP1-KM1)170,170,190
  170 LJI=JM1*N-JM1*J/2
      DO 180 I=JP1,KM1
      JI=LJI+I
      IM1=I-1
      IK=IM1*N-IM1*I/2+K
      AJ=A(JI)
      BJ=B(JI)
      AK=A(IK)
      BK=B(IK)
      A(JI)=AJ+CG*AK
      B(JI)=BJ+CG*BK
      A(IK)=AK+CA*AJ
  180 B(IK)=BK+CA*BJ
  190 AK=A(KK)
      BK=B(KK)
      A(KK)=AK+2.*CA*A(JK)+CA*CA*A(JJ)
      B(KK)=BK+2.*CA*B(JK)+CA*CA*B(JJ)
      A(JJ)=A(JJ)+2.*CG*A(JK)+CG*CG*AK
      B(JJ)=B(JJ)+2.*CG*B(JK)+CG*CG*BK
      A(JK)=ZERO
      B(JK)=ZERO
C
C     UPDATE THE EIGENVECTOR MATRIX AFTER EACH ROTATION
C
      DO 200 I=1,N
      XJ=X(I,J)
      XK=X(I,K)
      X(I,J)=XJ+CG*XK
  200 X(I,K)=XK+CA*XJ
  210 CONTINUE
C
C     UPDATE THE EIGENVALUES AFTER EACH SWEEP
C
      II=1
      DO 220 I=1,N
      IF(A(II).GT.ZERO .AND. B(II).GT.ZERO) GOTO 215
      WRITE(6,2020)
      STOP
  215 EIGV(I)=A(II)/B(II)
  220 II=II+N1-I
C
C     CHECK FOR CONVERGENCE
C
      DO 240 I=1,N
      TOL=RTOL*D(I)
      DIF=ABS(EIGV(I)-D(I))
      IF(DIF.GT.TOL) GOTO 280
  240 CONTINUE
C
C     CHECK ALL OFF-DIAGONAL ELEMENTS TO SEE
C        IF ANOTHER SWEEP IS REQUIRED
C
      DO 250 J=1,NR
      JM1=J-1
      JP1=J+1
      LJK=JM1*N-JM1*J/2
      JJ=LJK+J
      DO 250 K=JP1,N
      KM1=K-1
      JK=LJK+K
      KK=KM1*N-KM1*K/2+K
      IF(ABS(A(JK)).LT.SCALE2) A(JK)=ZERO
      IF(ABS(B(JK)).LT.SCALE2) B(JK)=ZERO
      IF(ABS(A(JK)).GT.RTOL*SQRT(A(JJ)*A(KK)).OR.
     X   ABS(B(JK)).GT.RTOL*SQRT(B(JJ)*B(KK))) GOTO 280
  250 CONTINUE
C
C     SCALE EIGENVECTORS
C
  255 II=1
      DO 275 I=1,N
      BB=ONE/SQRT(B(II))
      DO 270 K=1,N
  270 X(K,I)=X(K,I)*BB
  275 II=II+N1-I
C
C     ARRANGE EIGENVALUES IN ASCENDING ORDER
C
  350 IS=0
      DO 360 I=1,NR
      IF(EIGV(I+1).GE.EIGV(I)) GOTO 360
      IS=IS+1
      EIGVT=EIGV(I+1)
      EIGV(I+1)=EIGV(I)
      EIGV(I)=EIGVT
      DO 370 K=1,N
      RT=X(K,I+1)
      X(K,I+1)=X(K,I)
  370 X(K,I)=RT
  360 CONTINUE
      IF(IS.GT.0)GOTO 350
C
      RETURN
C
C     UPDATE D MATRIX AND START NEW SWEEP, IF ALLOWED
C
  280 IF(NSWEEP.GE.NSMAX) GOTO 255
      DO 290 I=1,N
  290 D(I)=EIGV(I)
      GOTO 40
C
C     FORMAT STATEMENTS
C
 2000 FORMAT(27H0SWEEP NUMBER IN *JACOBI* = ,I5)
 2010 FORMAT(10(1PE12.3))
 2020 FORMAT(// ' *** ERROR SOLUTION STOPS IN *JACOBI* ',/
     X          '     MATRICES NOT POSITIVE DEFINITE ')
 2030 FORMAT(36H0CURRENT EIGENVALUES IN *JACOBI* ARE ,/)
C
      END

      SUBROUTINE MULT1(A,V,W,IDIAG,NEQ)
C-----------------------------------------------------------------------
C.... PROGRAM TO PERFORM A . V = W
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
      DIMENSION A(1),V(1),W(1),IDIAG(1)
C
      JH=1
      DO 100 J=1,NEQ
      JD=IDIAG(J)
      KK=JD-JH+1
      W(J)=PROSCA(A(JH),V(J-KK+1),KK)
C
      IF(J.GE.NEQ) RETURN
C
      I=0
      J1=J+1
      DO 200 K=J1,NEQ
      I=I+1
      JK=IDIAG(K)-I
      IF(JK.GT.IDIAG(K-1)) W(J)=W(J)+A(JK)*V(K)
 200  CONTINUE
 100  JH=JD+1
C
      RETURN
      END
      SUBROUTINE MULT2(A,V,W,NEQ)
C-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
      DIMENSION A(1),V(1),W(1)
C
      DO 100 I=1,NEQ
  100 W(I)=A(I)*V(I)
C
      RETURN
      END


      SUBROUTINE SCHECK(EIGV,RTOLV,BUPC,NEIV,NC,NEI,RTOL,DECAL)
C-----------------------------------------------------------------------
C     P R O G R A M
C          TO EVALUATE SHIFT ( DECAL ) FOR STURM SEQUENCE CHECK
C-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C      COMMON/IO/IIN,IOUT
C
      DIMENSION EIGV(NC),RTOLV(NC),BUPC(NC)
C
      INTEGER NEIV(NC)
C
      FTOL=0.001
C
      NROOT=0
      DO 120 I=1,NC
  120 IF(RTOLV(I).LE.RTOL) NROOT=NROOT+1
      IF(NROOT.GE.1) GOTO 200
      WRITE (6,1010)
      STOP
C
C       FIND UPPER BOUNDS ON EIGENVALUE CLUSTERS
C
  200 DO 240 I=1,NROOT
  240 NEIV(I)=1
      IF(NROOT.NE.1) GOTO 260
      BUPC(1)=EIGV(1)*(1.+FTOL)
      LM=1
      L=1
      I=2
      GOTO 295
  260 L=1
      I=2
  270 IF(EIGV(I-1)*(1.+FTOL) .LE. EIGV(I)*(1.-FTOL)) GOTO 280
      NEIV(L)=NEIV(L)+1
      I=I+1
      IF(I.LE.NROOT) GOTO 270
  280 BUPC(L)=EIGV(I-1)*(1.+FTOL)
      IF(I.GT.NROOT) GOTO 290
      L=L+1
      I=I+1
      IF(I.LE.NROOT) GOTO 270
      BUPC(L)=EIGV(I-1)*(1.+FTOL)
  290 LM=L
C     IF(NROOT.EQ.NC) GOTO 300
  295 IF(EIGV(I-1)*(1.+FTOL) .LE. EIGV(I)*(1.-FTOL)) GOTO 300
      IF(RTOLV(I).GT.RTOL) GOTO 300
      BUPC(L)=EIGV(I)*(1.+FTOL)
      NEIV(L)=NEIV(L)+1
      NROOT=NROOT+1
      IF(NROOT.EQ.NC) GOTO 300
      I=I+1
      GOTO 295
C
C       FIND SHIFT
C
  300 WRITE (6,1020)
      WRITE (6,1005) (BUPC(I),I=1,LM)
      WRITE (6,1030)
      WRITE (6,1006) (NEIV(I),I=1,LM)
      LL=LM-1
      IF(LM.EQ.1) GOTO 310
  330 DO 320 I=1,LL
  320 NEIV(L)=NEIV(L)+NEIV(I)
      L=L-1
      LL=LL-1
      IF(L.NE.1) GOTO 330
  310 WRITE (6,1040)
      WRITE (6,1006) (NEIV(I),I=1,LM)
      L=0
      DO 340 I=1,LM
      L=L+1
      IF(NEIV(I).GE.NROOT) GOTO 350
  340 CONTINUE
  350 DECAL=BUPC(L)
      NEI=NEIV(L)
C
      RETURN
C
 1005 FORMAT(5(1PE15.6))
 1006 FORMAT(5I15)
 1010 FORMAT(// ' *** ERROR   SOLUTION STOPS IN *SCHECK* ',/
     X          '     NO EIGENVALUES FOUND. ')
 1020 FORMAT(1H1,// ' *** STURM-SEQUENCE CHECK ',//
     X          '     UPPER BOUNDS ON EIGENVALUE CLUSTERS ',/)
 1030 FORMAT(// ' *** NO. OF EIGENVALUES IN EACH CLUSTER ',/)
 1040 FORMAT(// ' *** NO. OF EIGENVALUES LESS THAN UPPER BOUNDS ',/)
      END
      
      SUBROUTINE MULTMATVEC (A,V,W,IDIA,NEQ)
COMM *********************************************************
COMM * Subroutine to perform W = A.V                         *
COMM *********************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(1),V(1),W(1),IDIA(1)
COMM ** Initalisation du vecteur resultat                   **
COMM **   (calcul des termes diagonaux).                    **
      DO 10 J=1,NEQ
         W(J) = A(IDIA(J)) * V(J)
 10   CONTINUE
COMM ** Calcul des termes non diagonaux.                    **
      DO 20 J=NEQ,2,-1
        I = J
        DO 30 IPOS = IDIA(J)-1,IDIA(J-1)+1,-1
          I    = I - 1
          W(J) = W(J) + A(IPOS) * V(I)
          W(I) = W(I) + A(IPOS) * V(J)
 30     CONTINUE
 20   CONTINUE
      END

CBR
CBR%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CBR    Methode d'elimination de Gauss, matrice carre
C  
      SUBROUTINE MATINV(A,IPIVOT,INDEX,PIVOT,N)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION A(N,N),IPIVOT(N),INDEX(N,3),PIVOT(N)
C               DIMENSION IPIVOT(M),INDEX(M,3)
C               COMPLEX A(M,M),PIVOT(M)
C               COMPLEX SWAP,T,AMAX,DETERM
********************
*    INITIALISATION
*     DETERM=1.

c********************
               DO 20 J=1,N
  20           IPIVOT(J)=0
               DO 550 I=1,N
C*************************
C  LA RECHERCHE DU PIVOT
C*************************
               AMAX=0.
               DO 105 J=1,N
               IF(IPIVOT(J)-1)60,105,60
  60           DO 100 K=1,N
               IF(IPIVOT(K)-1)80,100,740
  80           IF(ABS(AMAX)-ABS(A(J,K)))85,100,100
  85           IROW=J
               ICOLUM=K
               AMAX=A(J,K)
  100          CONTINUE
  105          CONTINUE
               IPIVOT(ICOLUM)=IPIVOT(ICOLUM)+1
C**************************************************************
C  CHANGEMENT DES LIGNES POUR METTRE LE PIVOT DANS LA DIAGONALE
C**************************************************************
               IF(IROW-ICOLUM)140,260,140
c***************
C140      DETERM=-DETERM
C****************
  140           CONTINUE
C                SWAP=SM(IROW)
C                SM(IROW)=SM(ICOLUM)
C                SM(ICOLUM)=SWAP
                DO 200 L=1,N
                SWAP=A(IROW,L)
                A(IROW,L)=A(ICOLUM,L)
  200           A(ICOLUM,L)=SWAP
  260           INDEX(I,1)=IROW
                INDEX(I,2)=ICOLUM
                PIVOT(I)=A(ICOLUM,ICOLUM)
c*****************************************
C              DETERM=DETERM*PIVOT(I)
c  DIVISION DE LA LIGNE PIVOT PAR LE PIVOT
C*****************************************
                A(ICOLUM,ICOLUM)=1.
                DO 350 L=1,N
                A(ICOLUM,L)=A(ICOLUM,L)/PIVOT(I)
 350  CONTINUE
c****************************
c REDUCTION DE LA LIGNE PIVOT
C****************************
                DO 551 L1=1,N
                IF(L1-ICOLUM)400,551,400
  400           T=A(L1,ICOLUM)
                A(L1,ICOLUM)=0.
                DO 450 L=1,N
  450           A(L1,L)=A(L1,L)-A(ICOLUM,L)*T
  551           CONTINUE
  550           CONTINUE
C******************************
C  CHANGEMENT DE COLUMNS
C*****************************
                DO 710 I=1,N
                L=N+1-I
                IF(INDEX(L,1)-INDEX(L,2))630,710,630
  630           JROW=INDEX(L,1)
                JCOLUM=INDEX(L,2)
                DO 705 K=1,N
                SWAP=A(K,JROW)
                A(K,JROW)=A(K,JCOLUM)
                A(K,JCOLUM)=SWAP
  705           CONTINUE
  710           CONTINUE
  740           RETURN
                END

      
      
      
