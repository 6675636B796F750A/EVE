      PROGRAM EVE
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %  Origine :  LE PROGAMME ELEMENT FINI 2D  "EVE" (Juin 1986)     %
COMM %  developpe par Yves MEZIERE au L.M.S. - ECOLE POLYTECHNIQUE -  %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %  Developpement de EVE au L.P.M.M. de l'UNIVERSITE de METZ      %
COMM %                                                                %
COMM %               Bruno COCHELIN                                   %
COMM %               Les etudiants du DESS GMP CFMAO                  %
COMM %               Les etudiants du DEA                             %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %                  EVE  Version 2.1     (Novembre 94)            %
COMM %                                                                %
COMM %                                                                %
COMM %        Pour tous renseignements sur le fonctionnement          %
COMM %       du code de calcul, ou sur le contenu et le role          %
COMM %      de chaque subroutines, consulter la documentation         %
COMM %                  de EVE Version 2.1                            %
COMM %                                                                %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      CHARACTER*4 RMOT
      CHARACTER*80 TITRE,TITCHA
      LOGICAL TOURNE
      PARAMETER (IDIMT=16000000)
      PARAMETER (IDIMCO=100)
      COMMON T(IDIMT)
      COMMON/INI/ICO(200),IT(200)
      COMMON/TIT/TITRE,TITCHA(10)
COMM
COMM ..................................................................
COMM ////// AFFICHAGE D'UN ENTETE DANS LE FICHIER DE SORTIE      //////
COMM ..................................................................
      CALL ENTETE
COMM ..................................................................
COMM ////// OUVERTURE DES FICHIERS   5   FICHIER DES MOTS CLES  //////
COMM //////                          6   FICHIER DE SORTIE      //////
COMM ..................................................................
c        OPEN(UNIT=5,FILE='pnlg.cle',STATUS='OLD',ACCESS='SEQUENTIAL')
c        OPEN(UNIT=6,FILE='^2',ACCESS='SEQUENTIAL')
c        OPEN(UNIT=5,FILE='plr.pert.cle',STATUS='OLD',ACCESS='SEQUENTIAL')
c        OPEN(UNIT=6,FILE='plr.pert.pr',ACCESS='SEQUENTIAL')
COMM ..................................................................
COMM //////     OUVERTURE DES FICHIERS DE TRAVAIL INTERNES      //////
COMM ..................................................................
C      OPEN(UNIT=90,STATUS='SCRATCH',
C     *ACCESS='SEQUENTIAL',FORM='UNFORMATTED')
C      OPEN(UNIT=91,STATUS='SCRATCH',
C     *ACCESS='SEQUENTIAL',FORM='UNFORMATTED')
C      OPEN(UNIT=92,STATUS='SCRATCH',
C     *ACCESS='SEQUENTIAL',FORM='UNFORMATTED')
C      OPEN(UNIT=101,STATUS='SCRATCH',
C     *ACCESS='SEQUENTIAL',FORM='UNFORMATTED')
C      OPEN(UNIT=102,STATUS='SCRATCH',
C     *ACCESS='SEQUENTIAL',FORM='UNFORMATTED')      
COMM ..................................................................
COMM //////            LECTURE DES MOT CLE                      //////
COMM ..................................................................
      TOURNE=.TRUE.
      DO WHILE(TOURNE)
      READ(5,'(A4)') RMOT
      WRITE(6,'(A4)') RMOT
      IF(RMOT.EQ.'DEFI') THEN
COMM ..................................................................
COMM //////            CAS DU MOT CLE "DEFINITION"              //////
COMM ..................................................................
      CALL DONNEE
COMM
      ELSE IF(RMOT.EQ.'ELAS') THEN
COMM ..................................................................
COMM //////       CAS DU MOT CLE "ELASTICITE LINEAIRE"           //////
COMM ..................................................................
      CALL ELASTI(IDIMT)
COMM
      ELSE IF(RMOT.EQ.'NLGE') THEN
COMM ..................................................................
COMM //////       CAS DU MOT CLE "NON LINEAIRE GEOMETRIQUE"      //////
COMM ..................................................................
      CALL NLGEOM(IDIMT)
COMM
      ELSE IF(RMOT.EQ.'FLAM') THEN
COMM ..................................................................
COMM //////       CAS DU MOT CLE "FLAMBAGE"                      //////
COMM ..................................................................
      CALL FLAMBE(IDIMT)
COMM
      ELSE IF(RMOT.EQ.'VIBR') THEN
COMM ..................................................................
COMM //////       CAS DU MOT CLE "VIBRATION"                     //////
COMM ..................................................................
      CALL  VIBLIB(IDIMT)
COMM
      ELSE IF(RMOT.EQ.'DYNA') THEN                                   
COMM ..................................................................
COMM //////       CAS DU MOT CLE "DYNAMIQUE"                     //////
COMM ..................................................................
      CALL DYNAMI(IDIMT)
COMM
      ELSE IF(RMOT.EQ.'MODA') THEN                                   
COMM ..................................................................
COMM //////       CAS DU MOT CLE "MODALE"                       //////
COMM ..................................................................
      CALL MODALE(IDIMT)
COMM
      ELSE IF(RMOT.EQ.'ANM2') THEN                                   
COMM ..................................................................
COMM //////       CAS DU MOT CLE "ASYMP-NUMER-METHOD  CADRE 2"   //////
COMM ..................................................................
C      CALL ANMCA2(IDIMT) 
COMM           
      ELSE IF(RMOT.EQ.'MPLA') THEN
COMM ..................................................................
COMM //////       CAS DU MOT CLE "MAN_PLAS1_2D"   //////
COMM ..................................................................
      CALL MAN_PLASTIC_CHARGE_2D(IDIMT)
COMM
      ELSE IF(RMOT.EQ.'FIN ') THEN  
COMM ..................................................................
COMM //////       CAS DU MOT CLE "FIN"                           //////
COMM ..................................................................
      STOP'\\//\\//\\//\\//ARRET NORMAL\\//\\//\\//\\//'
      ELSE
       CALL ERROR1
      ENDIF
      ENDDO
      END

COMM .......................////////////...............................
COMM //////             FIN DU PROGRAMME PRINCIPAL               //////
COMM .......................////////////...............................


