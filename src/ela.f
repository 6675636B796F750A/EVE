COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %       //////  MODULE PILOTE DU CALCUL ELASTIQUE   //////        %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE ELASTI(IDIMT)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %              CALCUL ELASTIQUE DE LA STRUCTURE                   %
COMM %               POUR LES DIFFERENTS CHARGEMENTS                   %
COMM %                                                                 %
COMM %   Organisation de la memoire  (Super-Tableau T + pointeurs IT)  %
COMM %   --------------------------                                    %
COMM %                                                                 %
COMM %      adresse              contenu de T                 type     %
COMM %                                                                 %
COMM %   IT(11)---IDIA       Pointeurs de la diagonale       (ENTIER)  %
COMM %   IT(12)---Fi         vecteur de chargement           (DP)      %
COMM %   IT(13)---RIG        Matrice de rigidite             (DP)      %
COMM %   IT(14)---V          Vecteur deplacement             (DP)      %
COMM %   IT(15)---SIG        Contraintes aux points de Gauss (DP)      % 
COMM %   IT(16)---EPS        Deformation aux points de Gauss (DP)      %
COMM %   IT(17)---Ftra       Vecteur de travail chargement   (DP)      %
COMM %   IT(18)---Vtra       Vecteur de travail deplacement  (DP)      % 
COMM %   IT(19)---G.K-1.GT   Matrice de travail   relation   (DP)      %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      COMMON T(1)
      COMMON/INI/ICO(50),IT(50)
COMM ...........................////////////...........................
      KTYP =ICO(1)
      NCPN =ICO(2)
      NDPN =ICO(3)
      NNPE =ICO(4)      
      NSPG =ICO(5)      
      NDL  =ICO(7)
      NMAI =ICO(8)
      NVMA =ICO(10)
      NCHA =ICO(12)
      NLIAI=ICO(11)
      NRELA=ICO(24)
COMM
COMM .....//// RESERVATION DES PLACES DANS LE SUPER-TABLEAU ////......
COMM
      IT(12)=IT(11)+NDL 
      IF( IT(12)-((IT(12)/2)*2).EQ.0) IT(12)=IT(12)+1   
      IT(13)=IT(12)+2*NDL      
      CALL PROFBI(T(IT(11)),T(IT(2)),NDL,NMAI,NRIG,NNPE,NDPN,KTYP)
      ICO(26)=NRIG
      IT(14)=IT(13)+2*NRIG
      IT(15)=IT(14)+2*NDL
      CALL LONSIG(T(IT(2)),NMAI,NNPE,NSPG,NSIG,NEPS,KTYP)
      ICO(27)=NSIG
      ICO(28)=NEPS
      IT(16)=IT(15)+2*NSIG
      IT(17)=IT(16)+2*NEPS
      IT(18)=IT(17)+2*NDL
      IT(19)=IT(18)+2*NDL
      IT(20)=IT(19)+NRELA*(NRELA+1)
COMM
      WRITE(6,150)        
      WRITE(6,100)       
      DO 1 I=1,19
  1   WRITE(6,120) I,IT(I),IT(I+1)-IT(I)
      WRITE(6,130) IT(20),IDIMT
      IF(IT(20).GT.IDIMT) STOP 'Taille du Super-Tableau -->  ela.f'     
COMM            
COMM .../// ASSEMBLAGE DE LA MATRICE DE RIGIDITE , PRISE EN COMPTE //..
COMM .../// DES BLOCAGES, ET DECOMPOSITION                         //..
COMM
      CALL RIGELA(T(IT(13)),T(IT(11)),T(IT(1)),T(IT(2)),T(IT(3)),
     *T(IT(4)),NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)   
      CALL CALIRI(T(IT( 13)),T(IT(11)),T(IT(5)),NLIAI,NDPN,NDL)
      CALL DECCHO(T(IT( 13)),T(IT(11)),NDL,0)
COMM   
COMM ...////// DANS LE CAS DE RELATIONS ENTRE LES DDL   ///////........
COMM ...////// ASSEMBLAGE DE LA MATRICE DES RELATIONS   ///////........
COMM
      IF(NRELA.NE.0) THEN
      CALL MATREL(T(IT(13)),T(IT(11)),T(IT(7)),T(IT(18)),T(IT(17)),
     *T(IT(19)),NRELA,NDL)
      ENDIF
COMM
COMM ...////// POUR CHAQUE CHARGEMENT ON RESOUD   ///////.............. 
COMM     
      IAD=0
      DO 6 IC=1,NCHA
       CALL CAFORC(T(IT(6)+IAD),T(IT(12)))
       CALL SOLVE(T(IT(13)),T(IT(19)),T(IT(14)),T(IT(12)),T(IT(7)),
     *            T(IT(11)),T(IT(5)),NLIAI,NDL,NDPN,NRELA)
       CALL DZERO(T(IT(15)),NSIG)   
       CALL SIGELA(T(IT(15)),T(IT(14)),T(IT(1)),T(IT(2)),T(IT(3)),
     *             T(IT(4)),NMAI,NCPN,NDPN,NNPE,NVMA,KTYP)
       CALL IMPTCH(IC)
       CALL IMPELA(T(IT(14)),T(IT(15)),T(IT(2)))
       IAD=IAD+ICO(12+IC)
 6    CONTINUE
COMM ..................../////////////................................      
 150  FORMAT(////////,
     ;'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%',/,
     ;'  %                                                        %',/, 
     ;'  %         CALCUL   ELASTIQUE   LINEAIRE                  %',/,      
     ;'  %                                                        %',/,      
     ;'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')           
 100  FORMAT('1',//,' ',60('%'),///,
     ;'  ORGANISATION DU SUPER-TABLEAU  T() ',//)
 110  FORMAT(' ',60('%'),/////)
 120  FORMAT('   IT(',I3,')=',I8,'     Longueur=',I8)
 130  FORMAT(///,'   Taille memoire necessaire   : ',I8,/,
     ;           '   Dimension du Super-Tableau  : ',I8,///)      
      END

      SUBROUTINE SOLVE(RG,RGR,V,F,DRE,IDIA,LIAI,NLIAI,NDL,NDPN,NRELA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %                 RESOLUTION DU SYSTEME                          %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RG(1),RGR(1),V(1),F(1),IDIA(1),LIAI(1),IDIR(300)
      REAL DRE(1)
COMM .................////// CALCUL DE K**-1..F //////.................
      CALL SHIFTD(F,V,NDL)
      CALL CALIMP(LIAI,V,NLIAI,NDPN)
      CALL RESCHO(RG,V,IDIA,NDL)
COMM ................////// PRISE EN COMPTE DES RELATIONS /////........      
      IF(NRELA.NE.0) THEN
COMM ................////// CALCUL DE G..K**-1..F-UD //////............
        CALL PMATG1(DRE,V,NDL,F,NRELA)
        NT=0
        DO 1 IR=1,NRELA
        NC=INT(DRE(NT+1))
        NT=NT+2+2*NC
        UD=DRE(NT)
        F(IR)=F(IR)-UD
 1      CONTINUE
COMM .......////// CALCUL DES MULTIPLICATEURS DE LAGRANGE //////.......
        IDIR(1)=1
        DO 10 I=2,NRELA
 10     IDIR(I)=IDIR(I-1)+I
        CALL RESCHO(RGR,F,IDIR,NRELA)
COMM ...................////// CALCUL DE U //////......................
        CALL PMATG2(DRE,F,NRELA,NDL)
        CALL RESCHO(RG,F,IDIA,NDL)
        DO 2 I=1,NDL
 2      V(I)=V(I)-F(I)
      ENDIF
      END
      
      SUBROUTINE MATREL(RG,IDIA,DRE,A,B,RMRE,NRELA,NDL)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %           CALCUL DE LA MATRICE  G..K**-1..GT                   %
COMM %           QUE L'ON TRIANGULE                                   %
COMM %                                                                %
COMM %           G = MATRICE DES RELATIONS                            %
CPMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RG(1),IDIA(1),A(1),B(1),RMRE(1),IDIR(300)
      REAL DRE(1)
COMM ...........................////////////...........................
      IDIR(1)=1
      DO 1 I=2,NRELA
 1    IDIR(I)=IDIR(I-1)+I
      NT=0
      DO 10 IR=1,NRELA
      NC=INT(DRE(NT+1))
      CALL DZERO(A,NDL)
        DO 2 I=1,NC
        NI=INT(DRE(NT+2*I+1))
        CI=DRE(NT+2*I)
 2      A(NI)=CI
      NT=NT+2*NC+2
      CALL RESCHO(RG,A,IDIA,NDL)
      CALL PMATG1(DRE,A,NDL,B,NRELA)
      RMRE(IDIR(IR))=B(IR)
        DO 3 I=IR+1,NRELA
        IA=IDIR(I)+IR-I
 3      RMRE(IA)=B(I)
 10   CONTINUE
      CALL DECCHO(RMRE,IDIR,NRELA,0)
      END

      SUBROUTINE PMATG1(DRE,V,NDL,F,NRELA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %                 CALCUL DU PRODUIT F=G..V                       %
COMM %                 G = MATRICE DES RELATIONS                      %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION F(1),V(1)
      REAL DRE(1)
      NT=0
      DO 1 I=1,NRELA
      NC=INT(DRE(NT+1))
      F(I)=0.D0
      DO 2 J=1,NC
      CI=DRE(NT+2*J)
      NI=INT(DRE(NT+2*J+1))
 2    F(I)=F(I)+CI*V(NI)
      NT=NT+2*NC+2
 1    CONTINUE
      END

      SUBROUTINE PMATG2(DRE,V,NRELA,NDL)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %                   CALCUL DU PRODUIT V=GT..V                    %
COMM %                   G = MATRICE DES RELATIONS                    %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION V(1),F(300)
      REAL DRE(1)
      DO 10 I=1,NRELA
 10   F(I)=V(I)
      DO 11 I=1,NDL
 11   V(I)=0.
      NT=0
      DO 1 I=1,NRELA
      NC=INT(DRE(NT+1))
      DO 2 J=1,NC
      CI=DRE(NT+2*J)
      NI=INT(DRE(NT+2*J+1))
 2    V(NI)=V(NI)+CI*F(I)
      NT=NT+2*NC+2
 1    CONTINUE
      END

COMM ...........................////////////...........................
COMM ...........////// FIN DU MODULE PILOTE DE CALCUL //////...........
COMM ...........//////           ELASTIQUE            //////...........
COMM ...........................////////////...........................
