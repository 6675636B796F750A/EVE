COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %  //////  MODULE REGROUPANT LES SUBROUTINES DE CALCULS  //////   %
COMM %  //////   DE RIGIDITE TANGENTE -- NON LIN GEOM COQ --  //////   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

       SUBROUTINE RTANCO(RIG,IDIA,COOR,MAIL,EMAI,EPAI,V,SIG,
     *                   NMAI,NDL,NCPN,NDPN,NNPE,NSPG,NVMA,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   CALCUL DE LA MATRICE DE RIGIDITE TANGENTE GLOBALE             %
COMM %   POUR UNE STRUCTURE FORMEE DE PLAQUES ET DE COQUES             %
COMM %                                                                 %
COMM %   RIG   MATRICE DE RIGIDITE GLOBALE                             %
COMM %   IDIA  TABLEAU POINTEUR DE LA DIAGONALE                        %
COMM %   MAIL  TABLEAU DECRIVANT LA NUMEROTATION DES ELEMENTS          %
COMM %   COOR  COORDONNEES DES NOEUDS                                  %
COMM %   EMAI  CARACTERISTIQUES MECANIQUES DES ELEMENTS                %
COMM %   EPAI  EPAISSEUR DES ELEMENTS                                  %
COMM %   V     VECTEUR DE DEPLACEMENT INITIAL                          %
COMM %   SIG   VECTEUR DE CONTRAINTE INITIAL                           %
COMM %   NMAI  NOMBRE D ELEMENTS                                       %
COMM %   NDL   NOMBRE DE DDL TOTAL                                     %
COMM %   NCPN  NOMBRE DE COORDONNEES PAR NOEUD                         %
COMM %   NDPN  NOMBRE DE DDL PAR NOEUD                                 %
COMM %   NNPE  NOMBRE DE NOEUDS MARIMUN PAR ELEMENT                    %
COMM %                                                                 %
COMM %   NBN   NOMBRE DE NOEUD DE L'ELEMENT                            %
COMM %   NDDLE NOMBRE DE DDL DE L'ELEMENT                              %
COMM %   NBG   NOMBRE DE POINT DE GAUSS DE L'ELEMENT                   %
COMM %   GAUSS COORDONNEES POINT DE GAUSS DE L'ELEMENT                 %
COMM %   POIDS POID AU POINT DE GAUSS DE L'ELEMENT                     %
COMM %   RGL   MATRICE DE RIGIDITE ELEMENTAIRE                         %
COMM %   NO    NUMEROS GLOBAUX DES NOEUDS                              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RIG(1),IDIA(1),COOR(NCPN,1),MAIL(NNPE+2,1),EMAI(NVMA,1),
     *EPAI(1),V(1),SIG(1)      
      DIMENSION RGL(171),IGLO(18),NO(3),X(3),Y(3),Z(3),D(6,6),VEG(18)
COMM ...........................////////////...........................
      CALL DZERO(RIG,IDIA(NDL))
COMM ...........................////////////...........................
      REWIND 90
      IAD=0
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////.............       
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)           
      CALL CAELCO(ITYP,NBN,NDDLE,NBG)          
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))
      Z(I)=COOR(3,NO(I))
      DO 31 K=1,NDPN
 31   VEG(NDPN*(I-1)+K)=V(NDPN*(NO(I)-1)+K)                  
 3    CONTINUE
COMM  ......//////// ON FORME LA MATRICE DECOMPORTEMENT D //////........
      ND=6    
      CALL DISOCO(EMAI(1,IMAT),EMAI(2,IMAT),EPAI(IM),D)
COMM.....................///// CALCUL DE RGL /////.....................
      IF(ITYP.EQ.31) THEN      
       CALL RTAELE_DKT(X,Y,Z,D,VEG,SIG,IAD,RGL,NBN,NDDLE,NBG,ND)
      ELSE
       STOP ' Pb ITYP : RTANCO '
      ENDIF      
COMM .........////// ASSEMBLAGE DE LA MAT. RIGID. ELEM. ////...........       
      CALL ASSMAT(RIG,IDIA,RGL,NO,IGLO,NBN,NDPN)
 20   CONTINUE
      END

      SUBROUTINE RTAELE_DKT(X,Y,Z,D,VEG,SIG,IAD,RGL,NBN,NDDLE,NBG,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE RIGIDITE TANGENTE ELEMENTAIRE    ' DKT+CST '        %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION X(3),Y(3),Z(3),D(6,6),VEG(18),VEL(18),TRA(3,3),SIG(1)
      DIMENSION RGL(171),RGEL(18,18),RGE(18,18),R(18,18),RINT(18,18)
COMM  ........////  Calcul des coordonnees locales  ////................
      CALL COOLOC(X,Y,Z,X2,X3,Y3,TRA,R)
COMM  ......////// CALCUL DES DEPLACEMENTS DANS LE REPERE LOCAL/////....
      CALL MATVEC(R,VEG,VEL,18,18)   
COMM  .. //// Matrice de rigidite elementaire dans le repere local ////..
      CALL RTELLO_DKT(X2,X3,Y3,D,VEL,SIG,IAD,RGL,NBN,NDDLE,NBG,ND)
COMM  .. //// passage du stockage en ligne au stockage en matrice ///....
      CALL LIGMAT(RGL,RGE,NDDLE)         
COMM  .. ///// Rigidite additionnelles pour le d.d.l. 0z /////...........
      CT1=D(4,4)*0.0001
      CT2=-CT1*0.5
      RGE( 6, 6)=CT1
      RGE( 6,12)=CT2
      RGE( 6,18)=CT2
      RGE(12, 6)=CT2
      RGE(12,12)=CT1
      RGE(12,18)=CT2
      RGE(18, 6)=CT2
      RGE(18,12)=CT2
      RGE(18,18)=CT1
COMM  .....///// PASSAGE AU REPERE GLOBAL /////.........................
      CALL PASGLO(RGE,R,RINT,RGEL)
COMM  ...//// passage du stockage en matrice au stockage en ligne  /////......
      CALL MATLIG(RGEL,RGL,NDDLE)
      END

      SUBROUTINE RTELLO_DKT(X2,X3,Y3,D,VEL,SIG,IAD,RGL,NBN,NDDLE,NBG,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE RIGIDITE ELEMENTAIRE REPERE LOCAL  'DKT+CST'        %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DOUBLE PRECISION KSI,ETA
      DIMENSION RGL(171),ARGL(171),D(6,6),B(6,18),BNL(6,18),DBNL(6,18),
     *          VEL(18),SIG(1),
     *          T(2),G(2,18),S(2,2),SG(2,18),
     *          GAUSS(2,3),POIDS(3)
COMM
      ITS=(NDDLE*NDDLE+NDDLE)/2
      CALL DZERO(RGL,ITS)
      CALL GRIGCO(31,GAUSS,POIDS)
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////.............      
      DO 7 IG=1,NBG 
        KSI=GAUSS(1,IG)
        ETA=GAUSS(2,IG)
COMM ............///// RIGIDITE    Kela + Ku /////.....................        
        CALL BMAT_DKT(X2,X3,Y3,KSI,ETA,B,DETJ)
        CALL BNLU_DKT(X2,X3,Y3,KSI,ETA,B,VEL,T,G,BNL)
        WRITE(90) DETJ,BNL,G 
        CALL TBDB(BNL,D,DBNL,ARGL,ND,NDDLE,ITS)
        COEF=DETJ*POIDS(IG)
        CALL DACTUA(RGL,ARGL,ITS,COEF)                              
COMM ...///// RIGIDITE GEOMETRIQUE   Ks(SIG) /////.....................             
        S(1,1)=SIG(IAD+1)
        S(1,2)=SIG(IAD+3)
        S(2,1)=SIG(IAD+3)
        S(2,2)=SIG(IAD+2)
        IAD=IAD+6             
        CALL TBDB(G,S,SG,ARGL,2,NDDLE,ITS)      
        CALL DACTUA(RGL,ARGL,ITS,COEF) 
 7    CONTINUE 
      END

      SUBROUTINE BNLU_DKT(X2,X3,Y3,KSI,ETA,B,VEL,T,G,BNL) 
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %  CALCUL DE LA MATRICE BNL POUR LA RIGIDITE     KO + KU          %
COMM % ---CALCUL NON LINEAIRES GEOMETRIQUES---          ELEMENT DKT    %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION KSI,ETA
      DIMENSION B(6,18),VEL(18),T(2),BU(6,18),G(2,18),A(6,2),BNL(6,18) 
COMM ...////  LA MATRICE G  /////.......................................
      CALL BGEO_DKT(X2,X3,Y3,KSI,ETA,G,DETJ)      
COMM .....///// LE VECTEUR teta /////...................................
      CALL MATVEC(G,VEL,T,2,18)
COMM ....////  LA MATRICE A  /////......................................
      CALL DZERO(A,12)
      A(1,1)=T(1) 
      A(2,2)=T(2)    
      A(3,1)=T(2)
      A(3,2)=T(1)      
COMM ...////  LA MATRICE BNL = B + A(VEL).G............................. 
      CALL MATMAT(A,G,BU,6,2,18)
      CALL SHIFTD(B,BNL,108)
      CALL DSOMME(BNL,BU,108)
      END      
      
COMM ...........................////////////...........................
COMM ...//////  FIN DU MODULE REGROUPANT LES SUBROUTINES DE  //////....
COMM ...//////  CALCULS  DE RIGIDITE TANGENTE                //////....
COMM ...........................////////////...........................

