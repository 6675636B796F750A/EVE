COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %  //////  MODULE REGROUPANT LES SUBROUTINES DE CALCULS  //////   %
COMM %  //////  DES CONTRAINTES ELASTIQUES -- LINEAIRE  3D -- //////   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE SELA3D(SIG,V,COOR,MAIL,EMAI,NMAI,NCPN,NNPE,NVMA,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   CALCUL DES CONTRAINTES ASSOCIEES AU VECTEUR DEPLACEMENT V    %
COMM %        ET AUX CONTRAINTES INITIALES CONTENUES DANS SIG         %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION SIG(1),V(1),COOR(NCPN,1),MAIL(NNPE+2,1),EMAI(NVMA,1),
     *          NO(20),X(20),Y(20),Z(20),VE(60),D(6,6)
COMM ...........................////////////...........................
      IAD=0
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////.............
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)     
      CALL CAEL3D(ITYP,NBN,NDDLE,NBG) 
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))
      Z(I)=COOR(3,NO(I))         
      VE(3*(I-1)+1)=V(3*(NO(I)-1)+1)
      VE(3*(I-1)+2)=V(3*(NO(I)-1)+2)
      VE(3*(I-1)+3)=V(3*(NO(I)-1)+3)
 3    CONTINUE
COMM  ......//////// ON FORME LA MATRICE DE COMPORTEMENT D //////.......
      ND=6
      CALL DISO3D(EMAI(1,IMAT),EMAI(2,IMAT),D)
COMM................///// CALCUL DES CONTRAINTES /////..................
      IF(ITYP.EQ.81.OR.ITYP.EQ.200.OR.ITYP.EQ.201) THEN
         CALL SIGELE_ISOPARA3D(X,Y,Z,D,VE,SIG,IAD,ITYP,NBN,NDDLE,NBG,ND)
      ELSE
         STOP ' SELA3D : pb sur ITYP  '
      ENDIF      
 20   CONTINUE
      END
            
      SUBROUTINE SIGELE_ISOPARA3D(X,Y,Z,D,VE,SIG,IAD,
     *                            ITYP,NBN,NDDLE,NBG,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  CONTRAINTES   : ELEMENTS ISOPARAMETRIQUES 3D                   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION X(20),Y(20),Z(20),D(6,6),VE(60),SIG(1),
     *          B(6,60),EPS(6),SE(6),GAUSS(3,27),POIDS(27)      
COMM ..................////////////////////............................
      CALL GRIG3D(ITYP,GAUSS,POIDS)        
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////.............
      DO 7 NG=1,NBG     
        R=GAUSS(1,NG)
        S=GAUSS(2,NG)
        T=GAUSS(3,NG)
COMM ............////// CALCUL DE LA MATRICE B  //////.................
        CALL BLINISO3D(X,Y,Z,R,S,T,B,DETJ,NDDLE,NBN,ITYP)
COMM........////// CALCUL DE EPS=B.VE puis SE=D.EPS ////////...........
        CALL MATVEC(B,VE,EPS,ND,NDDLE)
        CALL MATVEC(D,EPS,SE,ND,ND)
COMM.....////// STOCKAGE DANS SIG (ON AJOUTE SANS ECRASER)///////...... 
        DO 9 I=1,6      
 9      SIG(IAD+I)=SIG(IAD+I)+SE(I)      
        IAD=IAD+6      
COMM ...........................////////////...........................
  7   CONTINUE
      END
      
