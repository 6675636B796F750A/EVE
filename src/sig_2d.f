COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %  //////  MODULE REGROUPANT LES SUBROUTINES DE CALCULS  //////   %
COMM %  //////  DES CONTRAINTES ELASTIQUES -- LINEAIRE 2D --  //////   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE SELA2D(SIG,V,COOR,MAIL,EMAI,NMAI,NCPN,NNPE,NVMA,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   CALCUL DES CONTRAINTES ASSOCIEES AU VECTEUR DEPLACEMENT V    %
COMM %        ET AUX CONTRAINTES INITIALES CONTENUES DANS SIG         %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION SIG(1),V(1),COOR(NCPN,1),MAIL(NNPE+2,1),EMAI(NVMA,1),
     *          NO(8),X(8),Y(8),VE(16),D(16)
COMM ...........................////////////...........................
      IAD=0
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////............. 
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)     
      CALL CAEL2D(ITYP,NBN,NDDLE,NBG)
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))      
      VE(2*I-1)=V(2*NO(I)-1)
      VE(2*I  )=V(2*NO(I)  )
 3    CONTINUE
COMM  ....///// ON FORME LA MATRICE DECOMPORTEMENT D (ND,ND) /////.....
      IF      (KTYP.EQ.1) THEN
         ND=3
         CALL DISODP(EMAI(1,IMAT),EMAI(2,IMAT),D)
      ELSE IF (KTYP.EQ.2) THEN
         ND=3
         CALL DISOCP(EMAI(1,IMAT),EMAI(2,IMAT),D)
      ELSE IF (KTYP.EQ.3) THEN
         ND=4
         CALL DISOAX(EMAI(1,IMAT),EMAI(2,IMAT),D)
      ELSE 
         STOP ' SELA2D : pb sur KTYP '
      ENDIF 
COMM..........///// CALCUL DES CONTRAINTES /////........................
      IF(ITYP.EQ.30.OR.ITYP.EQ.40.OR.
     *   ITYP.EQ.60.OR.ITYP.EQ.80.OR.ITYP.EQ.81) THEN
        CALL SIGELE_ISOPARA2D(X,Y,D,VE,SIG,IAD,
     *       EMAI(1,IMAT),EMAI(2,IMAT),KTYP,ITYP,NBN,NDDLE,NBG,ND)
      ELSE IF(ITYP.EQ.41) THEN
C       CALL SIGELE_Q4WT(  )
      ELSE      
        STOP ' SELA2D : pb sur ITYP '  
      ENDIF      
 20   CONTINUE      
      END

      SUBROUTINE SIGELE_ISOPARA2D(X,Y,D,VE,SIG,IAD,YG,PS,
     *                            KTYP,ITYP,NBN,NDDLE,NBG,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  CONTRAINTES   : ELEMENTS ISOPARAMETRIQUES 2D                   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION X(8),Y(8),D(16),VE(16),SIG(1),
     *          B(64),EPS(4),SE(4),GAUSS(2,9),POIDS(9)      
      DOUBLE PRECISION KSI,ETA
COMM ...................////////////////////////////....................            
      CALL GRIG2D(ITYP,GAUSS,POIDS)                 
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////..............
      DO 7 NG=1,NBG     
        KSI=GAUSS(1,NG)
        ETA=GAUSS(2,NG)
COMM ............////// CALCUL DE LA MATRICE B  //////..................
        CALL BLINISO2D(X,Y,KSI,ETA,B,DETJ,RAY,NDDLE,NBN,ND,ITYP,KTYP)
COMM........////// CALCUL DE EPS=B.VE puis SE=D.EPS ////////.............
        CALL MATVEC(B,VE,EPS,ND,NDDLE)
        CALL MATVEC(D,EPS,SE,ND,ND)      
COMM.....////// CALCUL DE E33 ET S33 POUR LE CAS PLAN////////............ 
        IF(KTYP.EQ.1) THEN
          EPS(4)=0.D0
          SE(4)=PS*(SE(1)+SE(2)) 
        ENDIF    
        IF(KTYP.EQ.2) THEN
          EPS(4)=-PS*(SE(1)+SE(2))/YG
          SE(4)=0.D0
        ENDIF      
COMM.....////// STOCKAGE DANS SIG (ON AJOUTE SANS ECRASER)///////........
        DO 9 I=1,4      
 9      SIG(IAD+I)=SIG(IAD+I)+SE(I)      
        IAD=IAD+4      
COMM ...........................////////////..............................
  7   CONTINUE
      END
      
