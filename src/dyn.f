COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %          //////   MODULE PILOTE DES CALCULS    //////           %
COMM %          //////     DE DYNAMIQUE LINEAIRE      //////           %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE DYNAMI(IDIMT)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                CALCUL DYNAMIQUE LINEAIRE                        %
COMM %                                                                 %
COMM %   Organisation de la memoire  (Super-Tableau T + pointeurs IT)  %
COMM %   --------------------------                                    %
COMM %                                                                 %
COMM %      adresse              contenu de T                 type     %
COMM %                                                                 %
COMM %   IT(11)---IDIA       Pointeurs de la diagonale       (ENTIER)  %
COMM %   IT(12)---F1         Vecteur de charge donne         (DP)      %
COMM %   IT(13)---F          Vecteur de charge effectif      (DP)      %
COMM %   IT(14)---RIG        Matrice de rigidite             (DP)      %
COMM %   IT(15)---RMA        Matrice de masse                (DP)      %
COMM %   IT(16)---AMO        Matrice d'amortissement         (DP)      % 
COMM %   IT(17)---V          Deplacement  a T                (DP)      %
COMM %   IT(18)---VP         Vitesse      a T                (DP)      % 
COMM %   IT(19)---VPP        Acceleration a T                (DP)      %
COMM %   IT(20)---V1         Deplacement  a T+DT             (DP)      %
COMM %   IT(21)---VP1        Vitesse      a T+DT             (DP)      % 
COMM %   IT(22)---VPP1       Acceleration a T+DT             (DP)      %
COMM %   IT(23)---VINT       Vecteur de travail              (DP)      %
COMM %   IT(24)---FINT       Vecteur de travail              (DP)      %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION ALPHA,BETA,ABSI(100),ORDO(100)
      CHARACTER*4 RMOT
      COMMON T(1)
      COMMON/INI/ICO(50),IT(50)
COMM ...........................////////////...........................
      KTYP =ICO(1)
      NCPN =ICO(2)
      NDPN =ICO(3)
      NNPE =ICO(4)
      NSPG =ICO(5)      
      NNOE =ICO(6)
      NDL  =ICO(7)
      NMAI =ICO(8)
      NVMA =ICO(10)         
      NLIAI=ICO(11)
COMM
COMM .....//// RESERVATION DES PLACES DANS LE SUPER-TABLEAU ////......
COMM
      IT(12)=IT(11)+NDL     
      IT(13)=IT(12)+2*NDL
      IT(14)=IT(13)+2*NDL      
      CALL PROFBI(T(IT(11)),T(IT(2)),NDL,NMAI,NRIG,NNPE,NDPN,KTYP)
      ICO(26)=NRIG
      IT(15)=IT(14)+2*NRIG
      IT(16)=IT(15)+2*NRIG
      IT(17)=IT(16)+2*NRIG
      IT(18)=IT(17)+2*NDL
      IT(19)=IT(18)+2*NDL
      IT(20)=IT(19)+2*NDL
      IT(21)=IT(20)+2*NDL
      IT(22)=IT(21)+2*NDL
      IT(23)=IT(22)+2*NDL
      IT(24)=IT(23)+2*NDL      
      IT(25)=IT(24)+2*NDL     
COMM 
      WRITE(6,150)       
      WRITE(6,100)       
      DO 1 I=1,24
  1   WRITE(6,120) I,IT(I),IT(I+1)-IT(I)
      WRITE(6,130) IT(25),IDIMT
      WRITE(6,110)
      IF(IT(25).GT.IDIMT) STOP 'Taille du Super-Tableau'    
COMM
COMM ..../// LECTURE DU MOT CLE AMORTISSEMENT  ////.....................
COMM
      READ(5,'(A4)')  RMOT
      IF(RMOT.EQ.'AMOR') THEN
       READ(5,*) ALPHA,BETA
      ELSE
       CALL ERROR1
      ENDIF
COMM
COMM ..../// LECTURE DU MOT CLE VARIATION CHARGE  ////..................
COMM
      READ(5,'(A4)')  RMOT
      IF(RMOT.EQ.'VARI') THEN
       READ(5,*) NPOINT
       DO 2 I=1,NPOINT
 2     READ(5,*) ABSI(I),ORDO(I)
      ELSE
       CALL ERROR1
      ENDIF       
COMM            
COMM ........../// ASSEMBLAGE DU VECTEUR DE CHARGE ///................
COMM               
      CALL CAFORC(T(IT(6)),T(IT(12)))
COMM            
COMM ...../// ASSEMBLAGE DE LA MATRICE DE RIGIDITE  ///...............
COMM
      CALL RIGELA(T(IT(14)),T(IT(11)),T(IT(1)),T(IT(2)),T(IT(3)),
     *            T(IT( 4)),NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP) 
COMM            
COMM ..../// ASSEMBLAGE DE LA MATRICE DE MASSE COERENTE  ///..........
COMM
      CALL MASCOE(T(IT(15)),T(IT(11)),T(IT(1)),T(IT(2)),T(IT(3)),
     *            T(IT( 4)),NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)       
COMM            
COMM ......./// ASSEMBLAGE DE LA MATRICE D'AMORTISSEMENT ///..........
COMM      AMORTISSEMENT DE RAYLEIGH  C= ALPHA * K + BETA * M
COMM      
      CALL  DZERO (T(IT(16)),NRIG) 
      CALL  DACTUA(T(IT(16)),T(IT(14)),NRIG,ALPHA)  
      CALL  DACTUA(T(IT(16)),T(IT(15)),NRIG,BETA)
COMM
COMM ....///// INITIALISATION DE  V   VP   VPP   /////................
COMM 
      CALL DZERO(T(IT(17)),NDL)
      CALL DZERO(T(IT(18)),NDL)
      CALL DZERO(T(IT(19)),NDL)
COMM
COMM ......///// CHOIX DU SHEMA D'INTEGRATION /////...................
COMM      
      READ(5,'(A4)')  RMOT
      IF(RMOT.EQ.'NEWM') THEN
      CALL NEWMARK(T(IT(11)),T(IT(12)),T(IT(13)),T(IT(14)),T(IT(15)),
     ;             T(IT(16)),T(IT(17)),T(IT(18)),T(IT(19)),T(IT(20)),
     ;             T(IT(21)),T(IT(22)),T(IT(23)),T(IT(24)),T(IT( 5)),
     ;             NLIAI,NDPN,NRIG,NDL,NPOINT,ABSI,ORDO)
      ELSE
       CALL ERROR1
      ENDIF            
COMM ..................../////////////................................
 150  FORMAT(////////,
     ;'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%',/,
     ;'  %                                                        %',/, 
     ;'  %         CALCUL   DYNAMIQUE   LINEAIRE                  %',/,      
     ;'  %                                                        %',/,      
     ;'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')           
 100  FORMAT('1',//,' ',60('%'),///,
     ;'  ORGANISATION DU SUPER-TABLEAU  T() ',//)
 110  FORMAT(' ',60('%'),/////)
 120  FORMAT('   IT(',I3,')=',I8,'     Longueur=',I8)
 130  FORMAT(///,'   Taille memoire necessaire   : ',I8,/,
     ;           '   Dimension du Super-Tableau  : ',I8,///)  
      END      
      
      SUBROUTINE NEWMARK(IDIA,F1,F,RIG,RMA,AMO,V,VP,VPP,V1,VP1,VPP1,
     *                   VINT,FINT,LIAI,NLIAI,NDPN,NRIG,NDL,
     *                   NPOINT,ABSI,ORDO)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   RESOLUTION DU SYSTEME :  M Vpp + C Vp + K V = lamdba F       %
COMM %   A L'AIDE D'UN SCHEMA D'INTEGRATION DE TYPE NEWMARK           %
COMM %                                                                %
COMM %  D'apres 'FINITE ELEMENT PROCEDURES IN ENGINEERING ANALYSIS'   %
COMM %           K-J  BATHE   edition 1982    Chapitre 9 page 512     %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)      
      DOUBLE PRECISION LAMBDA,ABSI(100),ORDO(100)
      DIMENSION IDIA(1),F1(1),F(1),RIG(1),RMA(1),AMO(1),
     *          V(1),VP(1),VPP(1),V1(1),VP1(1),VPP1(1),VINT(1),FINT(1) 
COMM
COMM ....///// STOCKAGE DE LA MATRICA DE RIGIDITE ELASTIQUE ////......
COMM
      REWIND(91)
      CALL ECFICH(RIG,NRIG,91)    
COMM
COMM ..../// LECTURE DES PARAMETRES DU SCHEMA D'INTEGRATION ////.......
COMM
       READ(5,*) A,B           
COMM
COMM ......./// NOMBRE DE SEQUENCE A PAS CONSTANT (20 MAXI)  ////......
COMM
      READ(5,*) NSEQ 
COMM  .......//////    BOUCLE SUR LES SEQUENCES   ///////..............
      TEMPS=0.D0
      DO 2 ISEQ=1,NSEQ
        READ(5,*)  NPAS,DT
COMM  ......//////  DEFINITION DE QUELQUES CONSTANTES  /////............
        A0=1.D0/(A*DT*DT)
        A1=   B/(A*DT)
        A2=1.D0/(A*DT)
        A3=1.D0/(2.D0*A) -1.D0
        A4=B/A-1.D0
        A5=(DT/2.D0)*(B/A-2.D0)
        A6=DT*(1.D0-B)
        A7=B*DT
COMM  ......////// MATRICE DE RIGIDITE EFFECTIVE   //////................      
        REWIND(91)
        CALL LEFICH(RIG,NRIG,91)
        CALL DACTUA(RIG,RMA,NRIG,A0) 
        CALL DACTUA(RIG,AMO,NRIG,A1)                
COMM  .....//// PRISE EN COMPTE DES BLOCAGES ET DECOMPOSITION ////.......
        CALL CALIRI(RIG,IDIA,LIAI,NLIAI,NDPN,NDL)
        CALL DECCHO(RIG,IDIA,NDL,0)
COMM  ............//////    BOUCLE SUR LES PAS   ///////.................                        
        DO 3 IPAS=1,NPAS
        TEMPS=TEMPS+DT
COMM  ...........//// VECTEUR DE CHARGE EFFECTIF /////...................
         CALL DZERO (F,NDL)
         CALL INTLAM(NPOINT,ABSI,ORDO,TEMPS,LAMBDA)
         CALL DACTUA(F,F1,NDL,LAMBDA)
COMM
         DO 7 I=1,NDL
 7       VINT(I)= A0*V(I) + A2*VP(I) + A3*VPP(I)
         CALL MULTMATVEC(RMA,VINT,FINT,IDIA,NDL)
         CALL DSOMME(F,FINT,NDL)
COMM   
         DO 8 I=1,NDL
 8       VINT(I)= A1*V(I) + A4*VP(I) + A5*VPP(I)
         CALL MULTMATVEC(AMO,VINT,FINT,IDIA,NDL)
         CALL DSOMME(F,FINT,NDL)
COMM  .......///// DEPLACEMENT A T+DT  ////............................         
         CALL SHIFTD(F,V1,NDL)
         CALL CALIMP(LIAI,V1,NLIAI,NDPN)
         CALL RESCHO(RIG,V1,IDIA,NDL)         
COMM  .......///// ACCELERATION A T+DT ////............................
         DO 5 I=1,NDL
 5       VPP1(I) = A0*(V1(I)-V(I)) - A2*VP(I) - A3*VPP(I)       
COMM  .......///// VITESSE  A T+DT ////................................
         DO 6 I=1,NDL
 6       VP1(I) = VP(I) + A6*VPP(I) + A7*VPP1(I)          
COMM  .....///// IMPRESSION DES RESULTATS..............................  
         CALL  IMPDYN(TEMPS,V1,VP1,VPP1)
COMM  ....//// TRANSLATION DES RESULTATS...............................
         CALL SHIFTD(V1  ,V  ,NDL)
         CALL SHIFTD(VP1 ,VP ,NDL)         
         CALL SHIFTD(VPP1,VPP,NDL)                  
 3    CONTINUE     
 2    CONTINUE
      END
      
      SUBROUTINE INTLAM(NPOINT,ABSI,ORDO,TEMPS,LAMBDA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   INTERPOLATION DE LAMBDA(TEMPS)                               %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION ABSI(100),ORDO(100),TEMPS,LAMBDA,RATIO
      
      DO 1 I=1,NPOINT-1
       IF(ABSI(I).LT.TEMPS.AND.ABSI(I+1).GE.TEMPS) THEN
        RATIO =(TEMPS-ABSI(I))/(ABSI(I+1)-ABSI(I))
        LAMBDA=ORDO(I)+RATIO*(ORDO(I+1)-ORDO(I))
        RETURN
       ENDIF
 1    CONTINUE
      STOP ' Pb Interpolation de lambda : INTLAM '
      END     

COMM ...........................////////////...........................
COMM ...........////// FIN DU MODULE PILOTE DE CALCUL //////...........
COMM ...........//////      DYNANIQUE  LINEAIRE       //////...........
COMM ...........................////////////...........................
