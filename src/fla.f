COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %     ////  MODULE PILOTE DES CALCULS DE FLAMBAGE    ////         %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE FLAMBE(IDIMT)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %         CALCUL DES  MODES DE  FLAMBAGES  DE LA STRUCTURE        %
COMM %                                                                 %
COMM %   Organisation de la memoire  (Super-Tableau T + pointeurs IT)  %
COMM %   --------------------------                                    %
COMM %                                                                 %
COMM %      adresse              contenu de T                 type     %
COMM %                                                                 %
COMM %   IT(11)---IDIA       Pointeurs de la diagonale       (ENTIER)  %
COMM %   IT(12)---RIG        Matrice de rigidite elastique   (DP)      %
COMM %   IT(13)---RMA        Matrice de rigidite geometrique (DP)      %
COMM %   IT(14)---VEP        Les vecteurs propres            (DP)      %
COMM %   IT(15)---VAP        Les valeurs propres             (DP)      % 
COMM %   IT(16)---Vtra       Vecteur de travail              (DP)      %
COMM %   IT(17)---Wtra       Vecteur de travail              (DP)      %
COMM %   IT(18)---AR         Matrice de travail              (DP)      % 
COMM %   IT(19)---BR         Matrice de travail              (DP)      %
COMM %   IT(20)---VEC        Matrice de travail              (DP)      %
COMM %   IT(21)---D          Vecteur de travail              (DP)      %
COMM %   IT(22)---RTOLV      Vecteur de travail              (DP)      %
COMM %   IT(23)---ROLD       Matrice de travail              (DP)      %
COMM %   IT(24)---DOLD       Vecteur de travail              (DP)      %
COMM %   IT(25)---RATOLD     Vecteur de travail              (DP)      %
COMM %   IT(26)---RAT        Vecteur de travail              (DP)      %
COMM %   IT(27)---F puis V   Vecteur charge, Deplacement     (DP)      %
COMM %   IT(28)---SIG        Vecteur contrainte              (DP)      %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION RTOL
      COMMON T(1)
      COMMON/INI/ICO(50),IT(50)
COMM ...........................////////////...........................
      KTYP =ICO(1)
      NCPN =ICO(2)
      NDPN =ICO(3)
      NNPE =ICO(4)
      NSPG =ICO(5)            
      NNOE =ICO(6)
      NDL  =ICO(7)
      NMAI =ICO(8)
      NVMA =ICO(10)      
      NLIAI=ICO(11)
COMM ...........................////////////...........................  
      READ(5,*) NMODE
      READ(5,*) NITEM,IFSS
      NC =MIN(2*NMODE,NMODE+8)
      NNC=NC*(NC+1)/2    
COMM
COMM .....//// RESERVATION DES PLACES DANS LE SUPER-TABLEAU ////......
COMM
      IT(12)=IT(11)+NDL     
      CALL PROFBI(T(IT(11)),T(IT(2)),NDL,NMAI,NRIG,NNPE,NDPN,KTYP)
      ICO(26)=NRIG
      IT(13)=IT(12)+2*NRIG 
      IT(14)=IT(13)+2*NRIG
      IT(15)=IT(14)+2*NDL*NC
      IT(16)=IT(15)+2*NC
      IT(17)=IT(16)+2*NDL
      IT(18)=IT(17)+2*NDL
      IT(19)=IT(18)+2*NNC
      IT(20)=IT(19)+2*NNC
      IT(21)=IT(20)+2*NC*NC
      IT(22)=IT(21)+2*NC
      IT(23)=IT(22)+2*NC
      IT(24)=IT(23)+2*NDL*NC
      IT(25)=IT(24)+2*NC      
      IT(26)=IT(25)+2*NC
      IT(27)=IT(26)+2*NC
      IT(28)=IT(27)+2*NDL      
      CALL LONSIG(T(IT(2)),NMAI,NNPE,NSPG,NSIG,NEPS,KTYP)
      ICO(27)=NSIG
      IT(29)=IT(28)+2*NSIG
COMM
      WRITE(6,150)        
      WRITE(6,100)       
      DO 1 I=1,28
  1   WRITE(6,120) I,IT(I),IT(I+1)-IT(I)
      WRITE(6,130) IT(29),IDIMT
      IF(IT(29).GT.IDIMT) STOP 'Taille du Super-Tableau'     
COMM            
COMM ../// ASSEMBLAGE DE LA MATRICE DE RIGIDITE ET STOCKAGE ///......
COMM ../// PRISE EN COMPTE DES BLOCAGES ET DECOMPOSITION    ///......
COMM
      CALL RIGELA(T(IT(12)),T(IT(11)),T(IT(1)),T(IT(2)),T(IT(3)),
     *            T(IT( 4)),NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
      REWIND(91)
      CALL ECFICH(T(IT(12)),NRIG,91)   
      CALL CALIRI(T(IT(12)),T(IT(11)),T(IT(5)),NLIAI,NDPN,NDL)
      CALL DECCHO(T(IT(12)),T(IT(11)),NDL,0)
COMM
COMM  ..../// CALCUL DE LA SOLUTION ELASTIQUE   ///...................
COMM      
      CALL CAFORC(T(IT(6)),T(IT(27)))
      CALL CALIMP(T(IT(5)),T(IT(27)),NLIAI,NDPN)
      CALL RESCHO(T(IT(12)),T(IT(27)),T(IT(11)),NDL)
C
      CALL DZERO (T(IT(28)),NSIG)   
      CALL SIGELA(T(IT(28)),T(IT(27)),T(IT(1)),T(IT(2)),T(IT(3)),
     *            T(IT(4)),NMAI,NCPN,NDPN,NNPE,NVMA,KTYP)
      CALL IMPTCH(1)
      CALL IMPELA(T(IT(27)),T(IT(28)),T(IT(2)))
COMM
COMM ..../// REPRISE DE LA MATRICE DE RIGIDITE ELASTIQUE  /// .........
COMM
      REWIND(91)
      CALL LEFICH(T(IT(12)),NRIG,91)      
COMM            
COMM ..../// ASSEMBLAGE DE LA MATRICE DE RIGIDITE GEOMETRIQUE  ///.....
COMM
      CALL RIGGEO(T(IT(13)),T(IT(11)),T(IT(1)),T(IT(2)),T(IT(3)),
     *  T(IT( 4)),T(IT(28)),NMAI,NDL,NCPN,NDPN,NNPE,NSPG,NVMA,KTYP) 
COMM
COMM .../// CALCULS DES VALEURS PROPRES ET DES MODES PROPRES ///.......
COMM
      RTOL=1.0D-6 
      CALL SSPACE(T(IT(11)),T(IT(12)),T(IT(13)),T(IT(14)),T(IT(15)),
     ;            T(IT(16)),T(IT(17)),T(IT(18)),T(IT(19)),T(IT(20)),
     ;            T(IT(21)),T(IT(22)),T(IT(23)),T(IT(24)),T(IT(25)),
     ;            T(IT(26)),T(IT(5)),NLIAI,NDPN,NDL,
     ;            NRIG,NRIG,NMODE,NC,NNC,NITEM,IFSS,RTOL)          
COMM
COMM...../// IMPRESSION DES VALEURS ET DES MODES PROPRES ///...........
COMM 
      CALL IMPFLA(T(IT(14)),T(IT(15)),NMODE,NDL)
COMM      
COMM ..................../////////////.................................
COMM      
 150  FORMAT(////////,
     ;'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%',/,
     ;'  %                                                        %',/, 
     ;'  %         CALCUL  DE  FLAMBAGE  LINEAIRE                 %',/,      
     ;'  %                                                        %',/,      
     ;'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')           
 100  FORMAT('1',//,' ',60('%'),///,
     ;'  ORGANISATION DU SUPER-TABLEAU  T() ',//)
 110  FORMAT(' ',60('%'),/////)
 120  FORMAT('   IT(',I3,')=',I8,'     Longueur=',I8)
 130  FORMAT(///,'   Taille memoire necessaire   : ',I8,/,
     ;           '   Dimension du Super-Tableau  : ',I8,///)     
      END

COMM ...........................////////////...........................
COMM ...........////// FIN DU MODULE PILOTE DE CALCUL //////...........
COMM ...........//////           DE FLAMBAGE          //////...........
COMM ...........................////////////...........................
