C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	subroutine RACINES(P,NDEGRE,EPSILON2,RAC_MIN,RAC_MIN_C,
     *  DIVERGE,AM,NB_RAC,NB_RAC_POS,NB_RAC_C)
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
	DIMENSION RAC(NDEGRE),P(0:NDEGRE),RAC_C(NDEGRE)
	LOGICAL DIVERGE
	NB_RAC=0
	NB_RAC_POS=0
	NB_RAC_C=0



	call BAI(P,NDEGRE,epsilon2,RAC,RAC_C,NB_RAC,NB_RAC_POS,
     *  NB_RAC_C,DIVERGE,AM)

c             stop 'aortie de BAI dans RACINES'

	if (nb_rac_pos.gt.0) then
	call  RACINE_MIN(RAC,NB_RAC_POS,RAC_MIN)
	else
	rac_min=10.d0*am
	write(*,*) 'RAC_MIN',rac_min
	endif

	if (nb_rac_C.gt.0) then
	call  RACINE_MIN(RAC_C,NB_RAC_C,RAC_MIN_C)
	else
	rac_min_C=0.d0
	endif
C------- je multiplie par 2 car a+-ib
	NB_RAC_C=2*NB_RAC_C
	END


C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	SUBROUTINE RESOUT1(A,NDEGRE,RAC,NB_RAC,NB_RAC_POS)
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
	DIMENSION A(0:NDEGRE),RAC(1)
	RAPP=-A(0)/A(1)
	IF (RAPP.GE.0.D0) THEN
	RAC(NB_RAC_POS+1)=RAPP
	NB_RAC_POS=NB_RAC_POS+1
	ENDIF
	NB_RAC=NB_RAC+1
	END

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	SUBROUTINE RESOUT2(A,B,C,RAC,RAC_C,NB_RAC,NB_RAC_POS,NB_RAC_C)
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
	DIMENSION RAC(1),RAC_C(1)
	DELTA=B*B-4.D0*A*C
	IF (DELTA.GE.0.D0) THEN

	RAC1= (-B+DSQRT(DELTA))/(2.D0*A)
	RAC2=(-B-DSQRT(DELTA))/(2.D0*A)
	IF (RAC1.GE.0.D0) THEN
	RAC(NB_RAC_POS+1)=RAC1
	NB_RAC_POS=NB_RAC_POS+1
	ENDIF

	IF (RAC2.GE.0.D0) THEN
	RAC(NB_RAC_POS+1)=RAC2
	NB_RAC_POS=NB_RAC_POS+1
	ENDIF

	NB_RAC=NB_RAC+2
	ELSE
	c=dabs(DSQRT(-DELTA)/(2.D0*A))
	xx=-B/(2.D0*A)
c       on tient pas compte des racines complexes
	RAC_C(NB_RAC_C+1)=dsqrt(xx*xx+c*c)
	NB_RAC_C=NB_RAC_C+1
	NB_RAC=NB_RAC+2
c	WRITE(*,*) 'pole ',xx,' + ',c,' i'
c	WRITE(*,*) 'pole ',xx,' - ',c,' i'
	ENDIF
c
	END
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	SUBROUTINE RAPHSON(a,b,N,S,P,EPSILON2,REP)
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
	LOGICAL REP
	DIMENSION DBDS(0:N),DBDP(0:N),A(0:N),B(0:N)

	AS=S
	AP=P
	B(N)=A(N)
	B(N-1)=A(N-1)+S*B(N)
	DO K=N-2,1,-1
	B(K)=A(K)+S*B(K+1)-P*B(K+2)
	ENDDO
	B(0)=A(0)-P*B(2)
	DBDS(N)=0.D0
	DBDS(N-1)=B(N)+S*DBDS(N)
	DO K=N-2,1,-1
	DBDS(K)=B(K+1)+S*DBDS(K+1)-P*DBDS(K+2)
	enddo
	DBDS(0)=-P*DBDS(2)
	
	DBDP(N)=0.D0
	DBDP(N-1)=S*DBDP(N)
	DO K=N-2,1,-1
	DBDP(K)=S*DBDP(K+1)-B(K+2)-P*DBDP(K+2)
	enddo
	DBDP(0)=-B(2)-P*DBDP(2)
	DET=DBDS(1)*DBDP(0)-DBDS(0)*DBDP(1)
	S=AS+(-DBDP(0)*B(1)+DBDP(1)*B(0))/DET
	P=AP+(DBDS(0)*B(1)-DBDS(1)*B(0))/DET
	val=((S-AS)*(S-AS)+(P-AP)*(P-AP))
	IF (val.LT.epsilon2) THEN
	REP=.TRUE.
	ENDIF
	END
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	SUBROUTINE BAI(A,NDEGRE,EPSILON2,RAC,RAC_C,NB_RAC,NB_RAC_POS,
     *  NB_RAC_C,DIVERGE,AM)
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
	DIMENSION A(0:NDEGRE),B(0:NDEGRE),RAC(1),RAC_C(1)
	LOGICAL REP,DIVERGE
	
	N=NDEGRE
c-------test si poly > 2
	DIVERGE=.FALSE.
	IF (N.GT.2) THEN
	S=AM+AM
	p=AM*AM
	DO WHILE(N.GT.2.AND.DIVERGE.EQV..FALSE.)
        S=AM+AM
	p=AM*AM
	REP=.FALSE.
	iter=0
	ii=0
	DO WHILE(REP.EQV..false..and.ii.LT.20)
	iter=0
	S=(AM-DBLE(ii)*AM/10.d0)*2.D0
	p=(AM-DBLE(ii)*AM/10.D0)**2.d0-(AM-dble(ii)*AM/10.d0)
	DO WHILE(REP.EQV..FALSE..and.iter.LE.1000)
	CALL RAPHSON(a,b,N,S,P,EPSILON2,REP)
	iter=iter+1
	ENDDO
	ii=ii+1
	ENDDO
c	write (*,*) 'nb_iterations', iter
c-------test si n-r a converge: si oui on continue 
c-------sinon on sort
	IF (REP.EQV..TRUE.) THEN
	CALL RESOUT2(1.D0,-S,P,RAC,RAC_C,NB_RAC,NB_RAC_POS,
     *  NB_RAC_C)
	DO K=0,N-2
	A(K)=B(K+2)
	enddo
	N=N-2
	else
	WRITE(*,*) 'NR n a pas trouve toutes les racines'
	DIVERGE=.TRUE.
	endif
c-------fin test n-r convergence
	enddo
	
	IF (N.EQ.2.AND.DIVERGE.EQV..FALSE.) THEN
	CALL RESOUT2(A(2),A(1),A(0),RAC,RAC_C,NB_RAC,
     *  NB_RAC_POS,NB_RAC_C)
	else
	IF (N.EQ.1.AND.DIVERGE.EQV..FALSE.) THEN
	CALL RESOUT1(A,NDEGRE,RAC,NB_RAC,NB_RAC_POS)
	ENDIF
	endif
c-------fin test si poly > 2
	ELSE
c-------si le polynome de depart < 3
	IF (N.EQ.2) THEN
	CALL RESOUT2(A(2),A(1),A(0),RAC,RAC_C,NB_RAC,
     *  NB_RAC_POS,NB_RAC_C)
	else
	IF (N.EQ.1) THEN
	CALL RESOUT1(A,NDEGRE,RAC,NB_RAC,NB_RAC_POS)
	ENDIF
	endif
c-------fin si le polynome de depart < 3
c-------fin si poly >2
	ENDIF	
	END

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	double precision function RANDOM(xr)
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
        xpi=4.d0*datan(1.d0)
        xr=xr+xpi
        xr=xr*xr*xr*xr*xr*xr*xr*xr
        xr=xr-dble(int(xr))
        RANDOM=XR
	END
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	SUBROUTINE RACINE_MIN(RAC,NB_RAC,RAC_MIN)
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
	DIMENSION RAC(1)
C-------le tableau contient que des racines positives
C------- et au moins une racine
	RAC_MIN=RAC(1)
	DO I=1,NB_RAC
	     IF (RAC(I).LT.RAC_MIN) THEN
	     RAC_MIN=RAC(I)
	     ENDIF
	ENDDO

	END
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	SUBROUTINE RACINE_MAX(RAC,NB_RAC,RAC_MAX)
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
	DIMENSION RAC(1)
C-------le tableau contient que des racines positives
C------- et au moins une racine	
	RAC_MAX=RAC(1)
	DO I=1,NB_RAC
	     IF (RAC(I).GT.RAC_MAX) THEN
	     RAC_MAX=RAC(I)
	     ENDIF
	ENDDO
	
	END

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	subroutine RACINES_G(P,NDEGRE,EPSILON2,RAC_MAX,RAC_MIN_C,
     *  DIVERGE,AM,NB_RAC,NB_RAC_NEG,NB_RAC_C)
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
	DIMENSION RAC(NDEGRE),P(0:ndegre),RAC_C(NDEGRE)
	LOGICAL DIVERGE
	NB_RAC=0
	NB_RAC_NEG=0
	NB_RAC_C=0
	call BAI_G(P,NDEGRE,epsilon2,RAC,RAC_C,NB_RAC,NB_RAC_NEG,
     *  NB_RAC_C,DIVERGE,AM)
	
	
	if (nb_rac_NEG.gt.0) then
	call  RACINE_MAX(RAC,NB_RAC_NEG,RAC_MAX)
	else
	rac_mAX=10.d0*am
	write(*,*) 'RAC_MIN',rac_max
	endif 
	
	if (nb_rac_C.gt.0) then
	call  RACINE_MIN(RAC_C,NB_RAC_C,RAC_MIN_C)
	else
	rac_min_C=0.d0
	endif 
C------- je multiplie par 2 car a+-ib
	NB_RAC_C=2*NB_RAC_C
	END
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	SUBROUTINE RESOUT1_G(A,NDEGRE,RAC,NB_RAC,NB_RAC_NEG)
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
	DIMENSION A(0:NDEGRE),RAC(1)
	RAPP=-A(0)/A(1)
	IF (RAPP.LE.0.D0) THEN 
	RAC(NB_RAC_NEG+1)=RAPP
	NB_RAC_NEG=NB_RAC_NEG+1
	ENDIF
	NB_RAC=NB_RAC+1
	END
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	SUBROUTINE RESOUT2_G(A,B,C,RAC,RAC_C,NB_RAC,NB_RAC_NEG,NB_RAC_C)
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
	DIMENSION RAC(1),RAC_C(1)
	DELTA=B*B-4.D0*A*C
	IF (DELTA.GE.0.D0) THEN
	
	RAC1= (-B+DSQRT(DELTA))/(2.D0*A)
	RAC2=(-B-DSQRT(DELTA))/(2.D0*A)
	IF (RAC1.LE.0.D0) THEN
	RAC(NB_RAC_NEG+1)=RAC1
	NB_RAC_NEG=NB_RAC_NEG+1
	ENDIF
	
	IF (RAC2.LE.0.D0) THEN
	RAC(NB_RAC_NEG+1)=RAC2
	NB_RAC_NEG=NB_RAC_NEG+1
	ENDIF
	
	NB_RAC=NB_RAC+2
	ELSE 
	c=dabs(DSQRT(-DELTA)/(2.D0*A))
	xx=-B/(2.D0*A)
c       on tient pas compte des racines complexes
	RAC_C(NB_RAC_C+1)=dsqrt(xx*xx+c*c)
	NB_RAC_C=NB_RAC_C+1
	NB_RAC=NB_RAC+2
c	WRITE(*,*) 'pole ',xx,' + ',c,' i'
c	WRITE(*,*) 'pole ',xx,' - ',c,' i'
	ENDIF
c	
	END

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	SUBROUTINE BAI_G(A,NDEGRE,EPSILON2,RAC,RAC_C,NB_RAC,NB_RAC_NEG,
     *  NB_RAC_C,DIVERGE,AM)
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
	DIMENSION A(0:NDEGRE),B(0:NDEGRE),RAC(1),RAC_C(1)
	LOGICAL REP,DIVERGE
	
	n=ndegre
c-------test si poly > 2
	DIVERGE=.FALSE.
	if (n.gt.2) then
	S=AM+AM
	p=AM*AM
	DO WHILE(N.GT.2.AND.DIVERGE.EQV..FALSE.)
        S=AM+AM
	p=AM*AM
	REP=.FALSE.
	iter=0
	ii=0
	DO WHILE(REP.EQV..false..and.ii.lt.20)
	iter=0
	S=(AM-dble(ii)*AM/10.d0)*2.D0
	p=(AM-dble(ii)*AM/10.D0)**2.d0-(AM-dble(ii)*AM/10.d0)
	DO WHILE(REP.EQV..FALSE..and.iter.le.1000)
	CALL RAPHSON(a,b,N,S,P,EPSILON2,rep)
	iter=iter+1
	ENDDO
	ii=ii+1
	ENDDO
c	write (*,*) 'nb_iterations', iter
c-------test si n-r a converge: si oui on continue 
c-------sinon on sort 
	if (REP.EQV..true.) then
	CALL RESOUT2_G(1.D0,-S,P,RAC,RAC_C,NB_RAC,NB_RAC_NEG,
     *  NB_RAC_C)
	DO K=0,N-2
	A(K)=B(K+2)
	enddo
	N=N-2
	else
	WRITE(*,*) 'NR n a pas trouve toutes les racines'
	DIVERGE=.TRUE.
	endif
c-------fin test n-r convergence	
	enddo	
	
	IF (N.eq.2.AND.DIVERGE.EQV..FALSE.) THEN
	CALL RESOUT2_G(A(2),A(1),A(0),RAC,RAC_C,NB_RAC,
     *  NB_RAC_NEG,NB_RAC_C)
	else
	IF (N.EQ.1.AND.DIVERGE.EQV..FALSE.) THEN
	CALL RESOUT1_G(A,NDEGRE,RAC,NB_RAC,NB_RAC_NEG)
	ENDIF
	endif
c-------fin test si poly > 2
	ELSE
c-------si le polynome de depart < 3
	IF (N.eq.2) THEN
	CALL RESOUT2_G(A(2),A(1),A(0),RAC,RAC_C,NB_RAC,
     *  NB_RAC_NEG,NB_RAC_C)
	else
	IF (N.eq.1) THEN
	CALL RESOUT1_G(A,NDEGRE,RAC,NB_RAC,NB_RAC_NEG)
	ENDIF
	endif
c-------fin si le polynome de depart < 3
c-------fin si poly >2
	ENDIF	
	END
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
	
	





