COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %  //////  MODULE REGROUPANT LES SUBROUTINES DE CALCULS  //////   %
COMM %  //////  DU RESIDU ET DES  CONTRAINTES -- NLG COQ --   //////   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE RESICO(V,COOR,MAIL,EMAI,EPAI,SIG,RES,F,
     *                  NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   CALCUL DU RESIDU ET DES CONTRAINTES                          %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION V(1),COOR(NCPN,1),MAIL(NNPE+2,1),EMAI(NVMA,1),
     *          EPAI(1),SIG(1),RES(1),F(1)
      DIMENSION NO(3),X(3),Y(3),Z(3),D(6,6),VEG(18),REG(18),IGLO(18)     
COMM ...........................////////////...........................
      IAD=0
      CALL DZERO(RES,NDL)
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////.............      
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)           
      CALL CAELCO(ITYP,NBN,NDDLE,NBG)          
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))
      Z(I)=COOR(3,NO(I))
      DO 31 K=1,NDPN
 31   VEG(NDPN*(I-1)+K)=V(NDPN*(NO(I)-1)+K)                  
 3    CONTINUE
COMM  ......//////// ON FORME LA MATRICE DECOMPORTEMENT D //////........
      ND=6    
      CALL DISOCO(EMAI(1,IMAT),EMAI(2,IMAT),EPAI(IM),D)
COMM........///// CALCUL DES CONTRAINTES ET DU RESIDU  /////............  
       IF(ITYP.EQ.31) THEN      
       CALL RESELE_DKT(X,Y,Z,D,VEG,SIG,IAD,REG,NBN,NDDLE,NBG)
      ELSE
       STOP ' Pb ITYP : RESICO '
      ENDIF
COMM .../// ASSEMBLAGE DU VEC. DES FORCES INT.(stocke dans RES) ////.....
      CALL ASSVEC(RES,REG,NO,IGLO,NBN,NDPN)
 20   CONTINUE
COMM .....//// VECTEUR RESIDU = FORCE EXT. - FORCE INT. ////............. 
      DO 25 I=1,NDL
 25   RES(I)=F(I)-RES(I) 
      END
  
      SUBROUTINE RESELE_DKT(X,Y,Z,D,VEG,SIG,IAD,REG,NBN,NDDLE,NBG)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  CALCUL DES CONTRAINTES AU POINTS DE GAUSS    ' DKT+CST '       %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DOUBLE PRECISION KSI,ETA
      DIMENSION X(3),Y(3),Z(3),D(6,6),VEG(18),SIG(1),REG(18),
     *          TRA(3,3),R(18,18),VEL(18),REL(18),AREL(18),
     *          B(6,18),BNL(6,18),T(2),G(2,18),EPS(6),SE(6),
     *          GAUSS(2,3),POIDS(3)
COMM  ........////  Calcul des coordonnees locales  ////................
      CALL COOLOC(X,Y,Z,X2,X3,Y3,TRA,R)
COMM  ......////// CALCUL DES DEPLACEMENTS DANS LE REPERE LOCAL/////....
      CALL MATVEC(R,VEG,VEL,18,18) 
COMM.........////// INITIALISATIONS //////..............................
      CALL DZERO(REL,18)      
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////.............
      CALL GRIGCO(31,GAUSS,POIDS)  
      DO 7 IG=1,NBG     
        KSI=GAUSS(1,IG)
        ETA=GAUSS(2,IG)
COMM ............////// CALCUL DES CONTRAINTES  //////.................
        CALL BMAT_DKT(X2,X3,Y3,KSI,ETA,B,DETJ)
        CALL BNLU_DKT(X2,X3,Y3,KSI,ETA,B,VEL,T,G,BNL)  
        CALL MATVEC(B,VEL,EPS,6,18)
        EPS(1)=EPS(1)+0.5*(T(1)*T(1))
        EPS(2)=EPS(2)+0.5*(T(2)*T(2))
        EPS(3)=EPS(3)+    (T(1)*T(2))
        CALL MATVEC(D,EPS,SE,6,6)     
COMM.....////// STOCKAGE DANS SIG ///////.............................. 
        DO 9 I=1,6     
 9      SIG(IAD+I)=SE(I)      
        IAD=IAD+6        
COMM ............////// CALCUL DU RESIDU  //////....................... 
        CALL TMATVEC(BNL,SE,AREL,6,18)
        COEF=DETJ*POIDS(IG)
        CALL DACTUA(REL,AREL,18,COEF)                   
 7    CONTINUE
COMM  .. ///// Rigidite additionnelles pour le d.d.l. 0z /////...........
      CT1=D(4,4)*0.0001
      REL( 6)=REL( 6)+CT1*(VEL( 6)-0.5*(VEL(12)+VEL(18)))
      REL(12)=REL(12)+CT1*(VEL(12)-0.5*(VEL(18)+VEL( 6)))
      REL(18)=REL(18)+CT1*(VEL(18)-0.5*(VEL( 6)+VEL(12))) 
COMM ......///// PASSAGE AU REPERE GLOBAL /////........................
      CALL TMATVEC(R,REL,REG,18,18)
      END
      

