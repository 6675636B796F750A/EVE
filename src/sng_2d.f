COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %  //////  MODULE REGROUPANT LES SUBROUTINES DE CALCULS  //////   %
COMM %  //////  DU RESIDU ET DES  CONTRAINTES -- NLG  2D --   //////   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE RESI2D(V,COOR,MAIL,EMAI,EPAI,SIG,RES,F,
     *                  NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   CALCUL DU RESIDU ET DES CONTRAINTES                          %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION V(1),COOR(NCPN,1),MAIL(NNPE+2,1),EMAI(NVMA,1),
     *          EPAI(1),SIG(1),RES(1),F(1)
      DIMENSION NO(8),X(8),Y(8),VE(16),D(16),RE(16),IGLO(16)      
COMM ...........................////////////...........................
      IAD=0
      CALL DZERO(RES,NDL)
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////.............
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)          
      CALL CAEL2D(ITYP,NBN,NDDLE,NBG) 
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))
      VE(2*I-1)=V(2*NO(I)-1)
      VE(2*I  )=V(2*NO(I)  )             
 3    CONTINUE
      H=EPAI(IM)       
COMM  ....///// ON FORME LA MATRICE DECOMPORTEMENT D (ND,ND) /////.....
      IF      (KTYP.EQ.1) THEN
         ND=3
         CALL DISODP(EMAI(1,IMAT),EMAI(2,IMAT),D)
      ELSE IF (KTYP.EQ.2) THEN
         ND=3
         CALL DISOCP(EMAI(1,IMAT),EMAI(2,IMAT),D)
      ELSE IF (KTYP.EQ.3) THEN
         ND=4
         CALL DISOAX(EMAI(1,IMAT),EMAI(2,IMAT),D)
      ELSE 
         STOP ' RESI2D : pb sur KTYP '
      ENDIF 
COMM..........///// CALCUL DU RESIDU  ELEMENTAIRE /////............                            
      IF(ITYP.EQ.30.OR.ITYP.EQ.40.OR.
     *   ITYP.EQ.60.OR.ITYP.EQ.80.OR.ITYP.EQ.81) THEN
c        CALL RESELE_ISOPARA2D(X,Y,D,H,VE,SIG,IAD,RE,
        CALL RESELE_ISOPARA2D(X,Y,D,H,VE,SIG,IAD,
     *       EMAI(1,IMAT),EMAI(2,IMAT),KTYP,ITYP,NBN,NDDLE,NBG,ND)
      ELSE      
        STOP ' RESI2D : pb sur ITYP '  
      ENDIF      
COMM .../// ASSEMBLAGE DU VEC. DES FORCES INT.(stocke dans RES) ////.....
      CALL ASSVEC(RES,RE,NO,IGLO,NBN,NDPN)
 20   CONTINUE      
COMM .....//// VECTEUR RESIDU = FORCE EXT. - FORCE INT ////............. 
      DO 25 I=1,NDL
 25   RES(I)=F(I)-RES(I) 
      END      
      
      SUBROUTINE RESELE_ISOPARA2D(X,Y,D,H,VE,SIG,IAD,RE,YG,PS,
     *                            KTYP,ITYP,NBN,NDDLE,NBG,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  RESIDU ELEMENTAIRE  :  ELEMENTS ISOPARAMETRIQUES 2D            %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION X(8),Y(8),D(16),VE(16),SIG(1),RE(18),ARE(18),
     *          BNL(64),G(80),T(5),EPS(4),SE(4),GAUSS(2,9),POIDS(9)     
      DOUBLE PRECISION KSI,ETA             
COMM .......////// INITIALISATION DU RESIDU ELEMENTAIRE //////.........
      CALL DZERO(RE,NDDLE)
      CALL GRIG2D(ITYP,GAUSS,POIDS)          
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////............    
      DO 7 NG=1,NBG     
        KSI=GAUSS(1,NG)
        ETA=GAUSS(2,NG)
COMM ............////// CALCUL DE LA MATRICE BNL  //////.................
        CALL BNLGISO2D(X,Y,KSI,ETA,VE,BNL,G,T,DETJ,RAY,
     *                 NDDLE,NBN,ND,ITYP,KTYP)
COMM...........//// CALCUL DES DEFORMATIONS /////........................
         EPS(1)=   T(1)  + 0.5 *( T(1)*T(1) + T(3)*T(3) )
         EPS(2)=   T(4)  + 0.5 *( T(2)*T(2) + T(4)*T(4) )
         EPS(3)=T(2)+T(3)+      ( T(1)*T(2) + T(3)*T(4) ) 
         IF(KTYP.EQ.3) THEN
         EPS(4)=   T(5)  + 0.5 *( T(5)*T(5) )
         ENDIF              
COMM...........//// CALCUL DES CONTRAINTES SE=D.EPS /////................
         CALL MATVEC(D,EPS,SE,ND,ND)
COMM.....////// CALCUL DE E33 ET S33 POUR LE CAS PLAN////////............
        IF(KTYP.EQ.1) THEN
          EPS(4)=0.D0
          SE(4)=PS*(SE(1)+SE(2)) 
        ENDIF    
        IF(KTYP.EQ.2) THEN
          EPS(4)=-PS*(SE(1)+SE(2))/YG
          SE(4)=0.D0
        ENDIF
COMM........///// STOCKAGE DES CONTRAINTES DANS SIG ///////.............. 
        DO 9 I=1,4      
 9      SIG(IAD+I)=SE(I)      
        IAD=IAD+4      
COMM .......///// CALCUL DES FORCES INT. AUX NOEUDS///////...............
        CALL TMATVEC(BNL,SE,ARE,ND,NDDLE)
COMM
        IF(KTYP.EQ.1.OR.KTYP.EQ.2) THEN        
         COEF=DETJ*H*POIDS(NG)
        ELSE IF (KTYP.EQ.3) THEN
         COEF=DETJ*6.2831853*RAY*POIDS(NG)
        ENDIF
        CALL DACTUA(RE,ARE,NDDLE,COEF)
  7   CONTINUE
      END
