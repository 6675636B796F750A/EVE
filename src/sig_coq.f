COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %  //////  MODULE REGROUPANT LES SUBROUTINES DE CALCULS  //////   %
COMM %  //////  DES CONTRAINTES ELASTIQUES -- LINEAIRE  COQ --//////   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE SELACO(SIG,V,COOR,MAIL,EMAI,EPAI,
     *                  NMAI,NCPN,NDPN,NNPE,NVMA,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   CALCUL DES CONTRAINTES ASSOCIEES AU VECTEUR DEPLACEMENT V    %
COMM %        ET AUX CONTRAINTES INITIALES CONTENUES DANS SIG         %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION SIG(1),V(1),COOR(NCPN,1),MAIL(NNPE+2,1),
     *          EMAI(NVMA,1),EPAI(1),
     *          NO(3),X(3),Y(3),Z(3),VEG(18),D(6,6)     
COMM ...........................////////////...........................
      IAD=0
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////............
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)           
      CALL CAELCO(ITYP,NBN,NDDLE,NBG)          
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))
      Z(I)=COOR(3,NO(I))
      DO 31 K=1,NDPN
 31   VEG(NDPN*(I-1)+K)=V(NDPN*(NO(I)-1)+K)                  
 3    CONTINUE
COMM  ......//////// ON FORME LA MATRICE DECOMPORTEMENT D //////........
      ND=6    
      CALL DISOCO(EMAI(1,IMAT),EMAI(2,IMAT),EPAI(IM),D)
COMM..................///// CALCUL DES CONTRAINTES /////.................
      IF(ITYP.EQ.31) THEN      
       CALL SIGELE_DKT(X,Y,Z,D,VEG,SIG,IAD,NBN,NDDLE,NBG,ND)
      ELSE
       STOP ' Pb ITYP : SELACO '
      ENDIF        
COMM ...........................////////////...........................
 20   CONTINUE
      END
  
      SUBROUTINE SIGELE_DKT(X,Y,Z,D,VEG,SIG,IAD,NBN,NDDLE,NBG,ND)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  CALCUL DES CONTRAINTES AU POINTS DE GAUSS    ' DKT+CST '       %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION X(3),Y(3),Z(3),VEG(18),D(6,6),SIG(1),
     *          TRA(3,3),R(18,18),VEL(18),B(6,18),EPS(6),SE(6),
     *          GAUSS(2,3),POIDS(3)
      DOUBLE PRECISION KSI,ETA     
COMM  ........////  Calcul des coordonnees locales  ////................
      CALL COOLOC(X,Y,Z,X2,X3,Y3,TRA,R)
COMM  ......////// CALCUL DES DEPLACEMENTS DANS LE REPERE LOCAL/////....
      CALL MATVEC(R,VEG,VEL,18,18)  
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////.............
      CALL GRIGCO(31,GAUSS,POIDS)  
      DO 7 IG=1,NBG     
        KSI=GAUSS(1,IG)
        ETA=GAUSS(2,IG)
COMM ............////// CALCUL DE LA MATRICE B  //////.................
        CALL BMAT_DKT(X2,X3,Y3,KSI,ETA,B,DETJ)
COMM........////// CALCUL DE EPS=B.VE puis SE=D.EPS ////////..............
        CALL MATVEC(B,VEL,EPS,ND,NDDLE)
        CALL MATVEC(D,EPS,SE,ND,ND)        
COMM.....////// STOCKAGE DANS SIG (ON AJOUTE SANS ECRASER)///////......... 
        DO 9 I=1,6     
 9      SIG(IAD+I)=SIG(IAD+I)+SE(I)      
        IAD=IAD+6                   
 7    CONTINUE 
      END
