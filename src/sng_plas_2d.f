COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version  (Mars 2003)                       %
COMM %                                                                 %
COMM %  //////  MODULE REGROUPANT LES SUBROUTINES DE CALCULS  //////   %
COMM %  //////              DU RESIDU                         //////   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      SUBROUTINE RESI_PLAS_2D(V,COOR,MAIL,EMAI,EPAI,SIG,EPS,RES,F,
     &           SIGQ,SIGe,FC,ZP,GP,DEN,EPSP,PN,NBG,NMAI,NDL,NCPN,
     &           NDPN,NSPG,NNPE,NVMA,KTYP,NSIG,IP,SIG1,EPSP1,XP0,
     &           YP0)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   CALCUL DU RESIDU                                             %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION COOR(NCPN,1),MAIL(NNPE+2,1),EMAI(NVMA,1),V(1),
     &          EPAI(1),SIG(NSIG),RES(1),F(1),EPS(NSIG),VE(16),
     &          SIGQ(NBG*NMAI),SIGe(NBG*NMAI),FC(NBG*NMAI),
     &          ZP(NBG*NMAI),GP(NBG*NMAI),DEN(NBG*NMAI),
     &          EPSP(NSIG),PN(NSIG),SIG1(NSIG),EPSP1(NSIG),
     &          XP0(NBG*NMAI),YP0(NBG*NMAI)
      DIMENSION NO(8),X(8),Y(8),RE(16),IGLO(16),D(16)
COMM ...........................////////////...........................
      IADS=1
      IAD0=1
      CALL DZERO(RES,NDL)
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////.............
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)
      YG=EMAI(1,IMAT)
      PS=EMAI(2,IMAT)
      SIGY=EMAI(3,IMAT)
      hp=EMAI(4,IMAT)
c      DEPSc=EMAI(5,IMAT)
      EPSc=EMAI(5,IMAT)
      BETA=10.D0
      DEPSc=EPSc/BETA
      ETA1=EMAI(6,IMAT)
      ETA2=EMAI(7,IMAT)
      ETA3=EMAI(8,IMAT)
      ETA4=EMAI(9,IMAT)
      CALL CAEL2D(ITYP,NBN,NDDLE,NBG)
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))
      VE(2*I-1)=V(2*NO(I)-1)
      VE(2*I  )=V(2*NO(I)  )
 3    CONTINUE
      H=EPAI(IM)
COMM  ....///// ON DONNE LE TYPE DE PROBLEME  /////.....
      IF      (KTYP.EQ.1) THEN
      ND=3
      CALL DISODP(YG,PS,D)
      ELSE IF (KTYP.EQ.2) THEN
      ND=3
      CALL DISOCP(YG,PS,D)
      ELSE IF (KTYP.EQ.3) THEN
      ND=4
      CALL DISOAX(YG,PS,D)
      ELSE
      STOP ' RESI_PLAS_2D : pb sur KTYP '
      ENDIF
COMM..........///// CALCUL DU RESIDU  ELEMENTAIRE /////............
      IF(ITYP.EQ.30.OR.ITYP.EQ.40.OR.
     &   ITYP.EQ.60.OR.ITYP.EQ.80.OR.ITYP.EQ.81) THEN
      CALL RESELE_ISOPARA_PLAS_2D(VE,D,X,Y,H,SIG,EPS,
     &     RE,SIGQ,SIGe,FC,ZP,GP,DEN,EPSP,PN,
     &     NSIG,KTYP,ITYP,NBN,NDDLE,NBG,NMAI,NSPG,ND,IADS,IAD0,
     &     YG,PS,SIGY,hp,ETA1,ETA3,ETA4,IP,SIG1,EPSP1,XP0,YP0,DEPSc
     &     ,ETA2)
      ELSE
      STOP ' RESI_PLAS_2D : pb sur ITYP '  
      ENDIF      
COMM .../// ASSEMBLAGE DU VEC. DES FORCES INT.(stocke dans RES) ////.....
      CALL ASSVEC(RES,RE,NO,IGLO,NBN,NDPN)
 20   CONTINUE      
      DO 25 I=1,NDL
 25   RES(I)=F(I)-RES(I) 
      END

      SUBROUTINE RESELE_ISOPARA_PLAS_2D(VE,D,X,Y,H,SIG,EPS,
     &           RE,SIGQ,SIGe,FC,ZP,GP,DEN,EPSP,PN,
     &           NSIG,KTYP,ITYP,NBN,NDDLE,NBG,NMAI,NSPG,ND,
     &           IADS,IAD0,YG,PS,SIGY,hp,ETA1,ETA3,ETA4,IP,
     &           SIG1,EPSP1,XP0,YP0,DEPSc,ETA2)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  RESIDU ELEMENTAIRE  :  ELEMENTS ISOPARAMETRIQUES 2D            %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION X(8),Y(8),SIG(NSIG),RE(NDDLE),ARE(NDDLE),EPS(NSIG),
     &       BL(ND,NDDLE),SE(ND),GAUSS(2,9),POIDS(9),VE(NDDLE),
     &       SIGQ(NBG*NMAI),SIGe(NBG*NMAI),FC(NBG*NMAI),
     &       ZP(NBG*NMAI),GP(NBG*NMAI),EPSP(NSIG),PN(NSIG),
     &       D(ND,ND),DEN(NBG*NMAI),XP0(NBG*NMAI),YP0(NBG*NMAI),
     &       RES_COMP(ND),SIG_I(3,3),SIGD(3),SIG1(NSIG),EPSP1(NSIG)
      DOUBLE PRECISION KSI,ETA
COMM .......////// INITIALISATION DU RESIDU ELEMENTAIRE //////.........
      CALL DZERO(RE,NDDLE)
      CALL GRIG2D(ITYP,GAUSS,POIDS)
      CALL MAT_SIGMA_I(SIG_I,ND)
      RMU2=YG/(1.D0+PS)
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////............
      DO 7 NG=1,NBG     
      KSI=GAUSS(1,NG)
      ETA=GAUSS(2,NG)
COMM ............////// CALCUL DE LA MATRICE BNL  //////.................
      CALL BLINISO2D(X,Y,KSI,ETA,BL,DETJ,RAY,NDDLE,NBN,ND,ITYP,KTYP)
      CALL MATVEC(BL,VE,EPS(IADS),ND,NDDLE)
comm     calcul DE XP0 et YP0
      XP0(IAD0)=(SIG1(IADS)*PN(IADS)+SIG1(IADS+1)*PN(IADS+1)+
     &          2.d0*SIG1(IADS+2)*PN(IADS+2))/(RMU2*DEPSc)+
     &          (EPSP1(IADS)*PN(IADS)+EPSP1(IADS+1)*PN(IADS+1)+
     &           EPSP1(IADS+2)*PN(IADS+2))/DEPSc+
     & (EPSP1(IADS)+EPSP1(IADS+1))*(PN(IADS)+PN(IADS+1))/DEPSc
      YP0(IAD0)=(XP0(IAD0)+DSQRT(XP0(IAD0)**2+4.D0*ETA2**2))/2.D0
      
COMM..Calcul du residu de SIGQ
      PROD=SIGD_SIGD_2D(SIG(IADS),SIG(IADS),ND)
      SIGQcal=DSQRT(1.5D0*PROD+(ETA1*SIGY)**2)
      RES_SIGQ=SIGQ(IAD0)-SIGQcal
      WRITE(13,*)'IPAS,PtGAUSS =',IP,IAD0
      WRITE(13,*)'RESIDU RES_SIGQ :',RES_SIGQ
      
      EPSP11=EPS(IADS)-(SIG(IADS)-PS*SIG(IADS+1))/YG 
      EPSP21=EPS(IADS+1)-(SIG(IADS+1)-PS*SIG(IADS))/YG 
      EPSP31=EPS(IADS+2)-(2.D0*(1.D0+PS)/YG)*SIG(IADS+2)
      RES_EPSP1=EPSP(IADS)-EPSP11
      RES_EPSP2=EPSP(IADS+1)-EPSP21
      RES_EPSP3=EPSP(IADS+2)-EPSP31
      WRITE(13,*)'RES_EPSP=',RES_EPSP1,RES_EPSP2,RES_EPSP3

      EPSPe0=DSQRT(4.D0*(EPSP(IADS)**2+
     &             EPSP(IADS+1)**2+
     &            (EPSP(IADS+2)/2.D0)**2+
     &             EPSP(IADS)*EPSP(IADS+1))/3.D0)
      SIGe0=SIGY+hp*EPSPe0
      RES_SIGe=SIGe(IAD0)-SIGe0
      WRITE(13,*)'RES_SIGe=',RES_SIGe

      RES_COMP(1)=(SIG(IADS)-PS*SIG(IADS+1))/YG-EPS(IADS)+EPSP(IADS)
      RES_COMP(2)=(SIG(IADS+1)-PS*SIG(IADS))/YG-EPS(IADS+1)+EPSP(IADS+1)
      RES_COMP(3)=(1.D0+PS)*SIG(IADS+3)/YG-(EPS(IADS)-EPSP(IADS))/2.D0
      WRITE(13,*)'RES_COMP =',RES_COMP

      RES_FC=FC(IAD0)-(SIGQ(IAD0)-SIGe(IAD0))/SIGe(IAD0)
      WRITE(13,*)'RES_FC=',RES_FC

      RES_ZP=ZP(IAD0)-FC(IAD0)**2
      WRITE(13,*)'RES_ZP=',RES_ZP

      RES_DEN=DEN(IAD0)-(SIGe(IAD0)*ZP(IAD0)/RMU2+ETA3*
     &                  (1.5D0+hp*(1.D0+FC(IAD0))/RMU2))
      WRITE(13,*)'RES_DEN=',RES_DEN
      
      RES_GP=GP(IAD0)-ETA3/DEN(IAD0)
      WRITE(13,*)'RES_GP=',RES_GP

      CALL MATVEC(SIG_I,SIG(IADS),SIGD,ND,ND)
      RES_N1=PN(IADS)-1.5D0*SIGD(1)/SIGQ(IAD0)
      RES_N2=PN(IADS+1)-1.5D0*SIGD(2)/SIGQ(IAD0)
      RES_N3=PN(IADS+2)-1.5D0*SIGD(3)/SIGQ(IAD0)
      WRITE(13,*)'RES_N=',RES_N1,RES_N2,RES_N3


COMM........///// STOCKAGE DES CONTRAINTES DANS SIG ///////..............
      DO 9 I=1,ND
 9    SE(I)=SIG(IADS-1+I)
COMM .......///// CALCUL DES FORCES INT. AUX NOEUDS///////...............
      CALL TMATVEC(BL,SE,ARE,ND,NDDLE)
      IADS=IADS+NSPG
      IAD0=IAD0+1
COMM
      IF(KTYP.EQ.1.OR.KTYP.EQ.2) THEN        
      COEF=DETJ*H*POIDS(NG)
      ELSE IF (KTYP.EQ.3) THEN
      COEF=DETJ*6.2831853*RAY*POIDS(NG)
      ENDIF
      CALL DACTUA(RE,ARE,NDDLE,COEF)
      WRITE(13,*)'==================================================='
  7   CONTINUE
      END












