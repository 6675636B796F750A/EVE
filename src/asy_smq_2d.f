COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %  //////   CALCUL DE  FQ   --- elasticite 2D ---  //////         %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE SMFQ2D(F,V0,V,SIG,TET,COOR,MAIL,EMAI,EPAI,KTYP,
     *NMAI,NDL,NSIG,NTET,NNPE,NCPN,NDPN,NSPG,NTPG,NVMA,IORDRE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %   ASSEMBLAGE DU SECOND MEMBRE ET CALCUL DES CONTRAINTES         %
COMM %                       A L'ORDRE   IORDRE                        %
COMM %                                                                 % 
COMM %   lorsque l'on rentre dans cette subroutine :                   %
COMM %    - les vecteurs deplacement jusqu'a l'ordre iordre-1          %
COMM %      sont entierement calcules                                  %
COMM %    - les contraintes jusqu'a l'ordre iordre-2                   %
COMM %      sont entierement calculees                                 %
COMM %    - les contraintes a l'ordre iordre-1 sont                    %
COMM %      partiellement calculees                                    %
COMM %                                                                 %
COMM %   dans cette subroutine :                                       %
COMM %    - on termine le calcul des contrainte a l'ordre iordre-1     %
COMM %      a l'aide du vecteur deplacement a l'ordre iordre-1         %
COMM %    - on calcule le second membre a l'ordre  iordre              %
COMM %    - on calcule partiellement les contraintes normales a        %
COMM %      l'ordre iordre a a l'aide des vecteurs V0, V1, Viordre-1   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION KSI,ETA
      DIMENSION F(1),V0(1),V(NDL,*),SIG(NSIG,*),TET(NTET,*),
     *          COOR(NCPN,1),MAIL(NNPE+2,1),EMAI(NVMA,1),EPAI(1),
     *          FE(16),IGLO(16),NO(8),X(8),Y(8),D(16),FEGAUSS(16),
     *          VE(16),VE0(16),BNL(64),G(80),T(5),GAUSS(2,9),POIDS(9)
COMM ...///  On initialise le second membre a zero ///................
      CALL DZERO(F,NDL)
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      IADS=1
      IADT=1
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////.............   
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)    
      CALL CAEL2D(ITYP,NBN,NDDLE,NBG)          
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))      
      DO 51 K=1,NDPN
      VE0(NDPN*(I-1)+K)=V0(NDPN*(NO(I)-1)+K)
 51   VE (NDPN*(I-1)+K)=V (NDPN*(NO(I)-1)+K,IORDRE-1)            
 3    CONTINUE
COMM  ....///// ON FORME LA MATRICE DECOMPORTEMENT D (ND,ND) /////.....
      IF      (KTYP.EQ.1) THEN
         ND=3
         CALL DISODP(EMAI(1,IMAT),EMAI(2,IMAT),D)
      ELSE IF (KTYP.EQ.2) THEN
         ND=3
         CALL DISOCP(EMAI(1,IMAT),EMAI(2,IMAT),D)
      ELSE IF (KTYP.EQ.3) THEN
         ND=4
         CALL DISOAX(EMAI(1,IMAT),EMAI(2,IMAT),D)
      ELSE 
         STOP ' SMFQ2D : pb sur KTYP '
      ENDIF      
COMM.........////// INITIALISATIONS //////..............................
      CALL DZERO(FE,NDDLE)
      NT=ND+1
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////.............
      CALL GRIG2D(ITYP,GAUSS,POIDS)  
      DO 7 IG=1,NBG
        KSI=GAUSS(1,IG)
        ETA=GAUSS(2,IG)
COMM .....//// CALCUL DE LA MATRICE BNL ET DE DETJ ( et RAY)////.......
        CALL BNLGISO2D(X,Y,KSI,ETA,VE0,BNL,G,T,DETJ,RAY,
     *                 NDDLE,NBN,ND,ITYP,KTYP)                                           
        CALL SGAU2D(BNL,G,D,VE,SIG(IADS,1),TET(IADT,1 ),FEGAUSS,
     *              ND,NT,NDDLE,NSIG,NTET,NSPG,IORDRE,KTYP)
        IADS=IADS+NSPG
        IADT=IADT+NTPG     
COMM...........////// CALCUL DU COEFFICIENT  ////////////............
        IF(KTYP.EQ.1.OR.KTYP.EQ.2) THEN        
          COEF=DETJ*EPAI(IM)*POIDS(IG)
        ELSE IF (KTYP.EQ.3) THEN
          COEF=DETJ*6.2831853*RAY*POIDS(IG)
        ENDIF
        CALL DACTUA(FE,FEGAUSS,NDDLE,COEF)         
 7    CONTINUE     
COMM ......../// ASSEMBLAGE DU VECTEUR F  ////.........................
      CALL ASSVEC(F,FE,NO,IGLO,NBN,NDPN)          
 20   CONTINUE
      DO 15 I=1,NDL
 15   F(I)=-F(I)
      END
                
      SUBROUTINE SGAU2D(BNL,G,D,VL,SIG,TET,FEGAUSS,
     *                  ND,NT,NDDLE,NSIG,NTET,NSPG,IORDRE,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %    CALCUL DU SECOND MENBRE FQ EN UN POINT DE GAUSS   '2D'       %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)  
      DIMENSION BNL(ND,NDDLE),G(NT,NDDLE),D(ND,ND),VL(NDDLE),
     *          SIG(NSIG,*),TET(NTET,*),A(ND,NT),ATETA(ND),TAS(NT),
     *          GNL(ND),FEGAUSS(NDDLE),SINT(ND)
COMM ......///// Initialisations ///// .................................
      CALL DZERO(FEGAUSS,NDDLE)
COMM
COMM .../// on termine le calcul des contraintes normales iordre-1  ///...
COMM .../// et on calcule le gradient de depl. T a l'ordre iordre-1 ///... 
COMM
      CALL MATVEC(BNL,VL,GNL,ND,NDDLE)
      CALL MATVECSI(D,GNL,SIG(1,IORDRE-1),ND,ND)
      CALL MATVEC(G,VL,TET(1,IORDRE-1),NT,NDDLE)
COMM
COMM .../// Calcul des produits A(p-r).T(r) et tA(p-r).S(r) ///.........
COMM 
      DO 14 I=1,ND
 14   ATETA(I)=0.D0
      DO 15 I=1,NT
 15   TAS(I)=0.D0
      DO 34 I=1,ND
      DO 34 J=1,NT
 34   A(I,J)=0.D0 
 
C
      DO 2 IR=1,IORDRE-1
        IOMR=IORDRE-IR
        A(1,1)=TET(1,IR)
        A(1,3)=TET(3,IR)
        A(2,2)=TET(2,IR)
        A(2,4)=TET(4,IR)
        A(3,1)=TET(2,IR)
        A(3,2)=TET(1,IR)
        A(3,3)=TET(4,IR)
        A(3,4)=TET(3,IR)
        IF(KTYP.EQ.3) A(5,5)=TET(5,IR)
        CALL MATVECSI(A,TET(1,IOMR),ATETA,ND,NT)
        CALL TMATVECSI(A,SIG(1,IOMR),TAS,ND,NT)
 2    CONTINUE
      DO 23 I=1,ND
 23   ATETA(I)=0.5*ATETA(I)
COMM         
COMM  ...//// le second menbre a l'ordre iordre ////......................
COMM
      CALL MATVEC(D,ATETA,SINT,ND,ND)
      CALL TMATVECSI(G,TAS,FEGAUSS,NT,NDDLE)
      CALL TMATVECSI(BNL,SINT,FEGAUSS,ND,NDDLE)
COMM
COMM ....//// on calcul partiellement les contraintes  iordre /////.......
COMM
      DO 32 I=1,NSPG     
 32   SIG(I,IORDRE)=SINT(I)
      END
