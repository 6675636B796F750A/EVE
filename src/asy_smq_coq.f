COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94                  %
COMM %                                                                 %
COMM %  //////   CALCUL DE  FQ   --- cas des COQUES ---  //////        %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


      SUBROUTINE SMFQCO(F,V,SIG,TET,COOR,MAIL,EMAI,EPAI,NMAI,
     *           NDL,NSIG,NTET,NNPE,NCPN,NDPN,NSPG,NTPG,NVMA,IORDRE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %   ASSEMBLAGE DU SECOND MEMBRE ET CALCUL DES CONTRAINTES         %
COMM %                       A L'ORDRE   IORDRE                        %
COMM %                                                                 % 
COMM %   lorsque l'on rentre dans cette subroutine :                   %
COMM %    - les vecteurs deplacement jusqu'a l'ordre iordre-1          %
COMM %      sont entierement calcules                                  %
COMM %    - les contraintes jusqu'a l'ordre iordre-2                   %
COMM %      sont entierement calculees                                 %
COMM %    - les contraintes a l'ordre iordre-1 sont                    %
COMM %      partiellement calculees                                    %
COMM %                                                                 %
COMM %   dans cette subroutine :                                       %
COMM %    - on termine le calcul des contrainte a l'ordre iordre-1     %
COMM %      a l'aide du vecteur deplacement a l'ordre iordre-1         %
COMM %    - on calcule le second membre a l'ordre  iordre              %
COMM %    - on calcule partiellement les contraintes normales a        %
COMM %      l'ordre iordre a a l'aide des vecteurs V0, V1, Viordre-1   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION F(1),V(NDL,*),SIG(NSIG,*),TET(NTET,*),
     *          COOR(NCPN,1),MAIL(NNPE+2,1),EMAI(NVMA,1),EPAI(1),
     *          FE(18),IGLO(18),NO(3),X(3),Y(3),Z(3),D(6,6),VG(18)
COMM ......................///////////////////...........................
      IADS=1
      IADT=1
      REWIND 90
COMM ...///  On initialise le second membre a zero ///................
      CALL DZERO(F,NDL)
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////.............  
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)       
      CALL CAELCO(ITYP,NBN,NDDLE,NBG)          
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))
      Z(I)=COOR(3,NO(I))           
      DO 51 K=1,NDPN
 51   VG (NDPN*(I-1)+K)=V (NDPN*(NO(I)-1)+K,IORDRE-1)            
 3    CONTINUE
COMM  ......//////// ON FORME LA MATRICE DECOMPORTEMENT D //////........
      ND=6    
      CALL DISOCO(EMAI(1,IMAT),EMAI(2,IMAT),EPAI(IM),D) 
COMM ...////  Calcul des seconds membres elementaires /////............
      CALL SELECO(X,Y,Z,D,VG,SIG,TET,FE,IADS,IADT,
     *            NBN,NBG,NSIG,NTET,NSPG,NTPG,IORDRE)
COMM ......../// ASSEMBLAGE DU VECTEUR F  ////.........................
      CALL ASSVEC(F,FE,NO,IGLO,NBN,NDPN)          
 20   CONTINUE
      DO 15 I=1,NDL
 15   F(I)=-F(I)
      END
      
      SUBROUTINE SELECO(X,Y,Z,D,VG,SIG,TET,FEG,IADS,IADT,
     *                  NBN,NBG,NSIG,NTET,NSPG,NTPG,IORDRE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %    CALCUL ELEMENTAIRE DU SECOND MENBRE FQ      'DKT+CST'        %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION KSI,ETA
      DIMENSION X(3),Y(3),Z(3),TRA(3,3),D(6,6),SIG(NSIG,*),TET(NTET,*),     
     *          BNL(6,18),G(2,18),
     *          VG(18),VL(18),FE(18),FEG(18),FEGAUSS(18),R(18,18),
     *          GAUSS(2,3),POIDS(3)
COMM  ........////  Calcul des coordonnees locales  ////................
      CALL COOLOC(X,Y,Z,X2,X3,Y3,TRA,R)
COMM  ......////// CALCUL DES DEPLACEMENTS DANS LE REPERE LOCAL/////....
      CALL MATVEC(R,VG,VL,18,18)          
COMM.........////// INITIALISATIONS //////..............................
      CALL DZERO(FE,18)  
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////.............
      CALL GRIGCO(31,GAUSS,POIDS)  
      DO 7 IG=1,NBG
        KSI=GAUSS(1,IG)
        ETA=GAUSS(2,IG)
C        CALL BMAT_DKT(X2,X3,Y3,KSI,ETA,B,DETJ)
C        CALL BNLU_DKT(X2,X3,Y3,KSI,ETA,B,VEL,T,G,BNL)
C
        READ(90) DETJ,BNL,G
C                     
        COEF=DETJ*POIDS(IG)                    
        CALL SGAUCO(BNL,G,D,VL,SIG(IADS,1),TET(IADT,1 ),FEGAUSS,
     *              NSIG,NTET,IORDRE)
        IADS=IADS+NSPG
        IADT=IADT+NTPG     
        CALL DACTUA(FE,FEGAUSS,18,COEF)     
 7    CONTINUE
COMM ......///// PASSAGE AU REPERE GLOBAL /////......................... 
      CALL TMATVEC(R,FE,FEG,18,18)
      END
          
      SUBROUTINE SGAUCO(BNL,G,D,VL,SIG,TET,FEGAUSS,NSIG,NTET,IORDRE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %    CALCUL DU SECOND MENBRE FQ EN UN POINT DE GAUSS   'DKT+CST'  %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)  
      DIMENSION BNL(6,18),G(2,18),D(6,6),VL(18),SIG(NSIG,*),TET(NTET,*)
      DIMENSION GNL(6),ATETA(3),TAS(2),FEGAUSS(18),SINT(6)
COMM
COMM ......///// Initialisations ///// .................................
      CALL DZERO(FEGAUSS,18)
COMM
COMM .../// on termine le calcul des contraintes normales iordre-1  ///...
COMM .../// et on calcule le gradient de depl. T a l'ordre iordre-1 ///... 
COMM
      CALL MATVEC(BNL,VL,GNL,6,18)
      CALL MATVECSI(D,GNL,SIG(1,IORDRE-1),6,6)
      CALL MATVEC(G,VL,TET(1,IORDRE-1),2,18)
COMM
COMM .../// Calcul des produits A(p-r).T(r) et tA(p-r).S(r) ///.........
COMM 
      DO 14 I=1,3
 14   ATETA(I)=0.D0
      DO 15 I=1,2
 15   TAS(I)=0.D0
C
      DO 2 IR=1,(IORDRE-1)/2 
      IOMR=IORDRE-IR
C
      ATETA(1)=ATETA(1)+(TET(1,IR)*TET(1,IOMR))
      ATETA(2)=ATETA(2)+(TET(2,IR)*TET(2,IOMR))
      ATETA(3)=ATETA(3)+(TET(1,IR)*TET(2,IOMR)+TET(2,IR)*TET(1,IOMR)) 
C 
      TAS(1)=TAS(1) + SIG(1,IOMR)*TET(1,IR)+SIG(3,IOMR)*TET(2,IR)                   
      TAS(2)=TAS(2) + SIG(3,IOMR)*TET(1,IR)+SIG(2,IOMR)*TET(2,IR)
C                                      
      TAS(1)=TAS(1) + SIG(1,IR)*TET(1,IOMR)+SIG(3,IR)*TET(2,IOMR)            
      TAS(2)=TAS(2) + SIG(3,IR)*TET(1,IOMR)+SIG(2,IR)*TET(2,IOMR)             
C      
 2    CONTINUE
C
      IF((IORDRE-((IORDRE/2)*2)).EQ.0) THEN
       IPR=IORDRE/2 
C
       ATETA(1)=ATETA(1)+0.5*(TET(1,IPR)*TET(1,IPR))
       ATETA(2)=ATETA(2)+0.5*(TET(2,IPR)*TET(2,IPR))
       ATETA(3)=ATETA(3)+    (TET(1,IPR)*TET(2,IPR)) 
C 
       TAS(1)=TAS(1)+(SIG(1,IPR)*TET(1,IPR)+SIG(3,IPR)*TET(2,IPR))
       TAS(2)=TAS(2)+(SIG(3,IPR)*TET(1,IPR)+SIG(2,IPR)*TET(2,IPR))
C      
      ENDIF
COMM         
COMM  ...//// le second menbre a l'ordre iordre ////......................
COMM
      DO 17 I=1,6
 17   SINT(I)=0.D0
      DO 18 I=1,6
      DO 18 J=1,3
 18   SINT(I)=SINT(I)+D(I,J)*ATETA(J)
      CALL TMATVECSI(G,TAS,FEGAUSS,2,18)
      CALL TMATVECSI(BNL,SINT,FEGAUSS,6,18)   
COMM
COMM ....//// on calcul partiellement les contraintes  iordre /////.......
COMM
      DO 32 I=1,6     
 32   SIG(I,IORDRE)=SINT(I)
      END
