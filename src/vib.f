COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %   ////  MODULE PILOTE DES CALCULS DE VIBRATIONS LIBRES   ////   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE VIBLIB(IDIMT)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %              CALCUL DES FREQUENCES ET DES MODES DE              %
COMM %               VIBRATIONS LIBRE DE LA STRUCTURE                  %
COMM %                                                                 %
COMM %   Organisation de la memoire  (Super-Tableau T + pointeurs IT)  %
COMM %   --------------------------                                    %
COMM %                                                                 %
COMM %      adresse              contenu de T                 type     %
COMM %                                                                 %
COMM %   IT(11)---IDIA       Pointeurs de la diagonale       (ENTIER)  %
COMM %   IT(12)---RIG        Matrice de rigidite             (DP)      %
COMM %   IT(13)---RMA        Matrice de masse                (DP)      %
COMM %   IT(14)---VEP        Les vecteurs propres            (DP)      %
COMM %   IT(15)---VAP        Les valeurs propres             (DP)      % 
COMM %   IT(16)---Vtra       Vecteur de travail              (DP)      %
COMM %   IT(17)---Wtra       Vecteur de travail              (DP)      %
COMM %   IT(18)---AR         Matrice de travail              (DP)      % 
COMM %   IT(19)---BR         Matrice de travail              (DP)      %
COMM %   IT(20)---VEC        Matrice de travail              (DP)      %
COMM %   IT(21)---D          Vecteur de travail              (DP)      %
COMM %   IT(22)---RTOLV      Vecteur de travail              (DP)      %
COMM %   IT(23)---ROLD       Matrice de travail              (DP)      %
COMM %   IT(24)---DOLD       Vecteur de travail              (DP)      %
COMM %   IT(25)---RATOLD     Vecteur de travail              (DP)      %
COMM %   IT(26)---RAT        Vecteur de travail              (DP)      %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION RTOL
      COMMON T(1)
      COMMON/INI/ICO(50),IT(50)
COMM ...........................////////////...........................
      KTYP =ICO(1)
      NCPN =ICO(2)
      NDPN =ICO(3)
      NNPE =ICO(4)            
      NNOE =ICO(6)
      NDL  =ICO(7)
      NMAI =ICO(8)
      NVMA =ICO(10)      
      NLIAI=ICO(11)
COMM ...........................////////////...........................  
      READ(5,*) NMODE
      READ(5,*) NITEM,IFSS
      NC =MIN(2*NMODE,NMODE+8)
      NNC=NC*(NC+1)/2    
COMM
COMM .....//// RESERVATION DES PLACES DANS LE SUPER-TABLEAU ////......
COMM
      IT(12)=IT(11)+NDL     
      CALL PROFBI(T(IT(11)),T(IT(2)),NDL,NMAI,NRIG,NNPE,NDPN,KTYP)
      ICO(26)=NRIG
      IT(13)=IT(12)+2*NRIG 
      IT(14)=IT(13)+2*NRIG
      IT(15)=IT(14)+2*NDL*NC
      IT(16)=IT(15)+2*NC
      IT(17)=IT(16)+2*NDL
      IT(18)=IT(17)+2*NDL
      IT(19)=IT(18)+2*NNC
      IT(20)=IT(19)+2*NNC
      IT(21)=IT(20)+2*NC*NC
      IT(22)=IT(21)+2*NC
      IT(23)=IT(22)+2*NC
      IT(24)=IT(23)+2*NDL*NC
      IT(25)=IT(24)+2*NC      
      IT(26)=IT(25)+2*NC
      IT(27)=IT(26)+2*NC      
COMM 
      WRITE(6,150)       
      WRITE(6,100)       
      DO 1 I=1,26
  1   WRITE(6,120) I,IT(I),IT(I+1)-IT(I)
      WRITE(6,130) IT(27),IDIMT
      IF(IT(27).GT.IDIMT) STOP 'Taille du Super-Tableau'     
COMM            
COMM ....../// ASSEMBLAGE DE LA MATRICE DE RIGIDITE  ///...............
COMM ......///    PRISE EN COMPTE DES BLOCAGES       ///...............
COMM
      CALL RIGELA(T(IT(12)),T(IT(11)),T(IT(1)),T(IT(2)),T(IT(3)),
     *            T(IT( 4)),NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)   
      CALL CALIRI(T(IT( 13)),T(IT(11)),T(IT(5)),NLIAI,NDPN,NDL)
COMM            
COMM ..../// ASSEMBLAGE DE LA MATRICE DE MASSE COERENTE  ///..........
COMM
      CALL MASCOE(T(IT(13)),T(IT(11)),T(IT(1)),T(IT(2)),T(IT(3)),
     *            T(IT( 4)),NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP) 
COMM
COMM...../// CALCULS DES FREQENCES ET DES MODES PROPRES ///............
COMM
      RTOL=1.0D-6 
      NNLF=0 
      CALL SSPACE(T(IT(11)),T(IT(12)),T(IT(13)),T(IT(14)),T(IT(15)),
     ;            T(IT(16)),T(IT(17)),T(IT(18)),T(IT(19)),T(IT(20)),
     ;            T(IT(21)),T(IT(22)),T(IT(23)),T(IT(24)),T(IT(25)),
     ;            T(IT(26)),T(IT(5)),NLIAI,NDPN,NDL,
     ;            NRIG,NRIG,NMODE,NC,NNC,NITEM,IFSS,RTOL)          
COMM
COMM...../// IMPRESSION DES FREQENCES ET DES MODES PROPRES ///........
COMM 
      CALL IMPVIB(T(IT(14)),T(IT(15)),NMODE,NDL)
COMM      
COMM ..................../////////////................................
COMM      
 150  FORMAT(////////,
     ;'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%',/,
     ;'  %                                                        %',/, 
     ;'  %         CALCUL   DE   VIBRATIONS   LIBRES              %',/,      
     ;'  %                                                        %',/,      
     ;'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')           
 100  FORMAT('1',//,' ',60('%'),///,
     ;'  ORGANISATION DU SUPER-TABLEAU  T() ',//)
 110  FORMAT(' ',60('%'),/////)
 120  FORMAT('   IT(',I3,')=',I8,'     Longueur=',I8)
 130  FORMAT(///,'   Taille memoire necessaire   : ',I8,/,
     ;           '   Dimension du Super-Tableau  : ',I8,///)    
      END

COMM ...........................////////////...........................
COMM ...........////// FIN DU MODULE PILOTE DE CALCUL //////...........
COMM ...........//////      VIBRATIONS LIBRES        //////...........
COMM ...........................////////////...........................
