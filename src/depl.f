COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)!!!!             %
COMM %                                                                 %
COMM %  //////   MODULE DE CALCULS DES DEPLACEMENTS IMPOSES   //////   %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE CADEPL(DFOR,F,ALPHA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM % CALCUL DU VECTEUR FORCE F POUR LES DONNEES DE CHARGEMENT DFOR   %
COMM %                                                                 %
COMM %  CE VECTEUR CORRESPOND A DES DEPLACEMENTS IMPOSES               %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DIMENSION DFOR(1)
      DOUBLE PRECISION F(1),ALPHA
      COMMON T(1)
      COMMON/INI/ICO(200),IT(200)
COMM ..........................////////////............................
      NDPN=ICO(3)
      NDL =ICO(7)
      NNOE=ICO(6)
c      CALL DZERO(F,NDL)
COMM
COMM ..........................////////////............................
COMM /// POUR CUMULER LES DIFFERENTS CHARGEMENTS T(IAF) N'EST PAS ///
COMM /// INITIALISE A ZERO DANS FVOLRO , FVOLTH , FSURFA......... ///
COMM ..........................////////////............................
COMM  .......//////  DEPLACEMENT IMPOSE SUR LE BORD  //////............
COMM
      IF(INT(DFOR(8)).GT.0) THEN
       CALL DEPL_SURFA(DFOR,F,NDPN,ALPHA)
      ENDIF
      END

      SUBROUTINE DEPL_SURFA(DFOR,F,NDPN,ALPHA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   CALCUL DES FORCES CONCENTREES ET DES PRESSIONS               %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION F(1)
      REAL DFOR(1)
COMM ..........................////////////............................
      NKF=INT(DFOR(8))
      INIT=8
      IAD=INIT+1
      DO 1 IKF=1,NKF
       ITYPF=INT(DFOR(IAD))
       NLEC =INT(DFOR(IAD+1))
       IAD=IAD+2 
COMM ..............//////  CAS DES FORCES IMPOSEES  //////.............
       IF(ITYPF.EQ.1) THEN
        DO 56 I=1,NLEC
        J =INT(DFOR(IAD  ))
        FX=    DFOR(IAD+1)
        ND=INT(DFOR(IAD+2))
        IAD=IAD+3
 56     F(NDPN*(J-1)+ND)=FX*ALPHA
       ENDIF

1     CONTINUE
      END

      SUBROUTINE ANNULE_REAC(DFOR,RES,NDPN)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   CETTE PROCEDURE PERMET D'ANNULER LES TERMES DE REACTION DUS  %
COMM %   AU DEPLACEMENT IMPOSE (DANS LE VECTEUR RESIDU)               %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RES(1)
      REAL DFOR(1)
COMM ..........................////////////............................
      NKF=INT(DFOR(8))
      INIT=8
      IAD=INIT+1
      DO 1 IKF=1,NKF
       ITYPF=INT(DFOR(IAD))
       NLEC =INT(DFOR(IAD+1))
       IAD=IAD+2 
COMM ..............//////  CAS DES FORCES IMPOSEES  //////.............
       IF(ITYPF.EQ.1) THEN
        DO 56 I=1,NLEC
        J =INT(DFOR(IAD  ))
        FX=    DFOR(IAD+1)
        ND=INT(DFOR(IAD+2))
        IAD=IAD+3
 56     RES(NDPN*(J-1)+ND)=0.D0
       ENDIF
1     CONTINUE
      END

      SUBROUTINE DEPL_IMPOSE_MODIF_RIG(DFOR,RIG,IDIA,NDPN,ALPHA)

COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   CALCUL DES FORCES CONCENTREES ET DES PRESSIONS               %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RIG(1),IDIA(1)
      REAL DFOR(1)
COMM ..........................////////////............................
      NKF=INT(DFOR(8))
      INIT=8
      IAD=INIT+1
      DO 1 IKF=1,NKF
       ITYPF=INT(DFOR(IAD))
       NLEC =INT(DFOR(IAD+1))
       IAD=IAD+2 
COMM ..............//////  CAS DES FORCES IMPOSEES  //////.............
       IF(ITYPF.EQ.1) THEN
        DO 56 I=1,NLEC
        J =INT(DFOR(IAD  ))        !NOEUD CONCERNE
        ND=INT(DFOR(IAD+2))        !DDL CONCERNE
        IAD=IAD+3
        NN=NDPN*(J-1)+ND
 56     RIG(IDIA(NN))=RIG(IDIA(NN))+ALPHA
       ENDIF

1     CONTINUE
      END
     
COMM ..........................////////////............................
COMM .......//////  FIN DU MODULE DE CALCULS DES DEPLACEMENTS  //////........
COMM ..........................////////////............................
