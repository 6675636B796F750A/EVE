COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %   ////////    MODULE D ACQUISITION DES DONNEES    ////////      %
COMM %                                                                 %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE DONNEE
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                ACQUISITION DES DONNEES                          %
COMM %                                                                 %
COMM % OBJECTIF : STOCKER TOUTES LES DONNEES RELATIVES AU MOT CLE      %
COMM %            " DEFINITION " DANS  - LE SUPER-TABLEAU T()          %
COMM %                                 - LE TABLEAU D'ENTIER ICO()     %
COMM %                                                                 %
COMM %                                                                 %
COMM %   Organisation de la memoire  (Super-Tableau T + pointeurs IT)  %
COMM %   --------------------------                                    %
COMM %                                                                 %
COMM %      adresse              contenu de T                 type     %
COMM %                                                                 %
COMM %   IT( 1)---COOR       Coordonnees des noeuds          (DP)      %
COMM %   IT( 2)---MAIL       Description des elements        (ENTIER)  %
COMM %   IT( 3)---EMAI       Caracteristiques des materiaux  (DP)      %
COMM %   IT( 4)---EPAI       Epaisseur des elements          (DP)      %
COMM %   IT( 5)---LIAI       Donnees des blocages            (ENTIER)  %         
COMM %   IT( 6)---DFOR       Donnees des chargements         (SP+ENT)  %
COMM %   IT( 7)---DREL       Donnees des relations           (SP+ENT)  %
COMM %   IT( 8)---           libre                                     %
COMM %   IT( 9)---           libre                                     %
COMM %   IT(10)---           libre                                     %
COMM %                                                                 %
COMM %   Pour le tableau ICO, consulter la notice                      %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      CHARACTER*4  RMOT
      CHARACTER*20 NOMFIC
      CHARACTER*80 TITRE,TITCHA
      INTEGER NVF(10)
      COMMON T(1)
      COMMON/INI/ICO(200),IT(200)
      COMMON/TIT/TITRE,TITCHA(10)
COMM ......................../////  TITRE  ///////......................
      READ(5,'(A80)')TITRE
COMM ...................//////  TYPE DE CALCUL  //////.................
      READ(5,'(A4)') RMOT
      IF     (RMOT.EQ.'DEFO') THEN
       KTYP=1
       NCPN=2     
       NDPN=2     
       NNPE=8     
       NSPG=4        
      ELSE IF(RMOT.EQ.'CONT') THEN
       KTYP=2
       NCPN=2     
       NDPN=2     
       NNPE=8     
       NSPG=4        
      ELSE IF(RMOT.EQ.'AXIS') THEN
       KTYP=3
       NCPN=2     
       NDPN=2     
       NNPE=8     
       NSPG=4               
      ELSE IF(RMOT.EQ.'3D  ') THEN
       KTYP=4
       NCPN=3
       NDPN=3
       NNPE=20
       NSPG=6        
      ELSE IF(RMOT.EQ.'COQU') THEN
       KTYP=5
       NCPN=3
       NDPN=6
       NNPE=8
       NSPG=6       
      ELSE IF(RMOT.EQ.'COQD') THEN
       KTYP=6
       NCPN=7
       NDPN=5
       NNPE=4
       NSPG=5 
      ELSE IF(RMOT.EQ.'RAMM') THEN
       KTYP=7
       NCPN=7
       NDPN=6
       NNPE=8
       NSPG=6  
      ELSE IF(RMOT.EQ.'COQ8') THEN
       KTYP=8
       NCPN=7
       NDPN=6
       NNPE=8
       NSPG=5  
      ELSE
       CALL ERROR1
      ENDIF
COMM.............................//////////............................
      IT(1)=1
COMM .....................//////  GEOMETRIE  //////....................
      READ(5,'(A4)') RMOT
      IF(RMOT.EQ.'GEOM') THEN
        READ(5,'(A20)') NOMFIC
        OPEN(UNIT=70,FILE=NOMFIC,STATUS='OLD',ACCESS='SEQUENTIAL')             
        CALL ACCOOR(T(IT(1)),NNOE,NCPN)
        NDL=NDPN*NNOE
        IT(2)=IT(1)+2*NNOE*NCPN        
        CALL ACMAIL(T(IT(2)),NMAI,NNPE)    
COMM            
        IT(3)=IT(2)+(NNPE+2)*NMAI
        IF( IT(3)-((IT(3)/2)*2).EQ.0) IT(3)=IT(3)+1
        CLOSE(70)
      ELSE
         CALL ERROR1
      ENDIF        
COMM ....................///////  MATERIAU  //////......................
      READ(5,'(A4)') RMOT
      IF(RMOT.EQ.'MATE') THEN
        NVMA=9
        CALL ACMATE(T(IT(3)),NMAI,NVMA,NMAT)
COMM   
        IT(4)=IT(3)+2*NVMA*NMAT                
      ELSE
         CALL ERROR1
      ENDIF
COMM ............/////// EPAISSEUR (facultatif)  //////..................    
      READ(5,'(A4)') RMOT
      CALL ACEPAI(T(IT(4)),NMAI,RMOT)              
      IT(5)=IT(4)+2*NMAI           
COMM ..................//////  LIAISONS RIGIDES  //////..................
      IF(RMOT.EQ.'LIAI') THEN  
        CALL ACLIAI(T(IT(5)),RMOT,NLIAI)
COMM  
        IT(6)=IT(5)+2*NLIAI
      ELSE
         CALL ERROR1
      ENDIF 
COMM ..........//////  CHARGEMENT : FORCES,PRESSION IMPOSEES  //////....
      IF(RMOT.EQ.'CHAR') THEN 
        CALL ACCHAR(T(IT(6)),NVF,NCHA,NFORT) 
        IT(7)=IT(6)+NFORT
        IF( IT(7)-((IT(7)/2)*2).EQ.0) IT(7)=IT(7)+1
      ELSE
         CALL ERROR1
      ENDIF 
COMM ..................//////  RELATION (facultatif) //////..............
      READ(5,'(A4)') RMOT
      IF(RMOT.EQ.'RELA') THEN       
        CALL ACRELA(T(IT(7)),NVRE,NRELA)
        IT(8)=IT(7)+NVRE
        IF( IT(8)-((IT(8)/2)*2).EQ.0) IT(8)=IT(8)+1        
        READ(5,'(A4)') RMOT      
      ELSE
        NRELA=0
        NVRE =0
        IT(8)=IT(7)         
      ENDIF 
COMM ................/////// PLACE ENCORE VIDE //////....................
      IT(9)=IT(8)
      IT(10)=IT(9)
      IT(11)=IT(10)
COMM .....................////// FIN   //////...........................
      IF(RMOT.EQ.'FIN ') THEN 
       CALL LISDON(T(IT(1)),T(IT(2)),T(IT(3)),T(IT(4)),T(IT(5)),
     *  T(IT(6)),T(IT(7)),
     *  KTYP,NCPN,NDPN,NNPE,NNOE,NDL,NMAI,NMAT,NVMA,NLIAI,NCHA,NVF)  
      ELSE
         CALL ERROR1
      ENDIF
COMM .......////// STOCKAGE DES CONSTANTES DANS ICO( )   //////..........            
      ICO( 1)=KTYP
      ICO( 2)=NCPN
      ICO( 3)=NDPN
      ICO( 4)=NNPE
      ICO( 5)=NSPG
      ICO( 6)=NNOE
      ICO( 7)=NDL     
      ICO( 8)=NMAI      
      ICO( 9)=NMAT
      ICO(10)=NVMA      
      ICO(11)=NLIAI
      ICO(12)=NCHA     
      DO 1 I=1,NCHA
 1    ICO(12+I)=NVF(I)
      ICO(23)=NFORT
      ICO(24)=NRELA      
      ICO(25)=NVRE    
      END

      
      SUBROUTINE ACCOOR(COOR,NNOE,NCPN)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %      ACQUISITION DES COORDONNEES DES NOEUDS                    %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION COOR(NCPN,1:*)
       READ(70,*) NNOE
       DO J=1,NNOE
       READ(70,*)K,(COOR(I,J),I=1,NCPN)
       ENDDO
      END      
      SUBROUTINE ACMAIL(MAIL,NMAI,NNPE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %     ACQUISITION DE LA DESCRIPTION DES ELEMENTS                 %
COMM %  Le nombre de noeuds d'un element vaut son type divise par10   %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DIMENSION MAIL(NNPE+2,1:*)
      READ(70,*) NMAI
      DO 10 I=1,NMAI
        READ (70,*)  ITYP,IMAT,(MAIL(J,I),J=3,(ITYP/10)+2)
        MAIL(1,I) = ITYP
        MAIL(2,I) = IMAT
        DO 20 K=(ITYP/10)+3,NNPE+2  
 20     MAIL(K,I)=0
 10   CONTINUE
      END
         
      SUBROUTINE ACMATE(EMAI,NMAI,NVMA,NMAT)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ACQUISITION DES CARACTERISTIQUES MECANIQUES DES ELEMENTS      %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION EMAI(NVMA,1:*)
      CHARACTER*4 RMOT
      READ(5,*) NMAT
      DO 10 IMAT=1,NMAT
      READ(5,'(A4)') RMOT
      IF(RMOT.EQ.'ELAS') THEN
      READ(5,*) E,RNU,RO,AL      
      EMAI(1,IMAT)=E
      EMAI(2,IMAT)=RNU      
      EMAI(3,IMAT)=RO      
      EMAI(4,IMAT)=AL      
      ELSE IF(RMOT.EQ.'PLAS') THEN
      READ(5,*) E,RNU,SIGY,h,DEPSc,ETA1,ETA2,ETA3,ETA5
      EMAI(1,IMAT)=E
      EMAI(2,IMAT)=RNU
      EMAI(3,IMAT)=SIGY
      EMAI(4,IMAT)=h
      EMAI(5,IMAT)=DEPSc
      EMAI(6,IMAT)=ETA1
      EMAI(7,IMAT)=ETA2
      EMAI(8,IMAT)=ETA3
      EMAI(9,IMAT)=ETA5
      ELSE
      CALL ERROR1
      ENDIF
 10   CONTINUE     
      END
      
      SUBROUTINE ACEPAI(EPAI,NMAI,RMOT)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %     ACQUISITION DES EPAISSEURS : PAR DEFAUT H=1.               %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION EPAI(1)
      CHARACTER*4 RMOT
      DIMENSION NAKI(10000)      
      DO 70 I=1,NMAI
 70   EPAI(I)=1.D0
      IF(RMOT.EQ.'EPAI') THEN
        NSTOP=0
        DO WHILE(NSTOP.NE.1)
          CALL ACQUIM(NAKI,NI,NSTOP,RMOT)
          IF(NSTOP.EQ.1) RETURN
          READ(5,*) H
          DO 31 I=1,NI
 31       EPAI(NAKI(I))=H
        END DO
      ENDIF
      END 
           
      SUBROUTINE ACLIAI(LIAI,RMOT,NLIAI)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %    ACQUISITION DES BLOCAGES  DE DEGRES DE LIBERTE               %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DIMENSION NAKI(10000),LIAI(2,1:*)
      CHARACTER*4 RMOT
      NLIAI=0
COMM ..........................////////////............................
      NSTOP=0
      DO WHILE(NSTOP.NE.1)
        CALL ACQUIM(NAKI,NI,NSTOP,RMOT)
        IF(NSTOP.EQ.1) RETURN
        READ(5,'(A4)') RMOT
        IF     (RMOT.EQ.'UX') THEN
         ND=1
        ELSE IF(RMOT.EQ.'UY') THEN
         ND=2
        ELSE IF(RMOT.EQ.'UZ') THEN
         ND=3
        ELSE IF(RMOT.EQ.'RX') THEN
         ND=4
        ELSE IF(RMOT.EQ.'RY') THEN
         ND=5
        ELSE IF(RMOT.EQ.'RZ') THEN
         ND=6
        ELSE
         CALL ERROR1
        ENDIF
        DO 35 I=1,NI
         LIAI(1,NLIAI+I)=NAKI(I)
35       LIAI(2,NLIAI+I)=ND
        NLIAI=NLIAI+NI
      ENDDO
      END
      
      SUBROUTINE ACCHAR(DFOR,NVF,NCHA,NFORT)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ACQUISITION DES DONNEES RELATIVES AUX CHARGES EXTERIEURES     %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      CHARACTER*80 TITCHA,TITRE
      INTEGER NVF(10)
      REAL DFOR(1)
      COMMON/TIT/TITRE,TITCHA(10)
      NFORT=0
      READ(5,*) NCHA
      IF(NCHA.EQ.0) RETURN
      IF(NCHA.GT.10) STOP 'NOMBRE DE CHARGEMENTS > A 10' 
COMM ............//////  BOUCLE SUR LES CHARGEMENTS  //////............
      DO 1 ICHA=1,NCHA
      READ(5,'(A80)') TITCHA(ICHA)
      CALL ACFORC(ICHA,DFOR(NFORT+1),NIC)
      NVF(ICHA)=NIC
      NFORT=NFORT+NIC
 1    CONTINUE
      END

      SUBROUTINE ACFORC(ICHA,DFOR,NIC)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %    ACQUISITION DES DONNEES POUR LE CHARGEMENT NUMERO IC         %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DIMENSION NAKI(10000),DFOR(8)
      CHARACTER*4 RMOT
      CHARACTER*20 FIPREC,FITHERM
      LOGICAL TOURNE
C
      DFOR(1)=ICHA
      DFOR(2)=0
      DFOR(5)=0
      DFOR(6)=0
      DFOR(8)=0
COMM ..........................////////////............................
      INIT=8      
      IAD=INIT+1
      TOURNE=.TRUE.
      DO WHILE(TOURNE)
       READ(5,'(A4)') RMOT 
       IF(RMOT.EQ.'FORC') THEN      
         DFOR(IAD  )=1
         DFOR(IAD+1)=0
         IADSTOK=IAD+1
         IAD=IAD+2
         READ(5,*) NCARD
         DO 10 ICA=1,NCARD
           READ(5,*) NI,(NAKI(I),I=1,NI),FX,ND
           DO 11 J=1,NI
             DFOR(IAD  )=NAKI(J)
             DFOR(IAD+1)=FX
             DFOR(IAD+2)=ND
 11          IAD=IAD+3
 10        DFOR(IADSTOK)=DFOR(IADSTOK)+NI
         DFOR(8)=DFOR(8)+1      
       ELSE IF(RMOT.EQ.'PRES') THEN
         DFOR(IAD  )=2
         DFOR(IAD+1)=0
         IADSTOK=IAD+1
         IAD=IAD+2
         READ(5,*) NCARD
         DO 30 ICA=1,NCARD
           READ(5,*) NI,(NAKI(I),I=1,NI),P,NS
           DO 31 J=1,NI
             DFOR(IAD  )=NAKI(J)
             DFOR(IAD+1)=P
             DFOR(IAD+2)=NS
 31          IAD=IAD+3 
 30        DFOR(IADSTOK)=DFOR(IADSTOK)+NI
         DFOR(8)=DFOR(8)+1     
       ELSE IF(RMOT.EQ.'PESA') THEN
         READ(5,*) GX,GY
         DFOR(2)=1
         DFOR(3)=GX
         DFOR(4)=GY      
       ELSE IF(RMOT.EQ.'CONT') THEN
         READ(5,'(A20)') FIPREC
         DFOR(5)=1
         OPEN(72,FILE=FIPREC,STATUS='OLD',ACCESS='SEQUENTIAL')      
       ELSE IF(RMOT.EQ.'THER') THEN
         READ(5,'(A20)') FITHERM
         DFOR(6)=1
         OPEN(73,FILE=FITHERM,STATUS='OLD',ACCESS='SEQUENTIAL')      
       ELSE IF(RMOT.EQ.'RETO') THEN
         NIC=IAD-1
         TOURNE=.FALSE.
       ELSE
        CALL ERROR1
       ENDIF
      ENDDO
      END   
      
      SUBROUTINE ACRELA(DREL,NVRE,NRELA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %     ACQUISITION DES DONNEES RELATIVES AUX RELATIONS            %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      PARAMETER(NDIM=300)
      DIMENSION NDL(NDIM),CR(NDIM),DREL(1)
      READ(5,*) NRELA
      IF(NRELA.GT.NDIM) STOP ' Nombre de relations trop grand '      
      IAD=1
      DO 1 IR=1,NRELA
       READ(5,*) NC,(CR(I),NDL(I),I=1,NC),UD
       DREL(IAD)=NC
       DO 2 I=1,NC
        DREL(IAD+2*I-1 )=CR(I)
 2      DREL(IAD+2*I   )=NDL(I)
       DREL(IAD+2*NC+1)=UD
       IAD=IAD+2*NC+2
 1    CONTINUE
      NVRE=IAD-1
      END 
       
      SUBROUTINE LISDON(COOR,MAIL,EMAI,EPAI,LIAI,DFOR,DREL,
     *KTYP,NCPN,NDPN,NNPE,NNOE,NDL,NMAI,NMAT,NVMA,NLIAI,NCHA,NVF)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %       IMPRESSION DE LA LISTE DES DONNEES DANS LE               %
COMM %                 FICHIER DE SORTIE (6)                          %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION COOR(NCPN,1:*),EMAI(NVMA,1:*),EPAI(1)
      DIMENSION MAIL(NNPE+2,1:*),LIAI(2,1:*),DFOR(1),DREL(1)
      INTEGER NVF(10)
      CHARACTER*80 TITRE,TITCHA
      COMMON/TIT/TITRE,TITCHA(10)
COMM .........................////////////.............................
100   FORMAT(///,' ',100(1H%),/,' ',1H%,98(' '),1H%,
     ;/,' ',1H%,3(' '),A80,15(' '),1H%,/,' ',1H%,
     ;98(' '),1H%,/,' ',100(1H%))
101   FORMAT(/////,' TYPE DE CALCUL : 2D DEFORMATION PLANE')
102   FORMAT(/////,' TYPE DE CALCUL :  2D CONTRAINTE PLANE')
103   FORMAT(/////,' TYPE DE CALCUL :   2D   AXISYMETRIQUE')
104   FORMAT(/////,' TYPE DE CALCUL :     3D ')
105   FORMAT(/////,' TYPE DE CALCUL :   COQUE')
106   FORMAT(      ' -------------------------------------')
107   FORMAT(      ' ------------------------')
C
110   FORMAT(//,' NOEUDS',/,' ------',//,5X,'NUM.',15X,'COORDONNEES',/)
112   FORMAT(I4,7(1X,G15.8))
C
150   FORMAT(//,' CARACTERITIQUES DES MATERIAUX',/
     *          ' -----------------------------')
151   FORMAT(//,3X,'MATERIAU NUMERO ',I3,/,
     *          3X,'---------------')
152   FORMAT(/,5X,' MODULE D''YOUNG...........................',G15.8,/,
     *         5X,' COEFFICIENT DE POISSON...................',G15.8,/,   
     *         5X,' MASSE VOLUMIQUE..........................',G15.8,/,
     *         5X,' COEFFICIENT DE DILLATATION THERMIQUE.....',G15.8)
COMM
COMM %%%%%%%%%%%   MATERAU PLASTIQUE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM
153   FORMAT(/,5X,' MODULE D''YOUNG...........................',G15.8,/,
     *         5X,' COEFFICIENT DE POISSON...................',G15.8,/,   
     *         5X,' LIMITE ELASTIQUE.........................',G15.8,/,
     *         5X,' COEFFICIENT ETA1.........................',G15.8,/,
     *         5X,' COEFFICIENT ETA2.........................',G15.8)
C
113   FORMAT(//,' ELEMENTS',/,' --------')
114   FORMAT(//,2X,'NUM.',4X,'TYPE',3X,'MATERIAUX',2X,'EPAISSEUR',
     ;20X,'NOEUDS  ASSOCIES',/)
115   FORMAT(1X,I4,5X,I3,6X,I2,5X,G15.8,30(I4,2X))
C 
127   FORMAT(//' BLOCAGES',/,' --------')
128   FORMAT(//,5(6X,'NOEUD',2X,'DDL'),/)
129   FORMAT(5('       ',I4,3X,I2))
C     
116   FORMAT(///,3X,'CHARGEMENTS DE SURFACE',/,
     ;           3X,'----------------------',///)
117   FORMAT(/////,1X,'CHARGEMENT NUMERO',I4,/,
     ;             1X,'*****************',//)
118   FORMAT(//,5X,'FORCES IMPOSEES',//,
     ;          5X,'NOEUD      FORCE      D.D.L.',/)
120   FORMAT(//,5X,'PRESSION IMPOSEE',//,
     ;          5X,'ELEMENT    PRESSION   SURFACE',/)
121   FORMAT(5X,I4,2X,G15.8,2X,I4)
122   FORMAT(///,3X,'CHARGEMENTS DE VOLUME',/,
     ;           3X,'---------------------',///)
124   FORMAT(//,5X,'DIRECTION DE LA PESANTEUR',2X,G15.8,2X,G15.8,//)
125   FORMAT(///,5X,'ON TIENT COMPTE D UN CHAMP DE PRECONTRAINTES',/)
126   FORMAT(///,5X,'ON TIENT COMPTE D UN CHAMP DE TEMPERATURE',/)     
200   FORMAT(////)
COMM .................//////  DEBUT  //////...........................
      WRITE(6,100) TITRE
COMM .................//////  TYPE DE CALCUL  //////...................
      IF(KTYP.EQ.1) THEN
        WRITE(6,101)
        WRITE(6,106)
      ELSE IF(KTYP.EQ.2) THEN
        WRITE(6,102)
        WRITE(6,106)
      ELSE IF(KTYP.EQ.3) THEN
        WRITE(6,103)
        WRITE(6,106)        
      ELSE IF(KTYP.EQ.4) THEN
        WRITE(6,104)
        WRITE(6,107)        
      ELSE IF(KTYP.EQ.5.OR.KTYP.EQ.6.OR.KTYP.EQ.7.OR.KTYP.EQ.8) THEN       
        WRITE(6,105)
        WRITE(6,107)        
      ELSE  
        STOP ' Pb KTYP : LISDON'
      ENDIF
COMM ...................////// NOEUDS  //////.................
      WRITE(6,200)
      WRITE(6,110) 
      DO 6 I=1,NNOE
6     WRITE(6,112) I,(COOR(J,I),J=1,NCPN)       
COMM ............////// CARACTERISTIQUE MATERIAUX //////.............      
      WRITE(6,200)
      WRITE(6,150)
      DO 25 IMAT=1,NMAT            
      WRITE(6,151) IMAT 
 25   WRITE(6,153) EMAI(1,IMAT),EMAI(2,IMAT),EMAI(3,IMAT),EMAI(4,IMAT),
     *             EMAI(5,IMAT)   
COMM .................////// ELEMENTS //////.........................      
      WRITE(6,200)
      WRITE(6,113)
      WRITE(6,114)
      IF(KTYP.EQ.1.OR.KTYP.EQ.2.OR.KTYP.EQ.3.OR.KTYP.EQ.5) THEN      
        DO 10 I=1,NMAI
        WRITE(6,115) I,MAIL(1,I),MAIL(2,I),EPAI(I),
     *               (MAIL(J,I),J=3,NNPE+2)
10      CONTINUE
      ELSE IF (KTYP.EQ.4.OR.KTYP.EQ.6.OR.KTYP.EQ.7.OR.KTYP.EQ.8) THEN
        DO 13 I=1,NMAI
        WRITE(6,115) I,MAIL(1,I),MAIL(2,I),0.,
     *               (MAIL(J,I),J=3,NNPE+2)  
13      CONTINUE
      ELSE
        STOP ' Pb KTYP : LISDON'
      ENDIF            
COMM ...................////// LIAISONS //////.........................
      WRITE(6,200)
      WRITE(6,127)
      WRITE(6,128)
      WRITE(6,129) (LIAI(1,I),LIAI(2,I),I=1,NLIAI)
COMM .............////// BOUCLE SUR LES CHARGEMENTS //////.............
      WRITE(6,200)
      INIT=8
      NTIC=0
      DO 50 ICHA=1,NCHA
       WRITE(6,117) ICHA
       NC=0
       WRITE(6,122)
        IF(INT(DFOR(NTIC+2)).EQ.1) THEN
         WRITE(6,124) DFOR(NTIC+3),DFOR(NTIC+4)
        ENDIF
        IF(INT(DFOR(NTIC+5)).EQ.1) THEN
         WRITE(6,125) 
        ENDIF
        IF(INT(DFOR(NTIC+6)).EQ.1) THEN
         WRITE(6,126) 
        ENDIF
        NKF=INT(DFOR(NTIC+8))
       IF(NKF.GT.0) THEN
        WRITE(6,116)
        DO 24 IKF=1,NKF
         ITYP=INT(DFOR(NTIC+INIT+NC+1))
         NLEC=INT(DFOR(NTIC+INIT+NC+2))
         IF(ITYP.EQ.1) THEN
          WRITE(6,118) 
         ELSE IF(ITYP.EQ.2) THEN
          WRITE(6,120)
         ENDIF 
         DO 56 I=1,NLEC
           J =INT(DFOR(NTIC+INIT+NC+3*I  ))
           CX=    DFOR(NTIC+INIT+NC+3*I+1)
           ND=INT(DFOR(NTIC+INIT+NC+3*I+2))
 56        WRITE(6,121) J,CX,ND
         NC=NC+2+3*NLEC
 24     CONTINUE
        NTIC=NTIC+NVF(ICHA)
       ENDIF
 50   CONTINUE
      WRITE(6,200)
      END
      
      SUBROUTINE ACQUIM(NTB,NI,NSTOP,RMOT)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %              ACQUISITION DE NOMBRES ENTIERS                    %
COMM %                                                                %
COMM %   NTB  TABLEAU DES NOMBRES ENTIERS                             %
COMM %   NI   NOMBRES D ENTIERS LUS                                   %
COMM %   RMOT MOT CLE LU                                              %
COMM %   MOT CLE 'SEUL'   LES DONNEES SONT: NI,IT(1)...               %
COMM %   MOT CLE 'SUIT'   LES DONNEES SONT: IMIN,IMAX,PAS             %
COMM %   NSTOP = 0 IL RESTE DES DONNEES A ACQUERIR                    %
COMM %   NSTOP = 1 DONNEES ACQUISES (RMOT N'EST NI 'SEUL' NI 'SUIT')  %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DIMENSION NTB(1)
      CHARACTER*4 RMOT
COMM .........................////////////.............................
      NSTOP=0
      READ(5,'(A4)') RMOT
      WRITE(6,'(A4)') RMOT
      IF(RMOT.EQ.'SEUL') THEN
        READ(5,*) NI,(NTB(I),I=1,NI)      
      ELSE IF(RMOT.EQ.'SUIT') THEN
        READ(5,*) IMIN,IMAX,IPAS
        NI=0
        DO 4 I=IMIN,IMAX,IPAS
        NI=NI+1
4       NTB(NI)=I      
      ELSE      
       NSTOP=1      
      ENDIF
      END      

COMM ..........................////////////............................
COMM .....//////  FIN DU MODULE D ACQUISITION DES DONNEES  //////......
COMM ..........................////////////............................
