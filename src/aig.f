COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %  //////   MODULE D'AIGUILLAGE SELON LE TYPE DE CALCUL   //////  %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      SUBROUTINE RIGELA(RIG,IDIA,COOR,MAIL,EMAI,EPAI,
     *                  NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)   
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %         MATRICE DE RIDITE ELASTIQUE                             %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RIG(1),IDIA(1),COOR(1),MAIL(1),EMAI(1),EPAI(1)
COMM
      IF(KTYP.EQ.1.OR.KTYP.EQ.2.OR.KTYP.EQ.3) THEN       
           CALL RELA2D(RIG,IDIA,COOR,MAIL,EMAI,EPAI,
     *                 NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
      ELSE IF(KTYP.EQ.4) THEN
           CALL RELA3D(RIG,IDIA,COOR,MAIL,EMAI,
     *                 NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)     
      ELSE IF(KTYP.EQ.5) THEN     
           CALL RELACO(RIG,IDIA,COOR,MAIL,EMAI,EPAI,
     *                 NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
      ELSE 
        STOP  ' Pb KTYP : RIGELA ' 
      ENDIF
      END    
     
      SUBROUTINE MASCOE(RIG,IDIA,COOR,MAIL,EMAI,EPAI,
     *                  NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)   
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %         MATRICE DE MASSE COHERENTE                              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RIG(1),IDIA(1),COOR(1),MAIL(1),EMAI(1),EPAI(1)
COMM
      IF(KTYP.EQ.1.OR.KTYP.EQ.2.OR.KTYP.EQ.3) THEN       
           CALL MCOE2D(RIG,IDIA,COOR,MAIL,EMAI,EPAI,
     *                 NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
      ELSE IF(KTYP.EQ.4) THEN
           STOP ' Matrice de Masse 3D non encore developpee '
      ELSE IF(KTYP.EQ.5) THEN     
           CALL MCOECO(RIG,IDIA,COOR,MAIL,EMAI,EPAI,
     *                 NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)
      ELSE 
        STOP  ' Pb KTYP : MASCOE '
      ENDIF 
      END    

      SUBROUTINE RIGTAN(RIG,IDIA,COOR,MAIL,EMAI,EPAI,V,SIG,
     *                  NMAI,NDL,NCPN,NDPN,NNPE,NSPG,NVMA,KTYP)   
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %         MATRICE DE RIDITE TANGENTE                              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RIG(1),IDIA(1),COOR(1),MAIL(1),EMAI(1),EPAI(1),
     *          V(1),SIG(1)
COMM
      IF(KTYP.EQ.1.OR.KTYP.EQ.2.OR.KTYP.EQ.3) THEN       
           CALL RTAN2D(RIG,IDIA,COOR,MAIL,EMAI,EPAI,V,SIG,
     *                 NMAI,NDL,NCPN,NDPN,NNPE,NSPG,NVMA,KTYP)
      ELSE IF(KTYP.EQ.4) THEN
           STOP ' Matrice tangente 3D non encore developpee '
      ELSE IF(KTYP.EQ.5) THEN     
           CALL RTANCO(RIG,IDIA,COOR,MAIL,EMAI,EPAI,V,SIG,
     *                 NMAI,NDL,NCPN,NDPN,NNPE,NSPG,NVMA,KTYP)
      ELSE 
        STOP  ' Pb KTYP : RIGTAN ' 
      ENDIF
      END         


      SUBROUTINE RIGGEO(RIG,IDIA,COOR,MAIL,EMAI,EPAI,SIG,
     *                  NMAI,NDL,NCPN,NDPN,NNPE,NSPG,NVMA,KTYP)   
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %         MATRICE DE RIDITE GEOMETRIQUE                           %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RIG(1),IDIA(1),COOR(1),MAIL(1),EMAI(1),EPAI(1),SIG(1)
COMM
      IF(KTYP.EQ.1.OR.KTYP.EQ.2.OR.KTYP.EQ.3) THEN       
           STOP ' Matrice rig. geomtrique 2D non encore developpee '
      ELSE IF(KTYP.EQ.4) THEN
           STOP ' Matrice rig. geomtrique 3D non encore developpee '
      ELSE IF(KTYP.EQ.5) THEN     
           CALL RGEOCO(RIG,IDIA,COOR,MAIL,SIG,
     *                 NMAI,NDL,NCPN,NDPN,NNPE,NSPG,NVMA,KTYP)
      ELSE 
        STOP  ' Pb KTYP : RIGGEO ' 
      ENDIF
      END 
                   
      SUBROUTINE SIGELA(SIG,V,COOR,MAIL,EMAI,EPAI,
     *                  NMAI,NCPN,NDPN,NNPE,NVMA,KTYP)     
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %         CALCUL DES CONTRAINTES ELASTIQUES                       %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION SIG(1),V(1),COOR(1),MAIL(1),EMAI(1),EPAI(1)
COMM
      IF(KTYP.EQ.1.OR.KTYP.EQ.2.OR.KTYP.EQ.3) THEN       
          CALL SELA2D(SIG,V,COOR,MAIL,EMAI,NMAI,NCPN,NNPE,NVMA,KTYP)
      ELSE IF(KTYP.EQ.4) THEN      
          CALL SELA3D(SIG,V,COOR,MAIL,EMAI,NMAI,NCPN,NNPE,NVMA,KTYP)
      ELSE IF(KTYP.EQ.5) THEN     
          CALL SELACO(SIG,V,COOR,MAIL,EMAI,EPAI,NMAI,NCPN,NDPN,NNPE,
     *                NVMA,KTYP)
      ELSE 
        STOP  ' Pb KTYP : SIGELA '
      ENDIF 
      END    
        
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

      SUBROUTINE RESIDU(V,COOR,MAIL,EMAI,EPAI,SIG,RES,F,
     *                  NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)   
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   CALCUL DES CONTRAINTES EN NON LINEAIRES ET DU RESIDU          %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION V(1),COOR(1),MAIL(1),EMAI(1),EPAI(1),SIG(1),RES(1),F(1)

COMM
      IF(KTYP.EQ.1.OR.KTYP.EQ.2.OR.KTYP.EQ.3) THEN       
           CALL RESI2D(V,COOR,MAIL,EMAI,EPAI,SIG,RES,F,
     *                 NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP)        
      ELSE IF(KTYP.EQ.4) THEN
           STOP ' Residu 3D non encore developpee '
      ELSE IF(KTYP.EQ.5) THEN     
           CALL RESICO(V,COOR,MAIL,EMAI,EPAI,SIG,RES,F,
     *                 NMAI,NDL,NCPN,NDPN,NNPE,NVMA,KTYP) 
      ELSE 
        STOP  ' Pb KTYP : RESIDU '
      ENDIF 
      END            
     
          
COMM ..........................////////////............................
COMM .......//////      FIN DU MODULE D'AIGUILLAGE       //////........
COMM ..........................////////////............................
