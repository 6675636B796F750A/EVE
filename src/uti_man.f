
      SUBROUTINE DETRAY(CRIT,V,FQ,RAYON,NDL,NORDRE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ON DETERMINE LE RAYON DE CONVERGENCE DE LA SERIE              %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION V(NDL,*),FQ(NDL,*)
C
C-----calcul de am sur la base du   (Un - Un-1)/U1 < eps
C
      XNOR1=DSQRT(PROSCA(V(1,1),V(1,1),NDL))
      XNORP=DSQRT(PROSCA(V(1,NORDRE),V(1,NORDRE),NDL))
      RAYON=(CRIT*(XNOR1/XNORP))**(1.D0/DBLE(NORDRE-1))
C
C----calcul du rayon sur la base du residu   R= a**n+1 Fn+1
C
      XNF=DSQRT(PROSCA(FQ(1,NORDRE+1),FQ(1,NORDRE+1),NDL))
      RAY  =(CRIT/XNF)**(1.D0/DBLE(NORDRE+1))
C
      WRITE(9,50) XNOR1,XNORP,RAYON
      WRITE(9,60) CRIT,XNF,RAY
 50   FORMAT(//,' Rayon de convergence : Critere deplacement',//,
     *'Norme de   V(1)   =',G15.8,
     *'Norme de V(nordre)=',G15.8,
     *' Rayon dep = ',G15.8,//)
 60   FORMAT(//,' Rayon de convergence : Critere residu',//,
     *'residu maxi          =',G15.8,
     *'Norme de Fq(nordre+1)=',G15.8,
     *' Rayon res = ',G15.8,//)

      END

      SUBROUTINE SENSCO(V,U,IP,APOSITIF,RAYON,NPOI,DA,NDL)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ON DETERMINE LE SENS DE PARCOURT ET LE PAS                    %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL APOSITIF 
      DIMENSION V(NDL,1),U(1)
      IF(IP.EQ.1) THEN
       APOSITIF=.TRUE.
       DA=RAYON/DBLE(NPOI)          
      ELSE      
        PROJ=PROSCA(V(1,1),U,NDL)
        IF(PROJ.GT.0) THEN
         APOSITIF=.TRUE.
         DA=RAYON/DBLE(NPOI)       
        ELSE
         APOSITIF=.FALSE.
         DA=-RAYON/DBLE(NPOI)
        ENDIF
      ENDIF
      END
      
      SUBROUTINE VORIEN(V,U,APOSITIF,NOR,NDL,RAYON)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ON FORME UN VECTEUR ORIENTE DANS LE SENS DU PARCOURS          %
COMM %   A L'AIDE DE dV/dA EN FIN DE PAS (A=RAYON)                     %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      LOGICAL APOSITIF 
      DIMENSION V(NDL,1),U(1)
C
       IF(APOSITIF) THEN
        A=RAYON
       ELSE
        A=-RAYON
       ENDIF
       DO 5 K=1,NDL
 5     U(K)=V(K,1)
       DO 3  J=2,NOR 
        PA=1.D0
        DO 32 JJ=1,J-1
 32     PA=PA*A
        PA=PA*J
        DO 31 K=1,NDL
 31     U(K)=U(K)+V(K,J)*PA
 3     CONTINUE 
       IF(APOSITIF) THEN
       ELSE
       DO 6 K=1,NDL
 6     U(K)=-U(K)
       ENDIF
      END 
      
      SUBROUTINE SOLPOL_PLAS(V0,V,U,NDL,NOR,A)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ON FORME LA SOLUTIONS POLYNOMIALES --> U=V0+A*V(1)+...+ApV(p) %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      DIMENSION V0(1),V(NDL,1:*),U(1)
C
      DO 1 K=1,NDL
      U(K)=V0(K)
 1    CONTINUE
      DO 2  J=1,NOR 
      DO 4 K=1,NDL
      U(K)=U(K)+V(K,J)*A**J
 4    CONTINUE
 2    CONTINUE 
      END
      
      SUBROUTINE DENPADE(ALPHA,DEN,NORDRE,NPAD)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ON FORME LA SOLUTIONS POLYNOMIALES --> Lambda,U,S             %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      DIMENSION ALPHA(NORDRE,NORDRE),DEN(0:*)
C     
      DEN(0)=1.D0
      DO 10 I=1,NPAD
       DEN(I)=0.D0
       K=NPAD+1-I
       DO 20 J=0,I-1
 20    DEN(I)=DEN(I)+ DEN(J)*ALPHA(NPAD+1-J,K)
       DEN(I)=DEN(I)/(-ALPHA(K,K))
 10   CONTINUE     
COMM ......//////impression des DEN ////.....     
      WRITE(9,50) NPAD
      DO 15 I=0,NPAD
 15   WRITE(9,60) NPAD,I,DEN(I)
 50   FORMAT( ///,' DENOMINATEUR DES APPROX. DE PADE : ORDRE ',I3,/,
     *            ' -------------------------------------------- ',//)
 60   FORMAT( ' D(',I2,',',I2,')=',G15.8) 
      END
                   
      SUBROUTINE SOLPAD(C0,V0,SIG0,C,V,SIG,LAMBDA,U,S,
     *                  NDL,NSIG,NPAD,A,DEN,DELTA)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   ON FORME LA SOLUTIONS POLYNOMIALES --> Lambda,U,S             %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      DOUBLE PRECISION LAMBDA  
      DIMENSION V0(1),SIG0(1),C(1),V(NDL,1),SIG(NSIG,1),
     *          U(1),S(1),DEN(0:*),AN(NPAD),P(0:NPAD)
C
       LAMBDA=C0 
       DO 5 K=1,NDL
 5     U(K)=V0(K) 
C       DO 6 K=1,NSIG
C 6     S(K)=SIG0(K) 
C
       AN(1)=A
       DO 10 I=2,NPAD
 10     AN(I)=AN(I-1)*A 
       P(0)=DEN(0)       
       DO 11 I=1,NPAD
 11     P(I)=P(I-1)+AN(I)*DEN(I)
       DELTA=P(NPAD)
C
       DO 3  J=1,NPAD
        COEF=P(NPAD-J)/DELTA 
        WRITE(9,50) A,J,COEF        
        COEF=COEF*AN(J)
        LAMBDA=LAMBDA+C(J)*COEF
        DO 31 K=1,NDL
 31     U(K)=U(K)+V(K,J)*COEF
C        DO 33 K=1,NSIG
C 33     S(K)=S(K)+SIG(K,J)*COEF  
 3     CONTINUE
 50   FORMAT( ' a=',G15.8,' f(',I2,')=',G15.8) 
      END
                  
      SUBROUTINE IMPCVP(C,V,NDL,IOR)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 % 
COMM %  ON IMPRIME LE COEFFICIENT  C(IOR) ET LE VECTEUR V(IOR)         %
COMM %                                                                 %    
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION C(1),V(NDL,1)
C 
      WRITE(9,100) IOR,IOR,C(IOR),C(IOR)
      CALL ECDEPL(V(1,IOR),9)
 100  FORMAT ( ' ORDRE ',I2,' :   C(',I2,')=',G15.8,/,G15.8)

      END    
        
      SUBROUTINE EXOORD1(C,V,XL1,XL2,NDL)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  ON CALCULE LE COEFFICIENT  C(1) ET LE VECTEUR V(1)             %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION C(1),V(NDL,1)
COMM--------EXPRESSION DE LAMBDA1 POUR LES 3 IMPOS�(DEP,ARC,FORCE)--
      C(1)=1.D0/(DSQRT(XL1*PROSCA(V(1,1),V(1,1),NDL)+XL2))
      DO 1 I=1,NDL
 1    V(I,1)=V(I,1)*C(1)
      END

      SUBROUTINE EXOORDP(C,V,XL1,XL2,NDL,IORDRE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  ON CALCULE LE COEFFICIENT  C(p) ET ON TERMINE LE VECTEUR V(p)  %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION C(1),V(NDL,1)
COMM
COMM ....//// a={u,v1} +  l*C(1) /{v1,v1} + C(1)*C(1) /////..............
COMM
      a=-XL1*C(1)*PROSCA(V(1,IORDRE),V(1,1),NDL)
      b=XL1*PROSCA(V(1,1),V(1,1),NDL)+XL2*C(1)**2
      C(IORDRE)=a/b
COMM ..../////   calcul de V(p) //////...................................
COMM
      COEF=C(IORDRE)/C(1)
      DO 20 I=1,NDL
 20   V(I,IORDRE)=V(I,IORDRE)+V(I,1)*COEF
      END
      SUBROUTINE CVORD1_LOI1(C,V,XL,NDL)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 % 
COMM %  ON CALCULE LE COEFFICIENT  C(1) ET LE VECTEUR V(1)             %
COMM %                                                                 %    
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION C(1),V(NDL,1)
COMM    
C      C(1)=DSQRT((XL*XL)/(1.D0+PROSCA(V(1,1),V(1,1),NDL))) 
       C(1)=DSQRT((XL*XL)/(PROSCA(V(1,1),V(1,1),NDL))) 
C     C(1)=1.D0
      DO 1 I=1,NDL
 1    V(I,1)=V(I,1)*C(1)
      END
       
      SUBROUTINE CVORDP_LOI1(C,V,XL,NDL,IORDRE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 % 
COMM %  ON CALCULE LE COEFFICIENT  C(p) ET ON TERMINE LE VECTEUR V(p)  %
COMM %                                                                 %    
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION C(1),V(NDL,1)
COMM
COMM ....//// a={u,v1} +  l*C(1) /{v1,v1} + C(1)*C(1) /////..............
COMM
C     C(IORDRE)=-PROSCA(V(1,IORDRE),V(1,1),NDL)*C(1)/(XL*XL)
      C(IORDRE)=-PROSCA(V(1,IORDRE),V(1,1),NDL)*C(1)/
     &           PROSCA(V(1,1),V(1,1),NDL)
C     C(IORDRE)=0.D0
COMM  
COMM ..../////   calcul de V(p) //////...................................
COMM	
      COEF=C(IORDRE)/C(1)
      DO 20 I=1,NDL
 20   V(I,IORDRE)=V(I,IORDRE)+V(I,1)*COEF
      END 
      
      SUBROUTINE DCDVORD0(DC)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 % 
COMM %  ON CALCULE LE COEFFICIENT  DC(0)=1                             %
COMM %                                                                 %    
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION DC(0:*)
COMM 
      DC(0)=1.D0
      END 
      
      SUBROUTINE DCDVORDP(DC,DV,NDL,IORDRE)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 % 
COMM % ON CALCULE LE COEFFICIENT  DC(p) ET ON TERMINE LE VECTEUR DV(p) %
COMM %                                                                 %    
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION DC(0:*),DV(NDL,0:*)
COMM
      DC(IORDRE)=-PROSCA(DV(1,IORDRE),DV(1,0),NDL)
     *           /PROSCA(DV(1,0),DV(1,0),NDL)
COMM  
COMM ..../////   calcul de V(p) //////...................................
COMM
      DO 20 I=1,NDL
 20   DV(I,IORDRE)=DV(I,IORDRE)+DV(I,0)*DC(IORDRE)
      END      




      DOUBLE PRECISION FUNCTION NORME1(F,N)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 % 
COMM %  ON CALCULE LA NORME  DE F (vecteur de taille N )               %
COMM %                                                                 %    
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
      DOUBLE PRECISION F(1) 
      NORME1=0.D0
      DO 1 I=1,N
 1    IF(DABS(F(I)).GT.NORME1) NORME1=DABS(F(I))
      END
          
      SUBROUTINE NORMER(RES,F,IP,NDL,XNORM2R)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 % 
COMM %  ON CALCULE LES NORMES  DU RESIDU ET DE F                       %
COMM %                                                                 %    
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RES(1),F(1)
COMM 
      XNORM1R=0.D0
      DO 1 I=1,NDL
 1    IF(DABS(RES(I)).GT.XNORM1R) XNORM1R=DABS(RES(I))
      XNORM1F=0.D0
      DO 2 I=1,NDL
 2    IF(DABS(F(I)).GT.XNORM1F) XNORM1F=DABS(F(I))      
      XNORM2R=SQRT(PROSCA(RES,RES,NDL))
      XNORM2F=SQRT(PROSCA(F,F,NDL))
      RATIO1=XNORM1R/XNORM1F
      RATIO2=XNORM2R/XNORM2F                
C      WRITE(9,100) IP,XNORM1R,XNORM1F,RATIO1,XNORM2R,XNORM2F,RATIO2
COMM
 100  FORMAT( //,' Qualite de la solution au point numero ',I5,//,
     *        ' Plus grande composante du vecteur RESIDU :',G15.8,/,
     *        ' Plus grande composante du vecteur FORCE  :',G15.8,/,
     *        ' Ratio des plus grandes composantes       :',G15.8,///,
     *        ' Norme euclidienne du vecteur RESIDU :',G15.8,/,
     *        ' Norme euclidienne du vecteur FORCE  :',G15.8,/,
     *        ' Ratio des normes euclidiennes       :',G15.8,///)    

      END 

      SUBROUTINE CALCON(V,TET,NVECTR,SIG,COOR,MAIL,EMAI,EPAI,
     *                  NMAI,NDPN,NNPE,NVMA,KTYP)     
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %     CALCUL DES CONTRAINTES POUR RITZ                            %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION V(1),TET(1),SIG(1),COOR(1),MAIL(1),EMAI(1),EPAI(1)
COMM
      IF(KTYP.EQ.1.OR.KTYP.EQ.2.OR.KTYP.EQ.3) THEN       
           STOP ' Asymptotique 2D non encore developpee ' 
      ELSE IF(KTYP.EQ.4) THEN      
           STOP ' Asymptotique 3D non encore developpee ' 
      ELSE IF(KTYP.EQ.5) THEN     
C          CALL CACOCO(V,TET,NVECTR,SIG,COOR,MAIL,EMAI,EPAI,NMAI,NDPN,NNPE,
C     *                NVMA,KTYP)
      ELSE 
        STOP  ' Pb KTYP : CALCON '
      ENDIF 
      END      
