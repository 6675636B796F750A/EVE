COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %  //////  MODULE REGROUPANT LES SUBROUTINES DE CALCULS    ////// %
COMM %  //////   DE RIGIDITE GEOMETRIQUE -- FLAMBAGE  COQ --    ////// %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

       SUBROUTINE RGEOCO(RIG,IDIA,COOR,MAIL,SIG,
     *                   NMAI,NDL,NCPN,NDPN,NNPE,NSPG,NVMA,KTYP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %   CALCUL DE LA MATRICE DE RIGIDITE GEOMETRIQUE GLOBALE          %
COMM %   POUR UNE STRUCTURE FORMEE DE PLAQUES ET DE COQUES             %
COMM %                                                                 %
COMM %   RIG   MATRICE DE RIGIDITE GLOBALE                             %
COMM %   IDIA  TABLEAU POINTEUR DE LA DIAGONALE                        %
COMM %   MAIL  TABLEAU DECRIVANT LA NUMEROTATION DES ELEMENTS          %
COMM %   COOR  COORDONNEES DES NOEUDS                                  %
COMM %   SIG   VECTEUR DE CONTRAINTE INITIAL                           %
COMM %   NMAI  NOMBRE D ELEMENTS                                       %
COMM %   NDL   NOMBRE DE DDL TOTAL                                     %
COMM %   NCPN  NOMBRE DE COORDONNEES PAR NOEUD                         %
COMM %   NDPN  NOMBRE DE DDL PAR NOEUD                                 %
COMM %   NNPE  NOMBRE DE NOEUDS MARIMUN PAR ELEMENT                    %
COMM %                                                                 %
COMM %   NBN   NOMBRE DE NOEUD DE L'ELEMENT                            %
COMM %   NDDLE NOMBRE DE DDL DE L'ELEMENT                              %
COMM %   NBG   NOMBRE DE POINT DE GAUSS DE L'ELEMENT                   %
COMM %   GAUSS COORDONNEES POINT DE GAUSS DE L'ELEMENT                 %
COMM %   POIDS POID AU POINT DE GAUSS DE L'ELEMENT                     %
COMM %   RGL   MATRICE DE RIGIDITE ELEMENTAIRE                         %
COMM %   NO    NUMEROS DES NOEUDS                                      %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION RIG(1),IDIA(1),COOR(NCPN,1),MAIL(NNPE+2,1),SIG(1)      
      DIMENSION RGL(171),IGLO(18),NO(3),X(3),Y(3),Z(3)
COMM ...........................////////////...........................
      CALL DZERO(RIG,IDIA(NDL))
COMM ...........................////////////...........................
      IAD=0
COMM ..............////// BOUCLE SUR LES ELEMENTS //////...............
      DO 20 IM=1,NMAI
COMM .............////// CARACTERITIQUE DE L'ELEMENT /////.............       
      ITYP=MAIL(1,IM)
      IMAT=MAIL(2,IM)      
      CALL CAELCO(ITYP,NBN,NDDLE,NBG)          
      DO 3 I=1,NBN
      NO(I)=MAIL(2+I,IM)
      X(I)=COOR(1,NO(I))
      Y(I)=COOR(2,NO(I))
      Z(I)=COOR(3,NO(I))            
 3    CONTINUE
COMM.....................///// CALCUL DE RGL /////.....................
      IF(ITYP.EQ.31) THEN      
       CALL RGOELE_DKT(X,Y,Z,SIG,IAD,RGL,NBN,NDDLE,NBG)
      ELSE
       STOP ' Pb ITYP : RGEOCO '
      ENDIF      
COMM .........////// ASSEMBLAGE DE LA MAT. RIGID. ELEM. ////...........       
      CALL ASSMAT(RIG,IDIA,RGL,NO,IGLO,NBN,NDPN)
 20   CONTINUE
      END

      SUBROUTINE RGOELE_DKT(X,Y,Z,SIG,IAD,RGL,NBN,NDDLE,NBG)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE RIGIDITE GEOMETRIQUE ELEMENTAIRE    ' DKT+CST '     %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION X(3),Y(3),Z(3),TRA(3,3),SIG(1)
      DIMENSION RGL(171),RGEL(18,18),RGE(18,18),R(18,18),RINT(18,18)
COMM  ........////  Calcul des coordonnees locales  ////................
      CALL COOLOC(X,Y,Z,X2,X3,Y3,TRA,R)
COMM  .. //// Matrice de rigidite elementaire dans le repere local ////..
      CALL RGELLO_DKT(X2,X3,Y3,SIG,IAD,RGL,NBN,NDDLE,NBG)
COMM  .. //// passage du stockage en ligne au stockage en matrice ///....
      CALL LIGMAT(RGL,RGE,NDDLE)        
COMM  .....///// PASSAGE AU REPERE GLOBAL /////.........................
      CALL PASGLO(RGE,R,RINT,RGEL)
COMM  ...//// passage du stockage en matrice au stockage en ligne  /////......
      CALL MATLIG(RGEL,RGL,NDDLE)
      END

      SUBROUTINE RGELLO_DKT(X2,X3,Y3,SIG,IAD,RGL,NBN,NDDLE,NBG)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %  MATRICE DE RIGIDITE GEOMETRIQUE REPERE LOCAL  'DKT+CST'        %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION RGL(171),ARGL(171),SIG(1),G(2,18),SG(2,18),S(2,2),
     *          GAUSS(2,3),POIDS(3)
      DOUBLE PRECISION KSI,ETA     
COMM
      ITS=(NDDLE*NDDLE+NDDLE)/2
      CALL DZERO(RGL,ITS)
      CALL GRIGCO(31,GAUSS,POIDS)
COMM .........////// BOUCLE SUR LES POINTS DE GAUSS //////.............      
      DO 7 IG=1,NBG 
        KSI=GAUSS(1,IG)
        ETA=GAUSS(2,IG)
        CALL BGEO_DKT(X2,X3,Y3,KSI,ETA,G,DETJ)                                                
        S(1,1)=-SIG(IAD+1)
        S(1,2)=-SIG(IAD+3)
        S(2,1)=-SIG(IAD+3)
        S(2,2)=-SIG(IAD+2)
        IAD=IAD+6         
        CALL TBDB(G,S,SG,ARGL,2,NDDLE,ITS)
        COEF=DETJ*POIDS(IG)         
        CALL DACTUA(RGL,ARGL,ITS,COEF)          
 7    CONTINUE 
      END

      SUBROUTINE BGEO_DKT(X2,X3,Y3,KSI,ETA,G,DETJ) 
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %    MATRICE  DES GRADIENTS G                                     %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DOUBLE PRECISION KSI,ETA
      DIMENSION G(2,18)
COMM
      DETJ=X2*Y3
      DO 4 I=1,2
      DO 4 J=1,18
 4    G(I,J)=0.D0
      G(1,3)=-1./X2
      G(1,9)= 1./X2
      G(2,3)=(X3-X2)/(X2*Y3)
      G(2,9)=-X3/(X2*Y3)
      G(2,15)=1./Y3
      END      
      
COMM ...........................////////////...........................
COMM ...//////  FIN DU MODULE REGROUPANT LES SUBROUTINES DE  //////....
COMM ...//////  CALCULS  DE RIGIDITE GEOMETRIQUE             //////....
COMM ...........................////////////...........................

