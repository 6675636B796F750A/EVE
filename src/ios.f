COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                 %
COMM %                EVE   Version 2.1  (Novembre 94)                 %
COMM %                                                                 %
COMM %    //////   MODULE DE GESTION DES IMPRESSIONS   //////          %
COMM %                                                                 %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         
      SUBROUTINE POUTRE_MAIL(V,NNOE,NDPN,NDL)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION V(NDL)

      DO I=1,NNOE
      IP=NDPN*(I-1)
      Ux=V(IP+1)
      UY=V(IP+2)
      WRITE(4,*)I,Ux,Uy
      ENDDO
      END
      
      SUBROUTINE ECDEPL(V,NIO)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %  IMPRESSION DU VECTEUR DEPLACEMENT  V(I) DANS LE FICHIER NIO   %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION V(1)
      COMMON/INI/ICO(200),IT(200)
COMM
      KTYP=ICO(1)
      NDPN=ICO(3)
      NNOE=ICO(6)
COMM
      IF(KTYP.EQ.1.OR.KTYP.EQ.2.OR.KTYP.EQ.3) THEN   
        WRITE(NIO,100)
        WRITE(NIO,101) (I,(V(NDPN*(I-1)+J),J=1,NDPN),I=1,NNOE)      
      ELSE IF(KTYP.EQ.4) THEN      
        WRITE(NIO,102)
        WRITE(NIO,103) (I,(V(NDPN*(I-1)+J),J=1,NDPN),I=1,NNOE)
      ELSE IF(KTYP.EQ.5.OR.KTYP.EQ.7.OR.KTYP.EQ.8) THEN            
        WRITE(NIO,104)
        WRITE(NIO,105) (I,(V(NDPN*(I-1)+J),J=1,NDPN),I=1,NNOE) 
      ELSE IF(KTYP.EQ.6) THEN            
        WRITE(6,106)
        WRITE(6,107) (I,(V(NDPN*(I-1)+J),J=1,NDPN),I=1,NNOE) 
      ELSE
        STOP ' Pb KTYP dans ECDEPL fichier ios.f'                
      ENDIF          
COMM .........................////////////.............................
 100  FORMAT('  NOEUD',8X,'UX',15X,'UY',/)
 101  FORMAT(' ',I5,2X,G15.8,2X,G15.8)
 102  FORMAT('  NOEUD',8X,'UX',15X,'UY',15X,'UZ',/)
 103  FORMAT(' ',I5,2X,G15.8,2X,G15.8,2X,G15.8)
 104  FORMAT('  NOEUD',8X,'UX',15X,'UY',15X,'UZ',15X,
     ;'RX',15X,'RY',15X,'RZ',/)
 105  FORMAT(' ',I5,2X,G15.8,2X,G15.8,2X,G15.8,2X,G15.8,2X,G15.8,
     ;2X,G15.8) 
 106  FORMAT('  NOEUD',8X,'UX',15X,'UY',15X,'UZ',15X,
     ;'ALPHA',15X,'BETA'/)
 107  FORMAT(' ',I5,2X,G15.8,2X,G15.8,2X,G15.8,
     ;2X,G15.8,2X,G15.8)     
      END
      
      SUBROUTINE ECCONT(SIG,MAIL,NIO)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %  IMPRESSION DU VECTEUR CONTRAINTE S(I) DANS LE FICHIER NIO     %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION SIG(1),SGM(6)
      DIMENSION MAIL(1)
      COMMON/INI/ICO(200),IT(200)
COMM      
      KTYP=ICO(1)
      NNPE=ICO(4)
      NSPG=ICO(5)              
      NMAI=ICO(8) 
COMM 
      IF(KTYP.EQ.1.OR.KTYP.EQ.2) THEN
       WRITE(NIO,110)
      ELSE IF(KTYP.EQ.3) THEN
       WRITE(NIO,111)
      ELSE IF(KTYP.EQ.4) THEN
       WRITE(NIO,112)
      ELSE IF(KTYP.EQ.5) THEN
       WRITE(NIO,113) 
      ELSE IF(KTYP.EQ.6) THEN
       WRITE(NIO,114)  
      ELSE IF(KTYP.EQ.7.OR.KTYP.EQ.8) THEN
       WRITE(NIO,112)           
      ELSE
       STOP ' Pb KTYP dans ECCONT  ios.f'
      ENDIF
      IAD=1
      DO 1 IM=1,NMAI
       ITYP=MAIL((NNPE+2)*(IM-1)+1) 
       IF(KTYP.EQ.1.OR.KTYP.EQ.2.OR.KTYP.EQ.3) THEN
         CALL CAEL2D(ITYP,NBN,NDDLE,NBG)        
       ELSE IF(KTYP.EQ.4) THEN
         CALL CAEL3D(ITYP,NBN,NDDLE,NBG)
       ELSE IF(KTYP.EQ.5) THEN
         CALL CAELCO(ITYP,NBN,NDDLE,NBG)                  
       ELSE
         STOP ' Pb KTYP : IMPRIM  ios.f '
       ENDIF                 
       DO 2 IG=1,NBG
        CALL SHIFTD(SIG(IAD),SGM,NSPG) 
        IAD=IAD+NSPG  
        WRITE(NIO,120) IM,IG,(SGM(K),K=1,NSPG)  
 2     CONTINUE   
 1    CONTINUE 
COMM .........................////////////............................. 
 110  FORMAT(' MAILLE  PG     SIGMA XX        SIGMA YY          ',
     ;'SIGMAXY         SIGMA ZZ',/)
 111  FORMAT(' MAILLE  PG     SIGMA RR        SIGMA ZZ          ',
     ;'SIGMARZ         SIGMA TT',/)
 112  FORMAT(' MAILLE  PG     SIGMA XX        SIGMA YY          ',
     ;'SIGMAZZ        SIGMA XY         SIGMA YZ          SIGMA XZ',/)     
 113  FORMAT(' MAILLE  PG      NXX              NYY             ',
     ;'  NXY            MXX              MYY               MXY   ',/) 
 114  FORMAT(' MAILLE  PG     SIGMA XX        SIGMA YY          ',
     ;'SIGMA XY         SIGMA YZ          SIGMA XZ',/)      
 120  FORMAT(I5,4X,I2,6(2X,G15.8))  
      END
      
      SUBROUTINE IMPELA(V,SIG,MAIL)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   IMPRESSION DES DEPLACEMENTS V ET DES CONTRAINTES SIG         %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION V(1),SIG(1)
      DIMENSION MAIL(1)
COMM .........................////////////.............................
  80  FORMAT(' VECTEUR DEPLACEMENT',/,
     *       ' -------------------',//)
  90  FORMAT(' CONTRAINTES AUX POINTS DE GAUSS',/,
     *       ' -------------------------------',//)      
COMM .................................................................
      WRITE(6,80)
      CALL ECDEPL(V,6)      
      WRITE(6,'(///)') 
      WRITE(6,90)
      CALL ECCONT(SIG,MAIL,6)
      END
    
      SUBROUTINE IMPVIB(MODE,VP,NMODE,NDL)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   IMPRESSION : MODES DE VIBR, FREQUENCES, PULSATIONS, PERIODES %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION MODE(1),VP(1),PULS,FREQ,PERIOD
      WRITE(6,100)
      DO 20 I=1,NMODE
      PULS=DSQRT(VP(I))
      FREQ=PULS/6.2831853
      PERIOD=1.D0/FREQ      
      WRITE(6,101) I,FREQ,PULS,PERIOD
  20  CONTINUE
      IAD=1 
      DO 10 I=1,NMODE
      WRITE(6,102) I     
      CALL ECDEPL(MODE(IAD),6) 
      IAD=IAD+NDL
 10   CONTINUE
C
 100  FORMAT(///,' MODE      FREQUENCE           PULSATION',
     *'          PERIODE',/)
 101  FORMAT(I4,3X,G15.8,5X,G15.8,5X,G15.8)
 102  FORMAT(//,5X,'MODE DE VIBRATIONS ',I3,
     *        /,5X,'-------------------',/)    
      END 
      
      SUBROUTINE IMPFLA(MODE,VP,NMODE,NDL)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   IMPRESSION DES VALEURS CRITIQUES ET DES MODES DE FLAMBAGE    %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION MODE(1),VP(1)
      WRITE(6,100)
      DO 20 I=1,NMODE
      WRITE(6,101) I,VP(I)
  20  CONTINUE
      IAD=1 
      DO 10 I=1,NMODE
      WRITE(6,102) I     
      CALL ECDEPL(MODE(IAD),6) 
      IAD=IAD+NDL
 10   CONTINUE
C
 100  FORMAT(///,' MODE       LAMBDA',/)
 101  FORMAT(I4,3X,G15.8)
 102  FORMAT(//,5X,'MODE DE FLAMBAGE',I3,
     *        /,5X,'----------------',/)    
      END
       
      SUBROUTINE IMPNLG(LAMBDA,V,SIG,MAIL,IP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM % IMPRESSION : LAMBDA,DEPLACEMENTS,CONTRAINTES AU PAS IP         %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION LAMBDA,V(1),SIG(1)
      DIMENSION MAIL(1)
      WRITE(6,100) IP
      WRITE(6,110) LAMBDA
      CALL IMPELA(V,SIG,MAIL) 
      WRITE(6,200) 
C
      WRITE(30,*) ' Parametre de charge '
      WRITE(30,*) LAMBDA
      CALL ECDEPL(V,30)
C
 100  FORMAT(//,120(1H*),//,' SOLUTION  AU  PAS ',I3,/
     *                      ' ----------------- ',//)
 110  FORMAT( ' PARAMETRE DE CHARGE ',/
     *        ' ------------------- ',//,
     *        'LAMBDA = ',G15.8,//)
 200  FORMAT(//,120(1H*),//)
      END
      

      SUBROUTINE ECPAS(V,R,ITER)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM % IMPRESSION : LAMBDA,DEPLACEMENTS,CONTRAINTES AU PAS IP         %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION V(1),R(1)
COMM
      IF(ITER.EQ.0) THEN
      WRITE(6,102) 
      ELSE
      WRITE(6,100) ITER
      ENDIF
      WRITE(6,105)
      CALL ECDEPL(V,6)
      WRITE(6,106)
      CALL ECDEPL(R,6)
COMM
 100  FORMAT(///'  ITERATION NUMERO ',I3,/,
     *          '  ----------------'//) 
 102  FORMAT(///'  PAS TANGENT ',/,
     *          '  -----------'//) 
 105  FORMAT(///'    DEPLACEMENT ',/,
     *          '    -----------'//)      
 106  FORMAT(///'    RESIDU ',/,
     *          '    ------ '//)      
      END
      
      SUBROUTINE IMPDYN(TEMPS,V,VP,VPP)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM % IMPRESSION : TEMPS,DEPLACEMENTS                                %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION TEMPS,V(1),VP(1),VPP(1)
C
      WRITE(6,110) TEMPS
      WRITE(6,105)
      CALL ECDEPL(V,6)
C      WRITE(6,106)
C      CALL ECDEPL(VP,6)
C      WRITE(6,107)
C      CALL ECDEPL(VPP,6)
C
 110  FORMAT(/// ' SOLUTION A L''INSTANT  T= ',G12.6,' SECONDES',/,
     *           ' ------------------- ',//)
 105  FORMAT(//'    DEPLACEMENTS ',/,
     *          '    ------------ '//)     
 106  FORMAT(//'    VITESSES ',/,
     *          '    -------- '//)  
 107  FORMAT(//'    ACCELERATIONS ',/,
     *          '    ------------- '//)       
      END  
           
      SUBROUTINE ECFICH(A,N,NIO)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   ECRITURE DE A(I),I=1,N  SUR LE FICHIER D'UNITE LOGIQUE NIO   %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION A(1)
      WRITE(NIO) (A(I),I=1,N)
      END

      SUBROUTINE LEFICH(A,N,NIO)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %  LECTURE  DE A(I),I=1,N  SUR LE FICHIER D'UNITE LOGIQUE NIO    %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DOUBLE PRECISION A(1)
      READ(NIO) (A(I),I=1,N)
      END
      
      SUBROUTINE IMPTCH(IF)
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %      IMPRESSION DU TITRE DU CHARGEMENT                         %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      CHARACTER*80 TITCHA,TITRE
      COMMON/TIT/TITRE,TITCHA(10)
COMM
      WRITE(6,'(1X,120(1H*))')
      WRITE(6,100) IF,TITCHA(IF)
      WRITE(6,'(1X ,120(1H*))')
      WRITE(6,'(//)')
 100  FORMAT(//,'     CHARGEMENT NUMERO  ',I2,2X,A80,//)
      END

      SUBROUTINE ENTETE
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %   IMPRESSION DE L'ENTETE DANS LE FICHIER DE SORTIE (6)         %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      CHARACTER*70 LA,LB,LE,LS
1     FORMAT(38X,'PPP   RRR   OOO   GGG   RRR   AAA   M M   M M   EEE',/
     C       38X,'P P   R R   O O   G     R R   A A   MMM   MMM   E',/,
     C       38X,'PPP   RRR   O O   G     RRR   AAA   M M   M M   EE'
     C,/,    38X,'P     RR    O O   G G   RR    A A   M M   M M   E'
     C,/,    38X,'P     R R   OOO   GGG   R R   A A   M M   M M   EEE')
2     FORMAT(/////)
3     FORMAT(3(37X,12(1HE),8X,'VVV',7X,'VVV',8X,12(1HE),/),
     C       3(37X,'EEE',18X,'VVV',5X,'VVV',9X,'EEE',/),
     C       3(37X,7(1HE),15X,'VVV',3X,'VVV',10X,7(1HE),/),
     C       3(37X,'EEE',20X,'VVV',1X,'VVV',11X,'EEE',/),
     C       3(37X,12(1HE),13X,'VVV',13X,12(1HE),/))
      WRITE(6,2)
      WRITE(6,1)
      WRITE(6,2)
      WRITE(6,3)
      WRITE(6,2)
      LA='                      '
      LB=' %                                                          %'
      LS=' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
      WRITE(6,*)LA
      WRITE(6,*)LA
      WRITE(6,*)LS
      WRITE(6,*)LB
      WRITE(6,*)LB
      LE=' %             EVE (  MARS   2003   )                       %'
      WRITE(6,*)LE
      LE=' %   Pour tout renseignement consulter ZAHROUNI             %'
      WRITE(6,*)LE
      WRITE(6,*)LB
      WRITE(6,*)LS
      WRITE(6,*)LA
      WRITE(6,*)LA      
      END
      
      SUBROUTINE ERROR1
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
COMM %                                                                %
COMM %                    ERREUR DE MOT CLE                           %
COMM %                                                                %
COMM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 100  FORMAT(5X,'-  ERREUR DE MOT CLE  -  CELUI QUE VOUS AVEZ UTILISE',
     ;' N EXISTE PAS',//,
     ;5X,'RELISEZ LA DOC EVE 2.1 ET  VERIFIEZ VOTRE ORTHOGRAPHE',//,
     ;5X,'***  EVE  ***  ESPERE VOUS REVOIR TRES BIENTOT  *  ',
     ;'BON COURAGE  ?')
      WRITE(6,'(1H1)')
      WRITE(6,'(5(120(1H*),/))')
      WRITE(6,'(///)')
      WRITE(6,100)
      WRITE(6,'(///)')
      WRITE(6,'(5(120(1H*),/))')
      STOP
      END

COMM .........................////////////.............................
COMM ....////// FIN DU MODULE DE GESTION DES ENTREES/SORTIES //////....
COMM .........................////////////.............................
